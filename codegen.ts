import type { CodegenConfig } from '@graphql-codegen/cli'

const config: CodegenConfig = {
	overwrite: true,
	ignoreNoDocuments: true,
	generates: {
		'./src/app/api/generated/': {
			documents: ['./src/app/api/graphql/**/*.{ts,tsx}'],
			schema: 'https://supplier-api-dev.agrix.mn/graphql',
			preset: 'client'
		},
		'./src/app/apiProductMs/generated/': {
			documents: ['./src/app/apiProductMs/graphql/**/*.{ts,tsx}'],
			schema: 'https://product-graphql-dev.agrix.mn/graphql',
			preset: 'client'
		}
	}
}

export default config
