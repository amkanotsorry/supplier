import { NextRequestWithAuth, withAuth } from 'next-auth/middleware'
import { NextResponse } from 'next/server'

export default withAuth(
	function middleware(request: NextRequestWithAuth) {
		const path = request.nextUrl.pathname
		const authenticated = request.nextauth.token != null

		if (path.startsWith('/home') && !authenticated) {
			return NextResponse.rewrite(new URL('/login', request.url))
		}
	},
	{
		callbacks: {
			authorized: ({ token }) => {
				return token != null
			}
		}
	}
)

export const config = { matcher: ['/', '/:path(.*)'] }
