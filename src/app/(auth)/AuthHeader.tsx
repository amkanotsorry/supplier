'use client'

import { ChevronDownIcon, GlobeIcon, LogoIcon, PlusIcon } from '@/src/assets/icons'
import { Dropdown, DropdownItem, DropdownMenu, DropdownSection, DropdownTrigger, User } from '@nextui-org/react'
import { signOut } from 'next-auth/react'

export default function AuthHeader() {
	return (
		<div className="flex items-center justify-between w-full h-[56px] md:h-[72px] px-4 md:px-8 md:fixed z-50">
			<LogoIcon className="w-[128px] md:w-[136px] h-8 md:h-10" alt={'Agri-X Logo'} width="136" height="40" />

			<div>
				<div className="flex items-center">
					<GlobeIcon className="mr-2 md:mr-3 w-5 h-5" alt={'Globe Icon'} />

					<div className="md:mr-3">Mongolia</div>

					<Dropdown
						showArrow
						radius="sm"
						classNames={{
							base: 'before:bg-default-200', // change arrow background
							content: 'p-0 border-small border-divider bg-background'
						}}>
						<DropdownTrigger>
							<button className="outline-none">
								<ChevronDownIcon className="hidden md:block cursor-pointer" />
							</button>
						</DropdownTrigger>
						<DropdownMenu
							aria-label="Custom item styles"
							disabledKeys={['profile']}
							className="p-3"
							itemClasses={{
								base: [
									'rounded-md',
									'text-default-500',
									'transition-opacity',
									'data-[hover=true]:text-foreground',
									'data-[hover=true]:bg-default-100',
									'dark:data-[hover=true]:bg-default-50',
									'data-[selectable=true]:focus:bg-default-50',
									'data-[pressed=true]:opacity-70',
									'data-[focus-visible=true]:ring-default-500'
								]
							}}>
							<DropdownSection aria-label="Profile & Actions" showDivider>
								<DropdownItem isReadOnly key="profile" className="h-14 gap-2 opacity-100">
									<User
										name="Junior Garcia"
										description="@jrgarciadev"
										classNames={{
											name: 'text-default-600',
											description: 'text-default-500'
										}}
										avatarProps={{
											size: 'sm',
											src: 'https://avatars.githubusercontent.com/u/30373425?v=4'
										}}
									/>
								</DropdownItem>
							</DropdownSection>

							<DropdownSection aria-label="Preferences" showDivider>
								<DropdownItem
									isReadOnly
									key="language"
									className="cursor-default"
									endContent={
										<select
											className="z-10 outline-none w-16 py-0.5 rounded-md text-tiny group-data-[hover=true]:border-default-500 border-small border-default-300 dark:border-default-200 bg-transparent text-default-500"
											id="language"
											name="language">
											<option>Mongolia</option>
											<option>English</option>
										</select>
									}>
									Language
								</DropdownItem>
							</DropdownSection>

							<DropdownSection aria-label="Help & Feedback">
								<DropdownItem onClick={() => signOut()} className="text-danger-400" key="logout">
									Log Out
								</DropdownItem>
							</DropdownSection>
						</DropdownMenu>
					</Dropdown>
				</div>
			</div>
		</div>
	)
}
