import React from 'react'
import AuthHeader from './AuthHeader'

export default async function AuthLayout({ children }: { children: React.ReactNode }) {
	return (
		<div className="flex flex-col h-full">
			<AuthHeader />

			<div className="h-full">{children}</div>
		</div>
	)
}
