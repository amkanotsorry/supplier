'use client'

import { Button, Checkbox, Input, Spinner } from '@nextui-org/react'
import { signIn } from 'next-auth/react'
import { useRouter } from 'next/navigation'
import React, { useState } from 'react'

interface Props {}

const LoginSection: React.FC<Props> = () => {
	const router = useRouter()
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [loading, setLoading] = useState(false)
	const [error, setError] = useState('')

	const onClickLogin = async () => {
		if (!email || !password) return

		setError('')
		setLoading(true)

		const response = await signIn('credentials', {
			email,
			password,
			redirect: false
		})

		setLoading(false)

		if (response?.error) {
			setError('Invalid email or password!')
			return
		}

		router.push('/home/product')
	}

	return (
		<>
			<div className="text-2xl md:text-3xl font-semibold leading-10 mb-3">Login to Agri-X</div>
			<div className="text-[#64696E] text-sm font-medium leading-4 md:leading-6 mb-6 md:mb-12">Enter the fields below to get started</div>

			<div className="flex flex-col gap-4 md:gap-6 mb-8 md:mb-16">
				<Input label="Email address" value={email} onChange={e => setEmail(e.target.value)} />
				<Input label="Password" type="password" value={password} onChange={e => setPassword(e.target.value)} />

				{error && <div className="text-sm text-danger-400">{error}</div>}

				<div className="flex items-center justify-between">
					<div className="flex items-center ">
						<Checkbox />
						<span className="text-sm font-medium leading-5">Remember me</span>
					</div>

					<span className="text-sm font-semibold text-primary cursor-pointer">Forgot password?</span>
				</div>
			</div>

			<div className="flex flex-row mb-6">
				<div className="mr-3 text-sm font-medium">Not a member yet?</div>
				<div onClick={() => router.replace('/register')} className="cursor-pointer text-primary text-sm font-semibold">
					Register
				</div>
			</div>

			<Button disabled={loading} className="w-full h-10" color="primary" onClick={onClickLogin}>
				{loading ? <Spinner size="sm" color="white" /> : 'Login'}
			</Button>
		</>
	)
}

export default LoginSection
