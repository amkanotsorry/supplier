import { getServerSession } from 'next-auth'
import LoginSection from './LoginSection'
import { authOptions } from '../../api/auth/[...nextauth]/authOptions'
import { redirect } from 'next/navigation'

const LoginPage = async () => {
	const session = await getServerSession(authOptions)
	if (session?.user) redirect('/home/product')

	return (
		<div className="flex w-full h-full">
			<div className="flex h-full w-full px-4 py-8 md:items-center justify-center">
				<div className="flex-1 max-w-[512px]">
					<LoginSection />
				</div>
			</div>
		</div>
	)
}

export default LoginPage
