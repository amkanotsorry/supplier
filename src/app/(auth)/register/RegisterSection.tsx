'use client'

import { useRouter } from 'next/navigation'
import React, { useState } from 'react'
import { signUp } from './actions'
import { Platform_Type } from '../../api/generated/graphql'
import { signIn } from 'next-auth/react'
import { Button, Input, Spinner } from '@nextui-org/react'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '@/src/components/ui/select'

interface Props {}

const RegisterSection: React.FC<Props> = () => {
	const router = useRouter()
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [phoneNumber, setPhoneNumber] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [confirmPassword, setConfirmPassword] = useState('')
	const [error, setError] = useState('')
	const [loading, setLoading] = useState(false)

	const onSubmit = async () => {
		setError('')

		if (!email || !firstName || !lastName || !phoneNumber) {
			return setError('Required fields missing')
		}

		if (password.length < 8) {
			return setError('Password length must be above 8')
		}

		if (password !== confirmPassword) {
			return setError('Password does not match')
		}

		setLoading(true)

		const { response, error } = await signUp({
			email,
			firstName,
			lastName,
			phoneNumber,
			countryCode: '976',
			platform: Platform_Type.Supplier,
			isoCode: 'MN',
			password
		})

		setLoading(false)

		if (error) {
			setError(error)
		}

		if (response?.data) {
			await signIn('credentials', {
				email,
				password,
				redirect: false
			})

			return router.push(`/register/verify?email=${email}`)
		}
	}

	return (
		<>
			<div className="flex flex-col gap-4 md:gap-6 mb-8 md:mb-10">
				<Input label="First name" onChange={e => setFirstName(e.target.value)} />
				<Input label="Last name" onChange={e => setLastName(e.target.value)} />

				<div className="flex gap-4 md:gap-3">
					<Select>
						<SelectTrigger className="w-[120px]">
							<SelectValue placeholder="Country" />
						</SelectTrigger>
						<SelectContent>
							{['+1', '+976'].map((productMeta, index) => (
								<SelectItem className="py-3" key={index} value={productMeta}>
									{productMeta}
								</SelectItem>
							))}
						</SelectContent>
					</Select>

					<Input label="Phone number" onChange={e => setPhoneNumber(e.target.value)} />
				</div>

				<Input label="Email" onChange={e => setEmail(e.target.value)} />
				<Input type="password" label="Password" onChange={e => setPassword(e.target.value)} />
				<Input type="password" label="Confirm password" onChange={e => setConfirmPassword(e.target.value)} />

				{error && <div className="text-sm text-red-500">{error}</div>}
			</div>

			<div className="flex flex-row mb-4">
				<div className="mr-3 text-sm font-medium">Already have a account?</div>
				<div onClick={() => router.replace('/login')} className="cursor-pointer text-primary text-sm font-semibold">
					Sign in
				</div>
			</div>

			<Button disabled={loading} color="primary" onClick={onSubmit} className="w-full h-10">
				{loading ? <Spinner size="sm" color="white" /> : 'Create account'}
			</Button>
		</>
	)
}

export default RegisterSection
