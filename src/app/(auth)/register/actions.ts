'use server'

import { ApolloError, FetchResult } from '@apollo/client'
import {
	CreateEntityDocument,
	CreateEntityInput,
	CreateUserInput,
	RequestVerifyEmailDocument,
	SignupDocument,
	SignupMutation,
	VerifyEmailDocument
} from '../../api/generated/graphql'
import { getClient } from '../../api/graphql/client'
import { revalidatePath } from 'next/cache'

const client = getClient()

export const signUp = async (signupUserInput: CreateUserInput): Promise<{ response: FetchResult<SignupMutation> | null; error: string | null }> => {
	try {
		const response = await client.mutate({
			mutation: SignupDocument,
			variables: {
				signupUserInput
			}
		})

		return { response, error: null }
	} catch (error) {
		const { message } = error as ApolloError
		return { response: null, error: message }
	}
}

export const createEntity = async (createEntityInput: CreateEntityInput) => {
	try {
		const response = await client.mutate({
			mutation: CreateEntityDocument,
			variables: {
				createEntityInput
			}
		})

		revalidatePath('/home')

		return response
	} catch (error) {
		console.log('error', JSON.stringify(error, null, 2))
	}
}

export const requestVerifyEmail = async (email: string) => {
	try {
		const { data } = await client.mutate({
			mutation: RequestVerifyEmailDocument,
			variables: {
				email
			}
		})

		return data?.requestVerifyEmail
	} catch (error) {
		console.log('error', JSON.stringify(error, null, 2))
	}
}

export const verifyEmail = async (email: string, otp: string) => {
	try {
		const { data } = await client.mutate({
			mutation: VerifyEmailDocument,
			variables: {
				verifyEmailInput: {
					email,
					otp
				}
			}
		})

		return data?.verifyEmail
	} catch (error) {
		console.log('error', JSON.stringify(error, null, 2))
	}
}
