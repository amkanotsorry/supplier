import React from 'react'

interface Props {
	children?: React.ReactNode
}

const RegisterPageDesc: React.FC<Props> = ({ children }) => {
	return <div className="text-sm font-medium leading-6 mb-8 text-[#64696E]">{children}</div>
}

export default RegisterPageDesc
