import React from 'react'

interface Props {
	children?: React.ReactNode
}

const RegisterPageTitle: React.FC<Props> = ({ children }) => {
	return <div className="text-3xl font-semibold leading-10 mb-3 mt-12">{children}</div>
}

export default RegisterPageTitle
