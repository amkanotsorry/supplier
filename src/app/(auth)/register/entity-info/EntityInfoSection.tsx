'use client'

import { CreateLocationInput, Entity_Type } from '@/src/app/api/generated/graphql'
import { toast } from '@/src/components/ui/use-toast'
import { Button, Input, Spinner, Textarea } from '@nextui-org/react'
import { useSession } from 'next-auth/react'
import { useRouter } from 'next/navigation'
import React, { useState } from 'react'
import { createEntity } from '../actions'
import { Edit } from 'lucide-react'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '@/src/components/ui/select'
import DatePicker from '@/src/components/datepicker'
import AddressModal, { QueryAddress } from '@/src/components/modals/location-modals/address-modal'

interface Props {}

const EntityInfoSection: React.FC<Props> = () => {
	const router = useRouter()
	const session = useSession()
	const [name, setName] = useState('')
	const [registrationNumber, setRegistrationNumber] = useState('')
	const [phoneNumber, setPhoneNumber] = useState('')
	const [areasOfActivity, setAreasOfActivity] = useState('')
	const [incorporatedDate, setIncorporatedDate] = useState(new Date())
	const [description, setDescription] = useState('')
	const [isLoading, setIsLoading] = useState(false)
	const [address, setAddress] = useState<QueryAddress | null>(null)

	const onContinue = async () => {
		if (!session.data?.user.id) {
			router.push('/login')

			toast({ title: 'Login failed. Retry login' })
			return
		}

		setIsLoading(true)

		const response = await createEntity({
			name,
			registrationNumber,
			phoneNumber,
			company: {
				description,
				incorporatedDate
			},
			areasOfActivity,
			countryCode: '976',
			isoCode: 'MN',
			type: Entity_Type.Company,
			address: {
				cityProvince: {
					code: address?.cityProvince.code || 11,
					name: address?.cityProvince.name || '',
					nameMn: address?.cityProvince.nameMn || ''
				},
				coordinates: address?.coordinates as CreateLocationInput,
				detail: address?.detail || '',
				duuregSoum: {
					code: address?.duuregSoum.code || 11,
					name: address?.duuregSoum.name || '',
					nameMn: address?.duuregSoum.nameMn || ''
				},
				khorooBag: {
					code: address?.khorooBag.code || 11,
					name: address?.khorooBag.name || '',
					nameMn: address?.khorooBag.nameMn || ''
				}
			}
		})

		setIsLoading(false)

		if (response?.data?.createEntity._id) {
			router.push('/home/get-started')
			router.refresh()
			return
		}

		toast({ title: 'Error creating entity. Please check your info and try again', variant: 'destructive' })
	}

	return (
		<div>
			<div className="flex flex-col gap-6 mb-12">
				<Input label="Company name" onChange={e => setName(e.target.value)} />
				<Input label="Registration number" onChange={e => setRegistrationNumber(e.target.value)} />
				<DatePicker value={incorporatedDate} onChange={value => setIncorporatedDate(value || new Date())} placeholder="0000/00/00" />

				<AddressModal
					onCancel={() => {}}
					onOk={address => {
						console.log('wtf', address)
						setAddress(address)
					}}
					triggerComponent={
						<button>
							<Input
								value={address?.detail || ''}
								className="cursor-pointer"
								placeholder="Registered address"
								endContent={<Edit width={18} height={18} />}
							/>
						</button>
					}
				/>

				<div className="flex gap-6 md:gap-3">
					<Select>
						<SelectTrigger className="w-[120px]">
							<SelectValue placeholder="Country" />
						</SelectTrigger>
						<SelectContent>
							{['+1', '+976'].map((productMeta, index) => (
								<SelectItem className="py-3" key={index} value={productMeta}>
									{productMeta}
								</SelectItem>
							))}
						</SelectContent>
					</Select>

					<Input label="Phone number" onChange={e => setPhoneNumber(e.target.value)} />
				</div>

				<Input onChange={e => setAreasOfActivity(e.target.value)} label="Areas of Activity" />
				<Textarea onChange={e => setDescription(e.target.value)} label="Company description" required />
			</div>

			<Button disabled={isLoading} color="primary" className="h-10 w-full" onClick={onContinue}>
				{isLoading ? <Spinner size="sm" color="white" /> : 'Continue'}
			</Button>
		</div>
	)
}

export default EntityInfoSection
