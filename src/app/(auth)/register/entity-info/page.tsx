import { authOptions } from '@/src/app/api/auth/[...nextauth]/authOptions'
import { getServerSession } from 'next-auth'
import { redirect } from 'next/navigation'
import RegisterPageDesc from '../components/register-page-desc'
import RegisterPageTitle from '../components/register-page-title'
import EntityInfoSection from './EntityInfoSection'

const EntityInfoPage = async () => {
	const session = await getServerSession(authOptions)
	if (!session?.user) return redirect('/login')

	return (
		<div className="overflow-y-auto">
			<RegisterPageTitle>Entity Info</RegisterPageTitle>
			<RegisterPageDesc>
				These information will be reviewed and confirmed by Agri-X operation team. Provide valid information for smooth process.
			</RegisterPageDesc>
			<EntityInfoSection />
		</div>
	)
}

export default EntityInfoPage
