'use client'

import { useState } from 'react'
import EntityType, { EntityOptionType } from './EntityType'
import { useRouter } from 'next/navigation'

const ChooseEntity = () => {
	const router = useRouter()
	const [selectedEntity, setSelectedEntity] = useState<EntityOptionType>('individual')

	const onChooseEntity = () => {
		router.push(`/register/${selectedEntity}-info`)
	}

	return (
		<div className="flex flex-col gap-12">
			<div className="flex gap-6">
				<EntityType onSelect={type => setSelectedEntity(type)} selected={selectedEntity === 'individual'} type={'individual'} />
				<EntityType onSelect={type => setSelectedEntity(type)} selected={selectedEntity === 'entity'} type={'entity'} />
			</div>

			<div onClick={onChooseEntity} className="cursor-pointer px-[28px] py-[14px] flex align-center justify-center bg-primary rounded-xl text-white">
				Continue
			</div>
		</div>
	)
}

export default ChooseEntity
