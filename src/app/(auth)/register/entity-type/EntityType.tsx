'use client'

import React from 'react'
import { Check } from 'lucide-react'

export type EntityOptionType = 'individual' | 'entity'

interface Props {
	selected: boolean
	type: EntityOptionType
	onSelect: (type: EntityOptionType) => void
}

const EntityType: React.FC<Props> = ({ selected = false, type, onSelect }) => {
	return (
		<div
			onClick={() => onSelect(type)}
			className={`cursor-pointer relative flex flex-col items-center justify-center w-[244px] h-[244px] rounded-2xl border ${
				selected ? 'border-primary' : 'border-[#EDEFF1]'
			}`}>
			<div
				className={`flex items-center justify-center w-5 h-5 absolute right-4 top-4 rounded-full border ${
					selected ? 'bg-primary border-primary' : 'bg-transparent border-[#EDEFF1]'
				}`}>
				{selected && <Check color="white" width={14} height={14} />}
			</div>

			<div className="mb-2">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<path
						d="M20.3999 21.6004L20.4003 18.0007C20.4004 16.0124 18.7886 14.4004 16.8003 14.4004H7.20099C5.21292 14.4004 3.60121 16.0119 3.60099 18L3.60059 21.6004M15.6006 6.00039C15.6006 7.98862 13.9888 9.60039 12.0006 9.60039C10.0124 9.60039 8.40059 7.98862 8.40059 6.00039C8.40059 4.01217 10.0124 2.40039 12.0006 2.40039C13.9888 2.40039 15.6006 4.01217 15.6006 6.00039Z"
						stroke={selected ? '#22577A' : '#000000'}
						strokeWidth="2"
						strokeLinecap="round"
						strokeLinejoin="round"
					/>
				</svg>
			</div>

			<div className={`capitalize text-xl font-medium leading-6 ${selected ? 'text-primary' : 'text-[#161A1D]'}`}>{type}</div>
		</div>
	)
}

export default EntityType
