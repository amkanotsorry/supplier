import { NextPage } from 'next'
import ChooseEntity from './ChooseEntity'
import RegisterPageTitle from '../components/register-page-title'
import RegisterPageDesc from '../components/register-page-desc'
import { authOptions } from '@/src/app/api/auth/[...nextauth]/authOptions'
import { getServerSession } from 'next-auth'
import { redirect } from 'next/navigation'

const EntityTypePage: NextPage = async () => {
	const session = await getServerSession(authOptions)
	if (!session?.user) return redirect('/login')

	return (
		<>
			<RegisterPageTitle>Choose type</RegisterPageTitle>
			{/* <RegisterPageDesc>Lorem ipsum dolor sit amet consectetur. Tincidunt quisque leo nunc nunc integer felis.</RegisterPageDesc> */}
			<ChooseEntity />
		</>
	)
}

export default EntityTypePage
