'use client'

import Button from '@/src/components/Button'
import StockLocationModal from '@/src/components/modals/location-modals/stock-location-modal'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '@/src/components/ui/select'
import { Input } from '@nextui-org/react'
import { Edit } from 'lucide-react'
import React from 'react'

interface Props {}

const IndividualInfoSection: React.FC<Props> = () => {
	return (
		<div>
			<div className="flex flex-col gap-6 mb-8">
				<div className="flex gap-6">
					<Input classNames={{ inputWrapper: 'h-10' }} placeholder="First name" />
					<Input classNames={{ inputWrapper: 'h-10' }} placeholder="Last name" />
				</div>

				{/* <StockLocationModal
					triggerComponent={
						<Input
							classNames={{ inputWrapper: 'h-10' }}
							className="cursor-pointer"
							placeholder="Address"
							endContent={<Edit width={18} height={18} />}
						/>
					}
				/> */}

				<div className="flex gap-4 md:gap-3">
					<Select>
						<SelectTrigger className="w-[100px]">
							<SelectValue placeholder="Country" />
						</SelectTrigger>
						<SelectContent>
							{['+1', '+976'].map((productMeta, index) => (
								<SelectItem className="py-3" key={index} value={productMeta}>
									{productMeta}
								</SelectItem>
							))}
						</SelectContent>
					</Select>

					<Input classNames={{ inputWrapper: 'h-10' }} placeholder="Phone number" />
				</div>

				<Input classNames={{ inputWrapper: 'h-10' }} placeholder="Areas of activity" />
			</div>

			<Button className="h-10" text="Continue" />
		</div>
	)
}

export default IndividualInfoSection
