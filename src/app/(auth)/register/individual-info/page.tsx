import { authOptions } from '@/src/app/api/auth/[...nextauth]/authOptions'
import { getServerSession } from 'next-auth'
import { redirect } from 'next/navigation'
import RegisterPageDesc from '../components/register-page-desc'
import RegisterPageTitle from '../components/register-page-title'
import IndividualInfoSection from './IndividualInfoSection'

const IndividualInfoPage = async () => {
	const session = await getServerSession(authOptions)
	if (!session?.user) return redirect('/login')

	return (
		<>
			<RegisterPageTitle>Individual Info</RegisterPageTitle>

			{/* <RegisterPageDesc>Lorem ipsum dolor sit amet consectetur. Tincidunt quisque leo nunc nunc integer felis.</RegisterPageDesc> */}

			<IndividualInfoSection />
		</>
	)
}

export default IndividualInfoPage
