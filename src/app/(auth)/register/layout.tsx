import React from 'react'

export default function RegisterLayout({ children }: { children: React.ReactNode }) {
	return (
		<div className="w-full h-full overflow-hidden">
			<div className="h-full w-full overflow-y-auto flex px-4 py-8 md:items-center justify-center">
				<div className="h-full w-full max-w-[512px] pb-8">
					{children}

					{/* Margins and paddings are being ignored here, so this is added. Find better solution */}
					<div className="h-8" />
				</div>
			</div>
		</div>
	)
}
