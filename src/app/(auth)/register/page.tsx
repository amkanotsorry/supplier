import { NextPage } from 'next'
import RegisterSection from './RegisterSection'

const RegisterPage: NextPage = () => {
	return (
		<>
			<div className="text-2xl md:text-3xl font-semibold leading-10 mb-3 mt-8">Create your account</div>
			<div className="text-[#64696E] text-sm font-medium leading-4 md:leading-6 mb-6 md:mb-10">Enter the fields below to get started</div>

			<RegisterSection />
		</>
	)
}

export default RegisterPage
