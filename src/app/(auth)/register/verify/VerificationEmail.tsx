'use client'

import React, { useState } from 'react'
import PinField from 'react-pin-field'
import { verifyEmail } from '../actions'
import { Button, Spinner } from '@nextui-org/react'
import { useSession } from 'next-auth/react'
// import CheckIcon from 'src/assets/icons/check.svg'

type Props = { completed: boolean; onSend: () => void; email: string }

const VerificationEmail: React.FC<Props> = ({ completed = false, onSend, email }) => {
	const [value, setValue] = useState('')
	const [error, setError] = useState('')
	const [loading, setLoading] = useState(false)
	const { data, update } = useSession()

	const onSubmit = async () => {
		if (value.length !== 6) {
			setError('6 оронтой тоо оруулна уу!')
			return
		}
		setLoading(true)
		const response = await verifyEmail(email, value)
		setLoading(false)

		if (response?._id) {
			onSend()
		} else {
			setError('Wrong OTP. Try again')
		}
	}

	return (
		<div className={`flex flex-col border ${completed && '!border-[#3D8F6B]'} p-6 gap-y-6 rounded-2xl`}>
			<div className="flex flex-row justify-between">
				<div className="text-base font-semibold leading-6">1/2</div>

				{completed && <div className="rounded-full bg-[#3D8F6B] w-7 h-7">{/* <CheckIcon className="mx-auto mt-[10px]" /> */}</div>}
			</div>

			<div className="text-base font-medium">
				We mailed you a six-digit code to <span className="font-bold">{email}</span>.{' '}
				{completed ? 'confirmed.' : 'Enter the code below to confirm your phone number'}
			</div>

			{!completed && (
				<>
					<div className="flex flex-row gap-x-4 overflow-hidden">
						<PinField
							onChange={text => setValue(text)}
							length={6}
							className="pl-7 bg-[#F1F2F4] text-lg h-[68px] outline-none flex items-center justify-center rounded-lg w-[63px]"
							autoFocus
							validate="0123456789"
						/>
					</div>

					{error && <div className="text-danger text-sm">{error}</div>}
					<Button disabled={loading} onClick={onSubmit} color="primary" className="h-10">
						{loading ? <Spinner color="white" size="sm" /> : 'Илгээх'}
					</Button>
				</>
			)}
		</div>
	)
}

export default VerificationEmail
