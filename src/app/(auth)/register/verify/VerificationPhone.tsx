'use client'
import React from 'react'
import PinField from 'react-pin-field'

const VerificationPhone = () => {
	return (
		// <div className={`flex flex-col border border-[${true ? '#3D8F6B' : '#D7DCE1'}] p-6 gap-y-6 rounded-2xl`}>
		<div className={`flex flex-col border border-['#3D8F6B'] p-6 gap-y-6 rounded-2xl`}>
			<div className="text-base font-semibold leading-6">2/2</div>

			<div className="text-base font-medium">We messaged you a six-digit code to +976 80****13 Enter the code below to confirm your phone number</div>

			<div className="flex flex-row gap-x-4">
				<PinField length={6} className="pin-field" autoFocus validate="0123456789" />
			</div>

			<div className="cursor-pointer px-[28px] py-[14px] flex align-center justify-center bg-[#57CC99] rounded-xl text-white">Илгээх</div>
		</div>
	)
}

export default VerificationPhone
