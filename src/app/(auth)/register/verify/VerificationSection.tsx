'use client'

import { useSession } from 'next-auth/react'
import { useRouter } from 'next/navigation'
import React from 'react'
import VerificationEmail from './VerificationEmail'

const VerificationSection = ({ email }: { email: string }) => {
	const router = useRouter()
	const session = useSession()
	const [currentStep, setCurrentStep] = React.useState<'email' | 'phone'>('email')
	const [completed, setCompleted] = React.useState(false)

	const onSend = async () => {
		setCompleted(true)
		session.update()
		router.replace('/register/entity-info')
	}

	return (
		<div className="flex flex-col gap-6">
			<VerificationEmail email={email} onSend={onSend} completed={completed} />

			{/* <VerificationPhone /> */}
		</div>
	)
}

export default VerificationSection
