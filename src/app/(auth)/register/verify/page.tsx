import React from 'react'
import VerificationSection from './VerificationSection'
import { requestVerifyEmail } from '../actions'
import { Button } from '@nextui-org/react'
import VerifyError from './verify-error'
import { getServerSession } from 'next-auth'
import { authOptions } from '@/src/app/api/auth/[...nextauth]/authOptions'
import { redirect } from 'next/navigation'

interface Props {
	searchParams: { email: string }
}

const VerifyEmailPage: React.FC<Props> = async ({ searchParams }) => {
	const session = await getServerSession(authOptions)
	if (!session?.user) return redirect('/login')

	const response = await requestVerifyEmail(searchParams.email)
	if (!response?.isSent) return <VerifyError />

	return (
		<>
			<div className="text-2xl md:text-3xl font-semibold leading-10 mb-6 md:mb-9 mt-12">Verify your email address</div>
			<VerificationSection email={searchParams.email} />
		</>
	)
}

export default VerifyEmailPage
