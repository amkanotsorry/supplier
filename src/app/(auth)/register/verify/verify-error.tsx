'use client'

import { Button } from '@nextui-org/react'
import { signOut } from 'next-auth/react'
import { useRouter } from 'next/navigation'
import React from 'react'

interface Props {}

const VerifyError: React.FC<Props> = () => {
	const router = useRouter()
	return (
		<div className="flex flex-col gap-4 mt-12 w-full h-full justify-center items-center">
			Email verification error.
			<Button
				color="primary"
				onClick={async () => {
					await signOut()
				}}>
				Re-Login
			</Button>
		</div>
	)
}

export default VerifyError
