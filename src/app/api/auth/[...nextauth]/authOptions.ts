import type { NextAuthOptions } from 'next-auth'
import CredentialsProvider from 'next-auth/providers/credentials'
import { LoginDocument, Platform_Type } from '../../generated/graphql'
import { getClient } from '../../graphql/client'

const MINUTE = 60
const HOUR = 60 * MINUTE

export const authOptions: NextAuthOptions = {
	providers: [
		CredentialsProvider({
			credentials: {},
			authorize: async (credentials: any, _: any) => {
				if (!credentials?.email || !credentials.password) return null

				const client = getClient()
				const { email, password } = credentials

				let response
				try {
					response = await client.mutate({
						mutation: LoginDocument,
						variables: {
							loginUserInput: {
								email,
								password,
								platform: Platform_Type.Supplier
							}
						}
					})
				} catch (error) {
					console.log(JSON.stringify(error, null, 2))
				}

				const { login } = response?.data || {}
				if (!login) return null

				const {
					access_token,
					user: { _id, firstName, email: userEmail, currentEntityId, entityIds, isEmailVerified }
				} = login

				const user = {
					id: _id as string,
					token: access_token,
					name: firstName,
					email: userEmail,
					currentEntityId,
					entityIds,
					isEmailVerified
				}

				return user
			}
		})
	],
	pages: {
		signIn: '/login'
	},
	session: {
		strategy: 'jwt'
	},

	callbacks: {
		async jwt({ token, user, trigger, session }) {
			if (trigger === 'update') {
				return {
					...token,
					...session?.user
				}
			}

			return { ...token, ...user }
		},

		async session({ session, token }) {
			session.user = token
			return session
		}
	}
}
