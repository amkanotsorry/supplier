/* eslint-disable */
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  /** A date-time string at UTC, such as 2019-12-03T09:54:33Z, compliant with the date-time format. */
  DateTime: { input: any; output: any; }
};

export enum Animal_Type {
  Cattle = 'cattle',
  Goat = 'goat',
  Horse = 'horse',
  Sheep = 'sheep'
}

export type AddCarcassIdInput = {
  /** Хаяг */
  address?: InputMaybe<CreateAddressInput>;
  /** Тээвэрлэсэн температур */
  avgTemperature?: InputMaybe<Scalars['Float']['input']>;
  /** Карантинд орсон огноо */
  bioSecurityFacilityStartDate?: InputMaybe<Scalars['DateTime']['input']>;
  /** Түүхий эд */
  componentCategory: Scalars['String']['input'];
  /** Мал эмнэлгийн гэрчилгээ */
  megCertificate?: InputMaybe<Scalars['String']['input']>;
  /** Мал эмнэлгийн гэрчилгээний огноо */
  megCertificateDate?: InputMaybe<Scalars['String']['input']>;
  /** Захиалгын дугаар */
  parentCode: Scalars['String']['input'];
  /** Тоо ширхэг */
  quantity: Scalars['Int']['input'];
  /** Төхөөрөх үйлдвэр */
  slaughterhouse?: InputMaybe<Scalars['String']['input']>;
  /** Төхөөрсөн огноо */
  slaughterhouseDate?: InputMaybe<Scalars['String']['input']>;
  /** Төхөөрөх үйлдвэрээс тээвэрлэсэн огноо */
  transportStartedDate?: InputMaybe<Scalars['String']['input']>;
  /** Түүхий эдийн жин */
  weight: Scalars['Float']['input'];
};

export type AddEarTagInput = {
  /** Түүхий эд */
  componentCategory: Scalars['String']['input'];
  /** Ээмэгний дугаар */
  earTag: Scalars['String']['input'];
  /** Захиалгын дугаар */
  parentCode: Scalars['String']['input'];
  /** Тоо ширхэг */
  quantity: Scalars['Int']['input'];
};

export type AddEarTagOnReceiveOrderInput = {
  /** Түүхий эд */
  componentCategory: Scalars['String']['input'];
  /** Ээмэгний дугаар */
  earTag: Scalars['String']['input'];
  /** Захиалгын дугаар */
  parentCode: Scalars['String']['input'];
  /** Тоо ширхэг */
  quantity: Scalars['Int']['input'];
  /** Түүхий эдийн жин */
  weight: Scalars['Float']['input'];
};

export type AddEmployeeInput = {
  /** Хэрэглэгчийн цахим хаяг */
  email: Scalars['String']['input'];
  entityId: Scalars['String']['input'];
  /** Хэрэглэгчийн роль */
  roleId: Scalars['String']['input'];
};

export type Address = {
  __typename?: 'Address';
  /** City/Aimag */
  cityProvince: AddressInfo;
  /** Geo Location */
  coordinates: Location;
  /** Address detail */
  detail: Scalars['String']['output'];
  /** Duureg/Soum */
  duuregSoum: AddressInfo;
  /** Khoroo/Bag */
  khorooBag: AddressInfo;
};

export type AddressInfo = {
  __typename?: 'AddressInfo';
  code: Scalars['Int']['output'];
  name: Scalars['String']['output'];
  nameMn?: Maybe<Scalars['String']['output']>;
};

export type AssignRoleToUser = {
  entityId: Scalars['String']['input'];
  roleIds: Array<Scalars['String']['input']>;
  userId: Scalars['String']['input'];
};

export type Attribute = {
  __typename?: 'Attribute';
  isActive: Scalars['Boolean']['output'];
  option: Scalars['String']['output'];
  value: Scalars['String']['output'];
};

export enum Buyer_Type {
  Distributor = 'distributor',
  Individual = 'individual',
  Manufacturer = 'manufacturer',
  Other = 'other',
  Retailer = 'retailer',
  TradingCompany = 'trading_company'
}

export type BillingAddress = {
  __typename?: 'BillingAddress';
  /** Billing address id */
  _id: Scalars['String']['output'];
  /** Billing address */
  address: BillingAddressInfo;
  entityBuyer?: Maybe<Entity_Buyers>;
  /** Buyer entity Id  */
  entityBuyerId: Scalars['String']['output'];
  /** is active Billing address */
  isActive: Scalars['Boolean']['output'];
};

export type BillingAddressInfo = {
  __typename?: 'BillingAddressInfo';
  addressLine1: Scalars['String']['output'];
  addressLine2?: Maybe<Scalars['String']['output']>;
  city: Scalars['String']['output'];
  country: Country;
  firstName: Scalars['String']['output'];
  lastName: Scalars['String']['output'];
  phoneNumber: Scalars['String']['output'];
  state: Scalars['String']['output'];
  zipCode: Scalars['String']['output'];
};

export type Boolean_PropertyFilterInputType = {
  eq?: InputMaybe<Scalars['Boolean']['input']>;
  gt?: InputMaybe<Scalars['Boolean']['input']>;
  gte?: InputMaybe<Scalars['Boolean']['input']>;
  in?: InputMaybe<Array<Scalars['Boolean']['input']>>;
  lt?: InputMaybe<Scalars['Boolean']['input']>;
  lte?: InputMaybe<Scalars['Boolean']['input']>;
  ne?: InputMaybe<Scalars['Boolean']['input']>;
  nin?: InputMaybe<Array<Scalars['Boolean']['input']>>;
  regex?: InputMaybe<Scalars['Boolean']['input']>;
};

export type BuyerType = {
  __typename?: 'BuyerType';
  otherType?: Maybe<Scalars['String']['output']>;
  type: Buyer_Type;
};

export enum Certification_Status {
  Approved = 'approved',
  Declined = 'declined',
  Ignored = 'ignored',
  Pending = 'pending',
  Updated = 'updated'
}

export enum Check_Point_Type {
  /** Fill up point */
  FillUp = 'fill_up',
  /** Receiving point */
  Receive = 'receive'
}

export enum Country {
  Afghanistan = 'Afghanistan',
  AlandIslands = 'AlandIslands',
  Albania = 'Albania',
  Algeria = 'Algeria',
  AmericanSamoa = 'AmericanSamoa',
  Andorra = 'Andorra',
  Angola = 'Angola',
  Anguilla = 'Anguilla',
  Antarctica = 'Antarctica',
  AntiguaAndBarbuda = 'AntiguaAndBarbuda',
  Argentina = 'Argentina',
  Armenia = 'Armenia',
  Aruba = 'Aruba',
  Australia = 'Australia',
  Austria = 'Austria',
  Azerbaijan = 'Azerbaijan',
  Bahamas = 'Bahamas',
  Bahrain = 'Bahrain',
  Bangladesh = 'Bangladesh',
  Barbados = 'Barbados',
  Belarus = 'Belarus',
  Belgium = 'Belgium',
  Belize = 'Belize',
  Benin = 'Benin',
  Bermuda = 'Bermuda',
  Bhutan = 'Bhutan',
  Bolivia = 'Bolivia',
  BonaireSintEustatiusSaba = 'BonaireSintEustatiusSaba',
  BosniaAndHerzegovina = 'BosniaAndHerzegovina',
  Botswana = 'Botswana',
  BouvetIsland = 'BouvetIsland',
  Brazil = 'Brazil',
  BritishIndianOceanTerritory = 'BritishIndianOceanTerritory',
  BruneiDarussalam = 'BruneiDarussalam',
  Bulgaria = 'Bulgaria',
  BurkinaFaso = 'BurkinaFaso',
  Burundi = 'Burundi',
  Cambodia = 'Cambodia',
  Cameroon = 'Cameroon',
  Canada = 'Canada',
  CapeVerde = 'CapeVerde',
  CaymanIslands = 'CaymanIslands',
  CentralAfricanRepublic = 'CentralAfricanRepublic',
  Chad = 'Chad',
  Chile = 'Chile',
  China = 'China',
  ChristmasIsland = 'ChristmasIsland',
  CocosKeelingIslands = 'CocosKeelingIslands',
  Colombia = 'Colombia',
  Comoros = 'Comoros',
  Congo = 'Congo',
  CongoDemocraticRepublic = 'CongoDemocraticRepublic',
  CookIslands = 'CookIslands',
  CostaRica = 'CostaRica',
  CoteDIvoire = 'CoteDIvoire',
  Croatia = 'Croatia',
  Cuba = 'Cuba',
  Curacao = 'Curacao',
  Cyprus = 'Cyprus',
  CzechRepublic = 'CzechRepublic',
  Denmark = 'Denmark',
  Djibouti = 'Djibouti',
  Dominica = 'Dominica',
  DominicanRepublic = 'DominicanRepublic',
  Ecuador = 'Ecuador',
  Egypt = 'Egypt',
  ElSalvador = 'ElSalvador',
  EquatorialGuinea = 'EquatorialGuinea',
  Eritrea = 'Eritrea',
  Estonia = 'Estonia',
  Ethiopia = 'Ethiopia',
  FalklandIslands = 'FalklandIslands',
  FaroeIslands = 'FaroeIslands',
  Fiji = 'Fiji',
  Finland = 'Finland',
  France = 'France',
  FrenchGuiana = 'FrenchGuiana',
  FrenchPolynesia = 'FrenchPolynesia',
  FrenchSouthernTerritories = 'FrenchSouthernTerritories',
  Gabon = 'Gabon',
  Gambia = 'Gambia',
  Georgia = 'Georgia',
  Germany = 'Germany',
  Ghana = 'Ghana',
  Gibraltar = 'Gibraltar',
  Greece = 'Greece',
  Greenland = 'Greenland',
  Grenada = 'Grenada',
  Guadeloupe = 'Guadeloupe',
  Guam = 'Guam',
  Guatemala = 'Guatemala',
  Guernsey = 'Guernsey',
  Guinea = 'Guinea',
  GuineaBissau = 'GuineaBissau',
  Guyana = 'Guyana',
  Haiti = 'Haiti',
  HeardIslandMcdonaldIslands = 'HeardIslandMcdonaldIslands',
  HolySeeVaticanCityState = 'HolySeeVaticanCityState',
  Honduras = 'Honduras',
  HongKong = 'HongKong',
  Hungary = 'Hungary',
  Iceland = 'Iceland',
  India = 'India',
  Indonesia = 'Indonesia',
  Iran = 'Iran',
  Iraq = 'Iraq',
  Ireland = 'Ireland',
  IsleOfMan = 'IsleOfMan',
  Israel = 'Israel',
  Italy = 'Italy',
  Jamaica = 'Jamaica',
  Japan = 'Japan',
  Jersey = 'Jersey',
  Jordan = 'Jordan',
  Kazakhstan = 'Kazakhstan',
  Kenya = 'Kenya',
  Kiribati = 'Kiribati',
  Korea = 'Korea',
  KoreaDemocraticPeoplesRepublic = 'KoreaDemocraticPeoplesRepublic',
  Kuwait = 'Kuwait',
  Kyrgyzstan = 'Kyrgyzstan',
  LaoPeoplesDemocraticRepublic = 'LaoPeoplesDemocraticRepublic',
  Latvia = 'Latvia',
  Lebanon = 'Lebanon',
  Lesotho = 'Lesotho',
  Liberia = 'Liberia',
  LibyanArabJamahiriya = 'LibyanArabJamahiriya',
  Liechtenstein = 'Liechtenstein',
  Lithuania = 'Lithuania',
  Luxembourg = 'Luxembourg',
  Macao = 'Macao',
  Macedonia = 'Macedonia',
  Madagascar = 'Madagascar',
  Malawi = 'Malawi',
  Malaysia = 'Malaysia',
  Maldives = 'Maldives',
  Mali = 'Mali',
  Malta = 'Malta',
  MarshallIslands = 'MarshallIslands',
  Martinique = 'Martinique',
  Mauritania = 'Mauritania',
  Mauritius = 'Mauritius',
  Mayotte = 'Mayotte',
  Mexico = 'Mexico',
  Micronesia = 'Micronesia',
  Moldova = 'Moldova',
  Monaco = 'Monaco',
  Mongolia = 'Mongolia',
  Montenegro = 'Montenegro',
  Montserrat = 'Montserrat',
  Morocco = 'Morocco',
  Mozambique = 'Mozambique',
  Myanmar = 'Myanmar',
  Namibia = 'Namibia',
  Nauru = 'Nauru',
  Nepal = 'Nepal',
  Netherlands = 'Netherlands',
  NewCaledonia = 'NewCaledonia',
  NewZealand = 'NewZealand',
  Nicaragua = 'Nicaragua',
  Niger = 'Niger',
  Nigeria = 'Nigeria',
  Niue = 'Niue',
  NorfolkIsland = 'NorfolkIsland',
  NorthernMarianaIslands = 'NorthernMarianaIslands',
  Norway = 'Norway',
  Oman = 'Oman',
  Pakistan = 'Pakistan',
  Palau = 'Palau',
  PalestinianTerritory = 'PalestinianTerritory',
  Panama = 'Panama',
  PapuaNewGuinea = 'PapuaNewGuinea',
  Paraguay = 'Paraguay',
  Peru = 'Peru',
  Philippines = 'Philippines',
  Pitcairn = 'Pitcairn',
  Poland = 'Poland',
  Portugal = 'Portugal',
  PuertoRico = 'PuertoRico',
  Qatar = 'Qatar',
  Reunion = 'Reunion',
  Romania = 'Romania',
  RussianFederation = 'RussianFederation',
  Rwanda = 'Rwanda',
  SaintBarthelemy = 'SaintBarthelemy',
  SaintHelena = 'SaintHelena',
  SaintKittsAndNevis = 'SaintKittsAndNevis',
  SaintLucia = 'SaintLucia',
  SaintMartin = 'SaintMartin',
  SaintPierreAndMiquelon = 'SaintPierreAndMiquelon',
  SaintVincentAndGrenadines = 'SaintVincentAndGrenadines',
  Samoa = 'Samoa',
  SanMarino = 'SanMarino',
  SaoTomeAndPrincipe = 'SaoTomeAndPrincipe',
  SaudiArabia = 'SaudiArabia',
  Senegal = 'Senegal',
  Serbia = 'Serbia',
  Seychelles = 'Seychelles',
  SierraLeone = 'SierraLeone',
  Singapore = 'Singapore',
  SintMaarten = 'SintMaarten',
  Slovakia = 'Slovakia',
  Slovenia = 'Slovenia',
  SolomonIslands = 'SolomonIslands',
  Somalia = 'Somalia',
  SouthAfrica = 'SouthAfrica',
  SouthGeorgiaAndSandwichIsl = 'SouthGeorgiaAndSandwichIsl',
  SouthSudan = 'SouthSudan',
  Spain = 'Spain',
  SriLanka = 'SriLanka',
  Sudan = 'Sudan',
  Suriname = 'Suriname',
  SvalbardAndJanMayen = 'SvalbardAndJanMayen',
  Swaziland = 'Swaziland',
  Sweden = 'Sweden',
  Switzerland = 'Switzerland',
  SyrianArabRepublic = 'SyrianArabRepublic',
  Taiwan = 'Taiwan',
  Tajikistan = 'Tajikistan',
  Tanzania = 'Tanzania',
  Thailand = 'Thailand',
  TimorLeste = 'TimorLeste',
  Togo = 'Togo',
  Tokelau = 'Tokelau',
  Tonga = 'Tonga',
  TrinidadAndTobago = 'TrinidadAndTobago',
  Tunisia = 'Tunisia',
  Turkey = 'Turkey',
  Turkmenistan = 'Turkmenistan',
  TurksAndCaicosIslands = 'TurksAndCaicosIslands',
  Tuvalu = 'Tuvalu',
  Uganda = 'Uganda',
  Ukraine = 'Ukraine',
  UnitedArabEmirates = 'UnitedArabEmirates',
  UnitedKingdom = 'UnitedKingdom',
  UnitedStates = 'UnitedStates',
  UnitedStatesOutlyingIslands = 'UnitedStatesOutlyingIslands',
  Uruguay = 'Uruguay',
  Uzbekistan = 'Uzbekistan',
  Vanuatu = 'Vanuatu',
  Venezuela = 'Venezuela',
  Vietnam = 'Vietnam',
  VirginIslandsBritish = 'VirginIslandsBritish',
  VirginIslandsUs = 'VirginIslandsUS',
  WallisAndFutuna = 'WallisAndFutuna',
  WesternSahara = 'WesternSahara',
  Yemen = 'Yemen',
  Zambia = 'Zambia',
  Zimbabwe = 'Zimbabwe'
}

export type CalculateOfferPriceInput = {
  offerId: Scalars['String']['input'];
  variantInputs: Array<CalculateOfferPriceVariantInput>;
};

export type CalculateOfferPriceOutput = {
  __typename?: 'CalculateOfferPriceOutput';
  advance: Scalars['Float']['output'];
  final: Scalars['Float']['output'];
  interim: Scalars['Float']['output'];
  total: Scalars['Float']['output'];
  variants: Array<CalculateOfferPriceVariantPrice>;
};

export type CalculateOfferPriceVariantInput = {
  qty: Scalars['Int']['input'];
  variantId: Scalars['String']['input'];
};

export type CalculateOfferPriceVariantPrice = {
  __typename?: 'CalculateOfferPriceVariantPrice';
  price: Scalars['Float']['output'];
  qty: Scalars['Int']['output'];
  totalPrice: Scalars['Float']['output'];
  variantId: Scalars['String']['output'];
};

export type Component = {
  _id: Scalars['String']['input'];
  /** Түүхий эдийн ангилал */
  categoryId: Scalars['String']['input'];
  /** Бохир жин */
  grossWeight: Scalars['Float']['input'];
  /** Багц эсэх */
  isGroup: Scalars['Boolean']['input'];
  /** Багцлагдсан эсэх */
  isGrouped: Scalars['Boolean']['input'];
  /** Үе шат */
  level: Scalars['Int']['input'];
  /** Байршил */
  locationId?: InputMaybe<Scalars['String']['input']>;
  /** Нэр */
  name: Scalars['String']['input'];
  /** Гарал үүсэл */
  originId?: InputMaybe<Scalars['String']['input']>;
  /** Гарал үүслүүд */
  origins?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Савлагааны дугаар */
  packageId: Scalars['String']['input'];
  /** Эцэг түүхий эд */
  parentId?: InputMaybe<Scalars['String']['input']>;
  /** Боловсруулалт */
  processingId?: InputMaybe<Scalars['String']['input']>;
  /** Багцад орсон түүхий эдүүд */
  sourceComponents: Array<SourceComponent>;
  /** Төлөв */
  status: Scalars['String']['input'];
  /** Төрөл */
  type: Scalars['String']['input'];
  /** Жин */
  weight: Scalars['Float']['input'];
};

export type ComponentCategory = {
  _id: Scalars['String']['input'];
  /** Animal type */
  animalType: Animal_Type;
  /** Code */
  code: Scalars['String']['input'];
  /** Processing id */
  level?: InputMaybe<Scalars['Int']['input']>;
  /** Name */
  name: Scalars['String']['input'];
  /** Хувааж ачих тоо */
  numberOfPart?: InputMaybe<Scalars['Int']['input']>;
  /** Parent component id */
  parentId?: InputMaybe<Scalars['String']['input']>;
  /** Processing id */
  processing?: InputMaybe<Scalars['String']['input']>;
};

export type CreateAddressInfoInput = {
  code: Scalars['Int']['input'];
  name: Scalars['String']['input'];
  nameMn?: InputMaybe<Scalars['String']['input']>;
};

export type CreateAddressInput = {
  /** City/Aimag */
  cityProvince: CreateAddressInfoInput;
  /** Geo Location */
  coordinates: CreateLocationInput;
  /** Address detail */
  detail: Scalars['String']['input'];
  /** Duureg/Soum */
  duuregSoum: CreateAddressInfoInput;
  /** Khoroo/Bag */
  khorooBag: CreateAddressInfoInput;
};

export type CreateBillingAddressInfoInput = {
  addressLine1: Scalars['String']['input'];
  addressLine2?: InputMaybe<Scalars['String']['input']>;
  city: Scalars['String']['input'];
  country: Country;
  firstName: Scalars['String']['input'];
  lastName: Scalars['String']['input'];
  phoneNumber: Scalars['String']['input'];
  state: Scalars['String']['input'];
  zipCode: Scalars['String']['input'];
};

export type CreateBillingAddressInput = {
  /** Billing address */
  address: CreateBillingAddressInfoInput;
  /** Buyer entity Id  */
  entityBuyerId: Scalars['String']['input'];
  /** is active Billing address */
  isActive: Scalars['Boolean']['input'];
};

export type CreateBuyerTypeInput = {
  otherType?: InputMaybe<Scalars['String']['input']>;
  type: Buyer_Type;
};

export type CreateCarcassIdInput = {
  /** Хаяг */
  address: CreateAddressInput;
  /** Амьтны төрөл */
  animalType: Scalars['String']['input'];
  /** Тээвэрлэсэн температур */
  avgTemperature: Scalars['Float']['input'];
  /** Карантинд орсон огноо */
  bioSecurityFacilityStartDate: Scalars['DateTime']['input'];
  /** Түүхий эд */
  componentCategory: Scalars['String']['input'];
  /** Мал эмнэлгийн гэрчилгээ */
  megCertificate: Scalars['String']['input'];
  /** Мал эмнэлгийн гэрчилгээний огноо */
  megCertificateDate: Scalars['String']['input'];
  /** Хүлээн авах цэг */
  pointOfReceive: Scalars['String']['input'];
  /** Тоо ширхэг */
  quantity: Scalars['Int']['input'];
  /** Төхөөрөх үйлдвэр */
  slaughterhouse: Scalars['String']['input'];
  /** Төхөөрсөн огноо */
  slaughterhouseDate?: InputMaybe<Scalars['String']['input']>;
  /** Төхөөрөх үйлдвэрээс тээвэрлэсэн огноо */
  transportStartedDate: Scalars['String']['input'];
  /** Түүхий эдийн жин */
  weight: Scalars['Float']['input'];
};

export type CreateCategoryInput = {
  name: Scalars['String']['input'];
  permissionIds: Array<Scalars['String']['input']>;
  platformId: Scalars['String']['input'];
};

export type CreateCertificateInput = {
  /** Сэртификатын нэр */
  name: Scalars['String']['input'];
  /** Байгууллага */
  organization: Scalars['String']['input'];
};

export type CreateCertificationInput = {
  /** Хавсаргах файлууд (*jpg, *png, * pdf) */
  attachments: Array<S3ObjectInput>;
  expiryDate: Scalars['DateTime']['input'];
  issueDate: Scalars['DateTime']['input'];
  /** Сэртификатын нэр */
  name: Scalars['String']['input'];
  /** Байгууллага */
  organization: Scalars['String']['input'];
};

export type CreateCheckpointInput = {
  /** Нэр */
  name: Scalars['String']['input'];
  /** Төрөл */
  type: Check_Point_Type;
};

export type CreateCollectionPointInput = {
  /** Хаяг */
  address: CreateAddressInput;
  /** Нэр */
  name: Scalars['String']['input'];
  /** Утасны дугаар */
  phoneNumber: Scalars['String']['input'];
};

export type CreateComponentCategoryInput = {
  /** Animal type */
  animalType: Animal_Type;
  /** Name */
  name: Scalars['String']['input'];
  /** Хувааж ачих тоо */
  numberOfPart?: InputMaybe<Scalars['Int']['input']>;
  /** Parent component id */
  parentId?: InputMaybe<Scalars['String']['input']>;
};

export type CreateComponentInput = {
  /** Түүхий эд */
  componentCategory: Scalars['String']['input'];
  /** Савлагааны дугаар */
  packageId: Scalars['String']['input'];
  /** Эх түүхий эдүүд */
  sourceComponents: Array<SourceComponentInput>;
  /** Эх савлагааны дугаар */
  sourcePackageId?: InputMaybe<Scalars['String']['input']>;
  /** Жин */
  weight: Scalars['Float']['input'];
};

export type CreateComponentOutput = {
  __typename?: 'CreateComponentOutput';
  _id: Scalars['String']['output'];
  /** Түүхий эдийн ангилал */
  categoryId: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  /** Бохир жин */
  grossWeight: Scalars['Float']['output'];
  /** Багц эсэх */
  isGroup: Scalars['Boolean']['output'];
  /** Багцлагдсан эсэх */
  isGrouped: Scalars['Boolean']['output'];
  /** Үе шат */
  level: Scalars['Int']['output'];
  /** Байршил */
  locationId?: Maybe<Scalars['String']['output']>;
  /** Нэр */
  name: Scalars['String']['output'];
  /** Гарал үүсэл */
  originId?: Maybe<Scalars['String']['output']>;
  /** Гарал үүслүүд */
  origins?: Maybe<Array<Scalars['String']['output']>>;
  /** Савлагааны дугаар */
  packageId: Scalars['String']['output'];
  /** Эцэг түүхий эд */
  parentId?: Maybe<Scalars['String']['output']>;
  /** Түүхий эд */
  parentPackageId: Scalars['String']['output'];
  /** Боловсруулалт */
  processingId?: Maybe<Scalars['String']['output']>;
  /** Багцад орсон түүхий эдүүд */
  sourceComponents: Array<Source_Components>;
  /** Төлөв */
  status: Scalars['String']['output'];
  /** Төрөл */
  type: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
  /** Жин */
  weight: Scalars['Float']['output'];
};

export type CreateDeliveryDetailInput = {
  aimak?: InputMaybe<Scalars['String']['input']>;
  cityProvince?: InputMaybe<Scalars['String']['input']>;
  collectionPointId?: InputMaybe<Scalars['String']['input']>;
  deliveryType: Delivery_Type;
  duuregSoum?: InputMaybe<Scalars['String']['input']>;
  portId?: InputMaybe<Scalars['String']['input']>;
  stockLocationId?: InputMaybe<Scalars['String']['input']>;
};

export type CreateEntityBuyerInput = {
  buyerType: CreateBuyerTypeInput;
  preferredCategoryIds: Array<Scalars['String']['input']>;
  shippingAddress: Array<CreateAddressInput>;
};

export type CreateEntityCompanyInput = {
  description: Scalars['String']['input'];
  incorporatedDate: Scalars['DateTime']['input'];
  numberOfEmployees?: InputMaybe<Scalars['Int']['input']>;
};

export type CreateEntityInput = {
  /** Хаяг */
  address: CreateAddressInput;
  /** Үйл ажиллагааны чиглэл */
  areasOfActivity: Scalars['String']['input'];
  company?: InputMaybe<CreateEntityCompanyInput>;
  /** Утасны дугаарын урд дугаар (976, 1 гэх мэт) */
  countryCode: Scalars['String']['input'];
  entityBuyer?: InputMaybe<CreateEntityBuyerInput>;
  entitySupplier?: InputMaybe<CreateEntitySupplierInput>;
  /** Улсын код (MN, US гэх мэт) */
  isoCode: Scalars['String']['input'];
  /** Хэрэглэгчийн (компани) нэр */
  name: Scalars['String']['input'];
  /** Утасны дугаар */
  phoneNumber: Scalars['String']['input'];
  /** Регистрийн дугаар) */
  registrationNumber: Scalars['String']['input'];
  type: Entity_Type;
};

export type CreateEntitySupplierCompanyInput = {
  stateRegistrationOrder?: InputMaybe<S3ObjectInput>;
};

export type CreateEntitySupplierIndividualInput = {
  /** Иргэний үнэмлэхний зураг ар */
  identityBack?: InputMaybe<S3ObjectInput>;
  /** Иргэний үнэмлэхний зураг урд */
  identityFront?: InputMaybe<S3ObjectInput>;
  /** Иргэний үнэмлэхний зураг баталгаа */
  identitySelfie?: InputMaybe<S3ObjectInput>;
};

export type CreateEntitySupplierInput = {
  certificationIds: Array<Scalars['String']['input']>;
  /** Cover image */
  coverImages: Array<S3ObjectInput>;
  deliveryDetail: CreateDeliveryDetailInput;
  entitySupplierCompany?: InputMaybe<CreateEntitySupplierCompanyInput>;
  entitySupplierIndividual?: InputMaybe<CreateEntitySupplierIndividualInput>;
  entityType: Entity_Type;
  /** Нүүр зураг */
  image: S3ObjectInput;
  isDomestic?: InputMaybe<Scalars['Boolean']['input']>;
  isInternational?: InputMaybe<Scalars['Boolean']['input']>;
  /** Барааны төрлүүд */
  productTypeIds: Array<Scalars['String']['input']>;
  stockLocationIds: Array<Scalars['String']['input']>;
  /** Нийлүүлэгчийн төрөл */
  supplierTypeId: Scalars['String']['input'];
};

export type CreateFillUpOrderInput = {
  /** Түүхий эд */
  componentCategory: Scalars['String']['input'];
  /** Ээмэгний дугаар */
  earTag: Scalars['String']['input'];
  /** Татан авах цэг */
  pointOfFillUp: Scalars['String']['input'];
  /** Тоо ширхэг */
  quantity: Scalars['Int']['input'];
  /** Татан авалт дууссан огноо */
  transportFinished?: InputMaybe<Scalars['DateTime']['input']>;
  /** Татан авалт эхэлсэн огноо */
  transportStarted: Scalars['DateTime']['input'];
  /** Машины дугаар */
  transportationNumber: Scalars['String']['input'];
};

export type CreateKeyAttributeInput = {
  key: Scalars['String']['input'];
  keyType: Key_Type;
  /** Dropdown type options */
  options?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type CreateLocationInput = {
  latitude: Scalars['Float']['input'];
  longtitude: Scalars['Float']['input'];
};

export type CreateMassFillUpOrderInput = {
  data: Array<CreateMassFillUpOrderInputItem>;
};

export type CreateMassFillUpOrderInputItem = {
  /** Түүхий эдийн код */
  componentCategoryCode: Scalars['String']['input'];
  /** Ээмэгний дугаар */
  earTag: Scalars['String']['input'];
  /** Татан авах цэгийн код */
  pointOfFillUpCode: Scalars['String']['input'];
  /** Тоо ширхэг */
  quantity: Scalars['Int']['input'];
  /** Тээвэрлэлт дууссан */
  transportFinished?: InputMaybe<Scalars['String']['input']>;
  /** Тээвэрлэлт эхэлсэн */
  transportStarted: Scalars['String']['input'];
  /** Машины дугаар */
  transportationNumber: Scalars['String']['input'];
};

export type CreateMegInput = {
  /** Ээмэгний дугаар */
  earTag: Scalars['String']['input'];
  /** МЭГ */
  megNumber: Scalars['String']['input'];
  /** МЭГ МП-рүү шивсэн */
  megNumberMP: Scalars['String']['input'];
};

export type CreateMegsInput = {
  data: Array<CreateMegsInputItem>;
};

export type CreateMegsInputItem = {
  /** Ээмэгний дугаар */
  earTag: Scalars['String']['input'];
  /** МЭГ */
  megNumber: Scalars['String']['input'];
  /** МЭГ МП-рүү шивсэн */
  megNumberMP: Scalars['String']['input'];
};

export type CreateMpProductInput = {
  /** Package id */
  packageId: Scalars['String']['input'];
  productVariant: Scalars['String']['input'];
};

export type CreateMpProductsInput = {
  /** Components */
  components: Array<SourceComponent>;
  /** Products */
  products: Array<ProductInput>;
};

export type CreateOfferCustomizationInput = {
  color?: InputMaybe<CreateOfferCustomizationOptionInput>;
  custom?: InputMaybe<Array<CreateOfferCustomizationOptionInput>>;
  logo?: InputMaybe<CreateOfferCustomizationOptionInput>;
  package?: InputMaybe<CreateOfferCustomizationOptionInput>;
};

export type CreateOfferCustomizationOptionInput = {
  moq: Scalars['Int']['input'];
  name: Scalars['String']['input'];
};

export type CreateOfferDraftInput = {
  additionalDocuments?: InputMaybe<Array<Scalars['String']['input']>>;
  composition?: InputMaybe<Scalars['String']['input']>;
  customization?: InputMaybe<CreateOfferCustomizationInput>;
  deliveryDetail?: InputMaybe<CreateDeliveryDetailInput>;
  hasTemperatureControl?: InputMaybe<Scalars['Boolean']['input']>;
  isRefundable?: InputMaybe<Scalars['Boolean']['input']>;
  isReturnable?: InputMaybe<Scalars['Boolean']['input']>;
  isSampleDelivery?: InputMaybe<Scalars['Boolean']['input']>;
  isSensitiveToMoisture?: InputMaybe<Scalars['Boolean']['input']>;
  isShippableThroughAir?: InputMaybe<Scalars['Boolean']['input']>;
  isVerified?: InputMaybe<Scalars['Boolean']['input']>;
  marketCountry?: InputMaybe<Market_Country>;
  marketType?: InputMaybe<Array<Market_Type>>;
  offerEndDate?: InputMaybe<Scalars['DateTime']['input']>;
  offerStartDate?: InputMaybe<Scalars['DateTime']['input']>;
  packageDetail?: InputMaybe<Scalars['String']['input']>;
  paymentMethods?: InputMaybe<Array<CreatePaymentMethodInput>>;
  paymentType?: InputMaybe<CreatePaymentType>;
  productId?: InputMaybe<Scalars['String']['input']>;
  tags?: InputMaybe<Array<Scalars['String']['input']>>;
  variants?: InputMaybe<Array<CreateOfferVariantInput>>;
  warrantyPeriod?: InputMaybe<Scalars['Int']['input']>;
  warrantyPeriodType?: InputMaybe<Warranty_Period_Type>;
};

export type CreateOfferInput = {
  additionalDocuments?: InputMaybe<Array<Scalars['String']['input']>>;
  composition?: InputMaybe<Scalars['String']['input']>;
  customization?: InputMaybe<CreateOfferCustomizationInput>;
  deliveryDetail: CreateDeliveryDetailInput;
  hasTemperatureControl: Scalars['Boolean']['input'];
  isRefundable?: InputMaybe<Scalars['Boolean']['input']>;
  isReturnable?: InputMaybe<Scalars['Boolean']['input']>;
  isSampleDelivery?: InputMaybe<Scalars['Boolean']['input']>;
  isSensitiveToMoisture: Scalars['Boolean']['input'];
  isShippableThroughAir: Scalars['Boolean']['input'];
  isVerified?: InputMaybe<Scalars['Boolean']['input']>;
  marketCountry?: InputMaybe<Market_Country>;
  marketType: Array<Market_Type>;
  numberOfCounterOffer: Scalars['Int']['input'];
  numberOfOrderRequest: Scalars['Int']['input'];
  offerDraftId?: InputMaybe<Scalars['String']['input']>;
  offerEndDate: Scalars['DateTime']['input'];
  offerStartDate: Scalars['DateTime']['input'];
  packageDetail?: InputMaybe<Scalars['String']['input']>;
  paymentMethods: Array<CreatePaymentMethodInput>;
  paymentType: CreatePaymentType;
  productId: Scalars['String']['input'];
  tags?: InputMaybe<Array<Scalars['String']['input']>>;
  variants: Array<CreateOfferVariantInput>;
  warrantyPeriod: Scalars['Int']['input'];
  warrantyPeriodType: Warranty_Period_Type;
};

export type CreateOfferVariantInput = {
  availableDate: Scalars['DateTime']['input'];
  discloseCertificateOfOrigin?: InputMaybe<Scalars['Boolean']['input']>;
  disclosePlaceOfOrigin?: InputMaybe<Scalars['Boolean']['input']>;
  discloseTestReport?: InputMaybe<Scalars['Boolean']['input']>;
  moq: Scalars['Int']['input'];
  quantity: Scalars['Int']['input'];
  quantityRangeLeadTimes: Array<CreateQuantityRangeLeadTimeInput>;
  quantityRangePrices: Array<CreateQuantityRangePriceInput>;
  variantId: Scalars['String']['input'];
};

export type CreateOrderInput = {
  buyerId: Scalars['String']['input'];
  deliveryMethod: Order_Delivery_Method;
  isVerified?: InputMaybe<Scalars['Boolean']['input']>;
  offerCode: Scalars['String']['input'];
  packageDetail?: InputMaybe<Scalars['String']['input']>;
  productId: Scalars['String']['input'];
  productName: Scalars['String']['input'];
  status?: InputMaybe<Order_Status>;
};

export type CreatePaymentMethodInput = {
  acountNumber: Scalars['Int']['input'];
  icon: Scalars['String']['input'];
  name: Scalars['String']['input'];
};

export type CreatePaymentType = {
  advance: Scalars['Float']['input'];
  final: Scalars['Float']['input'];
  interim: Scalars['Float']['input'];
};

export type CreatePendingInviteInput = {
  /** Email */
  email: Scalars['String']['input'];
  entityId: Scalars['String']['input'];
  roleId: Scalars['String']['input'];
};

export type CreatePermissionInput = {
  code: Scalars['String']['input'];
  description: Scalars['String']['input'];
  isParent: Scalars['Boolean']['input'];
  name: Scalars['String']['input'];
  parentId?: InputMaybe<Scalars['String']['input']>;
  permissionType: Permission_Type;
  platformId: Scalars['String']['input'];
};

export type CreatePlatformInput = {
  /** Platform code */
  code: Platform_Type;
  /** Platform name */
  name: Scalars['String']['input'];
};

export type CreatePortInput = {
  address: CreateAddressInput;
  name: Scalars['String']['input'];
};

export type CreateProductMetaInput = {
  /** Product meta attributes */
  attributes?: InputMaybe<Array<Variant_Enum>>;
  /** Product category id */
  categoryId: Scalars['String']['input'];
  /** product meta active status */
  isActive: Scalars['Boolean']['input'];
  /** key attributes (material, type, etc...) */
  keyAttributeIds?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Product meta name */
  name: Scalars['String']['input'];
  /** Product meta unit */
  unit: Unit_Enum;
  /** Product meta unitVolume */
  unitVolume: Scalars['Float']['input'];
  /** Product meta unitWeight */
  unitWeight: Scalars['Float']['input'];
};

export type CreateProductTypeInput = {
  /** Product type code */
  code: Scalars['String']['input'];
  /** Product type name */
  name: Scalars['String']['input'];
  parentId?: InputMaybe<Scalars['String']['input']>;
};

export type CreateQuantityRangeLeadTimeInput = {
  days: Scalars['Int']['input'];
  max: Scalars['Int']['input'];
  min: Scalars['Int']['input'];
};

export type CreateQuantityRangePriceInput = {
  max: Scalars['Int']['input'];
  min: Scalars['Int']['input'];
  price: Scalars['Float']['input'];
};

export type CreateReceiveOrderInput = {
  /** Түүхий эд */
  componentCategory: Scalars['String']['input'];
  /** Ээмэгний дугаар */
  earTag: Scalars['String']['input'];
  /** Татан авах цэг */
  pointOfFillUp: Scalars['String']['input'];
  /** Хүлээн авах цэг */
  pointOfReceive: Scalars['String']['input'];
  /** Тоо ширхэг */
  quantity: Scalars['Int']['input'];
  /** Түүхий эдийн жин */
  weight: Scalars['Float']['input'];
};

export type CreateRoleInput = {
  code?: InputMaybe<Scalars['String']['input']>;
  color: Scalars['String']['input'];
  entityId: Scalars['String']['input'];
  name: Scalars['String']['input'];
  permissionIds?: InputMaybe<Array<Scalars['String']['input']>>;
  status: Scalars['Boolean']['input'];
};

export type CreateShippingAddressInput = {
  /** Shipping address */
  address: CreateAddressInput;
  /** Buyer entity Id  */
  entityBuyerId: Scalars['String']['input'];
  /** is active Billing address */
  isActive: Scalars['Boolean']['input'];
};

export type CreateSupplierCategoryInput = {
  /** Нийлүүлэгчийн төрлийн нэр */
  name: Scalars['String']['input'];
};

export type CreateTruckInput = {
  /** Number plate */
  numberplate: Scalars['String']['input'];
  /** Temperature sensor */
  tempSensor?: InputMaybe<Scalars['String']['input']>;
};

export type CreateUserInput = {
  /** Утасны дугаарын урд дугаар (976, 1 гэх мэт) */
  countryCode: Scalars['String']['input'];
  /** Цахим хаяг */
  email: Scalars['String']['input'];
  /** Хэрэглэгчийн нэр (firstName) */
  firstName: Scalars['String']['input'];
  invitaionId?: InputMaybe<Scalars['String']['input']>;
  /** Улсын код (MN, US гэх мэт) */
  isoCode: Scalars['String']['input'];
  lastName: Scalars['String']['input'];
  /** Нууц үг */
  password: Scalars['String']['input'];
  /** Утасны дугаар */
  phoneNumber: Scalars['String']['input'];
  platform: Platform_Type;
};

export enum Delivery_Type {
  Full = 'full',
  ThirdParty = 'third_party',
  ToAimagCollectionPoint = 'to_aimag_collection_point',
  ToLocalPort = 'to_local_port'
}

export type Date_PropertyFilterInputType = {
  eq?: InputMaybe<Scalars['DateTime']['input']>;
  gt?: InputMaybe<Scalars['DateTime']['input']>;
  gte?: InputMaybe<Scalars['DateTime']['input']>;
  in?: InputMaybe<Array<Scalars['DateTime']['input']>>;
  lt?: InputMaybe<Scalars['DateTime']['input']>;
  lte?: InputMaybe<Scalars['DateTime']['input']>;
  ne?: InputMaybe<Scalars['DateTime']['input']>;
  nin?: InputMaybe<Array<Scalars['DateTime']['input']>>;
  regex?: InputMaybe<Scalars['DateTime']['input']>;
};

export type DeliverToAimak = {
  __typename?: 'DeliverToAimak';
  aimak: Scalars['String']['output'];
  collectionPointId: Scalars['String']['output'];
  deliveryType: Delivery_Type;
};

export type DeliverToLocalPort = {
  __typename?: 'DeliverToLocalPort';
  cityProvince: Scalars['String']['output'];
  deliveryType: Delivery_Type;
  duuregSoum: Scalars['String']['output'];
  portId: Scalars['String']['output'];
};

export type DeliveryDetailUnion = DeliverToAimak | DeliverToLocalPort | FullDelivery | ThirdPartyDelivery;

export type DeliveryType = {
  __typename?: 'DeliveryType';
  /** Хүргэлтийн төрлийн код */
  code: Delivery_Type;
  /** Хүргэлтийн төрлийн тайлбар */
  description: Scalars['String']['output'];
  /** Хүргэлтийн төрлийн нэр */
  name: Scalars['String']['output'];
};

export enum Entity_Status {
  Approved = 'approved',
  Created = 'created',
  Review = 'review'
}

export enum Entity_Type {
  Company = 'company',
  Individual = 'individual'
}

export type EditEntitySupplierInput = {
  /** Cover image */
  coverImages?: InputMaybe<Array<S3ObjectInput>>;
  deliveryDetail?: InputMaybe<CreateDeliveryDetailInput>;
  /** Нүүр зураг */
  image?: InputMaybe<S3ObjectInput>;
  /** Барааны төрлүүд */
  productTypeIds?: InputMaybe<Array<Scalars['String']['input']>>;
  stockLocations?: InputMaybe<Array<UpdateStockLocationInput>>;
  /** Нийлүүлэгчийн төрөл */
  supplierTypeId?: InputMaybe<Scalars['String']['input']>;
};

export type EntitySupplierResolver_GetSupplierEntitiesWithOfferFilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  certificationIds?: InputMaybe<String_PropertyFilterInputType>;
  deliveryDetail?: InputMaybe<Null_PropertyFilterInputType>;
  entityId?: InputMaybe<String_PropertyFilterInputType>;
  entitySupplierCompanyId?: InputMaybe<String_PropertyFilterInputType>;
  entitySupplierIndividualId?: InputMaybe<String_PropertyFilterInputType>;
  isDomestic?: InputMaybe<Boolean_PropertyFilterInputType>;
  isInternational?: InputMaybe<Boolean_PropertyFilterInputType>;
  locationCode?: InputMaybe<Number_PropertyFilterInputType>;
  productTypeIds?: InputMaybe<String_PropertyFilterInputType>;
  reviewId?: InputMaybe<String_PropertyFilterInputType>;
  status?: InputMaybe<Null_PropertyFilterInputType>;
  stockLocationIds?: InputMaybe<String_PropertyFilterInputType>;
  supplierTypeId?: InputMaybe<String_PropertyFilterInputType>;
};

export type EntitySupplierResolver_GetSupplierEntitiesWithOffer_FilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  and?: InputMaybe<Array<EntitySupplierResolver_GetSupplierEntitiesWithOfferFilterInputType>>;
  certificationIds?: InputMaybe<String_PropertyFilterInputType>;
  deliveryDetail?: InputMaybe<Null_PropertyFilterInputType>;
  entityId?: InputMaybe<String_PropertyFilterInputType>;
  entitySupplierCompanyId?: InputMaybe<String_PropertyFilterInputType>;
  entitySupplierIndividualId?: InputMaybe<String_PropertyFilterInputType>;
  isDomestic?: InputMaybe<Boolean_PropertyFilterInputType>;
  isInternational?: InputMaybe<Boolean_PropertyFilterInputType>;
  locationCode?: InputMaybe<Number_PropertyFilterInputType>;
  or?: InputMaybe<Array<EntitySupplierResolver_GetSupplierEntitiesWithOfferFilterInputType>>;
  productTypeIds?: InputMaybe<String_PropertyFilterInputType>;
  reviewId?: InputMaybe<String_PropertyFilterInputType>;
  status?: InputMaybe<Null_PropertyFilterInputType>;
  stockLocationIds?: InputMaybe<String_PropertyFilterInputType>;
  supplierTypeId?: InputMaybe<String_PropertyFilterInputType>;
};

export enum Fill_Up_Order_Status {
  Completed = 'completed',
  Created = 'created',
  Delivery = 'delivery',
  Partialcomplete = 'partialcomplete'
}

export type FilterDataType = {
  __typename?: 'FilterDataType';
  /** код */
  code: Scalars['String']['output'];
  /** propertyName */
  field?: Maybe<Scalars['String']['output']>;
  /** нэр */
  name: Scalars['String']['output'];
};

export type FinishFillUpOrderInput = {
  /** Үндсэн захиалгын дугаар */
  parentCode: Scalars['String']['input'];
  /** Татан авалт эхэлсэн огноо */
  transportStarted: Scalars['DateTime']['input'];
};

export type FinishReceiveOrderInput = {
  /** Захиалгын дугаар */
  code: Scalars['String']['input'];
};

export type FullDelivery = {
  __typename?: 'FullDelivery';
  deliveryType: Delivery_Type;
  stockLocationId: Scalars['String']['output'];
};

export type GetComponentCategoriesInput = {
  /** Ear tag */
  earTag?: InputMaybe<Scalars['String']['input']>;
  /** Parent component id */
  parentId?: InputMaybe<Scalars['String']['input']>;
};

export type GetComponentDetailInput = {
  packageId: Scalars['String']['input'];
};

export type GetComponentDetailOutput = {
  __typename?: 'GetComponentDetailOutput';
  _id: Scalars['String']['output'];
  /** Түүхий эдийн ангилал */
  categoryId: Scalars['String']['output'];
  childComps: Array<Components>;
  compsTotalWeight: Scalars['Float']['output'];
  createdAt: Scalars['DateTime']['output'];
  /** Бохир жин */
  grossWeight: Scalars['Float']['output'];
  /** Багц эсэх */
  isGroup: Scalars['Boolean']['output'];
  /** Багцлагдсан эсэх */
  isGrouped: Scalars['Boolean']['output'];
  /** Үе шат */
  level: Scalars['Int']['output'];
  /** Байршил */
  locationId?: Maybe<Scalars['String']['output']>;
  /** Нэр */
  name: Scalars['String']['output'];
  /** Гарал үүсэл */
  originId?: Maybe<Scalars['String']['output']>;
  /** Гарал үүслүүд */
  origins?: Maybe<Array<Scalars['String']['output']>>;
  /** Савлагааны дугаар */
  packageId: Scalars['String']['output'];
  /** Эцэг түүхий эд */
  parentId?: Maybe<Scalars['String']['output']>;
  /** Боловсруулалт */
  processingId?: Maybe<Scalars['String']['output']>;
  /** Багцад орсон түүхий эдүүд */
  sourceComponents: Array<Source_Components>;
  /** Төлөв */
  status: Scalars['String']['output'];
  totalChildComps: Scalars['Int']['output'];
  /** Төрөл */
  type: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
  /** Жин */
  weight: Scalars['Float']['output'];
};

export type GetComponentInput = {
  _id?: InputMaybe<Scalars['String']['input']>;
  packageId?: InputMaybe<Scalars['String']['input']>;
};

export type GetComponentOutput = {
  __typename?: 'GetComponentOutput';
  _id: Scalars['String']['output'];
  category: Component_Categories;
  /** Түүхий эдийн ангилал */
  categoryId: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  /** Бохир жин */
  grossWeight: Scalars['Float']['output'];
  /** Багц эсэх */
  isGroup: Scalars['Boolean']['output'];
  /** Багцлагдсан эсэх */
  isGrouped: Scalars['Boolean']['output'];
  /** Үе шат */
  level: Scalars['Int']['output'];
  /** Байршил */
  locationId?: Maybe<Scalars['String']['output']>;
  /** Нэр */
  name: Scalars['String']['output'];
  /** Гарал үүсэл */
  originId?: Maybe<Scalars['String']['output']>;
  /** Гарал үүслүүд */
  origins?: Maybe<Array<Scalars['String']['output']>>;
  /** Савлагааны дугаар */
  packageId: Scalars['String']['output'];
  /** Эцэг түүхий эд */
  parentId?: Maybe<Scalars['String']['output']>;
  /** Боловсруулалт */
  processingId?: Maybe<Scalars['String']['output']>;
  /** Багцад орсон түүхий эдүүд */
  sourceComponents: Array<Source_Components>;
  /** Төлөв */
  status: Scalars['String']['output'];
  /** Төрөл */
  type: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
  /** Жин */
  weight: Scalars['Float']['output'];
};

export type GetComponentsInput = {
  /** Багц эсэх */
  isGroup?: InputMaybe<Scalars['Boolean']['input']>;
  /** Багцлагдсан эсэх */
  isGrouped?: InputMaybe<Scalars['Boolean']['input']>;
  /** Төлөв */
  status?: InputMaybe<Scalars['String']['input']>;
};

export type GetEarTagDetailInput = {
  /** Ээмэгний дугаар */
  earTag: Scalars['String']['input'];
};

export type GetEarTagDetailOutput = {
  __typename?: 'GetEarTagDetailOutput';
  animalType: Scalars['String']['output'];
  /** Түүхий эд */
  componentCategory: Scalars['String']['output'];
  /** Түүхий эдийн нэр */
  componentCategoryName: Scalars['String']['output'];
  /** Ээмэгний дугаар */
  earTag: Scalars['String']['output'];
  /** Тоо ширхэгийн дээд хэмжээ */
  maxQty: Scalars['Int']['output'];
  /** Захиалгын дугаар */
  parentCode?: Maybe<Scalars['String']['output']>;
  /** Тоо ширхэг */
  quantity?: Maybe<Scalars['Int']['output']>;
};

export type GetFillUpOrdersInput = {
  /** Захиалгын төлөв */
  status?: InputMaybe<Scalars['String']['input']>;
};

export type GetFillUpOrdersOutputItem = {
  __typename?: 'GetFillUpOrdersOutputItem';
  _id: Scalars['String']['output'];
  animalType?: Maybe<Scalars['String']['output']>;
  /** Захиалгын дугаар */
  code: Scalars['String']['output'];
  /** Түүхий эд */
  componentCategory?: Maybe<Scalars['String']['output']>;
  /** Түүхий эдийн нэр */
  componentCategoryName?: Maybe<Scalars['String']['output']>;
  createdAt: Scalars['DateTime']['output'];
  /** Ээмэгний дугаар */
  earTag?: Maybe<Scalars['String']['output']>;
  /** Эзэмшигч */
  holder: Scalars['String']['output'];
  /** Эзэмшигчийн имэйл */
  holderEmail?: Maybe<Scalars['String']['output']>;
  /** Үндсэн захиалга эсэх */
  isMain: Scalars['Boolean']['output'];
  /** Тоо ширхэгийн дээд хэмжээ */
  maxQty?: Maybe<Scalars['Int']['output']>;
  /** Үндсэн захиалгын дугаар */
  parentCode?: Maybe<Scalars['String']['output']>;
  /** Татан авах цэг */
  pointOfFillUp: Scalars['String']['output'];
  /** Татан авах цэгийн нэр */
  pointOfFillUpName: Scalars['String']['output'];
  /** Тоо ширхэг */
  quantity?: Maybe<Scalars['Int']['output']>;
  /** Төлөв */
  status: Fill_Up_Order_Status;
  /** Тээвэрлэсэн температур */
  temperature?: Maybe<Scalars['Float']['output']>;
  /** Татсан түүхий эдийн тоо */
  totalComponentCategories?: Maybe<Scalars['Int']['output']>;
  /** Татан авалт дууссан огноо */
  transportFinished?: Maybe<Scalars['DateTime']['output']>;
  /** Татан авалт эхэлсэн огноо */
  transportStarted?: Maybe<Scalars['DateTime']['output']>;
  /** Машины дугаар */
  transportationNumber: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
};

export type GetReceiveHistoryInput = {
  /** Ээмэгний дугаар */
  earTag: Scalars['String']['input'];
};

export type GetReceiveOrdersInput = {
  /** Захиалгын төлөв */
  status?: InputMaybe<Scalars['String']['input']>;
};

export type GetReceiveOrdersOutputItem = {
  __typename?: 'GetReceiveOrdersOutputItem';
  _id: Scalars['String']['output'];
  animalType?: Maybe<Scalars['String']['output']>;
  /** Захиалгын дугаар */
  code: Scalars['String']['output'];
  /** Түүхий эд */
  componentCategory?: Maybe<Scalars['String']['output']>;
  /** Түүхий эдийн нэр */
  componentCategoryName?: Maybe<Scalars['String']['output']>;
  createdAt: Scalars['DateTime']['output'];
  /** Ээмэгний дугаар */
  earTag?: Maybe<Scalars['String']['output']>;
  /** Ээмэгний дугаарын төрөл */
  earTagType?: Maybe<Scalars['String']['output']>;
  /** Үндсэн захиалга эсэх */
  isMain: Scalars['Boolean']['output'];
  /** Тоо ширхэгийн дээд хэмжээ */
  maxQty?: Maybe<Scalars['Int']['output']>;
  /** Үндсэн захиалгын дугаар */
  parentCode?: Maybe<Scalars['String']['output']>;
  /** Татан авах цэг */
  pointOfFillUp: Scalars['String']['output'];
  /** Татан авах цэгийн нэр */
  pointOfFillUpName: Scalars['String']['output'];
  /** Хүлээн авах цэг */
  pointOfReceive: Scalars['String']['output'];
  /** Хүлээн авах цэгийн нэр */
  pointOfReceiveName: Scalars['String']['output'];
  /** Цуглуулсан түүхий эдийн тоо */
  quantity?: Maybe<Scalars['Int']['output']>;
  /** Төлөв */
  status: Scalars['String']['output'];
  /** Татсан түүхий эдийн тоо */
  totalComponentCategories?: Maybe<Scalars['Int']['output']>;
  /** Нийт жин */
  totalWeight?: Maybe<Scalars['Int']['output']>;
  /** Машины дугаар */
  transportationNumber?: Maybe<Scalars['String']['output']>;
  updatedAt: Scalars['DateTime']['output'];
};

export type GetReceivingEarTagDetailInput = {
  /** Ээмэгний дугаар */
  earTag: Scalars['String']['input'];
};

export type GetReceivingEarTagDetailOutput = {
  __typename?: 'GetReceivingEarTagDetailOutput';
  animalType: Scalars['String']['output'];
  /** Түүхий эд */
  componentCategory: Scalars['String']['output'];
  /** Түүхий эдийн нэр */
  componentCategoryName: Scalars['String']['output'];
  /** Created At */
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  /** Ээмэгний дугаар */
  earTag: Scalars['String']['output'];
  /** Тоо ширхэгийн дээд хэмжээ */
  maxQty: Scalars['Int']['output'];
  /** Захиалгын дугаар */
  parentCode?: Maybe<Scalars['String']['output']>;
  /** Тоо ширхэг */
  quantity?: Maybe<Scalars['Int']['output']>;
  /** Жин */
  totalWeight: Scalars['Float']['output'];
  /** Updated At */
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type GetSubFillUpOrdersInput = {
  /** Захиалгын дугаар */
  parentCode: Scalars['String']['input'];
};

export type GetSubReceiveOrdersInput = {
  /** Захиалгын дугаар */
  parentCode: Scalars['String']['input'];
};

export type Int_PropertyFilterInputType = {
  eq?: InputMaybe<Scalars['Int']['input']>;
  gt?: InputMaybe<Scalars['Int']['input']>;
  gte?: InputMaybe<Scalars['Int']['input']>;
  in?: InputMaybe<Array<Scalars['Int']['input']>>;
  lt?: InputMaybe<Scalars['Int']['input']>;
  lte?: InputMaybe<Scalars['Int']['input']>;
  ne?: InputMaybe<Scalars['Int']['input']>;
  nin?: InputMaybe<Array<Scalars['Int']['input']>>;
  regex?: InputMaybe<Scalars['Int']['input']>;
};

export enum Key_Type {
  Dropdown = 'dropdown',
  Number = 'number',
  Text = 'text'
}

export type KeyAttribute = {
  __typename?: 'KeyAttribute';
  _id: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  key: Scalars['String']['output'];
  keyType: Key_Type;
  /** Dropdown type options */
  options?: Maybe<Array<Scalars['String']['output']>>;
  updatedAt: Scalars['DateTime']['output'];
};

export type Location = {
  __typename?: 'Location';
  latitude: Scalars['Float']['output'];
  longtitude: Scalars['Float']['output'];
};

export type LoginResponse = {
  __typename?: 'LoginResponse';
  access_token: Scalars['String']['output'];
  refresh_token: Scalars['String']['output'];
  user: LoginResponseUser;
};

export type LoginResponseUser = {
  __typename?: 'LoginResponseUser';
  _id: Scalars['String']['output'];
  /** Утасны дугаарын урд дугаар (976, 1 гэх мэт) */
  countryCode: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  createdEntities?: Maybe<Array<Entities>>;
  createdEntityIds: Array<Scalars['String']['output']>;
  currentEntity?: Maybe<Entities>;
  currentEntityId?: Maybe<Scalars['String']['output']>;
  currentPlatformId: Scalars['String']['output'];
  /** Email */
  email: Scalars['String']['output'];
  entities?: Maybe<Array<Entities>>;
  entityIds: Array<Scalars['String']['output']>;
  /** Хэрэглэгчийн нэр (firstName) */
  firstName: Scalars['String']['output'];
  image?: Maybe<Scalars['String']['output']>;
  isEmailVerified?: Maybe<Scalars['Boolean']['output']>;
  /** Улсын код (MN, US гэх мэт) */
  isoCode: Scalars['String']['output'];
  lastName: Scalars['String']['output'];
  phoneNumber: Scalars['String']['output'];
  /** Refresh Token Field */
  refreshToken?: Maybe<Scalars['String']['output']>;
  roleIds: Array<Scalars['String']['output']>;
  roles?: Maybe<Array<Roles>>;
  updatedAt: Scalars['DateTime']['output'];
  userRoles: Roles;
};

export type LoginUserInput = {
  email: Scalars['String']['input'];
  password: Scalars['String']['input'];
  platform: Platform_Type;
};

export type Marketcountry_PropertyFilterInputType = {
  eq?: InputMaybe<Market_Country>;
  in?: InputMaybe<Array<Market_Country>>;
  ne?: InputMaybe<Market_Country>;
  nin?: InputMaybe<Array<Market_Country>>;
};

export type Markettype_PropertyFilterInputType = {
  eq?: InputMaybe<Market_Type>;
  in?: InputMaybe<Array<Market_Type>>;
  ne?: InputMaybe<Market_Type>;
  nin?: InputMaybe<Array<Market_Type>>;
};

export enum Market_Country {
  Afghanistan = 'Afghanistan',
  AlandIslands = 'AlandIslands',
  Albania = 'Albania',
  Algeria = 'Algeria',
  AmericanSamoa = 'AmericanSamoa',
  Andorra = 'Andorra',
  Angola = 'Angola',
  Anguilla = 'Anguilla',
  Antarctica = 'Antarctica',
  AntiguaAndBarbuda = 'AntiguaAndBarbuda',
  Argentina = 'Argentina',
  Armenia = 'Armenia',
  Aruba = 'Aruba',
  Australia = 'Australia',
  Austria = 'Austria',
  Azerbaijan = 'Azerbaijan',
  Bahamas = 'Bahamas',
  Bahrain = 'Bahrain',
  Bangladesh = 'Bangladesh',
  Barbados = 'Barbados',
  Belarus = 'Belarus',
  Belgium = 'Belgium',
  Belize = 'Belize',
  Benin = 'Benin',
  Bermuda = 'Bermuda',
  Bhutan = 'Bhutan',
  Bolivia = 'Bolivia',
  BonaireSintEustatiusSaba = 'BonaireSintEustatiusSaba',
  BosniaAndHerzegovina = 'BosniaAndHerzegovina',
  Botswana = 'Botswana',
  BouvetIsland = 'BouvetIsland',
  Brazil = 'Brazil',
  BritishIndianOceanTerritory = 'BritishIndianOceanTerritory',
  BruneiDarussalam = 'BruneiDarussalam',
  Bulgaria = 'Bulgaria',
  BurkinaFaso = 'BurkinaFaso',
  Burundi = 'Burundi',
  Cambodia = 'Cambodia',
  Cameroon = 'Cameroon',
  Canada = 'Canada',
  CapeVerde = 'CapeVerde',
  CaymanIslands = 'CaymanIslands',
  CentralAfricanRepublic = 'CentralAfricanRepublic',
  Chad = 'Chad',
  Chile = 'Chile',
  China = 'China',
  ChristmasIsland = 'ChristmasIsland',
  CocosKeelingIslands = 'CocosKeelingIslands',
  Colombia = 'Colombia',
  Comoros = 'Comoros',
  Congo = 'Congo',
  CongoDemocraticRepublic = 'CongoDemocraticRepublic',
  CookIslands = 'CookIslands',
  CostaRica = 'CostaRica',
  CoteDIvoire = 'CoteDIvoire',
  Croatia = 'Croatia',
  Cuba = 'Cuba',
  Curacao = 'Curacao',
  Cyprus = 'Cyprus',
  CzechRepublic = 'CzechRepublic',
  Denmark = 'Denmark',
  Djibouti = 'Djibouti',
  Dominica = 'Dominica',
  DominicanRepublic = 'DominicanRepublic',
  Ecuador = 'Ecuador',
  Egypt = 'Egypt',
  ElSalvador = 'ElSalvador',
  EquatorialGuinea = 'EquatorialGuinea',
  Eritrea = 'Eritrea',
  Estonia = 'Estonia',
  Ethiopia = 'Ethiopia',
  FalklandIslands = 'FalklandIslands',
  FaroeIslands = 'FaroeIslands',
  Fiji = 'Fiji',
  Finland = 'Finland',
  France = 'France',
  FrenchGuiana = 'FrenchGuiana',
  FrenchPolynesia = 'FrenchPolynesia',
  FrenchSouthernTerritories = 'FrenchSouthernTerritories',
  Gabon = 'Gabon',
  Gambia = 'Gambia',
  Georgia = 'Georgia',
  Germany = 'Germany',
  Ghana = 'Ghana',
  Gibraltar = 'Gibraltar',
  Greece = 'Greece',
  Greenland = 'Greenland',
  Grenada = 'Grenada',
  Guadeloupe = 'Guadeloupe',
  Guam = 'Guam',
  Guatemala = 'Guatemala',
  Guernsey = 'Guernsey',
  Guinea = 'Guinea',
  GuineaBissau = 'GuineaBissau',
  Guyana = 'Guyana',
  Haiti = 'Haiti',
  HeardIslandMcdonaldIslands = 'HeardIslandMcdonaldIslands',
  HolySeeVaticanCityState = 'HolySeeVaticanCityState',
  Honduras = 'Honduras',
  HongKong = 'HongKong',
  Hungary = 'Hungary',
  Iceland = 'Iceland',
  India = 'India',
  Indonesia = 'Indonesia',
  Iran = 'Iran',
  Iraq = 'Iraq',
  Ireland = 'Ireland',
  IsleOfMan = 'IsleOfMan',
  Israel = 'Israel',
  Italy = 'Italy',
  Jamaica = 'Jamaica',
  Japan = 'Japan',
  Jersey = 'Jersey',
  Jordan = 'Jordan',
  Kazakhstan = 'Kazakhstan',
  Kenya = 'Kenya',
  Kiribati = 'Kiribati',
  Korea = 'Korea',
  KoreaDemocraticPeoplesRepublic = 'KoreaDemocraticPeoplesRepublic',
  Kuwait = 'Kuwait',
  Kyrgyzstan = 'Kyrgyzstan',
  LaoPeoplesDemocraticRepublic = 'LaoPeoplesDemocraticRepublic',
  Latvia = 'Latvia',
  Lebanon = 'Lebanon',
  Lesotho = 'Lesotho',
  Liberia = 'Liberia',
  LibyanArabJamahiriya = 'LibyanArabJamahiriya',
  Liechtenstein = 'Liechtenstein',
  Lithuania = 'Lithuania',
  Luxembourg = 'Luxembourg',
  Macao = 'Macao',
  Macedonia = 'Macedonia',
  Madagascar = 'Madagascar',
  Malawi = 'Malawi',
  Malaysia = 'Malaysia',
  Maldives = 'Maldives',
  Mali = 'Mali',
  Malta = 'Malta',
  MarshallIslands = 'MarshallIslands',
  Martinique = 'Martinique',
  Mauritania = 'Mauritania',
  Mauritius = 'Mauritius',
  Mayotte = 'Mayotte',
  Mexico = 'Mexico',
  Micronesia = 'Micronesia',
  Moldova = 'Moldova',
  Monaco = 'Monaco',
  Montenegro = 'Montenegro',
  Montserrat = 'Montserrat',
  Morocco = 'Morocco',
  Mozambique = 'Mozambique',
  Myanmar = 'Myanmar',
  Namibia = 'Namibia',
  Nauru = 'Nauru',
  Nepal = 'Nepal',
  Netherlands = 'Netherlands',
  NewCaledonia = 'NewCaledonia',
  NewZealand = 'NewZealand',
  Nicaragua = 'Nicaragua',
  Niger = 'Niger',
  Nigeria = 'Nigeria',
  Niue = 'Niue',
  NorfolkIsland = 'NorfolkIsland',
  NorthernMarianaIslands = 'NorthernMarianaIslands',
  Norway = 'Norway',
  Oman = 'Oman',
  Pakistan = 'Pakistan',
  Palau = 'Palau',
  PalestinianTerritory = 'PalestinianTerritory',
  Panama = 'Panama',
  PapuaNewGuinea = 'PapuaNewGuinea',
  Paraguay = 'Paraguay',
  Peru = 'Peru',
  Philippines = 'Philippines',
  Pitcairn = 'Pitcairn',
  Poland = 'Poland',
  Portugal = 'Portugal',
  PuertoRico = 'PuertoRico',
  Qatar = 'Qatar',
  Reunion = 'Reunion',
  Romania = 'Romania',
  RussianFederation = 'RussianFederation',
  Rwanda = 'Rwanda',
  SaintBarthelemy = 'SaintBarthelemy',
  SaintHelena = 'SaintHelena',
  SaintKittsAndNevis = 'SaintKittsAndNevis',
  SaintLucia = 'SaintLucia',
  SaintMartin = 'SaintMartin',
  SaintPierreAndMiquelon = 'SaintPierreAndMiquelon',
  SaintVincentAndGrenadines = 'SaintVincentAndGrenadines',
  Samoa = 'Samoa',
  SanMarino = 'SanMarino',
  SaoTomeAndPrincipe = 'SaoTomeAndPrincipe',
  SaudiArabia = 'SaudiArabia',
  Senegal = 'Senegal',
  Serbia = 'Serbia',
  Seychelles = 'Seychelles',
  SierraLeone = 'SierraLeone',
  Singapore = 'Singapore',
  SintMaarten = 'SintMaarten',
  Slovakia = 'Slovakia',
  Slovenia = 'Slovenia',
  SolomonIslands = 'SolomonIslands',
  Somalia = 'Somalia',
  SouthAfrica = 'SouthAfrica',
  SouthGeorgiaAndSandwichIsl = 'SouthGeorgiaAndSandwichIsl',
  SouthSudan = 'SouthSudan',
  Spain = 'Spain',
  SriLanka = 'SriLanka',
  Sudan = 'Sudan',
  Suriname = 'Suriname',
  SvalbardAndJanMayen = 'SvalbardAndJanMayen',
  Swaziland = 'Swaziland',
  Sweden = 'Sweden',
  Switzerland = 'Switzerland',
  SyrianArabRepublic = 'SyrianArabRepublic',
  Taiwan = 'Taiwan',
  Tajikistan = 'Tajikistan',
  Tanzania = 'Tanzania',
  Thailand = 'Thailand',
  TimorLeste = 'TimorLeste',
  Togo = 'Togo',
  Tokelau = 'Tokelau',
  Tonga = 'Tonga',
  TrinidadAndTobago = 'TrinidadAndTobago',
  Tunisia = 'Tunisia',
  Turkey = 'Turkey',
  Turkmenistan = 'Turkmenistan',
  TurksAndCaicosIslands = 'TurksAndCaicosIslands',
  Tuvalu = 'Tuvalu',
  Uganda = 'Uganda',
  Ukraine = 'Ukraine',
  UnitedArabEmirates = 'UnitedArabEmirates',
  UnitedKingdom = 'UnitedKingdom',
  UnitedStates = 'UnitedStates',
  UnitedStatesOutlyingIslands = 'UnitedStatesOutlyingIslands',
  Uruguay = 'Uruguay',
  Uzbekistan = 'Uzbekistan',
  Vanuatu = 'Vanuatu',
  Venezuela = 'Venezuela',
  Vietnam = 'Vietnam',
  VirginIslandsBritish = 'VirginIslandsBritish',
  VirginIslandsUs = 'VirginIslandsUS',
  WallisAndFutuna = 'WallisAndFutuna',
  WesternSahara = 'WesternSahara',
  Yemen = 'Yemen',
  Zambia = 'Zambia',
  Zimbabwe = 'Zimbabwe'
}

export enum Market_Type {
  Global = 'global',
  Local = 'local'
}

export type MassImportEarTagInput = {
  data: Array<MassImportEarTagInputItem>;
};

export type MassImportEarTagInputItem = {
  /** Түүхий эдийн код */
  componentCategoryCode: Scalars['String']['input'];
  /** Ээмэгний дугаар */
  earTag: Scalars['String']['input'];
  /** Татан авах цэгийн код */
  pointOfFillUpCode: Scalars['String']['input'];
  /** Хүлээн авах цэгийн код */
  pointOfReceiveCode: Scalars['String']['input'];
  /** Тоо ширхэг */
  quantity: Scalars['Int']['input'];
  /** Жин */
  weight: Scalars['Float']['input'];
};

export type MpProduct = {
  __typename?: 'MpProduct';
  _id: Scalars['String']['output'];
  /** Name */
  components: Array<Source_Components>;
  /** Үйлдвэрлэсэн огноо */
  manufactureDate: Scalars['DateTime']['output'];
  /** Name */
  name: Scalars['String']['output'];
  /** Гарал үүслүүд */
  origins: Array<Scalars['String']['output']>;
  /** Package id */
  packageId: Scalars['String']['output'];
  /** Product id */
  productId: Scalars['String']['output'];
  /** Product variant id */
  variantId: Scalars['String']['output'];
  /** Жин */
  weight: Scalars['Float']['output'];
};

export type Mutation = {
  __typename?: 'Mutation';
  acceptInvitation: Pending_Invites;
  addCarcassId: Receiveorders;
  addEarTag: Filluporders;
  addEarTagOnReceiveOrder: Receiveorders;
  createBillingAddress: BillingAddress;
  createCarcassId: Receiveorders;
  createCategory: Categories;
  createCertificate: Certificates;
  createCertification: Certifications;
  createCheckpoint: Checkpoints;
  createCollectionPoint: Collection_Point;
  createComponent: CreateComponentOutput;
  createComponentCategory: Component_Categories;
  createEntity: Entities;
  createEntityBuyer: Entity_Buyers;
  createEntitySupplier: Entity_Suppliers;
  createEntitySupplierCompany: Entity_Supplier_Companies;
  createEntitySupplierIndividual: Entity_Supplier_Individuals;
  createFillUpOrder: Filluporders;
  createKeyAttribute: KeyAttribute;
  createMPProducts: Array<MpProduct>;
  createMeg: Megs;
  createMegs: Array<Megs>;
  createMpProduct: MpProduct;
  createOffer: Offers;
  createOfferDraft: Offer_Drafts;
  createOrder: Order;
  createPendingInvite: Pending_Invites;
  createPermission: Permissions;
  createPlatform: Platforms;
  createPort: Port;
  createProductMeta: Product_Metas;
  createProductType: Product_Types;
  createReceiveOrder: Receiveorders;
  createReviewEntity: Supplier_Reviews;
  createRole: Roles;
  createShippingAddress: ShippingAddress;
  createStockLocations: Array<Stock_Locations>;
  createSupplierCategory: Supplier_Categories;
  createTruck: Trucks;
  declineInvitation: Pending_Invites;
  deleteSupplierStockLocations: Array<Scalars['String']['output']>;
  editEntitySupplier: Entity_Suppliers;
  finishFillUpOrder: Filluporders;
  finishReceiveOrder: Receiveorders;
  inviteEmployee: Pending_Invites;
  login: LoginResponse;
  massImportEarTag: Receiveorders;
  massImportFillUpOrder: Array<Filluporders>;
  refreshToken: LoginResponse;
  removeBillingAddress: BillingAddress;
  removeCategory: Categories;
  removeCertificate: Certificates;
  removeCertification: Certifications;
  removeCheckpoint: Checkpoints;
  removeComponent: Components;
  removeComponentCategory: Component_Categories;
  removeFillUpOrder: Filluporders;
  removeMeg: Megs;
  removeMpProduct: MpProduct;
  removeOffer: Offer_Drafts;
  removePendingInvite: Pending_Invites;
  removePermission: Permissions;
  removeProductMeta: Product_Metas;
  removeProductType: Product_Types;
  removeReceiveOrder: Receiveorders;
  removeRole: Scalars['String']['output'];
  removeShippingAddress: ShippingAddress;
  removeSupplierCategory: Supplier_Categories;
  removeTruck: Trucks;
  requestResetPassword: Scalars['String']['output'];
  requestVerifyEmail: RequestVerifyResponse;
  resetPassword: Users;
  signup: LoginResponse;
  submitSupplier: Entity_Suppliers;
  switchEntity: LoginResponse;
  updateBillingAddress: BillingAddress;
  updateBuyerUser: BuyerUsers;
  updateCategory: Product_Types;
  updateCertificate: Certificates;
  updateCertification: Certifications;
  updateCheckpoint: Checkpoints;
  updateCollectionPoint: Collection_Point;
  updateComponent: Components;
  updateComponentCategory: Component_Categories;
  updateEntity: Entities;
  updateEntityBuyer: Entity_Buyers;
  updateEntitySupplier: Entity_Suppliers;
  updateFillUpOrder: Filluporders;
  updateKeyAttribute: KeyAttribute;
  updateMeg: Megs;
  updateMpProduct: MpProduct;
  updateOffer: Offer_Drafts;
  updatePendingInvite: Pending_Invites;
  updatePermission: Permissions;
  updatePort: Port;
  updateProductMeta: Product_Metas;
  updateReceiveOrder: Receiveorders;
  updateRole: Roles;
  updateShippingAddress: ShippingAddress;
  updateStockLocation: Stock_Locations;
  updateSupplierCategory: Supplier_Categories;
  updateSupplierReview: Supplier_Reviews;
  updateSupplierUser: SupplierUsers;
  updateTruck: Trucks;
  updateUserRole: Users;
  verifyEmail: Users;
};


export type MutationAcceptInvitationArgs = {
  invitationId: Scalars['String']['input'];
};


export type MutationAddCarcassIdArgs = {
  AddCarcassIdInput: AddCarcassIdInput;
};


export type MutationAddEarTagArgs = {
  addEarTagInput: AddEarTagInput;
};


export type MutationAddEarTagOnReceiveOrderArgs = {
  addEarTagOnReceiveOrderInput: AddEarTagOnReceiveOrderInput;
};


export type MutationCreateBillingAddressArgs = {
  createBillingAddressInput: CreateBillingAddressInput;
};


export type MutationCreateCarcassIdArgs = {
  CreateCarcassIdInput: CreateCarcassIdInput;
};


export type MutationCreateCategoryArgs = {
  createCategoryInput: CreateCategoryInput;
};


export type MutationCreateCertificateArgs = {
  createCertificateInput: CreateCertificateInput;
};


export type MutationCreateCertificationArgs = {
  createCertificationInput: CreateCertificationInput;
};


export type MutationCreateCheckpointArgs = {
  createCheckpointInput: CreateCheckpointInput;
};


export type MutationCreateCollectionPointArgs = {
  createCollectionPointInput: CreateCollectionPointInput;
};


export type MutationCreateComponentArgs = {
  createComponentInput: CreateComponentInput;
};


export type MutationCreateComponentCategoryArgs = {
  createComponentCategoryInput: CreateComponentCategoryInput;
};


export type MutationCreateEntityArgs = {
  createEntityInput: CreateEntityInput;
};


export type MutationCreateEntityBuyerArgs = {
  createEntityBuyerInput: CreateEntityBuyerInput;
  entityId: Scalars['String']['input'];
};


export type MutationCreateEntitySupplierArgs = {
  createEntitySupplierInput: CreateEntitySupplierInput;
  entityId: Scalars['String']['input'];
  entityType: Entity_Type;
};


export type MutationCreateEntitySupplierCompanyArgs = {
  createEntitySupplierCompanyInput: CreateEntitySupplierCompanyInput;
  entitySupplierId: Scalars['String']['input'];
};


export type MutationCreateEntitySupplierIndividualArgs = {
  createEntitySupplierIndividualInput: CreateEntitySupplierIndividualInput;
  entitySupplierId: Scalars['String']['input'];
};


export type MutationCreateFillUpOrderArgs = {
  createFillUpOrderInput: CreateFillUpOrderInput;
};


export type MutationCreateKeyAttributeArgs = {
  createKeyAttributeInput: CreateKeyAttributeInput;
};


export type MutationCreateMpProductsArgs = {
  createMpProductsInput: CreateMpProductsInput;
};


export type MutationCreateMegArgs = {
  createMegInput: CreateMegInput;
};


export type MutationCreateMegsArgs = {
  createMegsInput: CreateMegsInput;
};


export type MutationCreateMpProductArgs = {
  createMpProductInput: CreateMpProductInput;
};


export type MutationCreateOfferArgs = {
  createOfferInput: CreateOfferInput;
};


export type MutationCreateOfferDraftArgs = {
  createOfferDraftInput: CreateOfferDraftInput;
};


export type MutationCreateOrderArgs = {
  createOrderInput: CreateOrderInput;
};


export type MutationCreatePendingInviteArgs = {
  createPendingInviteInput: CreatePendingInviteInput;
};


export type MutationCreatePermissionArgs = {
  createPermissionInput: CreatePermissionInput;
};


export type MutationCreatePlatformArgs = {
  createPlatformInput: CreatePlatformInput;
};


export type MutationCreatePortArgs = {
  createPortInput: CreatePortInput;
};


export type MutationCreateProductMetaArgs = {
  createProductMetaInput: CreateProductMetaInput;
};


export type MutationCreateProductTypeArgs = {
  createProductTypeInput: CreateProductTypeInput;
};


export type MutationCreateReceiveOrderArgs = {
  createReceiveOrderInput: CreateReceiveOrderInput;
};


export type MutationCreateReviewEntityArgs = {
  createEntityReviewInfoInput: SupplierReviewStatusInput;
};


export type MutationCreateRoleArgs = {
  createRoleInput: CreateRoleInput;
};


export type MutationCreateShippingAddressArgs = {
  createShippingAddressInput: CreateShippingAddressInput;
};


export type MutationCreateStockLocationsArgs = {
  createStockLocationsInput: Array<StockLocationInput>;
};


export type MutationCreateSupplierCategoryArgs = {
  createSupplierCategoryInput: CreateSupplierCategoryInput;
};


export type MutationCreateTruckArgs = {
  createTruckInput: CreateTruckInput;
};


export type MutationDeclineInvitationArgs = {
  invitationId: Scalars['String']['input'];
};


export type MutationDeleteSupplierStockLocationsArgs = {
  ids: Array<Scalars['String']['input']>;
  supplierId: Scalars['String']['input'];
};


export type MutationEditEntitySupplierArgs = {
  editEntitySupplierInput: EditEntitySupplierInput;
  supplierId: Scalars['String']['input'];
};


export type MutationFinishFillUpOrderArgs = {
  finishFillUpOrderInput: FinishFillUpOrderInput;
};


export type MutationFinishReceiveOrderArgs = {
  FinishReceiveOrderInput: FinishReceiveOrderInput;
};


export type MutationInviteEmployeeArgs = {
  addEmployeeInput: AddEmployeeInput;
};


export type MutationLoginArgs = {
  loginUserInput: LoginUserInput;
};


export type MutationMassImportEarTagArgs = {
  MassImportEarTagInput: MassImportEarTagInput;
};


export type MutationMassImportFillUpOrderArgs = {
  massImportFillUpOrder: CreateMassFillUpOrderInput;
};


export type MutationRemoveBillingAddressArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveCategoryArgs = {
  id: Scalars['Int']['input'];
};


export type MutationRemoveCertificateArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveCertificationArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveCheckpointArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveComponentArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveComponentCategoryArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveFillUpOrderArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveMegArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveMpProductArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveOfferArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemovePendingInviteArgs = {
  email: Scalars['String']['input'];
};


export type MutationRemovePermissionArgs = {
  id: Scalars['Int']['input'];
};


export type MutationRemoveProductMetaArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveProductTypeArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveReceiveOrderArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveRoleArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveShippingAddressArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveSupplierCategoryArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveTruckArgs = {
  id: Scalars['Int']['input'];
};


export type MutationRequestResetPasswordArgs = {
  email: Scalars['String']['input'];
};


export type MutationRequestVerifyEmailArgs = {
  email: Scalars['String']['input'];
};


export type MutationResetPasswordArgs = {
  resetPasswordInput: ResetPasswordInput;
};


export type MutationSignupArgs = {
  signupUserInput: CreateUserInput;
};


export type MutationSubmitSupplierArgs = {
  markets: Array<Supplier_Markets>;
  supplierId: Scalars['String']['input'];
};


export type MutationSwitchEntityArgs = {
  entityId: Scalars['String']['input'];
};


export type MutationUpdateBillingAddressArgs = {
  updateBillingAddressInput: UpdateBillingAddressInput;
};


export type MutationUpdateBuyerUserArgs = {
  updateBuyerUserInput: UpdateBuyerUserInput;
};


export type MutationUpdateCategoryArgs = {
  updateProductTypeInput: UpdateProductTypeInput;
};


export type MutationUpdateCertificateArgs = {
  updateCertificateInput: UpdateCertificateInput;
};


export type MutationUpdateCertificationArgs = {
  id: Scalars['String']['input'];
  updateCertificationInput: UpdateCertificationInput;
};


export type MutationUpdateCheckpointArgs = {
  updateCheckpointInput: UpdateCheckpointInput;
};


export type MutationUpdateCollectionPointArgs = {
  code: Scalars['String']['input'];
  updateCollectionPointInput: UpdateCollectionPointInput;
};


export type MutationUpdateComponentArgs = {
  updateComponentInput: UpdateComponentInput;
};


export type MutationUpdateComponentCategoryArgs = {
  updateComponentCategoryInput: UpdateComponentCategoryInput;
};


export type MutationUpdateEntityArgs = {
  id: Scalars['String']['input'];
  updateEntityInput: UpdateEntityInput;
};


export type MutationUpdateEntityBuyerArgs = {
  buyerId: Scalars['String']['input'];
  updateEntityBuyerInput: UpdateEntityBuyerInput;
};


export type MutationUpdateEntitySupplierArgs = {
  id: Scalars['String']['input'];
  updateEntitySupplierInput: UpdateEntitySupplierInput;
};


export type MutationUpdateFillUpOrderArgs = {
  updateFillUpOrderInput: UpdateFillUpOrderInput;
};


export type MutationUpdateKeyAttributeArgs = {
  updateKeyAttributeInput: UpdateKeyAttributeInput;
};


export type MutationUpdateMegArgs = {
  updateMegInput: UpdateMegInput;
};


export type MutationUpdateMpProductArgs = {
  updateMpProductInput: UpdateMpProductInput;
};


export type MutationUpdateOfferArgs = {
  id: Scalars['String']['input'];
  updateOfferDraftInput: UpdateOfferDraftInput;
};


export type MutationUpdatePendingInviteArgs = {
  updatePendingInviteInput: UpdatePendingInviteInput;
};


export type MutationUpdatePermissionArgs = {
  updatePermissionInput: UpdatePermissionInput;
};


export type MutationUpdatePortArgs = {
  id: Scalars['String']['input'];
  updatePortInput: UpdatePortInput;
};


export type MutationUpdateProductMetaArgs = {
  updateProductMetaInput: UpdateProductMetaInput;
};


export type MutationUpdateReceiveOrderArgs = {
  updateReceiveOrderInput: UpdateReceiveOrderInput;
};


export type MutationUpdateRoleArgs = {
  updateRoleInput: UpdateRoleInput;
};


export type MutationUpdateShippingAddressArgs = {
  updateShippingAddressInput: UpdateShippingAddressInput;
};


export type MutationUpdateStockLocationArgs = {
  updateStockLocationsInput: UpdateStockLocationInput;
};


export type MutationUpdateSupplierCategoryArgs = {
  updateSupplierCategoryInput: UpdateSupplierCategoryInput;
};


export type MutationUpdateSupplierReviewArgs = {
  id: Scalars['String']['input'];
  updateSupplierReviewInput: UpdateEntityReviewInput;
};


export type MutationUpdateSupplierUserArgs = {
  updateSupplierUserInput: UpdateSupplierUserInput;
};


export type MutationUpdateTruckArgs = {
  updateTruckInput: UpdateTruckInput;
};


export type MutationUpdateUserRoleArgs = {
  assignRolesToUserInput: AssignRoleToUser;
};


export type MutationVerifyEmailArgs = {
  verifyEmailInput: VerifyEmailInput;
};

export type Number_PropertyFilterInputType = {
  eq?: InputMaybe<Scalars['Float']['input']>;
  gt?: InputMaybe<Scalars['Float']['input']>;
  gte?: InputMaybe<Scalars['Float']['input']>;
  in?: InputMaybe<Array<Scalars['Float']['input']>>;
  lt?: InputMaybe<Scalars['Float']['input']>;
  lte?: InputMaybe<Scalars['Float']['input']>;
  ne?: InputMaybe<Scalars['Float']['input']>;
  nin?: InputMaybe<Array<Scalars['Float']['input']>>;
  regex?: InputMaybe<Scalars['Float']['input']>;
};

export type Offerstatus_PropertyFilterInputType = {
  eq?: InputMaybe<Offer_Status>;
  in?: InputMaybe<Array<Offer_Status>>;
  ne?: InputMaybe<Offer_Status>;
  nin?: InputMaybe<Array<Offer_Status>>;
};

export enum Offer_Status {
  Cancelled = 'cancelled',
  Listed = 'listed'
}

export type Orderdeliverymethod_PropertyFilterInputType = {
  eq?: InputMaybe<Order_Delivery_Method>;
  in?: InputMaybe<Array<Order_Delivery_Method>>;
  ne?: InputMaybe<Order_Delivery_Method>;
  nin?: InputMaybe<Array<Order_Delivery_Method>>;
};

export type Orderstatus_PropertyFilterInputType = {
  eq?: InputMaybe<Order_Status>;
  in?: InputMaybe<Array<Order_Status>>;
  ne?: InputMaybe<Order_Status>;
  nin?: InputMaybe<Array<Order_Status>>;
};

export enum Order_Delivery_Method {
  Express = 'express',
  Normal = 'normal'
}

export enum Order_Status {
  Cancelled = 'cancelled',
  Completed = 'completed',
  Created = 'created',
  InDelivery = 'in_delivery',
  InManufacture = 'in_manufacture'
}

export type OfferCustomization = {
  __typename?: 'OfferCustomization';
  color?: Maybe<OfferCustomizationOption>;
  custom?: Maybe<Array<OfferCustomizationOption>>;
  logo?: Maybe<OfferCustomizationOption>;
  package?: Maybe<OfferCustomizationOption>;
};

export type OfferCustomizationOption = {
  __typename?: 'OfferCustomizationOption';
  moq: Scalars['Int']['output'];
  name: Scalars['String']['output'];
};

export type OfferDraftPaginate = {
  __typename?: 'OfferDraftPaginate';
  docs: Array<Offer_Drafts>;
  hasNextPage: Scalars['Boolean']['output'];
  hasPrevPage: Scalars['Boolean']['output'];
  limit: Scalars['Int']['output'];
  nextPage?: Maybe<Scalars['Int']['output']>;
  offset?: Maybe<Scalars['Int']['output']>;
  page?: Maybe<Scalars['Int']['output']>;
  pagingCounter: Scalars['Int']['output'];
  prevPage?: Maybe<Scalars['Int']['output']>;
  totalDocs: Scalars['Int']['output'];
  totalPages: Scalars['Int']['output'];
};

export type OfferDraftResolver_FindFilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  additionalDocuments?: InputMaybe<String_PropertyFilterInputType>;
  composition?: InputMaybe<String_PropertyFilterInputType>;
  createdAt?: InputMaybe<Date_PropertyFilterInputType>;
  createdUserId?: InputMaybe<String_PropertyFilterInputType>;
  deliveryDetail?: InputMaybe<Null_PropertyFilterInputType>;
  hasTemperatureControl?: InputMaybe<Boolean_PropertyFilterInputType>;
  isRefundable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isReturnable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSampleDelivery?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSensitiveToMoisture?: InputMaybe<Boolean_PropertyFilterInputType>;
  isShippableThroughAir?: InputMaybe<Boolean_PropertyFilterInputType>;
  isVerified?: InputMaybe<Boolean_PropertyFilterInputType>;
  marketCountry?: InputMaybe<Null_PropertyFilterInputType>;
  marketType?: InputMaybe<Null_PropertyFilterInputType>;
  offerEndDate?: InputMaybe<Date_PropertyFilterInputType>;
  offerStartDate?: InputMaybe<Date_PropertyFilterInputType>;
  packageDetail?: InputMaybe<String_PropertyFilterInputType>;
  productId?: InputMaybe<String_PropertyFilterInputType>;
  productName?: InputMaybe<String_PropertyFilterInputType>;
  productTypeCode?: InputMaybe<String_PropertyFilterInputType>;
  supplierId?: InputMaybe<String_PropertyFilterInputType>;
  tags?: InputMaybe<String_PropertyFilterInputType>;
  updatedAt?: InputMaybe<Date_PropertyFilterInputType>;
  warrantyPeriod?: InputMaybe<Int_PropertyFilterInputType>;
  warrantyPeriodType?: InputMaybe<Null_PropertyFilterInputType>;
};

export type OfferDraftResolver_Find_FilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  additionalDocuments?: InputMaybe<String_PropertyFilterInputType>;
  and?: InputMaybe<Array<OfferDraftResolver_FindFilterInputType>>;
  composition?: InputMaybe<String_PropertyFilterInputType>;
  createdAt?: InputMaybe<Date_PropertyFilterInputType>;
  createdUserId?: InputMaybe<String_PropertyFilterInputType>;
  deliveryDetail?: InputMaybe<Null_PropertyFilterInputType>;
  hasTemperatureControl?: InputMaybe<Boolean_PropertyFilterInputType>;
  isRefundable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isReturnable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSampleDelivery?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSensitiveToMoisture?: InputMaybe<Boolean_PropertyFilterInputType>;
  isShippableThroughAir?: InputMaybe<Boolean_PropertyFilterInputType>;
  isVerified?: InputMaybe<Boolean_PropertyFilterInputType>;
  marketCountry?: InputMaybe<Null_PropertyFilterInputType>;
  marketType?: InputMaybe<Null_PropertyFilterInputType>;
  offerEndDate?: InputMaybe<Date_PropertyFilterInputType>;
  offerStartDate?: InputMaybe<Date_PropertyFilterInputType>;
  or?: InputMaybe<Array<OfferDraftResolver_FindFilterInputType>>;
  packageDetail?: InputMaybe<String_PropertyFilterInputType>;
  productId?: InputMaybe<String_PropertyFilterInputType>;
  productName?: InputMaybe<String_PropertyFilterInputType>;
  productTypeCode?: InputMaybe<String_PropertyFilterInputType>;
  supplierId?: InputMaybe<String_PropertyFilterInputType>;
  tags?: InputMaybe<String_PropertyFilterInputType>;
  updatedAt?: InputMaybe<Date_PropertyFilterInputType>;
  warrantyPeriod?: InputMaybe<Int_PropertyFilterInputType>;
  warrantyPeriodType?: InputMaybe<Null_PropertyFilterInputType>;
};

export type OfferPaginate = {
  __typename?: 'OfferPaginate';
  docs: Array<Offers>;
  hasNextPage: Scalars['Boolean']['output'];
  hasPrevPage: Scalars['Boolean']['output'];
  limit: Scalars['Int']['output'];
  nextPage?: Maybe<Scalars['Int']['output']>;
  offset?: Maybe<Scalars['Int']['output']>;
  page?: Maybe<Scalars['Int']['output']>;
  pagingCounter: Scalars['Int']['output'];
  prevPage?: Maybe<Scalars['Int']['output']>;
  totalDocs: Scalars['Int']['output'];
  totalPages: Scalars['Int']['output'];
};

export type OfferPaymentMethod = {
  __typename?: 'OfferPaymentMethod';
  acountNumber: Scalars['Int']['output'];
  icon: Scalars['String']['output'];
  name: Scalars['String']['output'];
};

export type OfferResolver_FindAllFilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  additionalDocuments?: InputMaybe<String_PropertyFilterInputType>;
  code?: InputMaybe<String_PropertyFilterInputType>;
  composition?: InputMaybe<String_PropertyFilterInputType>;
  createdUserId?: InputMaybe<String_PropertyFilterInputType>;
  deliveryDetail?: InputMaybe<Null_PropertyFilterInputType>;
  hasTemperatureControl?: InputMaybe<Boolean_PropertyFilterInputType>;
  isRefundable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isReturnable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSampleDelivery?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSensitiveToMoisture?: InputMaybe<Boolean_PropertyFilterInputType>;
  isShippableThroughAir?: InputMaybe<Boolean_PropertyFilterInputType>;
  isVerified?: InputMaybe<Boolean_PropertyFilterInputType>;
  marketCountry?: InputMaybe<Marketcountry_PropertyFilterInputType>;
  marketType?: InputMaybe<Markettype_PropertyFilterInputType>;
  numberOfCounterOffer?: InputMaybe<Int_PropertyFilterInputType>;
  numberOfOrderRequest?: InputMaybe<Int_PropertyFilterInputType>;
  offerEndDate?: InputMaybe<Date_PropertyFilterInputType>;
  offerStartDate?: InputMaybe<Date_PropertyFilterInputType>;
  packageDetail?: InputMaybe<String_PropertyFilterInputType>;
  productId?: InputMaybe<String_PropertyFilterInputType>;
  productName?: InputMaybe<String_PropertyFilterInputType>;
  productTypeCode?: InputMaybe<String_PropertyFilterInputType>;
  status?: InputMaybe<Offerstatus_PropertyFilterInputType>;
  supplierId?: InputMaybe<String_PropertyFilterInputType>;
  tags?: InputMaybe<String_PropertyFilterInputType>;
  warrantyPeriod?: InputMaybe<Int_PropertyFilterInputType>;
  warrantyPeriodType?: InputMaybe<Warrantyperiodtype_PropertyFilterInputType>;
};

export type OfferResolver_FindAll_FilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  additionalDocuments?: InputMaybe<String_PropertyFilterInputType>;
  and?: InputMaybe<Array<OfferResolver_FindAllFilterInputType>>;
  code?: InputMaybe<String_PropertyFilterInputType>;
  composition?: InputMaybe<String_PropertyFilterInputType>;
  createdUserId?: InputMaybe<String_PropertyFilterInputType>;
  deliveryDetail?: InputMaybe<Null_PropertyFilterInputType>;
  hasTemperatureControl?: InputMaybe<Boolean_PropertyFilterInputType>;
  isRefundable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isReturnable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSampleDelivery?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSensitiveToMoisture?: InputMaybe<Boolean_PropertyFilterInputType>;
  isShippableThroughAir?: InputMaybe<Boolean_PropertyFilterInputType>;
  isVerified?: InputMaybe<Boolean_PropertyFilterInputType>;
  marketCountry?: InputMaybe<Marketcountry_PropertyFilterInputType>;
  marketType?: InputMaybe<Markettype_PropertyFilterInputType>;
  numberOfCounterOffer?: InputMaybe<Int_PropertyFilterInputType>;
  numberOfOrderRequest?: InputMaybe<Int_PropertyFilterInputType>;
  offerEndDate?: InputMaybe<Date_PropertyFilterInputType>;
  offerStartDate?: InputMaybe<Date_PropertyFilterInputType>;
  or?: InputMaybe<Array<OfferResolver_FindAllFilterInputType>>;
  packageDetail?: InputMaybe<String_PropertyFilterInputType>;
  productId?: InputMaybe<String_PropertyFilterInputType>;
  productName?: InputMaybe<String_PropertyFilterInputType>;
  productTypeCode?: InputMaybe<String_PropertyFilterInputType>;
  status?: InputMaybe<Offerstatus_PropertyFilterInputType>;
  supplierId?: InputMaybe<String_PropertyFilterInputType>;
  tags?: InputMaybe<String_PropertyFilterInputType>;
  warrantyPeriod?: InputMaybe<Int_PropertyFilterInputType>;
  warrantyPeriodType?: InputMaybe<Warrantyperiodtype_PropertyFilterInputType>;
};

export type OfferResolver_GetOffersProductTypesFilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  additionalDocuments?: InputMaybe<String_PropertyFilterInputType>;
  code?: InputMaybe<String_PropertyFilterInputType>;
  composition?: InputMaybe<String_PropertyFilterInputType>;
  createdUserId?: InputMaybe<String_PropertyFilterInputType>;
  deliveryDetail?: InputMaybe<Null_PropertyFilterInputType>;
  hasTemperatureControl?: InputMaybe<Boolean_PropertyFilterInputType>;
  isRefundable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isReturnable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSampleDelivery?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSensitiveToMoisture?: InputMaybe<Boolean_PropertyFilterInputType>;
  isShippableThroughAir?: InputMaybe<Boolean_PropertyFilterInputType>;
  isVerified?: InputMaybe<Boolean_PropertyFilterInputType>;
  marketCountry?: InputMaybe<Marketcountry_PropertyFilterInputType>;
  marketType?: InputMaybe<Markettype_PropertyFilterInputType>;
  numberOfCounterOffer?: InputMaybe<Int_PropertyFilterInputType>;
  numberOfOrderRequest?: InputMaybe<Int_PropertyFilterInputType>;
  offerEndDate?: InputMaybe<Date_PropertyFilterInputType>;
  offerStartDate?: InputMaybe<Date_PropertyFilterInputType>;
  packageDetail?: InputMaybe<String_PropertyFilterInputType>;
  productId?: InputMaybe<String_PropertyFilterInputType>;
  productName?: InputMaybe<String_PropertyFilterInputType>;
  productTypeCode?: InputMaybe<String_PropertyFilterInputType>;
  status?: InputMaybe<Offerstatus_PropertyFilterInputType>;
  supplierId?: InputMaybe<String_PropertyFilterInputType>;
  tags?: InputMaybe<String_PropertyFilterInputType>;
  warrantyPeriod?: InputMaybe<Int_PropertyFilterInputType>;
  warrantyPeriodType?: InputMaybe<Warrantyperiodtype_PropertyFilterInputType>;
};

export type OfferResolver_GetOffersProductTypes_FilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  additionalDocuments?: InputMaybe<String_PropertyFilterInputType>;
  and?: InputMaybe<Array<OfferResolver_GetOffersProductTypesFilterInputType>>;
  code?: InputMaybe<String_PropertyFilterInputType>;
  composition?: InputMaybe<String_PropertyFilterInputType>;
  createdUserId?: InputMaybe<String_PropertyFilterInputType>;
  deliveryDetail?: InputMaybe<Null_PropertyFilterInputType>;
  hasTemperatureControl?: InputMaybe<Boolean_PropertyFilterInputType>;
  isRefundable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isReturnable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSampleDelivery?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSensitiveToMoisture?: InputMaybe<Boolean_PropertyFilterInputType>;
  isShippableThroughAir?: InputMaybe<Boolean_PropertyFilterInputType>;
  isVerified?: InputMaybe<Boolean_PropertyFilterInputType>;
  marketCountry?: InputMaybe<Marketcountry_PropertyFilterInputType>;
  marketType?: InputMaybe<Markettype_PropertyFilterInputType>;
  numberOfCounterOffer?: InputMaybe<Int_PropertyFilterInputType>;
  numberOfOrderRequest?: InputMaybe<Int_PropertyFilterInputType>;
  offerEndDate?: InputMaybe<Date_PropertyFilterInputType>;
  offerStartDate?: InputMaybe<Date_PropertyFilterInputType>;
  or?: InputMaybe<Array<OfferResolver_GetOffersProductTypesFilterInputType>>;
  packageDetail?: InputMaybe<String_PropertyFilterInputType>;
  productId?: InputMaybe<String_PropertyFilterInputType>;
  productName?: InputMaybe<String_PropertyFilterInputType>;
  productTypeCode?: InputMaybe<String_PropertyFilterInputType>;
  status?: InputMaybe<Offerstatus_PropertyFilterInputType>;
  supplierId?: InputMaybe<String_PropertyFilterInputType>;
  tags?: InputMaybe<String_PropertyFilterInputType>;
  warrantyPeriod?: InputMaybe<Int_PropertyFilterInputType>;
  warrantyPeriodType?: InputMaybe<Warrantyperiodtype_PropertyFilterInputType>;
};

export type OfferResolver_GetProductTypesWithOffersFilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  additionalDocuments?: InputMaybe<String_PropertyFilterInputType>;
  code?: InputMaybe<String_PropertyFilterInputType>;
  composition?: InputMaybe<String_PropertyFilterInputType>;
  createdUserId?: InputMaybe<String_PropertyFilterInputType>;
  deliveryDetail?: InputMaybe<Null_PropertyFilterInputType>;
  hasTemperatureControl?: InputMaybe<Boolean_PropertyFilterInputType>;
  isRefundable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isReturnable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSampleDelivery?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSensitiveToMoisture?: InputMaybe<Boolean_PropertyFilterInputType>;
  isShippableThroughAir?: InputMaybe<Boolean_PropertyFilterInputType>;
  isVerified?: InputMaybe<Boolean_PropertyFilterInputType>;
  marketCountry?: InputMaybe<Marketcountry_PropertyFilterInputType>;
  marketType?: InputMaybe<Markettype_PropertyFilterInputType>;
  numberOfCounterOffer?: InputMaybe<Int_PropertyFilterInputType>;
  numberOfOrderRequest?: InputMaybe<Int_PropertyFilterInputType>;
  offerEndDate?: InputMaybe<Date_PropertyFilterInputType>;
  offerStartDate?: InputMaybe<Date_PropertyFilterInputType>;
  packageDetail?: InputMaybe<String_PropertyFilterInputType>;
  productId?: InputMaybe<String_PropertyFilterInputType>;
  productName?: InputMaybe<String_PropertyFilterInputType>;
  productTypeCode?: InputMaybe<String_PropertyFilterInputType>;
  status?: InputMaybe<Offerstatus_PropertyFilterInputType>;
  supplierId?: InputMaybe<String_PropertyFilterInputType>;
  tags?: InputMaybe<String_PropertyFilterInputType>;
  warrantyPeriod?: InputMaybe<Int_PropertyFilterInputType>;
  warrantyPeriodType?: InputMaybe<Warrantyperiodtype_PropertyFilterInputType>;
};

export type OfferResolver_GetProductTypesWithOffers_FilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  additionalDocuments?: InputMaybe<String_PropertyFilterInputType>;
  and?: InputMaybe<Array<OfferResolver_GetProductTypesWithOffersFilterInputType>>;
  code?: InputMaybe<String_PropertyFilterInputType>;
  composition?: InputMaybe<String_PropertyFilterInputType>;
  createdUserId?: InputMaybe<String_PropertyFilterInputType>;
  deliveryDetail?: InputMaybe<Null_PropertyFilterInputType>;
  hasTemperatureControl?: InputMaybe<Boolean_PropertyFilterInputType>;
  isRefundable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isReturnable?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSampleDelivery?: InputMaybe<Boolean_PropertyFilterInputType>;
  isSensitiveToMoisture?: InputMaybe<Boolean_PropertyFilterInputType>;
  isShippableThroughAir?: InputMaybe<Boolean_PropertyFilterInputType>;
  isVerified?: InputMaybe<Boolean_PropertyFilterInputType>;
  marketCountry?: InputMaybe<Marketcountry_PropertyFilterInputType>;
  marketType?: InputMaybe<Markettype_PropertyFilterInputType>;
  numberOfCounterOffer?: InputMaybe<Int_PropertyFilterInputType>;
  numberOfOrderRequest?: InputMaybe<Int_PropertyFilterInputType>;
  offerEndDate?: InputMaybe<Date_PropertyFilterInputType>;
  offerStartDate?: InputMaybe<Date_PropertyFilterInputType>;
  or?: InputMaybe<Array<OfferResolver_GetProductTypesWithOffersFilterInputType>>;
  packageDetail?: InputMaybe<String_PropertyFilterInputType>;
  productId?: InputMaybe<String_PropertyFilterInputType>;
  productName?: InputMaybe<String_PropertyFilterInputType>;
  productTypeCode?: InputMaybe<String_PropertyFilterInputType>;
  status?: InputMaybe<Offerstatus_PropertyFilterInputType>;
  supplierId?: InputMaybe<String_PropertyFilterInputType>;
  tags?: InputMaybe<String_PropertyFilterInputType>;
  warrantyPeriod?: InputMaybe<Int_PropertyFilterInputType>;
  warrantyPeriodType?: InputMaybe<Warrantyperiodtype_PropertyFilterInputType>;
};

export type OfferVariant = {
  __typename?: 'OfferVariant';
  availableDate: Scalars['DateTime']['output'];
  createdAt: Scalars['DateTime']['output'];
  /** Minimum Order Quantity */
  moq: Scalars['Int']['output'];
  /** Must be lower than product stock quantity */
  quantity: Scalars['Int']['output'];
  quantityRangeLeadTimes: Array<QuantityRangeLeadTime>;
  quantityRangePrices: Array<QuantityRangePrice>;
  updatedAt: Scalars['DateTime']['output'];
  variant?: Maybe<Product_Variant>;
  variantId: Scalars['String']['output'];
};

export type OfferWithCategory = {
  __typename?: 'OfferWithCategory';
  offers: Array<Offers>;
  productType: Product_Types;
};

export type OrderPaginate = {
  __typename?: 'OrderPaginate';
  docs: Array<Order>;
  hasNextPage: Scalars['Boolean']['output'];
  hasPrevPage: Scalars['Boolean']['output'];
  limit: Scalars['Int']['output'];
  nextPage?: Maybe<Scalars['Int']['output']>;
  offset?: Maybe<Scalars['Int']['output']>;
  page?: Maybe<Scalars['Int']['output']>;
  pagingCounter: Scalars['Int']['output'];
  prevPage?: Maybe<Scalars['Int']['output']>;
  totalDocs: Scalars['Int']['output'];
  totalPages: Scalars['Int']['output'];
};

export type OrderResolver_FindAllFilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  buyerId?: InputMaybe<String_PropertyFilterInputType>;
  deliveryMethod?: InputMaybe<Orderdeliverymethod_PropertyFilterInputType>;
  isVerified?: InputMaybe<Boolean_PropertyFilterInputType>;
  offerCode?: InputMaybe<String_PropertyFilterInputType>;
  packageDetail?: InputMaybe<String_PropertyFilterInputType>;
  productName?: InputMaybe<String_PropertyFilterInputType>;
  status?: InputMaybe<Orderstatus_PropertyFilterInputType>;
};

export type OrderResolver_FindAll_FilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  and?: InputMaybe<Array<OrderResolver_FindAllFilterInputType>>;
  buyerId?: InputMaybe<String_PropertyFilterInputType>;
  deliveryMethod?: InputMaybe<Orderdeliverymethod_PropertyFilterInputType>;
  isVerified?: InputMaybe<Boolean_PropertyFilterInputType>;
  offerCode?: InputMaybe<String_PropertyFilterInputType>;
  or?: InputMaybe<Array<OrderResolver_FindAllFilterInputType>>;
  packageDetail?: InputMaybe<String_PropertyFilterInputType>;
  productName?: InputMaybe<String_PropertyFilterInputType>;
  status?: InputMaybe<Orderstatus_PropertyFilterInputType>;
};

export enum Permission_Type {
  Admin = 'admin',
  Default = 'default',
  Super = 'super'
}

export enum Platform_Type {
  BackOffice = 'back_office',
  Buyer = 'buyer',
  Supplier = 'supplier'
}

export enum Product_Type_Log_Type {
  Create = 'create',
  Update = 'update'
}

export type PaginationDto = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
};

export type PaymentType = {
  __typename?: 'PaymentType';
  advance: Scalars['Float']['output'];
  final: Scalars['Float']['output'];
  interim: Scalars['Float']['output'];
};

export type PermissionInput = {
  _id: Scalars['String']['input'];
  /** Код */
  code?: InputMaybe<Scalars['String']['input']>;
  /** Тайлбар */
  description: Scalars['String']['input'];
  /** Групп мөн эсэх */
  isParent: Scalars['Boolean']['input'];
  /** Нэр */
  name: Scalars['String']['input'];
  parentId?: InputMaybe<Scalars['String']['input']>;
  permissionType: Permission_Type;
  /** Platform id */
  platformId: Scalars['String']['input'];
};

export type ProductInput = {
  /** Package id */
  packageId: Scalars['String']['input'];
  /** Product variant id */
  variant?: InputMaybe<Scalars['String']['input']>;
  /** Weight */
  weight?: InputMaybe<Scalars['Float']['input']>;
};

export type ProductKeyAttribute = {
  __typename?: 'ProductKeyAttribute';
  option: Scalars['String']['output'];
  value: Scalars['String']['output'];
};

export type ProductTypeHistory = {
  __typename?: 'ProductTypeHistory';
  createdAt: Scalars['DateTime']['output'];
  /** User who created or updated productType */
  employee: ConsoleUserInfoForHistory;
  /** log type */
  type: Product_Type_Log_Type;
  updatedAt: Scalars['DateTime']['output'];
};

export type ProductTypePaginate = {
  __typename?: 'ProductTypePaginate';
  docs: Array<Product_Types>;
  hasNextPage: Scalars['Boolean']['output'];
  hasPrevPage: Scalars['Boolean']['output'];
  limit: Scalars['Int']['output'];
  nextPage?: Maybe<Scalars['Int']['output']>;
  offset?: Maybe<Scalars['Int']['output']>;
  page?: Maybe<Scalars['Int']['output']>;
  pagingCounter: Scalars['Int']['output'];
  prevPage?: Maybe<Scalars['Int']['output']>;
  totalDocs: Scalars['Int']['output'];
  totalPages: Scalars['Int']['output'];
};

export type ProductTypeResolver_FindAllFilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  code?: InputMaybe<String_PropertyFilterInputType>;
  exampleCustomField?: InputMaybe<String_PropertyFilterInputType>;
  name?: InputMaybe<String_PropertyFilterInputType>;
  parentId?: InputMaybe<String_PropertyFilterInputType>;
  stockLocationAddressId?: InputMaybe<Int_PropertyFilterInputType>;
};

export type ProductTypeResolver_FindAll_FilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  and?: InputMaybe<Array<ProductTypeResolver_FindAllFilterInputType>>;
  code?: InputMaybe<String_PropertyFilterInputType>;
  exampleCustomField?: InputMaybe<String_PropertyFilterInputType>;
  name?: InputMaybe<String_PropertyFilterInputType>;
  or?: InputMaybe<Array<ProductTypeResolver_FindAllFilterInputType>>;
  parentId?: InputMaybe<String_PropertyFilterInputType>;
  stockLocationAddressId?: InputMaybe<Int_PropertyFilterInputType>;
};

export type ProductTypeResolver_FindFilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  code?: InputMaybe<String_PropertyFilterInputType>;
  name?: InputMaybe<String_PropertyFilterInputType>;
  parentId?: InputMaybe<String_PropertyFilterInputType>;
};

export type ProductTypeResolver_Find_FilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  and?: InputMaybe<Array<ProductTypeResolver_FindFilterInputType>>;
  code?: InputMaybe<String_PropertyFilterInputType>;
  name?: InputMaybe<String_PropertyFilterInputType>;
  or?: InputMaybe<Array<ProductTypeResolver_FindFilterInputType>>;
  parentId?: InputMaybe<String_PropertyFilterInputType>;
};

export type QuantityRangeLeadTime = {
  __typename?: 'QuantityRangeLeadTime';
  days: Scalars['Int']['output'];
  max: Scalars['Int']['output'];
  min: Scalars['Int']['output'];
};

export type QuantityRangePrice = {
  __typename?: 'QuantityRangePrice';
  max: Scalars['Int']['output'];
  min: Scalars['Int']['output'];
  price: Scalars['Float']['output'];
};

export type Query = {
  __typename?: 'Query';
  calculateOfferPrice: CalculateOfferPriceOutput;
  certificate: Certificates;
  certificates: Array<Certificates>;
  certification: Certifications;
  certifications: Array<Certifications>;
  checkInvitationToken: Scalars['String']['output'];
  checkpoint: Checkpoints;
  collectionPoint: Collection_Point;
  collectionPoints: Array<Collection_Point>;
  componentCategory: Component_Categories;
  entities: Array<Entities>;
  entity: Entities;
  entityBuyer: Entity_Buyers;
  entityBuyers: Array<Entity_Buyers>;
  entitySupplier: Entity_Suppliers;
  entitySuppliers: Array<Entity_Suppliers>;
  fillUpOrder: Filluporders;
  findByCategoryId: Product_Metas;
  findByOrg: Array<Roles>;
  findByPlatformId: Platforms;
  findOfferDrafts: OfferDraftPaginate;
  findProductTypes: ProductTypePaginate;
  findRoleById: Roles;
  getAllBillingAddresses: Array<BillingAddress>;
  getAllKeyAttributes: Array<KeyAttribute>;
  getAllOffers: Array<Offers>;
  getAllProductMetas: Array<Product_Metas>;
  getAllShippingAddresses: Array<ShippingAddress>;
  getAllSupplierCategories: Array<Supplier_Categories>;
  getBillingAddress: BillingAddress;
  getBillingAddressByBuyerId?: Maybe<Array<BillingAddress>>;
  getCachedPermissions: Array<Scalars['String']['output']>;
  getCapabilitiesList: Array<Capabilities>;
  getCategories: Array<Categories>;
  getCategoriesByPlatformId: Array<Categories>;
  getCategory: Categories;
  getCheckpoints: Array<Checkpoints>;
  getComponent: GetComponentOutput;
  getComponentById: Components;
  getComponentCategories: Array<Component_Categories>;
  getComponentDetail: GetComponentDetailOutput;
  getComponents: Array<Components>;
  /** Хүргэлтийн төрөл төтөх */
  getDeliveryTypes: Array<DeliveryType>;
  getEarTagDetail: Array<GetEarTagDetailOutput>;
  getEmployeesByEntityId: Array<Users>;
  getFillUpOrders: Array<GetFillUpOrdersOutputItem>;
  getFillUpPoints: Array<Checkpoints>;
  getKeyAttribute: KeyAttribute;
  getMPProducts: Array<MpProduct>;
  getOffer: Offers;
  getOffersProductTypes: Array<Product_Types>;
  getPermissionById: Permissions;
  getPermissions: Array<Permissions>;
  getPermissionsByPlatformId: Array<Permissions>;
  getProductMetaById: Product_Metas;
  getProductMetaList: Array<Product_Metas>;
  getProductTypesWithOffers: Array<OfferWithCategory>;
  getReceiveHistory: Array<Receiveorders>;
  getReceiveOrders: Array<GetReceiveOrdersOutputItem>;
  getReceivePoints: Array<Checkpoints>;
  getReceivingEarTagDetail: Array<GetReceivingEarTagDetailOutput>;
  getShippingAddress: ShippingAddress;
  getShippingAddressByBuyerId?: Maybe<Array<ShippingAddress>>;
  getSubFillUpOrders: Array<Filluporders>;
  getSubReceiveOrders: Array<Receiveorders>;
  getSupplierCategory: Supplier_Categories;
  getSupplierEntitiesWithOffer: Array<Entity_Suppliers>;
  /** Хүргэлтийн төрөл төтөх */
  getSupplierType: DeliveryType;
  getTrucks: Array<Trucks>;
  me: Users;
  meg: Megs;
  megs: Array<Megs>;
  mpProduct: MpProduct;
  offer: Offers;
  offerDraft: Offer_Drafts;
  offerDrafts: Array<Offer_Drafts>;
  offers: OfferPaginate;
  offersBy: Array<Offers>;
  order: Order;
  orders: OrderPaginate;
  paymentMethods: Array<Payment_Method>;
  pendingInvite: Pending_Invites;
  pendingInvites: Array<Pending_Invites>;
  platforms: Array<Platforms>;
  port: Port;
  ports: Array<Port>;
  productType: Product_Types;
  productTypes: Array<Product_Types>;
  productTypesByIds: Array<Product_Types>;
  productTypesByParentIds: Array<Product_Types>;
  receiveOrder: Receiveorders;
  roles: Array<Roles>;
  supplierReview: Supplier_Reviews;
  supplierReviewBySupplierId: Supplier_Reviews;
  truck: Trucks;
  user: Users;
  users: Array<Users>;
};


export type QueryCalculateOfferPriceArgs = {
  calculateOfferPriceInput: CalculateOfferPriceInput;
};


export type QueryCertificateArgs = {
  id: Scalars['String']['input'];
};


export type QueryCertificationArgs = {
  id: Scalars['String']['input'];
};


export type QueryCheckInvitationTokenArgs = {
  token: Scalars['String']['input'];
};


export type QueryCheckpointArgs = {
  id: Scalars['Int']['input'];
};


export type QueryCollectionPointArgs = {
  code: Scalars['String']['input'];
};


export type QueryComponentCategoryArgs = {
  id: Scalars['Int']['input'];
};


export type QueryEntityArgs = {
  id: Scalars['String']['input'];
};


export type QueryEntityBuyerArgs = {
  id: Scalars['String']['input'];
};


export type QueryEntitySupplierArgs = {
  id: Scalars['String']['input'];
};


export type QueryFillUpOrderArgs = {
  id: Scalars['Int']['input'];
};


export type QueryFindByCategoryIdArgs = {
  id: Scalars['String']['input'];
};


export type QueryFindByOrgArgs = {
  entityId: Scalars['String']['input'];
  limit: Scalars['Int']['input'];
  skip: Scalars['Int']['input'];
};


export type QueryFindByPlatformIdArgs = {
  id: Scalars['String']['input'];
};


export type QueryFindOfferDraftsArgs = {
  pagination?: InputMaybe<PaginationDto>;
  where?: InputMaybe<OfferDraftResolver_Find_FilterInputType>;
};


export type QueryFindProductTypesArgs = {
  pagination: PaginationDto;
  where?: InputMaybe<ProductTypeResolver_Find_FilterInputType>;
};


export type QueryFindRoleByIdArgs = {
  id: Scalars['String']['input'];
};


export type QueryGetBillingAddressArgs = {
  id: Scalars['String']['input'];
};


export type QueryGetBillingAddressByBuyerIdArgs = {
  buyerId: Scalars['String']['input'];
};


export type QueryGetCachedPermissionsArgs = {
  userId: Scalars['String']['input'];
};


export type QueryGetCategoriesByPlatformIdArgs = {
  platformId: Scalars['String']['input'];
};


export type QueryGetCategoryArgs = {
  id: Scalars['String']['input'];
};


export type QueryGetComponentArgs = {
  GetComponentInput: GetComponentInput;
};


export type QueryGetComponentByIdArgs = {
  id: Scalars['String']['input'];
};


export type QueryGetComponentCategoriesArgs = {
  getComponentCategoriesInput: GetComponentCategoriesInput;
};


export type QueryGetComponentDetailArgs = {
  getComponentDetailInput: GetComponentDetailInput;
};


export type QueryGetComponentsArgs = {
  getComponentsInput: GetComponentsInput;
};


export type QueryGetEarTagDetailArgs = {
  getEarTagDetailInput: GetEarTagDetailInput;
};


export type QueryGetEmployeesByEntityIdArgs = {
  entityId: Scalars['String']['input'];
};


export type QueryGetFillUpOrdersArgs = {
  getFillUpOrdersInput: GetFillUpOrdersInput;
};


export type QueryGetKeyAttributeArgs = {
  _id: Scalars['String']['input'];
};


export type QueryGetOfferArgs = {
  id: Scalars['String']['input'];
};


export type QueryGetOffersProductTypesArgs = {
  where?: InputMaybe<OfferResolver_GetOffersProductTypes_FilterInputType>;
};


export type QueryGetPermissionByIdArgs = {
  id: Scalars['String']['input'];
};


export type QueryGetPermissionsByPlatformIdArgs = {
  platformId: Scalars['String']['input'];
};


export type QueryGetProductMetaByIdArgs = {
  id: Scalars['String']['input'];
};


export type QueryGetProductTypesWithOffersArgs = {
  offerCountPerType?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<OfferResolver_GetProductTypesWithOffers_FilterInputType>;
};


export type QueryGetReceiveHistoryArgs = {
  getReceiveHistoryInput: GetReceiveHistoryInput;
};


export type QueryGetReceiveOrdersArgs = {
  GetReceiveOrdersInput: GetReceiveOrdersInput;
};


export type QueryGetReceivingEarTagDetailArgs = {
  getReceivingEarTagDetailInput: GetReceivingEarTagDetailInput;
};


export type QueryGetShippingAddressArgs = {
  id: Scalars['String']['input'];
};


export type QueryGetShippingAddressByBuyerIdArgs = {
  buyerId: Scalars['String']['input'];
};


export type QueryGetSubFillUpOrdersArgs = {
  getSubFillUpOrdersInput: GetSubFillUpOrdersInput;
};


export type QueryGetSubReceiveOrdersArgs = {
  getSubReceiveOrdersInput: GetSubReceiveOrdersInput;
};


export type QueryGetSupplierCategoryArgs = {
  id: Scalars['String']['input'];
};


export type QueryGetSupplierEntitiesWithOfferArgs = {
  offerCount?: InputMaybe<Scalars['Int']['input']>;
  pagination: PaginationDto;
  query?: InputMaybe<Scalars['String']['input']>;
  where?: InputMaybe<EntitySupplierResolver_GetSupplierEntitiesWithOffer_FilterInputType>;
};


export type QueryGetSupplierTypeArgs = {
  deliveryType: Delivery_Type;
};


export type QueryMegArgs = {
  id: Scalars['Int']['input'];
};


export type QueryMpProductArgs = {
  id: Scalars['Int']['input'];
};


export type QueryOfferArgs = {
  id: Scalars['String']['input'];
};


export type QueryOfferDraftArgs = {
  id: Scalars['String']['input'];
};


export type QueryOffersArgs = {
  pagination: PaginationDto;
  where?: InputMaybe<OfferResolver_FindAll_FilterInputType>;
};


export type QueryOffersByArgs = {
  status: Offer_Status;
};


export type QueryOrderArgs = {
  id: Scalars['String']['input'];
};


export type QueryOrdersArgs = {
  pagination: PaginationDto;
  where?: InputMaybe<OrderResolver_FindAll_FilterInputType>;
};


export type QueryPendingInviteArgs = {
  email: Scalars['String']['input'];
};


export type QueryPortArgs = {
  id: Scalars['Int']['input'];
};


export type QueryProductTypeArgs = {
  id: Scalars['String']['input'];
};


export type QueryProductTypesArgs = {
  where?: InputMaybe<ProductTypeResolver_FindAll_FilterInputType>;
};


export type QueryProductTypesByIdsArgs = {
  ids: Array<Scalars['String']['input']>;
};


export type QueryProductTypesByParentIdsArgs = {
  ids: Array<Scalars['String']['input']>;
};


export type QueryReceiveOrderArgs = {
  id: Scalars['Int']['input'];
};


export type QuerySupplierReviewArgs = {
  id: Scalars['String']['input'];
};


export type QuerySupplierReviewBySupplierIdArgs = {
  supplierId: Scalars['String']['input'];
};


export type QueryTruckArgs = {
  id: Scalars['Int']['input'];
};


export type QueryUserArgs = {
  userId: Scalars['String']['input'];
};

export enum Review_Status {
  Approved = 'approved',
  Declined = 'declined',
  Pending = 'pending',
  Updated = 'updated'
}

export type RequestVerifyResponse = {
  __typename?: 'RequestVerifyResponse';
  email: Scalars['String']['output'];
  isSent: Scalars['Boolean']['output'];
};

export type ResetPasswordInput = {
  /** Хэрэглэгчийн цахим хаяг */
  email: Scalars['String']['input'];
  /** Баталгаажуулах 6 оронтой код */
  otp: Scalars['String']['input'];
  password: Scalars['String']['input'];
};

export type S3Object = {
  __typename?: 'S3Object';
  bucket: Scalars['String']['output'];
  key: Scalars['String']['output'];
  location: Scalars['String']['output'];
};

export type S3ObjectInput = {
  bucket: Scalars['String']['input'];
  key: Scalars['String']['input'];
  location: Scalars['String']['input'];
};

export enum Supplier_Markets {
  Domestic = 'domestic',
  International = 'international'
}

export type ShippingAddress = {
  __typename?: 'ShippingAddress';
  /** Shipping address id */
  _id: Scalars['String']['output'];
  /** Shipping address */
  address: Address;
  /** Buyer entity Id  */
  entityBuyerId: Scalars['String']['output'];
  /** is active Billing address */
  isActive: Scalars['Boolean']['output'];
};

export type SourceComponent = {
  /** Түүхий эд */
  packageId: Scalars['String']['input'];
  /** Жин */
  weight: Scalars['Float']['input'];
};

export type SourceComponentInput = {
  /** Түүхий эд */
  packageId: Scalars['String']['input'];
  /** Жин */
  weight: Scalars['Float']['input'];
};

export type StockLocationInput = {
  address: CreateAddressInput;
  branchName: Scalars['String']['input'];
  phoneNumber: Scalars['String']['input'];
};

export type String_PropertyFilterInputType = {
  eq?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  ne?: InputMaybe<Scalars['String']['input']>;
  nin?: InputMaybe<Array<Scalars['String']['input']>>;
  regex?: InputMaybe<Scalars['String']['input']>;
};

export type SupplierReviewStatusInput = {
  delivery_type_status: Review_Status;
  entity_info_status: Review_Status;
  images_status: Review_Status;
  payment_method_status: Review_Status;
  product_category_status: Review_Status;
  status: Review_Status;
  stock_location_status: Review_Status;
  supplier_id: Scalars['String']['input'];
  supplier_type_status: Review_Status;
  uploaded_document_status: Review_Status;
};

export type SupplierUpdateEntityInfoInput = {
  /** Хаяг */
  address?: InputMaybe<CreateAddressInput>;
  /** Утасны дугаарын урд дугаар (976, 1 гэх мэт) */
  countryCode?: InputMaybe<Scalars['String']['input']>;
  /** Улсын код (MN, US гэх мэт) */
  isoCode?: InputMaybe<Scalars['String']['input']>;
  /** Хэрэглэгчийн (компани) нэр */
  name?: InputMaybe<Scalars['String']['input']>;
  /** Утасны дугаар */
  phoneNumber?: InputMaybe<Scalars['String']['input']>;
};

export type ThirdPartyDelivery = {
  __typename?: 'ThirdPartyDelivery';
  deliveryType: Delivery_Type;
};

export enum Unit_Enum {
  Kilogram = 'kilogram',
  Liter = 'liter',
  Piece = 'piece',
  Tonne = 'tonne'
}

export type UpdateBillingAddressInput = {
  /** Billing address id */
  _id: Scalars['String']['input'];
  /** Billing address */
  address?: InputMaybe<CreateBillingAddressInfoInput>;
  /** Buyer entity Id  */
  entityBuyerId?: InputMaybe<Scalars['String']['input']>;
  /** is active Billing address */
  isActive?: InputMaybe<Scalars['Boolean']['input']>;
};

export type UpdateBuyerUserInput = {
  /** Хэрэглэгчийн нэр (firstName) */
  firstName?: InputMaybe<Scalars['String']['input']>;
  lastName?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateCertificateInput = {
  id: Scalars['String']['input'];
  /** Сэртификатын нэр */
  name?: InputMaybe<Scalars['String']['input']>;
  /** Байгууллага */
  organization?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateCertificationInput = {
  _id: Scalars['String']['input'];
  /** Хавсаргах файлууд (*jpg, *png, * pdf) */
  attachments?: InputMaybe<Array<S3ObjectInput>>;
  expiryDate?: InputMaybe<Scalars['DateTime']['input']>;
  issueDate?: InputMaybe<Scalars['DateTime']['input']>;
  /** Сэртификатын нэр */
  name?: InputMaybe<Scalars['String']['input']>;
  /** Байгууллага */
  organization?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Certification_Status>;
};

export type UpdateCheckpointInput = {
  id: Scalars['String']['input'];
  /** Нэр */
  name?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateCollectionPointInput = {
  /** Хаяг */
  address?: InputMaybe<CreateAddressInput>;
  /** Нэр */
  name?: InputMaybe<Scalars['String']['input']>;
  /** Утасны дугаар */
  phoneNumber?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateComponentCategoryInput = {
  /** Animal type */
  animalType?: InputMaybe<Animal_Type>;
  id: Scalars['String']['input'];
  /** Name */
  name?: InputMaybe<Scalars['String']['input']>;
  /** Хувааж ачих тоо */
  numberOfPart?: InputMaybe<Scalars['Int']['input']>;
  /** Parent component id */
  parentId?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateComponentInput = {
  id: Scalars['String']['input'];
};

export type UpdateDeliveryDetailInput = {
  aimak?: InputMaybe<Scalars['String']['input']>;
  cityProvince?: InputMaybe<Scalars['String']['input']>;
  collectionPointId?: InputMaybe<Scalars['String']['input']>;
  deliveryType: Delivery_Type;
  duuregSoum?: InputMaybe<Scalars['String']['input']>;
  portId?: InputMaybe<Scalars['String']['input']>;
  stockLocationId?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateEntityBuyerInput = {
  buyerType?: InputMaybe<CreateBuyerTypeInput>;
  preferredCategoryIds?: InputMaybe<Array<Scalars['String']['input']>>;
  selectedBillingAddressId?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateEntityInput = {
  /** Хаяг */
  address?: InputMaybe<CreateAddressInput>;
  /** Утасны дугаарын урд дугаар (976, 1 гэх мэт) */
  countryCode?: InputMaybe<Scalars['String']['input']>;
  entitySupplier?: InputMaybe<UpdateEntitySupplierInput>;
  /** Улсын код (MN, US гэх мэт) */
  isoCode?: InputMaybe<Scalars['String']['input']>;
  /** Хэрэглэгчийн (компани) нэр */
  name?: InputMaybe<Scalars['String']['input']>;
  /** Утасны дугаар */
  phoneNumber?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateEntityReviewInput = {
  delivery_type_comment?: InputMaybe<Scalars['String']['input']>;
  delivery_type_status?: InputMaybe<Review_Status>;
  entity_info_comment?: InputMaybe<Scalars['String']['input']>;
  entity_info_status?: InputMaybe<Review_Status>;
  images_comment?: InputMaybe<Scalars['String']['input']>;
  images_status?: InputMaybe<Review_Status>;
  payment_method_comment?: InputMaybe<Scalars['String']['input']>;
  payment_method_status?: InputMaybe<Review_Status>;
  product_category_comment?: InputMaybe<Scalars['String']['input']>;
  product_category_status?: InputMaybe<Review_Status>;
  status?: InputMaybe<Review_Status>;
  stock_location_comment?: InputMaybe<Scalars['String']['input']>;
  stock_location_status?: InputMaybe<Review_Status>;
  supplier_id?: InputMaybe<Scalars['String']['input']>;
  supplier_type_comment?: InputMaybe<Scalars['String']['input']>;
  supplier_type_status?: InputMaybe<Review_Status>;
  uploaded_document_comment?: InputMaybe<Scalars['String']['input']>;
  uploaded_document_status?: InputMaybe<Review_Status>;
};

export type UpdateEntitySupplierCompanyInput = {
  stateRegistrationOrder?: InputMaybe<S3ObjectInput>;
};

export type UpdateEntitySupplierIndividualInput = {
  /** Иргэний үнэмлэхний зураг ар */
  identityBack?: InputMaybe<S3ObjectInput>;
  /** Иргэний үнэмлэхний зураг урд */
  identityFront?: InputMaybe<S3ObjectInput>;
  /** Иргэний үнэмлэхний зураг баталгаа */
  identitySelfie?: InputMaybe<S3ObjectInput>;
};

export type UpdateEntitySupplierInput = {
  certifications?: InputMaybe<Array<UpdateCertificationInput>>;
  /** Cover image */
  coverImages?: InputMaybe<Array<S3ObjectInput>>;
  deliveryDetail?: InputMaybe<UpdateDeliveryDetailInput>;
  entityInfo?: InputMaybe<SupplierUpdateEntityInfoInput>;
  entitySupplierCompany?: InputMaybe<UpdateEntitySupplierCompanyInput>;
  entitySupplierIndividual?: InputMaybe<UpdateEntitySupplierIndividualInput>;
  /** Нүүр зураг */
  image?: InputMaybe<S3ObjectInput>;
  isDomestic?: InputMaybe<Scalars['Boolean']['input']>;
  isInternational?: InputMaybe<Scalars['Boolean']['input']>;
  /** Барааны төрлүүд */
  productTypeIds?: InputMaybe<Array<Scalars['String']['input']>>;
  stockLocations?: InputMaybe<Array<UpdateStockLocationInput>>;
  /** Нийлүүлэгчийн төрөл */
  supplierTypeId?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateFillUpOrderInput = {
  id: Scalars['String']['input'];
  /** Тоо ширхэг */
  quantity?: InputMaybe<Scalars['Int']['input']>;
  /** Татан авалт дууссан огноо */
  transportFinished?: InputMaybe<Scalars['DateTime']['input']>;
  /** Татан авалт эхэлсэн огноо */
  transportStarted?: InputMaybe<Scalars['DateTime']['input']>;
  /** Машины дугаар */
  transportationNumber?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateKeyAttributeInput = {
  _id: Scalars['String']['input'];
  key?: InputMaybe<Scalars['String']['input']>;
  keyType?: InputMaybe<Key_Type>;
  /** Dropdown type options */
  options?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type UpdateMegInput = {
  /** Ээмэгний дугаар */
  earTag?: InputMaybe<Scalars['String']['input']>;
  id: Scalars['Int']['input'];
  /** МЭГ */
  megNumber?: InputMaybe<Scalars['String']['input']>;
  /** МЭГ МП-рүү шивсэн */
  megNumberMP?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateMpProductInput = {
  id: Scalars['Int']['input'];
  /** Package id */
  packageId?: InputMaybe<Scalars['String']['input']>;
  productVariant?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateOfferDraftInput = {
  additionalDocuments?: InputMaybe<Array<Scalars['String']['input']>>;
  composition?: InputMaybe<Scalars['String']['input']>;
  customization?: InputMaybe<CreateOfferCustomizationInput>;
  deliveryDetail?: InputMaybe<CreateDeliveryDetailInput>;
  hasTemperatureControl?: InputMaybe<Scalars['Boolean']['input']>;
  isRefundable?: InputMaybe<Scalars['Boolean']['input']>;
  isReturnable?: InputMaybe<Scalars['Boolean']['input']>;
  isSampleDelivery?: InputMaybe<Scalars['Boolean']['input']>;
  isSensitiveToMoisture?: InputMaybe<Scalars['Boolean']['input']>;
  isShippableThroughAir?: InputMaybe<Scalars['Boolean']['input']>;
  isVerified?: InputMaybe<Scalars['Boolean']['input']>;
  marketCountry?: InputMaybe<Market_Country>;
  marketType?: InputMaybe<Array<Market_Type>>;
  offerEndDate?: InputMaybe<Scalars['DateTime']['input']>;
  offerStartDate?: InputMaybe<Scalars['DateTime']['input']>;
  packageDetail?: InputMaybe<Scalars['String']['input']>;
  paymentMethods?: InputMaybe<Array<CreatePaymentMethodInput>>;
  paymentType?: InputMaybe<CreatePaymentType>;
  productId?: InputMaybe<Scalars['String']['input']>;
  tags?: InputMaybe<Array<Scalars['String']['input']>>;
  variants?: InputMaybe<Array<CreateOfferVariantInput>>;
  warrantyPeriod?: InputMaybe<Scalars['Int']['input']>;
  warrantyPeriodType?: InputMaybe<Warranty_Period_Type>;
};

export type UpdatePendingInviteInput = {
  /** Email */
  email?: InputMaybe<Scalars['String']['input']>;
  entityId?: InputMaybe<Scalars['String']['input']>;
  roleId?: InputMaybe<Scalars['String']['input']>;
};

export type UpdatePermissionInput = {
  code?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  id: Scalars['Int']['input'];
  isParent?: InputMaybe<Scalars['Boolean']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  parentId?: InputMaybe<Scalars['String']['input']>;
  permissionType?: InputMaybe<Permission_Type>;
  platformId?: InputMaybe<Scalars['String']['input']>;
};

export type UpdatePortInput = {
  address?: InputMaybe<CreateAddressInput>;
  name?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateProductMetaInput = {
  /** Product category id */
  categoryId?: InputMaybe<Scalars['String']['input']>;
  id: Scalars['String']['input'];
  /** product meta active status */
  isActive?: InputMaybe<Scalars['Boolean']['input']>;
  /** Product meta name */
  name?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateProductTypeInput = {
  _id: Scalars['String']['input'];
  /** Product type code */
  code?: InputMaybe<Scalars['String']['input']>;
  /** Product type name */
  name?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateReceiveOrderInput = {
  id: Scalars['String']['input'];
  /** Тоо ширхэг */
  quantity?: InputMaybe<Scalars['Int']['input']>;
  /** Түүхий эдийн жин */
  weight?: InputMaybe<Scalars['Float']['input']>;
};

export type UpdateRoleInput = {
  _id: Scalars['String']['input'];
  code?: InputMaybe<Scalars['String']['input']>;
  color?: InputMaybe<Scalars['String']['input']>;
  entityId: Scalars['String']['input'];
  name?: InputMaybe<Scalars['String']['input']>;
  permissionIds?: InputMaybe<Array<Scalars['String']['input']>>;
  status?: InputMaybe<Scalars['Boolean']['input']>;
};

export type UpdateShippingAddressInput = {
  /** Shipping address id */
  _id: Scalars['String']['input'];
  /** Shipping address */
  address?: InputMaybe<CreateAddressInput>;
  /** Buyer entity Id  */
  entityBuyerId?: InputMaybe<Scalars['String']['input']>;
  /** is active Billing address */
  isActive?: InputMaybe<Scalars['Boolean']['input']>;
};

export type UpdateStockLocationInput = {
  _id: Scalars['String']['input'];
  address?: InputMaybe<CreateAddressInput>;
  branchName?: InputMaybe<Scalars['String']['input']>;
  phoneNumber?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateSupplierCategoryInput = {
  /** Supplier category id */
  _id: Scalars['String']['input'];
  /** Нийлүүлэгчийн төрлийн нэр */
  name: Scalars['String']['input'];
};

export type UpdateSupplierUserInput = {
  /** Хэрэглэгчийн нэр (firstName) */
  firstName?: InputMaybe<Scalars['String']['input']>;
  lastName?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateTruckInput = {
  id: Scalars['Int']['input'];
  /** Number plate */
  numberplate?: InputMaybe<Scalars['String']['input']>;
  /** Temperature sensor */
  tempSensor?: InputMaybe<Scalars['String']['input']>;
};

export enum Variant_Enum {
  Color = 'color',
  Size = 'size',
  Type = 'type'
}

export type VerifyEmailInput = {
  /** Хэрэглэгчийн цахим хаяг */
  email: Scalars['String']['input'];
  /** Баталгаажуулах 6 оронтой код */
  otp: Scalars['String']['input'];
};

export type Warrantyperiodtype_PropertyFilterInputType = {
  eq?: InputMaybe<Warranty_Period_Type>;
  in?: InputMaybe<Array<Warranty_Period_Type>>;
  ne?: InputMaybe<Warranty_Period_Type>;
  nin?: InputMaybe<Array<Warranty_Period_Type>>;
};

export enum Warranty_Period_Type {
  Days = 'days',
  Months = 'months',
  Years = 'years'
}

export type BuyerUsers = {
  __typename?: 'buyerUsers';
  _id: Scalars['String']['output'];
  /** Утасны дугаарын урд дугаар (976, 1 гэх мэт) */
  countryCode: Scalars['String']['output'];
  /** Хэрэглэгчийн нэр (firstName) */
  firstName: Scalars['String']['output'];
  lastName: Scalars['String']['output'];
};

export type Capabilities = {
  __typename?: 'capabilities';
  /** код */
  code: Scalars['String']['output'];
  filterData?: Maybe<Array<FilterDataType>>;
  /** нэр */
  name: Scalars['String']['output'];
};

export type Categories = {
  __typename?: 'categories';
  _id: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  name: Scalars['String']['output'];
  permissionIds: Array<Scalars['String']['output']>;
  permissions?: Maybe<Array<Permissions>>;
  platformId: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
};

export type Certificates = {
  __typename?: 'certificates';
  _id: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  /** Сэртификатын нэр */
  name: Scalars['String']['output'];
  /** Байгууллага */
  organization: Scalars['String']['output'];
  standart?: Maybe<Scalars['String']['output']>;
  updatedAt: Scalars['DateTime']['output'];
};

export type Certifications = {
  __typename?: 'certifications';
  _id: Scalars['String']['output'];
  attachments: Array<S3Object>;
  createdAt: Scalars['DateTime']['output'];
  entityId?: Maybe<Scalars['String']['output']>;
  expiryDate: Scalars['DateTime']['output'];
  isSpecial: Scalars['Boolean']['output'];
  issueDate: Scalars['DateTime']['output'];
  /** Сэртификатын нэр */
  name: Scalars['String']['output'];
  /** Байгууллага */
  organization: Scalars['String']['output'];
  status: Certification_Status;
  updatedAt: Scalars['DateTime']['output'];
};

export type Checkpoints = {
  __typename?: 'checkpoints';
  _id: Scalars['String']['output'];
  /** Код */
  code: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  /** Нэр */
  name: Scalars['String']['output'];
  /** Төрөл */
  type: Check_Point_Type;
  updatedAt: Scalars['DateTime']['output'];
};

export type Collection_Point = {
  __typename?: 'collection_point';
  _id: Scalars['String']['output'];
  address: Address;
  code: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  name: Scalars['String']['output'];
  phoneNumber: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
};

export type Component_Categories = {
  __typename?: 'component_categories';
  _id: Scalars['String']['output'];
  /** Animal type */
  animalType: Animal_Type;
  /** Code */
  code: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  /** Processing id */
  level?: Maybe<Scalars['Int']['output']>;
  /** Name */
  name: Scalars['String']['output'];
  /** Хувааж ачих тоо */
  numberOfPart?: Maybe<Scalars['Int']['output']>;
  /** Parent component id */
  parentId?: Maybe<Scalars['String']['output']>;
  /** Processing id */
  processing?: Maybe<Scalars['String']['output']>;
  updatedAt: Scalars['DateTime']['output'];
};

export type Components = {
  __typename?: 'components';
  _id: Scalars['String']['output'];
  /** Түүхий эдийн ангилал */
  categoryId: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  /** Бохир жин */
  grossWeight: Scalars['Float']['output'];
  /** Багц эсэх */
  isGroup: Scalars['Boolean']['output'];
  /** Багцлагдсан эсэх */
  isGrouped: Scalars['Boolean']['output'];
  /** Үе шат */
  level: Scalars['Int']['output'];
  /** Байршил */
  locationId?: Maybe<Scalars['String']['output']>;
  /** Нэр */
  name: Scalars['String']['output'];
  /** Гарал үүсэл */
  originId?: Maybe<Scalars['String']['output']>;
  /** Гарал үүслүүд */
  origins?: Maybe<Array<Scalars['String']['output']>>;
  /** Савлагааны дугаар */
  packageId: Scalars['String']['output'];
  /** Эцэг түүхий эд */
  parentId?: Maybe<Scalars['String']['output']>;
  /** Боловсруулалт */
  processingId?: Maybe<Scalars['String']['output']>;
  /** Багцад орсон түүхий эдүүд */
  sourceComponents: Array<Source_Components>;
  /** Төлөв */
  status: Scalars['String']['output'];
  /** Төрөл */
  type: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
  /** Жин */
  weight: Scalars['Float']['output'];
};

export type ConsoleUserInfoForHistory = {
  __typename?: 'consoleUserInfoForHistory';
  _id: Scalars['String']['output'];
  /** Хэрэглэгчийн нэр (firstName) */
  firstName: Scalars['String']['output'];
  lastName: Scalars['String']['output'];
};

export type Entities = {
  __typename?: 'entities';
  _id: Scalars['String']['output'];
  /** Хаяг */
  address: Address;
  /** Үйл ажиллагааны чиглэл */
  areasOfActivity: Scalars['String']['output'];
  /** Утасны дугаарын урд дугаар (976, 1 гэх мэт) */
  countryCode: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  createdUser?: Maybe<Users>;
  /** Entity-г үүсгэсэн хэрэглэгч */
  createdUserId: Scalars['String']['output'];
  entityBuyer?: Maybe<Entity_Buyers>;
  entityBuyerId?: Maybe<Scalars['String']['output']>;
  entityCompany?: Maybe<Entity_Companies>;
  entityCompanyId?: Maybe<Scalars['String']['output']>;
  entitySupplier?: Maybe<Entity_Suppliers>;
  entitySupplierId?: Maybe<Scalars['String']['output']>;
  /** Улсын код (MN, US гэх мэт) */
  isoCode: Scalars['String']['output'];
  name: Scalars['String']['output'];
  phoneNumber: Scalars['String']['output'];
  registrationNumber: Scalars['String']['output'];
  type: Entity_Type;
  updatedAt: Scalars['DateTime']['output'];
};

export type Entity_Buyers = {
  __typename?: 'entity_buyers';
  _id: Scalars['String']['output'];
  buyerType: BuyerType;
  code: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  entity?: Maybe<Entities>;
  entityId: Scalars['String']['output'];
  preferredCategories?: Maybe<Array<Product_Types>>;
  preferredCategoryIds: Array<Scalars['String']['output']>;
  selectedBillingAddress?: Maybe<BillingAddress>;
  selectedBillingAddressId?: Maybe<Scalars['String']['output']>;
  shippingAddress?: Maybe<Array<ShippingAddress>>;
  updatedAt: Scalars['DateTime']['output'];
};

export type Entity_Companies = {
  __typename?: 'entity_companies';
  _id: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  /** Description */
  description: Scalars['String']['output'];
  entityId: Scalars['String']['output'];
  incorporatedDate: Scalars['DateTime']['output'];
  numberOfEmployees?: Maybe<Scalars['Int']['output']>;
  updatedAt: Scalars['DateTime']['output'];
};

export type Entity_Supplier_Companies = {
  __typename?: 'entity_supplier_companies';
  _id: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  entitySupplierId: Scalars['String']['output'];
  /** Улсын бүртгэлийн гэрчилгээ */
  stateRegistrationOrder?: Maybe<S3Object>;
  updatedAt: Scalars['DateTime']['output'];
};

export type Entity_Supplier_Individuals = {
  __typename?: 'entity_supplier_individuals';
  _id: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  entitySupplierId: Scalars['String']['output'];
  /** Иргэний үнэмлэхний зураг (ар) */
  identityBack?: Maybe<S3Object>;
  /** Иргэний үнэмлэхний зураг (урд) */
  identityFront?: Maybe<S3Object>;
  /** Үнэмлэх батлах зураг */
  identitySelfie?: Maybe<S3Object>;
  updatedAt: Scalars['DateTime']['output'];
};

export type Entity_Suppliers = {
  __typename?: 'entity_suppliers';
  _id: Scalars['String']['output'];
  /** Хавсаргасан бичиг баримтууд */
  certificationIds: Array<Scalars['String']['output']>;
  certifications?: Maybe<Array<Certifications>>;
  coverImages: Array<S3Object>;
  createdAt: Scalars['DateTime']['output'];
  deliveryDetail?: Maybe<DeliveryDetailUnion>;
  entity?: Maybe<Entities>;
  entityId: Scalars['String']['output'];
  entitySupplierCompany?: Maybe<Entity_Supplier_Companies>;
  entitySupplierCompanyId?: Maybe<Scalars['String']['output']>;
  entitySupplierIndividual?: Maybe<Entity_Supplier_Individuals>;
  entitySupplierIndividualId?: Maybe<Scalars['String']['output']>;
  image: S3Object;
  isDomestic: Scalars['Boolean']['output'];
  isInternational: Scalars['Boolean']['output'];
  offers?: Maybe<Array<Offers>>;
  /** Барааны төрлүүд */
  productTypeIds: Array<Scalars['String']['output']>;
  productTypes?: Maybe<Array<Product_Types>>;
  review?: Maybe<Supplier_Reviews>;
  reviewId?: Maybe<Scalars['String']['output']>;
  /** Төлөв (хүлээгдэж буй, зөвшөөрөгдсөн, цуцлагдсан) */
  status: Entity_Status;
  /** Бараа татах байршлууд */
  stockLocationIds: Array<Scalars['String']['output']>;
  stockLocations?: Maybe<Array<Stock_Locations>>;
  supplierType?: Maybe<Supplier_Categories>;
  /** Нийлүүлэгчийн төрөл */
  supplierTypeId: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
};

export type Filluporders = {
  __typename?: 'filluporders';
  _id: Scalars['String']['output'];
  animalType?: Maybe<Scalars['String']['output']>;
  /** Захиалгын дугаар */
  code: Scalars['String']['output'];
  /** Түүхий эд */
  componentCategory?: Maybe<Scalars['String']['output']>;
  /** Түүхий эдийн нэр */
  componentCategoryName?: Maybe<Scalars['String']['output']>;
  createdAt: Scalars['DateTime']['output'];
  /** Ээмэгний дугаар */
  earTag?: Maybe<Scalars['String']['output']>;
  /** Эзэмшигч */
  holder: Scalars['String']['output'];
  /** Эзэмшигчийн имэйл */
  holderEmail?: Maybe<Scalars['String']['output']>;
  /** Үндсэн захиалга эсэх */
  isMain: Scalars['Boolean']['output'];
  /** Тоо ширхэгийн дээд хэмжээ */
  maxQty?: Maybe<Scalars['Int']['output']>;
  /** Үндсэн захиалгын дугаар */
  parentCode?: Maybe<Scalars['String']['output']>;
  /** Татан авах цэг */
  pointOfFillUp: Scalars['String']['output'];
  /** Татан авах цэгийн нэр */
  pointOfFillUpName: Scalars['String']['output'];
  /** Тоо ширхэг */
  quantity?: Maybe<Scalars['Int']['output']>;
  /** Төлөв */
  status: Fill_Up_Order_Status;
  /** Тээвэрлэсэн температур */
  temperature?: Maybe<Scalars['Float']['output']>;
  /** Татан авалт дууссан огноо */
  transportFinished?: Maybe<Scalars['DateTime']['output']>;
  /** Татан авалт эхэлсэн огноо */
  transportStarted?: Maybe<Scalars['DateTime']['output']>;
  /** Машины дугаар */
  transportationNumber: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
};

export type Megs = {
  __typename?: 'megs';
  createdAt: Scalars['DateTime']['output'];
  /** Үүсгэсэн хэрэглэгч */
  createdUser: Scalars['String']['output'];
  /** Ээмэгний дугаар */
  earTag: Scalars['String']['output'];
  /** МЭГ */
  megNumber: Scalars['String']['output'];
  /** МЭГ МП-рүү шивсэн */
  megNumberMP: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
};

export type Null_PropertyFilterInputType = {
  eq?: InputMaybe<Entity_Status>;
  in?: InputMaybe<Array<Entity_Status>>;
  ne?: InputMaybe<Entity_Status>;
  nin?: InputMaybe<Array<Entity_Status>>;
};

export type Offer_Drafts = {
  __typename?: 'offer_drafts';
  _id: Scalars['String']['output'];
  additionalDocuments?: Maybe<Array<Scalars['String']['output']>>;
  composition?: Maybe<Scalars['String']['output']>;
  createdAt: Scalars['DateTime']['output'];
  createdUser?: Maybe<Users>;
  createdUserId: Scalars['String']['output'];
  customization?: Maybe<OfferCustomization>;
  deliveryDetail?: Maybe<DeliveryDetailUnion>;
  hasTemperatureControl?: Maybe<Scalars['Boolean']['output']>;
  isRefundable: Scalars['Boolean']['output'];
  isReturnable?: Maybe<Scalars['Boolean']['output']>;
  isSampleDelivery?: Maybe<Scalars['Boolean']['output']>;
  isSensitiveToMoisture?: Maybe<Scalars['Boolean']['output']>;
  isShippableThroughAir?: Maybe<Scalars['Boolean']['output']>;
  isVerified?: Maybe<Scalars['Boolean']['output']>;
  marketCountry?: Maybe<Market_Country>;
  marketType?: Maybe<Array<Market_Type>>;
  offerEndDate?: Maybe<Scalars['DateTime']['output']>;
  offerStartDate?: Maybe<Scalars['DateTime']['output']>;
  packageDetail?: Maybe<Scalars['String']['output']>;
  paymentMethods?: Maybe<Array<OfferPaymentMethod>>;
  paymentType?: Maybe<PaymentType>;
  productId?: Maybe<Scalars['String']['output']>;
  productName?: Maybe<Scalars['String']['output']>;
  productTypeCode?: Maybe<Scalars['String']['output']>;
  supplier?: Maybe<Entity_Suppliers>;
  supplierId: Scalars['String']['output'];
  tags?: Maybe<Array<Scalars['String']['output']>>;
  updatedAt: Scalars['DateTime']['output'];
  variants?: Maybe<Array<OfferVariant>>;
  warrantyPeriod?: Maybe<Scalars['Int']['output']>;
  warrantyPeriodType?: Maybe<Warranty_Period_Type>;
};

export type Offers = {
  __typename?: 'offers';
  _id: Scalars['String']['output'];
  additionalDocuments: Array<Scalars['String']['output']>;
  code: Scalars['String']['output'];
  composition?: Maybe<Scalars['String']['output']>;
  createdAt: Scalars['DateTime']['output'];
  createdUser?: Maybe<Users>;
  createdUserId: Scalars['String']['output'];
  customization?: Maybe<OfferCustomization>;
  deliveryDetail: DeliveryDetailUnion;
  hasTemperatureControl: Scalars['Boolean']['output'];
  isRefundable: Scalars['Boolean']['output'];
  isReturnable: Scalars['Boolean']['output'];
  isSampleDelivery: Scalars['Boolean']['output'];
  isSensitiveToMoisture: Scalars['Boolean']['output'];
  isShippableThroughAir: Scalars['Boolean']['output'];
  isVerified: Scalars['Boolean']['output'];
  marketCountry?: Maybe<Market_Country>;
  marketType: Array<Market_Type>;
  maxPrice: Scalars['Float']['output'];
  minPrice: Scalars['Float']['output'];
  moq: Scalars['Int']['output'];
  numberOfCounterOffer: Scalars['Int']['output'];
  numberOfOrderRequest: Scalars['Int']['output'];
  offerEndDate: Scalars['DateTime']['output'];
  offerStartDate: Scalars['DateTime']['output'];
  packageDetail?: Maybe<Scalars['String']['output']>;
  paymentMethods: Array<OfferPaymentMethod>;
  paymentType: PaymentType;
  product?: Maybe<Product>;
  productId: Scalars['String']['output'];
  productName: Scalars['String']['output'];
  productType?: Maybe<Product_Types>;
  productTypeCode: Scalars['String']['output'];
  status: Offer_Status;
  supplier?: Maybe<Entity_Suppliers>;
  supplierId: Scalars['String']['output'];
  tags: Array<Scalars['String']['output']>;
  updatedAt: Scalars['DateTime']['output'];
  variants: Array<OfferVariant>;
  warrantyPeriod: Scalars['Int']['output'];
  warrantyPeriodType?: Maybe<Warranty_Period_Type>;
};

export type Order = {
  __typename?: 'order';
  _id: Scalars['String']['output'];
  buyer?: Maybe<Entity_Buyers>;
  buyerId: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  deliveryMethod: Order_Delivery_Method;
  isVerified: Scalars['Boolean']['output'];
  offerCode: Scalars['String']['output'];
  packageDetail?: Maybe<Scalars['String']['output']>;
  productImage: S3Object;
  productName: Scalars['String']['output'];
  status: Order_Status;
  updatedAt: Scalars['DateTime']['output'];
};

export type Payment_Method = {
  __typename?: 'payment_method';
  _id: Scalars['String']['output'];
  acountNumber: Scalars['Int']['output'];
  icon: Scalars['String']['output'];
  name: Scalars['String']['output'];
};

export type Pending_Invites = {
  __typename?: 'pending_invites';
  _id: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  /** Email */
  email: Scalars['String']['output'];
  entityId: Scalars['String']['output'];
  roleId: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
  user?: Maybe<Users>;
};

export type Permissions = {
  __typename?: 'permissions';
  _id: Scalars['String']['output'];
  /** Код */
  code?: Maybe<Scalars['String']['output']>;
  createdAt: Scalars['DateTime']['output'];
  /** Тайлбар */
  description: Scalars['String']['output'];
  /** Групп мөн эсэх */
  isParent: Scalars['Boolean']['output'];
  /** Нэр */
  name: Scalars['String']['output'];
  parentId?: Maybe<Scalars['String']['output']>;
  permissionType: Permission_Type;
  /** Platform id */
  platformId: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
};

export type Platforms = {
  __typename?: 'platforms';
  /** Platform id */
  _id: Scalars['String']['output'];
  /** Platform code */
  code: Platform_Type;
  createdAt: Scalars['DateTime']['output'];
  /** Platform name */
  name: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
};

export type Port = {
  __typename?: 'port';
  _id: Scalars['String']['output'];
  address: Address;
  createdAt: Scalars['DateTime']['output'];
  name: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
};

export type Product = {
  __typename?: 'product';
  _id: Scalars['String']['output'];
  createdAt: Scalars['String']['output'];
  description: Scalars['String']['output'];
  images: Array<Scalars['String']['output']>;
  /** Product Key Attributes */
  keyAttributes?: Maybe<Array<ProductKeyAttribute>>;
  media: Scalars['String']['output'];
  meta?: Maybe<Product_Metas>;
  metaId?: Maybe<Scalars['String']['output']>;
  name: Scalars['String']['output'];
  updatedAt: Scalars['String']['output'];
  variants?: Maybe<Array<Product_Variant>>;
  weightUnit?: Maybe<Scalars['String']['output']>;
};

export type Product_Metas = {
  __typename?: 'product_metas';
  /** Product meta id */
  _id: Scalars['String']['output'];
  /** Product meta attributes */
  attributes: Array<Variant_Enum>;
  category?: Maybe<Product_Types>;
  /** Category id */
  categoryId: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  /** product meta active status */
  isActive: Scalars['Boolean']['output'];
  /** key attributes */
  keyAttributeIds?: Maybe<Array<Scalars['String']['output']>>;
  keyAttributes?: Maybe<Array<KeyAttribute>>;
  /** Meta name */
  name: Scalars['String']['output'];
  /** Product meta unit */
  unit: Unit_Enum;
  /** Product meta unitVolume */
  unitVolume: Scalars['Float']['output'];
  /** Product meta unitWeight */
  unitWeight: Scalars['Float']['output'];
  updatedAt: Scalars['DateTime']['output'];
};

export type Product_Types = {
  __typename?: 'product_types';
  _id: Scalars['String']['output'];
  children?: Maybe<Array<Product_Types>>;
  /** Product type code */
  code: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  history?: Maybe<Array<ProductTypeHistory>>;
  /** Product type name */
  name: Scalars['String']['output'];
  parent?: Maybe<Product_Types>;
  parentId?: Maybe<Scalars['String']['output']>;
  updatedAt: Scalars['DateTime']['output'];
};

export type Product_Variant = {
  __typename?: 'product_variant';
  _id: Scalars['String']['output'];
  /** Attributes */
  attributes?: Maybe<Array<Attribute>>;
  /** Variant certificate of origin */
  certificatesOfOrigin?: Maybe<Array<Scalars['String']['output']>>;
  createdAt: Scalars['String']['output'];
  /** Variant images */
  images?: Maybe<Array<Scalars['String']['output']>>;
  /** Variant quantity */
  placeOfOrigin?: Maybe<Scalars['String']['output']>;
  /** Variant price */
  price: Scalars['Float']['output'];
  /** productId */
  productId: Scalars['String']['output'];
  updatedAt: Scalars['String']['output'];
};

export type Receiveorders = {
  __typename?: 'receiveorders';
  _id: Scalars['String']['output'];
  animalType?: Maybe<Scalars['String']['output']>;
  /** Захиалгын дугаар */
  code: Scalars['String']['output'];
  /** Түүхий эд */
  componentCategory?: Maybe<Scalars['String']['output']>;
  /** Түүхий эдийн нэр */
  componentCategoryName?: Maybe<Scalars['String']['output']>;
  createdAt: Scalars['DateTime']['output'];
  /** Ээмэгний дугаар */
  earTag?: Maybe<Scalars['String']['output']>;
  /** Ээмэгний дугаарын төрөл */
  earTagType?: Maybe<Scalars['String']['output']>;
  /** Үндсэн захиалга эсэх */
  isMain: Scalars['Boolean']['output'];
  /** Тоо ширхэгийн дээд хэмжээ */
  maxQty?: Maybe<Scalars['Int']['output']>;
  /** Үндсэн захиалгын дугаар */
  parentCode?: Maybe<Scalars['String']['output']>;
  /** Татан авах цэг */
  pointOfFillUp: Scalars['String']['output'];
  /** Татан авах цэгийн нэр */
  pointOfFillUpName: Scalars['String']['output'];
  /** Хүлээн авах цэг */
  pointOfReceive: Scalars['String']['output'];
  /** Хүлээн авах цэгийн нэр */
  pointOfReceiveName: Scalars['String']['output'];
  /** Цуглуулсан түүхий эдийн тоо */
  quantity?: Maybe<Scalars['Int']['output']>;
  /** Төлөв */
  status: Scalars['String']['output'];
  /** Нийт жин */
  totalWeight?: Maybe<Scalars['Int']['output']>;
  /** Машины дугаар */
  transportationNumber?: Maybe<Scalars['String']['output']>;
  updatedAt: Scalars['DateTime']['output'];
};

export type Roles = {
  __typename?: 'roles';
  _id: Scalars['String']['output'];
  /** Ролийн код */
  code: Scalars['String']['output'];
  /** Өнгө */
  color: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  /** Байгууллагын ID */
  entityId: Scalars['String']['output'];
  /** Ролийн нэр */
  name: Scalars['String']['output'];
  permissionIds: Array<Scalars['String']['output']>;
  permissions?: Maybe<Array<Permissions>>;
  /** Идэвхтэй эсэх */
  status: Scalars['Boolean']['output'];
  updatedAt: Scalars['DateTime']['output'];
};

export type Source_Components = {
  __typename?: 'source_components';
  /** Түүхий эд */
  packageId: Scalars['String']['output'];
  /** Жин */
  weight: Scalars['Float']['output'];
};

export type Stock_Locations = {
  __typename?: 'stock_locations';
  _id: Scalars['String']['output'];
  address: Address;
  branchName: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  phoneNumber: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
};

export type SupplierUsers = {
  __typename?: 'supplierUsers';
  _id: Scalars['String']['output'];
  /** Хэрэглэгчийн нэр (firstName) */
  firstName: Scalars['String']['output'];
  lastName: Scalars['String']['output'];
};

export type Supplier_Categories = {
  __typename?: 'supplier_categories';
  /** Supplier category id */
  _id: Scalars['String']['output'];
  /** Нийлүүлэгчийн төрлийн код */
  code: Scalars['String']['output'];
  createdUser?: Maybe<ConsoleUserInfoForHistory>;
  /** Үүсгэсэн ажилтан */
  createdUserId: Scalars['String']['output'];
  /** Нийлүүлэгчийн төрлийн нэр */
  name: Scalars['String']['output'];
  updatedUser?: Maybe<ConsoleUserInfoForHistory>;
  /** Шинэчилсэн ажилтан */
  updatedUserId?: Maybe<Scalars['String']['output']>;
};

export type Supplier_Reviews = {
  __typename?: 'supplier_reviews';
  _id: Scalars['String']['output'];
  certificationIds?: Maybe<Array<Scalars['String']['output']>>;
  createdAt: Scalars['DateTime']['output'];
  delivery_type_comment?: Maybe<Scalars['String']['output']>;
  delivery_type_status: Review_Status;
  entity_info_comment?: Maybe<Scalars['String']['output']>;
  entity_info_status: Review_Status;
  images_comment?: Maybe<Scalars['String']['output']>;
  images_status: Review_Status;
  payment_method_comment?: Maybe<Scalars['String']['output']>;
  payment_method_status: Review_Status;
  product_category_comment?: Maybe<Scalars['String']['output']>;
  product_category_status: Review_Status;
  status: Review_Status;
  stock_location_comment?: Maybe<Scalars['String']['output']>;
  stock_location_status: Review_Status;
  supplier_id: Scalars['String']['output'];
  supplier_type_comment?: Maybe<Scalars['String']['output']>;
  supplier_type_status: Review_Status;
  updatedAt: Scalars['DateTime']['output'];
  uploaded_document_comment?: Maybe<Scalars['String']['output']>;
  uploaded_document_status: Review_Status;
};

export type Trucks = {
  __typename?: 'trucks';
  _id: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  /** Number plate */
  numberplate: Scalars['String']['output'];
  /** Temperature sensor */
  tempSensor?: Maybe<Scalars['String']['output']>;
  updatedAt: Scalars['DateTime']['output'];
};

export type Users = {
  __typename?: 'users';
  _id: Scalars['String']['output'];
  /** Утасны дугаарын урд дугаар (976, 1 гэх мэт) */
  countryCode: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  createdEntities?: Maybe<Array<Entities>>;
  createdEntityIds: Array<Scalars['String']['output']>;
  currentEntity?: Maybe<Entities>;
  currentEntityId?: Maybe<Scalars['String']['output']>;
  currentPlatformId: Scalars['String']['output'];
  /** Email */
  email: Scalars['String']['output'];
  entities?: Maybe<Array<Entities>>;
  entityIds: Array<Scalars['String']['output']>;
  /** Хэрэглэгчийн нэр (firstName) */
  firstName: Scalars['String']['output'];
  image?: Maybe<Scalars['String']['output']>;
  isEmailVerified?: Maybe<Scalars['Boolean']['output']>;
  /** Улсын код (MN, US гэх мэт) */
  isoCode: Scalars['String']['output'];
  lastName: Scalars['String']['output'];
  phoneNumber: Scalars['String']['output'];
  /** Refresh Token Field */
  refreshToken?: Maybe<Scalars['String']['output']>;
  roleIds: Array<Scalars['String']['output']>;
  roles?: Maybe<Array<Roles>>;
  updatedAt: Scalars['DateTime']['output'];
  userRoles: Roles;
};

export type LoginMutationVariables = Exact<{
  loginUserInput: LoginUserInput;
}>;


export type LoginMutation = { __typename?: 'Mutation', login: { __typename?: 'LoginResponse', access_token: string, refresh_token: string, user: { __typename?: 'LoginResponseUser', _id: string, firstName: string, email: string, currentEntityId?: string | null, entityIds: Array<string>, isEmailVerified?: boolean | null } } };

export type SignupMutationVariables = Exact<{
  signupUserInput: CreateUserInput;
}>;


export type SignupMutation = { __typename?: 'Mutation', signup: { __typename?: 'LoginResponse', access_token: string, refresh_token: string, user: { __typename?: 'LoginResponseUser', _id: string, firstName: string, email: string, currentEntityId?: string | null, entityIds: Array<string> } } };

export type RequestVerifyEmailMutationVariables = Exact<{
  email: Scalars['String']['input'];
}>;


export type RequestVerifyEmailMutation = { __typename?: 'Mutation', requestVerifyEmail: { __typename?: 'RequestVerifyResponse', email: string, isSent: boolean } };

export type VerifyEmailMutationVariables = Exact<{
  verifyEmailInput: VerifyEmailInput;
}>;


export type VerifyEmailMutation = { __typename?: 'Mutation', verifyEmail: { __typename?: 'users', _id: string } };

export type UsersQueryVariables = Exact<{ [key: string]: never; }>;


export type UsersQuery = { __typename?: 'Query', users: Array<{ __typename?: 'users', _id: string, email: string }> };

export type CreateCertificationMutationVariables = Exact<{
  createCertificationInput: CreateCertificationInput;
}>;


export type CreateCertificationMutation = { __typename?: 'Mutation', createCertification: { __typename?: 'certifications', _id: string, organization: string, name: string, issueDate: any, expiryDate: any, status: Certification_Status, isSpecial: boolean, entityId?: string | null, attachments: Array<{ __typename?: 'S3Object', location: string, bucket: string, key: string }> } };

export type CreateEntitySupplierMutationMutationVariables = Exact<{
  createEntitySupplierInput: CreateEntitySupplierInput;
  entityId: Scalars['String']['input'];
  entityType: Entity_Type;
}>;


export type CreateEntitySupplierMutationMutation = { __typename?: 'Mutation', createEntitySupplier: { __typename?: 'entity_suppliers', _id: string, entityId: string, stockLocationIds: Array<string>, productTypeIds: Array<string>, certificationIds: Array<string>, status: Entity_Status, isDomestic: boolean, isInternational: boolean, supplierType?: { __typename?: 'supplier_categories', _id: string, code: string, name: string } | null, image: { __typename?: 'S3Object', location: string, bucket: string, key: string }, coverImages: Array<{ __typename?: 'S3Object', location: string, bucket: string, key: string }> } };

export type CreateEntityMutationVariables = Exact<{
  createEntityInput: CreateEntityInput;
}>;


export type CreateEntityMutation = { __typename?: 'Mutation', createEntity: { __typename?: 'entities', _id: string } };

export type CreateOfferMutationVariables = Exact<{
  createOfferInput: CreateOfferInput;
}>;


export type CreateOfferMutation = { __typename?: 'Mutation', createOffer: { __typename?: 'offers', _id: string } };

export type CreateOfferDraftMutationVariables = Exact<{
  createOfferDraftInput: CreateOfferDraftInput;
}>;


export type CreateOfferDraftMutation = { __typename?: 'Mutation', createOfferDraft: { __typename?: 'offer_drafts', _id: string } };

export type CreateStockLocationsMutationVariables = Exact<{
  createStockLocationsInput: Array<StockLocationInput> | StockLocationInput;
}>;


export type CreateStockLocationsMutation = { __typename?: 'Mutation', createStockLocations: Array<{ __typename?: 'stock_locations', _id: string }> };

export type UpdateEntitySupplierMutationVariables = Exact<{
  updateEntitySupplierId: Scalars['String']['input'];
  updateEntitySupplierInput: UpdateEntitySupplierInput;
}>;


export type UpdateEntitySupplierMutation = { __typename?: 'Mutation', updateEntitySupplier: { __typename?: 'entity_suppliers', _id: string } };

export type UpdateStockLocationMutationVariables = Exact<{
  updateStockLocationsInput: UpdateStockLocationInput;
}>;


export type UpdateStockLocationMutation = { __typename?: 'Mutation', updateStockLocation: { __typename?: 'stock_locations', _id: string } };

export type GetAllCategoriesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetAllCategoriesQuery = { __typename?: 'Query', productTypes: Array<{ __typename?: 'product_types', _id: string, parentId?: string | null, name: string, code: string, createdAt: any, updatedAt: any }> };

export type GetAllSupplierCategoriesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetAllSupplierCategoriesQuery = { __typename?: 'Query', getAllSupplierCategories: Array<{ __typename?: 'supplier_categories', _id: string, code: string, name: string }> };

export type GetCertificatesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCertificatesQuery = { __typename?: 'Query', certificates: Array<{ __typename?: 'certificates', _id: string, name: string, organization: string }> };

export type GetCollectionPointsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCollectionPointsQuery = { __typename?: 'Query', collectionPoints: Array<{ __typename?: 'collection_point', _id: string, code: string, name: string }> };

export type GetEntityByIdQueryVariables = Exact<{
  entityId: Scalars['String']['input'];
}>;


export type GetEntityByIdQuery = { __typename?: 'Query', entity: { __typename?: 'entities', _id: string, entitySupplierId?: string | null, areasOfActivity: string, countryCode: string, createdAt: any, createdUserId: string, entityBuyerId?: string | null, entityCompanyId?: string | null, isoCode: string, name: string, phoneNumber: string, registrationNumber: string, type: Entity_Type, updatedAt: any, entitySupplier?: { __typename?: 'entity_suppliers', _id: string, certificationIds: Array<string>, createdAt: any, entitySupplierCompanyId?: string | null, entitySupplierIndividualId?: string | null, isDomestic: boolean, isInternational: boolean, productTypeIds: Array<string>, reviewId?: string | null, status: Entity_Status, stockLocationIds: Array<string>, updatedAt: any, coverImages: Array<{ __typename?: 'S3Object', bucket: string, key: string, location: string }>, deliveryDetail?: { __typename?: 'DeliverToAimak', aimak: string, collectionPointId: string, deliveryType: Delivery_Type } | { __typename?: 'DeliverToLocalPort', cityProvince: string, deliveryType: Delivery_Type, duuregSoum: string, portId: string } | { __typename?: 'FullDelivery', deliveryType: Delivery_Type, stockLocationId: string } | { __typename?: 'ThirdPartyDelivery', deliveryType: Delivery_Type } | null, entitySupplierCompany?: { __typename?: 'entity_supplier_companies', _id: string, stateRegistrationOrder?: { __typename?: 'S3Object', bucket: string, key: string, location: string } | null } | null, entitySupplierIndividual?: { __typename?: 'entity_supplier_individuals', _id: string, createdAt: any, entitySupplierId: string, updatedAt: any, identityBack?: { __typename?: 'S3Object', bucket: string, key: string, location: string } | null, identityFront?: { __typename?: 'S3Object', bucket: string, key: string, location: string } | null, identitySelfie?: { __typename?: 'S3Object', bucket: string, key: string, location: string } | null } | null, image: { __typename?: 'S3Object', bucket: string, key: string, location: string }, productTypes?: Array<{ __typename?: 'product_types', _id: string, code: string, createdAt: any, name: string, parentId?: string | null, updatedAt: any }> | null, stockLocations?: Array<{ __typename?: 'stock_locations', _id: string, branchName: string, createdAt: any, phoneNumber: string, updatedAt: any, address: { __typename?: 'Address', detail: string, cityProvince: { __typename?: 'AddressInfo', code: number, name: string, nameMn?: string | null }, coordinates: { __typename?: 'Location', latitude: number, longtitude: number }, duuregSoum: { __typename?: 'AddressInfo', code: number, name: string, nameMn?: string | null }, khorooBag: { __typename?: 'AddressInfo', code: number, name: string, nameMn?: string | null } } }> | null, supplierType?: { __typename?: 'supplier_categories', _id: string, code: string, name: string } | null } | null, address: { __typename?: 'Address', detail: string, duuregSoum: { __typename?: 'AddressInfo', code: number, name: string, nameMn?: string | null }, coordinates: { __typename?: 'Location', longtitude: number, latitude: number }, cityProvince: { __typename?: 'AddressInfo', code: number, name: string, nameMn?: string | null }, khorooBag: { __typename?: 'AddressInfo', code: number, nameMn?: string | null, name: string } }, createdUser?: { __typename?: 'users', _id: string, countryCode: string, createdAt: any, email: string, firstName: string, image?: string | null, isEmailVerified?: boolean | null, isoCode: string, lastName: string, phoneNumber: string, updatedAt: any } | null, entityCompany?: { __typename?: 'entity_companies', _id: string, incorporatedDate: any, numberOfEmployees?: number | null, description: string, createdAt: any, updatedAt: any } | null } };

export type GetEntitySupplierByIdQueryVariables = Exact<{
  entitySupplierId: Scalars['String']['input'];
}>;


export type GetEntitySupplierByIdQuery = { __typename?: 'Query', entitySupplier: { __typename?: 'entity_suppliers', _id: string, createdAt: any, entityId: string, entitySupplierCompanyId?: string | null, entitySupplierIndividualId?: string | null, isDomestic: boolean, isInternational: boolean, productTypeIds: Array<string>, reviewId?: string | null, status: Entity_Status, stockLocationIds: Array<string>, updatedAt: any, certifications?: Array<{ __typename?: 'certifications', _id: string, createdAt: any, entityId?: string | null, expiryDate: any, isSpecial: boolean, issueDate: any, name: string, organization: string, status: Certification_Status, updatedAt: any, attachments: Array<{ __typename?: 'S3Object', bucket: string, key: string, location: string }> }> | null, coverImages: Array<{ __typename?: 'S3Object', bucket: string, key: string, location: string }>, deliveryDetail?: { __typename?: 'DeliverToAimak', aimak: string, collectionPointId: string, deliveryType: Delivery_Type } | { __typename?: 'DeliverToLocalPort', cityProvince: string, deliveryType: Delivery_Type, duuregSoum: string, portId: string } | { __typename?: 'FullDelivery', deliveryType: Delivery_Type, stockLocationId: string } | { __typename?: 'ThirdPartyDelivery', deliveryType: Delivery_Type } | null, entity?: { __typename?: 'entities', _id: string, areasOfActivity: string, countryCode: string, createdAt: any, createdUserId: string, entityBuyerId?: string | null, entityCompanyId?: string | null, isoCode: string, name: string, phoneNumber: string, registrationNumber: string, type: Entity_Type, updatedAt: any, address: { __typename?: 'Address', detail: string, cityProvince: { __typename?: 'AddressInfo', code: number, name: string, nameMn?: string | null }, coordinates: { __typename?: 'Location', latitude: number, longtitude: number }, duuregSoum: { __typename?: 'AddressInfo', code: number, name: string, nameMn?: string | null }, khorooBag: { __typename?: 'AddressInfo', code: number, name: string, nameMn?: string | null } }, createdUser?: { __typename?: 'users', _id: string, countryCode: string, createdAt: any, email: string, firstName: string, image?: string | null, isEmailVerified?: boolean | null, isoCode: string, lastName: string, phoneNumber: string, updatedAt: any } | null, entityCompany?: { __typename?: 'entity_companies', _id: string, incorporatedDate: any, numberOfEmployees?: number | null, description: string, createdAt: any, updatedAt: any } | null } | null, entitySupplierCompany?: { __typename?: 'entity_supplier_companies', _id: string, stateRegistrationOrder?: { __typename?: 'S3Object', bucket: string, key: string, location: string } | null } | null, entitySupplierIndividual?: { __typename?: 'entity_supplier_individuals', _id: string, createdAt: any, entitySupplierId: string, updatedAt: any, identityBack?: { __typename?: 'S3Object', bucket: string, key: string, location: string } | null, identityFront?: { __typename?: 'S3Object', bucket: string, key: string, location: string } | null, identitySelfie?: { __typename?: 'S3Object', bucket: string, key: string, location: string } | null } | null, image: { __typename?: 'S3Object', bucket: string, key: string, location: string }, productTypes?: Array<{ __typename?: 'product_types', _id: string, code: string, createdAt: any, name: string, parentId?: string | null, updatedAt: any }> | null, stockLocations?: Array<{ __typename?: 'stock_locations', _id: string, branchName: string, createdAt: any, phoneNumber: string, updatedAt: any, address: { __typename?: 'Address', detail: string, cityProvince: { __typename?: 'AddressInfo', code: number, name: string, nameMn?: string | null }, coordinates: { __typename?: 'Location', latitude: number, longtitude: number }, duuregSoum: { __typename?: 'AddressInfo', code: number, name: string, nameMn?: string | null }, khorooBag: { __typename?: 'AddressInfo', code: number, name: string, nameMn?: string | null } } }> | null, supplierType?: { __typename?: 'supplier_categories', _id: string, code: string, name: string } | null } };

export type GetMyOfferDraftsQueryVariables = Exact<{
  where?: InputMaybe<OfferDraftResolver_Find_FilterInputType>;
}>;


export type GetMyOfferDraftsQuery = { __typename?: 'Query', findOfferDrafts: { __typename?: 'OfferDraftPaginate', docs: Array<{ __typename?: 'offer_drafts', _id: string, productName?: string | null, offerStartDate?: any | null, offerEndDate?: any | null, warrantyPeriod?: number | null, warrantyPeriodType?: Warranty_Period_Type | null, createdAt: any, variants?: Array<{ __typename?: 'OfferVariant', moq: number, variantId: string, quantityRangePrices: Array<{ __typename?: 'QuantityRangePrice', max: number, min: number, price: number }> }> | null }> } };

export type GetMyOffersQueryVariables = Exact<{
  pagination: PaginationDto;
  where?: InputMaybe<OfferResolver_FindAll_FilterInputType>;
}>;


export type GetMyOffersQuery = { __typename?: 'Query', offers: { __typename?: 'OfferPaginate', docs: Array<{ __typename?: 'offers', _id: string, createdAt: any, productName: string, offerStartDate: any, offerEndDate: any, code: string, variants: Array<{ __typename?: 'OfferVariant', availableDate: any, moq: number, quantityRangePrices: Array<{ __typename?: 'QuantityRangePrice', min: number, max: number, price: number }> }> }> } };

export type GetOfferDraftByIdQueryVariables = Exact<{
  offerDraftId: Scalars['String']['input'];
}>;


export type GetOfferDraftByIdQuery = { __typename?: 'Query', offerDraft: { __typename?: 'offer_drafts', _id: string, additionalDocuments?: Array<string> | null, composition?: string | null, createdUserId: string, hasTemperatureControl?: boolean | null, isRefundable: boolean, isReturnable?: boolean | null, isSampleDelivery?: boolean | null, isSensitiveToMoisture?: boolean | null, isShippableThroughAir?: boolean | null, marketCountry?: Market_Country | null, isVerified?: boolean | null, marketType?: Array<Market_Type> | null, offerEndDate?: any | null, offerStartDate?: any | null, packageDetail?: string | null, productId?: string | null, productName?: string | null, supplierId: string, tags?: Array<string> | null, warrantyPeriod?: number | null, warrantyPeriodType?: Warranty_Period_Type | null, deliveryDetail?: { __typename?: 'DeliverToAimak', aimak: string, collectionPointId: string, deliveryType: Delivery_Type } | { __typename?: 'DeliverToLocalPort', cityProvince: string, deliveryType: Delivery_Type, duuregSoum: string, portId: string } | { __typename?: 'FullDelivery', deliveryType: Delivery_Type, stockLocationId: string } | { __typename?: 'ThirdPartyDelivery', deliveryType: Delivery_Type } | null, customization?: { __typename?: 'OfferCustomization', color?: { __typename?: 'OfferCustomizationOption', name: string, moq: number } | null, custom?: Array<{ __typename?: 'OfferCustomizationOption', moq: number, name: string }> | null, logo?: { __typename?: 'OfferCustomizationOption', name: string, moq: number } | null, package?: { __typename?: 'OfferCustomizationOption', moq: number, name: string } | null } | null, paymentMethods?: Array<{ __typename?: 'OfferPaymentMethod', acountNumber: number, icon: string, name: string }> | null, paymentType?: { __typename?: 'PaymentType', advance: number, final: number, interim: number } | null, variants?: Array<{ __typename?: 'OfferVariant', availableDate: any, moq: number, quantity: number, variantId: string, quantityRangeLeadTimes: Array<{ __typename?: 'QuantityRangeLeadTime', min: number, max: number, days: number }>, quantityRangePrices: Array<{ __typename?: 'QuantityRangePrice', max: number, min: number, price: number }> }> | null } };

export type GetPortsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetPortsQuery = { __typename?: 'Query', ports: Array<{ __typename?: 'port', _id: string, name: string }> };

export type GetProductMetaListQueryVariables = Exact<{ [key: string]: never; }>;


export type GetProductMetaListQuery = { __typename?: 'Query', getProductMetaList: Array<{ __typename?: 'product_metas', _id: string, attributes: Array<Variant_Enum>, categoryId: string, createdAt: any, isActive: boolean, name: string, unit: Unit_Enum, unitVolume: number, unitWeight: number, updatedAt: any }> };

export type GetSupplierReviewByIdQueryVariables = Exact<{
  supplierReviewId: Scalars['String']['input'];
}>;


export type GetSupplierReviewByIdQuery = { __typename?: 'Query', supplierReview: { __typename?: 'supplier_reviews', _id: string, certificationIds?: Array<string> | null, createdAt: any, delivery_type_comment?: string | null, delivery_type_status: Review_Status, entity_info_comment?: string | null, entity_info_status: Review_Status, images_comment?: string | null, images_status: Review_Status, product_category_comment?: string | null, product_category_status: Review_Status, payment_method_status: Review_Status, payment_method_comment?: string | null, status: Review_Status, stock_location_comment?: string | null, stock_location_status: Review_Status, supplier_id: string, supplier_type_comment?: string | null, supplier_type_status: Review_Status, updatedAt: any, uploaded_document_comment?: string | null, uploaded_document_status: Review_Status } };

export type SubmitSupplierReviewMutationVariables = Exact<{
  markets: Array<Supplier_Markets> | Supplier_Markets;
  supplierId: Scalars['String']['input'];
}>;


export type SubmitSupplierReviewMutation = { __typename?: 'Mutation', submitSupplier: { __typename?: 'entity_suppliers', _id: string } };

export type UpdateSupplierReviewMutationVariables = Exact<{
  updateSupplierReviewId: Scalars['String']['input'];
  updateSupplierReviewInput: UpdateEntityReviewInput;
}>;


export type UpdateSupplierReviewMutation = { __typename?: 'Mutation', updateSupplierReview: { __typename?: 'supplier_reviews', _id: string, certificationIds?: Array<string> | null, createdAt: any, delivery_type_comment?: string | null, delivery_type_status: Review_Status, entity_info_comment?: string | null, entity_info_status: Review_Status, images_comment?: string | null, images_status: Review_Status, payment_method_comment?: string | null, payment_method_status: Review_Status, product_category_comment?: string | null, product_category_status: Review_Status, status: Review_Status, stock_location_comment?: string | null, stock_location_status: Review_Status, supplier_id: string, supplier_type_comment?: string | null, supplier_type_status: Review_Status, updatedAt: any, uploaded_document_comment?: string | null, uploaded_document_status: Review_Status } };

export type UserByIdQueryVariables = Exact<{
  userId: Scalars['String']['input'];
}>;


export type UserByIdQuery = { __typename?: 'Query', user: { __typename?: 'users', _id: string, createdAt: any, createdEntityIds: Array<string>, currentEntityId?: string | null, currentPlatformId: string, email: string, firstName: string, image?: string | null, lastName: string, phoneNumber: string, refreshToken?: string | null, isEmailVerified?: boolean | null, entityIds: Array<string>, createdEntities?: Array<{ __typename?: 'entities', _id: string, name: string, phoneNumber: string, registrationNumber: string, type: Entity_Type, entityCompanyId?: string | null, entitySupplierId?: string | null, entityBuyerId?: string | null, updatedAt: any }> | null, currentEntity?: { __typename?: 'entities', name: string, phoneNumber: string, registrationNumber: string, type: Entity_Type, createdUserId: string, entityBuyerId?: string | null, entityCompanyId?: string | null, entitySupplierId?: string | null, _id: string, entitySupplier?: { __typename?: 'entity_suppliers', certificationIds: Array<string>, createdAt: any, entitySupplierCompanyId?: string | null, entitySupplierIndividualId?: string | null, isDomestic: boolean, isInternational: boolean, productTypeIds: Array<string>, reviewId?: string | null, status: Entity_Status, stockLocationIds: Array<string>, updatedAt: any, coverImages: Array<{ __typename?: 'S3Object', bucket: string, key: string, location: string }>, deliveryDetail?: { __typename?: 'DeliverToAimak', aimak: string, collectionPointId: string, deliveryType: Delivery_Type } | { __typename?: 'DeliverToLocalPort', cityProvince: string, deliveryType: Delivery_Type, duuregSoum: string, portId: string } | { __typename?: 'FullDelivery', deliveryType: Delivery_Type, stockLocationId: string } | { __typename?: 'ThirdPartyDelivery', deliveryType: Delivery_Type } | null, entitySupplierCompany?: { __typename?: 'entity_supplier_companies', _id: string, stateRegistrationOrder?: { __typename?: 'S3Object', bucket: string, key: string, location: string } | null } | null, entitySupplierIndividual?: { __typename?: 'entity_supplier_individuals', _id: string, createdAt: any, entitySupplierId: string, updatedAt: any, identityBack?: { __typename?: 'S3Object', bucket: string, key: string, location: string } | null, identityFront?: { __typename?: 'S3Object', bucket: string, key: string, location: string } | null, identitySelfie?: { __typename?: 'S3Object', bucket: string, key: string, location: string } | null } | null, image: { __typename?: 'S3Object', bucket: string, key: string, location: string }, productTypes?: Array<{ __typename?: 'product_types', _id: string, code: string, createdAt: any, name: string, parentId?: string | null, updatedAt: any }> | null, stockLocations?: Array<{ __typename?: 'stock_locations', _id: string, branchName: string, createdAt: any, phoneNumber: string, updatedAt: any, address: { __typename?: 'Address', detail: string, cityProvince: { __typename?: 'AddressInfo', code: number, name: string, nameMn?: string | null }, coordinates: { __typename?: 'Location', latitude: number, longtitude: number }, duuregSoum: { __typename?: 'AddressInfo', code: number, name: string, nameMn?: string | null }, khorooBag: { __typename?: 'AddressInfo', code: number, name: string, nameMn?: string | null } } }> | null, supplierType?: { __typename?: 'supplier_categories', _id: string, code: string, name: string } | null } | null } | null } };


export const LoginDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"Login"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"loginUserInput"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"LoginUserInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"login"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"loginUserInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"loginUserInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"access_token"}},{"kind":"Field","name":{"kind":"Name","value":"refresh_token"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"firstName"}},{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"currentEntityId"}},{"kind":"Field","name":{"kind":"Name","value":"entityIds"}},{"kind":"Field","name":{"kind":"Name","value":"isEmailVerified"}}]}}]}}]}}]} as unknown as DocumentNode<LoginMutation, LoginMutationVariables>;
export const SignupDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"Signup"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"signupUserInput"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"CreateUserInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"signup"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"signupUserInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"signupUserInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"access_token"}},{"kind":"Field","name":{"kind":"Name","value":"refresh_token"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"firstName"}},{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"currentEntityId"}},{"kind":"Field","name":{"kind":"Name","value":"entityIds"}}]}}]}}]}}]} as unknown as DocumentNode<SignupMutation, SignupMutationVariables>;
export const RequestVerifyEmailDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"RequestVerifyEmail"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"email"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"requestVerifyEmail"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"email"},"value":{"kind":"Variable","name":{"kind":"Name","value":"email"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"isSent"}}]}}]}}]} as unknown as DocumentNode<RequestVerifyEmailMutation, RequestVerifyEmailMutationVariables>;
export const VerifyEmailDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"VerifyEmail"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"verifyEmailInput"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"VerifyEmailInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"verifyEmail"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"verifyEmailInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"verifyEmailInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}}]}}]}}]} as unknown as DocumentNode<VerifyEmailMutation, VerifyEmailMutationVariables>;
export const UsersDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"Users"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"users"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"email"}}]}}]}}]} as unknown as DocumentNode<UsersQuery, UsersQueryVariables>;
export const CreateCertificationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CreateCertification"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"createCertificationInput"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"CreateCertificationInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createCertification"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"createCertificationInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"createCertificationInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"organization"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"issueDate"}},{"kind":"Field","name":{"kind":"Name","value":"expiryDate"}},{"kind":"Field","name":{"kind":"Name","value":"attachments"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"location"}},{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}}]}},{"kind":"Field","name":{"kind":"Name","value":"status"}},{"kind":"Field","name":{"kind":"Name","value":"isSpecial"}},{"kind":"Field","name":{"kind":"Name","value":"entityId"}}]}}]}}]} as unknown as DocumentNode<CreateCertificationMutation, CreateCertificationMutationVariables>;
export const CreateEntitySupplierMutationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CreateEntitySupplierMutation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"createEntitySupplierInput"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"CreateEntitySupplierInput"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"entityId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"entityType"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ENTITY_TYPE"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createEntitySupplier"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"createEntitySupplierInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"createEntitySupplierInput"}}},{"kind":"Argument","name":{"kind":"Name","value":"entityId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"entityId"}}},{"kind":"Argument","name":{"kind":"Name","value":"entityType"},"value":{"kind":"Variable","name":{"kind":"Name","value":"entityType"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"entityId"}},{"kind":"Field","name":{"kind":"Name","value":"stockLocationIds"}},{"kind":"Field","name":{"kind":"Name","value":"productTypeIds"}},{"kind":"Field","name":{"kind":"Name","value":"certificationIds"}},{"kind":"Field","name":{"kind":"Name","value":"supplierType"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"status"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"location"}},{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}}]}},{"kind":"Field","name":{"kind":"Name","value":"coverImages"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"location"}},{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}}]}},{"kind":"Field","name":{"kind":"Name","value":"isDomestic"}},{"kind":"Field","name":{"kind":"Name","value":"isInternational"}}]}}]}}]} as unknown as DocumentNode<CreateEntitySupplierMutationMutation, CreateEntitySupplierMutationMutationVariables>;
export const CreateEntityDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CreateEntity"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"createEntityInput"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"CreateEntityInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createEntity"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"createEntityInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"createEntityInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}}]}}]}}]} as unknown as DocumentNode<CreateEntityMutation, CreateEntityMutationVariables>;
export const CreateOfferDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CreateOffer"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"createOfferInput"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"CreateOfferInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createOffer"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"createOfferInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"createOfferInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}}]}}]}}]} as unknown as DocumentNode<CreateOfferMutation, CreateOfferMutationVariables>;
export const CreateOfferDraftDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CreateOfferDraft"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"createOfferDraftInput"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"CreateOfferDraftInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createOfferDraft"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"createOfferDraftInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"createOfferDraftInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}}]}}]}}]} as unknown as DocumentNode<CreateOfferDraftMutation, CreateOfferDraftMutationVariables>;
export const CreateStockLocationsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CreateStockLocations"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"createStockLocationsInput"}},"type":{"kind":"NonNullType","type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"StockLocationInput"}}}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createStockLocations"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"createStockLocationsInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"createStockLocationsInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}}]}}]}}]} as unknown as DocumentNode<CreateStockLocationsMutation, CreateStockLocationsMutationVariables>;
export const UpdateEntitySupplierDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateEntitySupplier"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"updateEntitySupplierId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"updateEntitySupplierInput"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"UpdateEntitySupplierInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateEntitySupplier"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"updateEntitySupplierId"}}},{"kind":"Argument","name":{"kind":"Name","value":"updateEntitySupplierInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"updateEntitySupplierInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}}]}}]}}]} as unknown as DocumentNode<UpdateEntitySupplierMutation, UpdateEntitySupplierMutationVariables>;
export const UpdateStockLocationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateStockLocation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"updateStockLocationsInput"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"UpdateStockLocationInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateStockLocation"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"updateStockLocationsInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"updateStockLocationsInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}}]}}]}}]} as unknown as DocumentNode<UpdateStockLocationMutation, UpdateStockLocationMutationVariables>;
export const GetAllCategoriesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetAllCategories"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"productTypes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"parentId"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}}]}}]} as unknown as DocumentNode<GetAllCategoriesQuery, GetAllCategoriesQueryVariables>;
export const GetAllSupplierCategoriesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetAllSupplierCategories"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"getAllSupplierCategories"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]} as unknown as DocumentNode<GetAllSupplierCategoriesQuery, GetAllSupplierCategoriesQueryVariables>;
export const GetCertificatesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetCertificates"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"certificates"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"organization"}}]}}]}}]} as unknown as DocumentNode<GetCertificatesQuery, GetCertificatesQueryVariables>;
export const GetCollectionPointsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetCollectionPoints"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"collectionPoints"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]} as unknown as DocumentNode<GetCollectionPointsQuery, GetCollectionPointsQueryVariables>;
export const GetEntityByIdDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetEntityById"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"entityId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"entity"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"entityId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierId"}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplier"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"certificationIds"}},{"kind":"Field","name":{"kind":"Name","value":"coverImages"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"deliveryDetail"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"DeliverToAimak"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"aimak"}},{"kind":"Field","name":{"kind":"Name","value":"collectionPointId"}},{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"DeliverToLocalPort"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"cityProvince"}},{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}},{"kind":"Field","name":{"kind":"Name","value":"duuregSoum"}},{"kind":"Field","name":{"kind":"Name","value":"portId"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"FullDelivery"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}},{"kind":"Field","name":{"kind":"Name","value":"stockLocationId"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ThirdPartyDelivery"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierCompany"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"stateRegistrationOrder"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierCompanyId"}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierIndividual"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierId"}},{"kind":"Field","name":{"kind":"Name","value":"identityBack"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"identityFront"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"identitySelfie"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierIndividualId"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"isDomestic"}},{"kind":"Field","name":{"kind":"Name","value":"isInternational"}},{"kind":"Field","name":{"kind":"Name","value":"productTypeIds"}},{"kind":"Field","name":{"kind":"Name","value":"productTypes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"parentId"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"reviewId"}},{"kind":"Field","name":{"kind":"Name","value":"status"}},{"kind":"Field","name":{"kind":"Name","value":"stockLocationIds"}},{"kind":"Field","name":{"kind":"Name","value":"stockLocations"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"address"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"cityProvince"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}}]}},{"kind":"Field","name":{"kind":"Name","value":"coordinates"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"latitude"}},{"kind":"Field","name":{"kind":"Name","value":"longtitude"}}]}},{"kind":"Field","name":{"kind":"Name","value":"detail"}},{"kind":"Field","name":{"kind":"Name","value":"duuregSoum"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}}]}},{"kind":"Field","name":{"kind":"Name","value":"khorooBag"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"branchName"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"phoneNumber"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"supplierType"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"address"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"duuregSoum"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}}]}},{"kind":"Field","name":{"kind":"Name","value":"detail"}},{"kind":"Field","name":{"kind":"Name","value":"coordinates"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"longtitude"}},{"kind":"Field","name":{"kind":"Name","value":"latitude"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cityProvince"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}}]}},{"kind":"Field","name":{"kind":"Name","value":"khorooBag"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"areasOfActivity"}},{"kind":"Field","name":{"kind":"Name","value":"countryCode"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"createdUser"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"countryCode"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"firstName"}},{"kind":"Field","name":{"kind":"Name","value":"image"}},{"kind":"Field","name":{"kind":"Name","value":"isEmailVerified"}},{"kind":"Field","name":{"kind":"Name","value":"isoCode"}},{"kind":"Field","name":{"kind":"Name","value":"lastName"}},{"kind":"Field","name":{"kind":"Name","value":"phoneNumber"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"createdUserId"}},{"kind":"Field","name":{"kind":"Name","value":"entityBuyerId"}},{"kind":"Field","name":{"kind":"Name","value":"entityCompany"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"incorporatedDate"}},{"kind":"Field","name":{"kind":"Name","value":"numberOfEmployees"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"entityCompanyId"}},{"kind":"Field","name":{"kind":"Name","value":"isoCode"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"phoneNumber"}},{"kind":"Field","name":{"kind":"Name","value":"registrationNumber"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}}]}}]} as unknown as DocumentNode<GetEntityByIdQuery, GetEntityByIdQueryVariables>;
export const GetEntitySupplierByIdDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetEntitySupplierById"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"entitySupplierId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"entitySupplier"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"entitySupplierId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"certifications"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"attachments"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"entityId"}},{"kind":"Field","name":{"kind":"Name","value":"expiryDate"}},{"kind":"Field","name":{"kind":"Name","value":"isSpecial"}},{"kind":"Field","name":{"kind":"Name","value":"issueDate"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"organization"}},{"kind":"Field","name":{"kind":"Name","value":"status"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"coverImages"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"deliveryDetail"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"DeliverToAimak"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"aimak"}},{"kind":"Field","name":{"kind":"Name","value":"collectionPointId"}},{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"DeliverToLocalPort"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"cityProvince"}},{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}},{"kind":"Field","name":{"kind":"Name","value":"duuregSoum"}},{"kind":"Field","name":{"kind":"Name","value":"portId"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"FullDelivery"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}},{"kind":"Field","name":{"kind":"Name","value":"stockLocationId"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ThirdPartyDelivery"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"entity"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"address"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"cityProvince"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}}]}},{"kind":"Field","name":{"kind":"Name","value":"coordinates"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"latitude"}},{"kind":"Field","name":{"kind":"Name","value":"longtitude"}}]}},{"kind":"Field","name":{"kind":"Name","value":"detail"}},{"kind":"Field","name":{"kind":"Name","value":"duuregSoum"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}}]}},{"kind":"Field","name":{"kind":"Name","value":"khorooBag"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"areasOfActivity"}},{"kind":"Field","name":{"kind":"Name","value":"countryCode"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"createdUser"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"countryCode"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"firstName"}},{"kind":"Field","name":{"kind":"Name","value":"image"}},{"kind":"Field","name":{"kind":"Name","value":"isEmailVerified"}},{"kind":"Field","name":{"kind":"Name","value":"isoCode"}},{"kind":"Field","name":{"kind":"Name","value":"lastName"}},{"kind":"Field","name":{"kind":"Name","value":"phoneNumber"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"createdUserId"}},{"kind":"Field","name":{"kind":"Name","value":"entityBuyerId"}},{"kind":"Field","name":{"kind":"Name","value":"entityCompany"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"incorporatedDate"}},{"kind":"Field","name":{"kind":"Name","value":"numberOfEmployees"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"entityCompanyId"}},{"kind":"Field","name":{"kind":"Name","value":"isoCode"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"phoneNumber"}},{"kind":"Field","name":{"kind":"Name","value":"registrationNumber"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"entityId"}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierCompany"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"stateRegistrationOrder"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierCompanyId"}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierIndividual"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierId"}},{"kind":"Field","name":{"kind":"Name","value":"identityBack"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"identityFront"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"identitySelfie"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierIndividualId"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"isDomestic"}},{"kind":"Field","name":{"kind":"Name","value":"isInternational"}},{"kind":"Field","name":{"kind":"Name","value":"productTypeIds"}},{"kind":"Field","name":{"kind":"Name","value":"productTypes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"parentId"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"reviewId"}},{"kind":"Field","name":{"kind":"Name","value":"status"}},{"kind":"Field","name":{"kind":"Name","value":"stockLocationIds"}},{"kind":"Field","name":{"kind":"Name","value":"stockLocations"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"address"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"cityProvince"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}}]}},{"kind":"Field","name":{"kind":"Name","value":"coordinates"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"latitude"}},{"kind":"Field","name":{"kind":"Name","value":"longtitude"}}]}},{"kind":"Field","name":{"kind":"Name","value":"detail"}},{"kind":"Field","name":{"kind":"Name","value":"duuregSoum"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}}]}},{"kind":"Field","name":{"kind":"Name","value":"khorooBag"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"branchName"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"phoneNumber"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"supplierType"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}}]}}]} as unknown as DocumentNode<GetEntitySupplierByIdQuery, GetEntitySupplierByIdQueryVariables>;
export const GetMyOfferDraftsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetMyOfferDrafts"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"OfferDraftResolver_Find_FilterInputType"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"findOfferDrafts"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"docs"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"productName"}},{"kind":"Field","name":{"kind":"Name","value":"offerStartDate"}},{"kind":"Field","name":{"kind":"Name","value":"offerEndDate"}},{"kind":"Field","name":{"kind":"Name","value":"variants"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"moq"}},{"kind":"Field","name":{"kind":"Name","value":"quantityRangePrices"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"max"}},{"kind":"Field","name":{"kind":"Name","value":"min"}},{"kind":"Field","name":{"kind":"Name","value":"price"}}]}},{"kind":"Field","name":{"kind":"Name","value":"variantId"}}]}},{"kind":"Field","name":{"kind":"Name","value":"warrantyPeriod"}},{"kind":"Field","name":{"kind":"Name","value":"warrantyPeriodType"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}}]}}]}}]}}]} as unknown as DocumentNode<GetMyOfferDraftsQuery, GetMyOfferDraftsQueryVariables>;
export const GetMyOffersDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetMyOffers"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"pagination"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"PaginationDto"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"OfferResolver_FindAll_FilterInputType"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"offers"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"pagination"},"value":{"kind":"Variable","name":{"kind":"Name","value":"pagination"}}},{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"docs"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"productName"}},{"kind":"Field","name":{"kind":"Name","value":"offerStartDate"}},{"kind":"Field","name":{"kind":"Name","value":"offerEndDate"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"variants"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"availableDate"}},{"kind":"Field","name":{"kind":"Name","value":"moq"}},{"kind":"Field","name":{"kind":"Name","value":"quantityRangePrices"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"min"}},{"kind":"Field","name":{"kind":"Name","value":"max"}},{"kind":"Field","name":{"kind":"Name","value":"price"}}]}}]}}]}}]}}]}}]} as unknown as DocumentNode<GetMyOffersQuery, GetMyOffersQueryVariables>;
export const GetOfferDraftByIdDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetOfferDraftById"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"offerDraftId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"offerDraft"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"offerDraftId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"additionalDocuments"}},{"kind":"Field","name":{"kind":"Name","value":"composition"}},{"kind":"Field","name":{"kind":"Name","value":"createdUserId"}},{"kind":"Field","name":{"kind":"Name","value":"deliveryDetail"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"DeliverToAimak"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"aimak"}},{"kind":"Field","name":{"kind":"Name","value":"collectionPointId"}},{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"DeliverToLocalPort"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"cityProvince"}},{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}},{"kind":"Field","name":{"kind":"Name","value":"duuregSoum"}},{"kind":"Field","name":{"kind":"Name","value":"portId"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"FullDelivery"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}},{"kind":"Field","name":{"kind":"Name","value":"stockLocationId"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ThirdPartyDelivery"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"customization"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"color"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"moq"}}]}},{"kind":"Field","name":{"kind":"Name","value":"custom"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"moq"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"logo"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"moq"}}]}},{"kind":"Field","name":{"kind":"Name","value":"package"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"moq"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"hasTemperatureControl"}},{"kind":"Field","name":{"kind":"Name","value":"isRefundable"}},{"kind":"Field","name":{"kind":"Name","value":"isReturnable"}},{"kind":"Field","name":{"kind":"Name","value":"isSampleDelivery"}},{"kind":"Field","name":{"kind":"Name","value":"isSensitiveToMoisture"}},{"kind":"Field","name":{"kind":"Name","value":"isShippableThroughAir"}},{"kind":"Field","name":{"kind":"Name","value":"marketCountry"}},{"kind":"Field","name":{"kind":"Name","value":"isVerified"}},{"kind":"Field","name":{"kind":"Name","value":"marketType"}},{"kind":"Field","name":{"kind":"Name","value":"offerEndDate"}},{"kind":"Field","name":{"kind":"Name","value":"offerStartDate"}},{"kind":"Field","name":{"kind":"Name","value":"packageDetail"}},{"kind":"Field","name":{"kind":"Name","value":"paymentMethods"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"acountNumber"}},{"kind":"Field","name":{"kind":"Name","value":"icon"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"paymentType"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"advance"}},{"kind":"Field","name":{"kind":"Name","value":"final"}},{"kind":"Field","name":{"kind":"Name","value":"interim"}}]}},{"kind":"Field","name":{"kind":"Name","value":"productId"}},{"kind":"Field","name":{"kind":"Name","value":"productName"}},{"kind":"Field","name":{"kind":"Name","value":"supplierId"}},{"kind":"Field","name":{"kind":"Name","value":"tags"}},{"kind":"Field","name":{"kind":"Name","value":"variants"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"availableDate"}},{"kind":"Field","name":{"kind":"Name","value":"moq"}},{"kind":"Field","name":{"kind":"Name","value":"quantity"}},{"kind":"Field","name":{"kind":"Name","value":"quantityRangeLeadTimes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"min"}},{"kind":"Field","name":{"kind":"Name","value":"max"}},{"kind":"Field","name":{"kind":"Name","value":"days"}}]}},{"kind":"Field","name":{"kind":"Name","value":"quantityRangePrices"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"max"}},{"kind":"Field","name":{"kind":"Name","value":"min"}},{"kind":"Field","name":{"kind":"Name","value":"price"}}]}},{"kind":"Field","name":{"kind":"Name","value":"variantId"}}]}},{"kind":"Field","name":{"kind":"Name","value":"warrantyPeriod"}},{"kind":"Field","name":{"kind":"Name","value":"warrantyPeriodType"}}]}}]}}]} as unknown as DocumentNode<GetOfferDraftByIdQuery, GetOfferDraftByIdQueryVariables>;
export const GetPortsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetPorts"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"ports"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]} as unknown as DocumentNode<GetPortsQuery, GetPortsQueryVariables>;
export const GetProductMetaListDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetProductMetaList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"getProductMetaList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"}},{"kind":"Field","name":{"kind":"Name","value":"categoryId"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"isActive"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"unit"}},{"kind":"Field","name":{"kind":"Name","value":"unitVolume"}},{"kind":"Field","name":{"kind":"Name","value":"unitWeight"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}}]}}]} as unknown as DocumentNode<GetProductMetaListQuery, GetProductMetaListQueryVariables>;
export const GetSupplierReviewByIdDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetSupplierReviewById"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"supplierReviewId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"supplierReview"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"supplierReviewId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"certificationIds"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"delivery_type_comment"}},{"kind":"Field","name":{"kind":"Name","value":"delivery_type_status"}},{"kind":"Field","name":{"kind":"Name","value":"entity_info_comment"}},{"kind":"Field","name":{"kind":"Name","value":"entity_info_status"}},{"kind":"Field","name":{"kind":"Name","value":"images_comment"}},{"kind":"Field","name":{"kind":"Name","value":"images_status"}},{"kind":"Field","name":{"kind":"Name","value":"product_category_comment"}},{"kind":"Field","name":{"kind":"Name","value":"product_category_status"}},{"kind":"Field","name":{"kind":"Name","value":"payment_method_status"}},{"kind":"Field","name":{"kind":"Name","value":"payment_method_comment"}},{"kind":"Field","name":{"kind":"Name","value":"status"}},{"kind":"Field","name":{"kind":"Name","value":"stock_location_comment"}},{"kind":"Field","name":{"kind":"Name","value":"stock_location_status"}},{"kind":"Field","name":{"kind":"Name","value":"supplier_id"}},{"kind":"Field","name":{"kind":"Name","value":"supplier_type_comment"}},{"kind":"Field","name":{"kind":"Name","value":"supplier_type_status"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}},{"kind":"Field","name":{"kind":"Name","value":"uploaded_document_comment"}},{"kind":"Field","name":{"kind":"Name","value":"uploaded_document_status"}}]}}]}}]} as unknown as DocumentNode<GetSupplierReviewByIdQuery, GetSupplierReviewByIdQueryVariables>;
export const SubmitSupplierReviewDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"SubmitSupplierReview"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"markets"}},"type":{"kind":"NonNullType","type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"SUPPLIER_MARKETS"}}}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"supplierId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"submitSupplier"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"markets"},"value":{"kind":"Variable","name":{"kind":"Name","value":"markets"}}},{"kind":"Argument","name":{"kind":"Name","value":"supplierId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"supplierId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}}]}}]}}]} as unknown as DocumentNode<SubmitSupplierReviewMutation, SubmitSupplierReviewMutationVariables>;
export const UpdateSupplierReviewDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateSupplierReview"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"updateSupplierReviewId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"updateSupplierReviewInput"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"UpdateEntityReviewInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateSupplierReview"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"updateSupplierReviewId"}}},{"kind":"Argument","name":{"kind":"Name","value":"updateSupplierReviewInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"updateSupplierReviewInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"certificationIds"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"delivery_type_comment"}},{"kind":"Field","name":{"kind":"Name","value":"delivery_type_status"}},{"kind":"Field","name":{"kind":"Name","value":"entity_info_comment"}},{"kind":"Field","name":{"kind":"Name","value":"entity_info_status"}},{"kind":"Field","name":{"kind":"Name","value":"images_comment"}},{"kind":"Field","name":{"kind":"Name","value":"images_status"}},{"kind":"Field","name":{"kind":"Name","value":"payment_method_comment"}},{"kind":"Field","name":{"kind":"Name","value":"payment_method_status"}},{"kind":"Field","name":{"kind":"Name","value":"product_category_comment"}},{"kind":"Field","name":{"kind":"Name","value":"product_category_status"}},{"kind":"Field","name":{"kind":"Name","value":"status"}},{"kind":"Field","name":{"kind":"Name","value":"stock_location_comment"}},{"kind":"Field","name":{"kind":"Name","value":"stock_location_status"}},{"kind":"Field","name":{"kind":"Name","value":"supplier_id"}},{"kind":"Field","name":{"kind":"Name","value":"supplier_type_comment"}},{"kind":"Field","name":{"kind":"Name","value":"supplier_type_status"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}},{"kind":"Field","name":{"kind":"Name","value":"uploaded_document_comment"}},{"kind":"Field","name":{"kind":"Name","value":"uploaded_document_status"}}]}}]}}]} as unknown as DocumentNode<UpdateSupplierReviewMutation, UpdateSupplierReviewMutationVariables>;
export const UserByIdDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"UserById"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"userId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"user"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"userId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"userId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"createdEntityIds"}},{"kind":"Field","name":{"kind":"Name","value":"createdEntities"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"phoneNumber"}},{"kind":"Field","name":{"kind":"Name","value":"registrationNumber"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"entityCompanyId"}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierId"}},{"kind":"Field","name":{"kind":"Name","value":"entityBuyerId"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"currentEntity"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"phoneNumber"}},{"kind":"Field","name":{"kind":"Name","value":"registrationNumber"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"createdUserId"}},{"kind":"Field","name":{"kind":"Name","value":"entityBuyerId"}},{"kind":"Field","name":{"kind":"Name","value":"entityCompanyId"}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplier"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"certificationIds"}},{"kind":"Field","name":{"kind":"Name","value":"coverImages"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"deliveryDetail"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"DeliverToAimak"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"aimak"}},{"kind":"Field","name":{"kind":"Name","value":"collectionPointId"}},{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"DeliverToLocalPort"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"cityProvince"}},{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}},{"kind":"Field","name":{"kind":"Name","value":"duuregSoum"}},{"kind":"Field","name":{"kind":"Name","value":"portId"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"FullDelivery"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}},{"kind":"Field","name":{"kind":"Name","value":"stockLocationId"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ThirdPartyDelivery"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deliveryType"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierCompany"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"stateRegistrationOrder"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierCompanyId"}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierIndividual"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierId"}},{"kind":"Field","name":{"kind":"Name","value":"identityBack"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"identityFront"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"identitySelfie"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierIndividualId"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bucket"}},{"kind":"Field","name":{"kind":"Name","value":"key"}},{"kind":"Field","name":{"kind":"Name","value":"location"}}]}},{"kind":"Field","name":{"kind":"Name","value":"isDomestic"}},{"kind":"Field","name":{"kind":"Name","value":"isInternational"}},{"kind":"Field","name":{"kind":"Name","value":"productTypeIds"}},{"kind":"Field","name":{"kind":"Name","value":"productTypes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"parentId"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"reviewId"}},{"kind":"Field","name":{"kind":"Name","value":"status"}},{"kind":"Field","name":{"kind":"Name","value":"stockLocationIds"}},{"kind":"Field","name":{"kind":"Name","value":"stockLocations"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"address"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"cityProvince"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}}]}},{"kind":"Field","name":{"kind":"Name","value":"coordinates"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"latitude"}},{"kind":"Field","name":{"kind":"Name","value":"longtitude"}}]}},{"kind":"Field","name":{"kind":"Name","value":"detail"}},{"kind":"Field","name":{"kind":"Name","value":"duuregSoum"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}}]}},{"kind":"Field","name":{"kind":"Name","value":"khorooBag"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"nameMn"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"branchName"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"phoneNumber"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"supplierType"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"entitySupplierId"}},{"kind":"Field","name":{"kind":"Name","value":"_id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"currentEntityId"}},{"kind":"Field","name":{"kind":"Name","value":"currentPlatformId"}},{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"firstName"}},{"kind":"Field","name":{"kind":"Name","value":"image"}},{"kind":"Field","name":{"kind":"Name","value":"lastName"}},{"kind":"Field","name":{"kind":"Name","value":"phoneNumber"}},{"kind":"Field","name":{"kind":"Name","value":"refreshToken"}},{"kind":"Field","name":{"kind":"Name","value":"isEmailVerified"}},{"kind":"Field","name":{"kind":"Name","value":"entityIds"}}]}}]}}]} as unknown as DocumentNode<UserByIdQuery, UserByIdQueryVariables>;