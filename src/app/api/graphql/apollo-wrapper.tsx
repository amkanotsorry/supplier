'use client'
import { ApolloNextAppProvider } from '@apollo/experimental-nextjs-app-support/ssr'
import { ReactNode } from 'react'
import { ApolloLink, HttpLink } from '@apollo/client'
import { setContext } from '@apollo/client/link/context'
import { NextSSRApolloClient, NextSSRInMemoryCache, SSRMultipartLink } from '@apollo/experimental-nextjs-app-support/ssr'
import { useSession } from 'next-auth/react'

type Props = {
	children?: ReactNode
}

const uploadLink = new HttpLink({
	uri: 'http://localhost:3000/graphql',
	headers: { 'Apollo-Require-Preflight': 'true' }
})

export function ApolloWrapper({ children }: Props) {
	const { data: session } = useSession()

	const authLink = setContext(async (_, { headers }) => {
		const { token } = session?.user || {}
		return {
			headers: {
				...headers,
				authorization: token ? `Bearer ${token}` : ''
			}
		}
	})

	const errorLogger = new ApolloLink((operation, forward) => {
		return forward(operation).map(response => {
			try {
				if (response.errors) {
					response.errors.forEach(async error => {
						console.error('GraphQL Error:', error)
						if (error.message === 'Unauthorized') {
							console.log('logout...')
						}
					})
				}
			} catch (e) {
				console.log('errorLogger e:', e)
			}

			return response
		})
	})

	function makeClient() {
		const withMultiPartLink = ApolloLink.from([
			new SSRMultipartLink({
				stripDefer: true
			}),
			errorLogger,
			authLink.concat(uploadLink)
		])
		return new NextSSRApolloClient({
			cache: new NextSSRInMemoryCache(),
			link: typeof window === 'undefined' ? withMultiPartLink : ApolloLink.from([authLink, uploadLink])
		})
	}

	return <ApolloNextAppProvider makeClient={makeClient}>{children}</ApolloNextAppProvider>
}

export function ApolloFileUploadWrapper({ children }: Props) {
	const { data: session } = useSession()

	const authLink = setContext(async (_, { headers }) => {
		const { token } = session?.user || {}

		return {
			headers: {
				...headers,
				authorization: token ? `Bearer ${token}` : ''
			}
		}
	})

	function makeClient() {
		const withMultiPartLink = ApolloLink.from([
			new SSRMultipartLink({
				stripDefer: true
			}),
			authLink.concat(uploadLink)
		])
		return new NextSSRApolloClient({
			cache: new NextSSRInMemoryCache(),
			link: typeof window === 'undefined' ? withMultiPartLink : ApolloLink.from([authLink, uploadLink])
		})
	}

	return <ApolloNextAppProvider makeClient={makeClient}>{children}</ApolloNextAppProvider>
}
