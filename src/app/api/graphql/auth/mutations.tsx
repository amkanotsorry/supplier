import { graphql } from '../../generated'

export const LoginMutation = graphql(`
	mutation Login($loginUserInput: LoginUserInput!) {
		login(loginUserInput: $loginUserInput) {
			access_token
			refresh_token
			user {
				_id
				firstName
				email
				currentEntityId
				entityIds
				isEmailVerified
			}
		}
	}
`)

export const SignupMutation = graphql(`
	mutation Signup($signupUserInput: CreateUserInput!) {
		signup(signupUserInput: $signupUserInput) {
			access_token
			refresh_token
			user {
				_id
				firstName
				email
				currentEntityId
				entityIds
			}
		}
	}
`)

export const RequestVerifyEmailMutation = graphql(`
	mutation RequestVerifyEmail($email: String!) {
		requestVerifyEmail(email: $email) {
			email
			isSent
		}
	}
`)

export const VerifyEmailMutation = graphql(`
	mutation VerifyEmail($verifyEmailInput: VerifyEmailInput!) {
		verifyEmail(verifyEmailInput: $verifyEmailInput) {
			_id
		}
	}
`)
