import { graphql } from '../../generated'

export const Users = graphql(`
	query Users {
		users {
			_id
			email
		}
	}
`)
