'use client'
import { signOut } from 'next-auth/react'

export const autoSignOut = async () => {
	try {
		await signOut()
	} catch (error) {
		console.log('sign out error: ', error)
	}
}
