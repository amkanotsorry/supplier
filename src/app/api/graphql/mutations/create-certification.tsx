import { graphql } from '../../generated'

export const CreateCertification = graphql(`
	mutation CreateCertification($createCertificationInput: CreateCertificationInput!) {
		createCertification(createCertificationInput: $createCertificationInput) {
			_id
			organization
			name
			issueDate
			expiryDate
			attachments {
				location
				bucket
				key
			}
			status
			isSpecial
			entityId
		}
	}
`)
