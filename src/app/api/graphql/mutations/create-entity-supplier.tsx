import { graphql } from '../../generated'

export const CreateEntitySupplier = graphql(`
	mutation CreateEntitySupplierMutation($createEntitySupplierInput: CreateEntitySupplierInput!, $entityId: String!, $entityType: ENTITY_TYPE!) {
		createEntitySupplier(createEntitySupplierInput: $createEntitySupplierInput, entityId: $entityId, entityType: $entityType) {
			_id
			entityId
			stockLocationIds
			productTypeIds
			certificationIds
			supplierType {
				_id
				code
				name
			}
			status
			image {
				location
				bucket
				key
			}
			coverImages {
				location
				bucket
				key
			}
			isDomestic
			isInternational
		}
	}
`)
