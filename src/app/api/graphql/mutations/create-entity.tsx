import { graphql } from '../../generated'

export const CreateEntity = graphql(`
	mutation CreateEntity($createEntityInput: CreateEntityInput!) {
		createEntity(createEntityInput: $createEntityInput) {
			_id
		}
	}
`)
