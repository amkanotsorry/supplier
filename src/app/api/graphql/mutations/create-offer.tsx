import { graphql } from '../../generated'

export const CreateOfferMutation = graphql(`
	mutation CreateOffer($createOfferInput: CreateOfferInput!) {
		createOffer(createOfferInput: $createOfferInput) {
			_id
		}
	}
`)

export const CreateOfferDraftMutation = graphql(`
	mutation CreateOfferDraft($createOfferDraftInput: CreateOfferDraftInput!) {
		createOfferDraft(createOfferDraftInput: $createOfferDraftInput) {
			_id
		}
	}
`)
