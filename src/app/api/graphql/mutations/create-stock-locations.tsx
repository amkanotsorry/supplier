import { graphql } from '../../generated'

export const CreateStockLocations = graphql(`
	mutation CreateStockLocations($createStockLocationsInput: [StockLocationInput!]!) {
		createStockLocations(createStockLocationsInput: $createStockLocationsInput) {
			_id
		}
	}
`)
