import { graphql } from '../../../apiProductMs/generated'

export const UpdateEntitySupplier = graphql(`
	mutation UpdateEntitySupplier($updateEntitySupplierId: String!, $updateEntitySupplierInput: UpdateEntitySupplierInput!) {
		updateEntitySupplier(id: $updateEntitySupplierId, updateEntitySupplierInput: $updateEntitySupplierInput) {
			_id
		}
	}
`)
