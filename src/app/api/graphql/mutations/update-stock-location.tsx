import { graphql } from '../../generated'

export const UpdateStockLocation = graphql(`
	mutation UpdateStockLocation($updateStockLocationsInput: UpdateStockLocationInput!) {
		updateStockLocation(updateStockLocationsInput: $updateStockLocationsInput) {
			_id
		}
	}
`)
