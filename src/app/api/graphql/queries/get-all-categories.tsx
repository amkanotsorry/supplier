import { graphql } from '../../../apiProductMs/generated'

export const GetAllCategories = graphql(`
	query GetAllCategories {
		productTypes {
			_id
			parentId
			name
			code
			createdAt
			updatedAt
		}
	}
`)
