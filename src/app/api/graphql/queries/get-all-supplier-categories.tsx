import { graphql } from '../../generated'

export const GetAllSupplierCategories = graphql(`
	query GetAllSupplierCategories {
		getAllSupplierCategories {
			_id
			code
			name
		}
	}
`)
