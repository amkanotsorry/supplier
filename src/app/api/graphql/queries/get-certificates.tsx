import { graphql } from '../../../apiProductMs/generated'

export const GetCertificates = graphql(`
	query GetCertificates {
		certificates {
			_id
			name
			organization
		}
	}
`)
