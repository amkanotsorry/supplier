import { graphql } from '../../generated'

export const GetCollectionPoints = graphql(`
	query GetCollectionPoints {
		collectionPoints {
			_id
			code
			name
		}
	}
`)
