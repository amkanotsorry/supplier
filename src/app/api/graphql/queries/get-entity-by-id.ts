import { graphql } from '../../generated'

export const GetEntityByIdQuery = graphql(`
	query GetEntityById($entityId: String!) {
		entity(id: $entityId) {
			_id
			entitySupplierId
			entitySupplier {
				_id
				certificationIds
				coverImages {
					bucket
					key
					location
				}
				createdAt
				deliveryDetail {
					... on DeliverToAimak {
						aimak
						collectionPointId
						deliveryType
					}
					... on DeliverToLocalPort {
						cityProvince
						deliveryType
						duuregSoum
						portId
					}
					... on FullDelivery {
						deliveryType
						stockLocationId
					}
					... on ThirdPartyDelivery {
						deliveryType
					}
				}
				entitySupplierCompany {
					_id
					stateRegistrationOrder {
						bucket
						key
						location
					}
				}
				entitySupplierCompanyId
				entitySupplierIndividual {
					_id
					createdAt
					entitySupplierId
					identityBack {
						bucket
						key
						location
					}
					identityFront {
						bucket
						key
						location
					}
					identitySelfie {
						bucket
						key
						location
					}
					updatedAt
				}
				entitySupplierIndividualId
				image {
					bucket
					key
					location
				}
				isDomestic
				isInternational
				productTypeIds
				productTypes {
					_id
					code
					createdAt
					name
					parentId
					updatedAt
				}
				reviewId
				status
				stockLocationIds
				stockLocations {
					_id
					address {
						cityProvince {
							code
							name
							nameMn
						}
						coordinates {
							latitude
							longtitude
						}
						detail
						duuregSoum {
							code
							name
							nameMn
						}
						khorooBag {
							code
							name
							nameMn
						}
					}
					branchName
					createdAt
					phoneNumber
					updatedAt
				}
				supplierType {
					_id
					code
					name
				}
				updatedAt
			}
			address {
				duuregSoum {
					code
					name
					nameMn
				}
				detail
				coordinates {
					longtitude
					latitude
				}
				cityProvince {
					code
					name
					nameMn
				}
				khorooBag {
					code
					nameMn
					name
				}
			}
			areasOfActivity
			countryCode
			createdAt
			createdUser {
				_id
				countryCode
				createdAt
				email
				firstName
				image
				isEmailVerified
				isoCode
				lastName
				phoneNumber
				updatedAt
			}
			createdUserId
			entityBuyerId
			entityCompany {
				_id
				incorporatedDate
				numberOfEmployees
				description
				createdAt
				updatedAt
			}
			entityCompanyId
			isoCode
			name
			phoneNumber
			registrationNumber
			type
			updatedAt
		}
	}
`)
