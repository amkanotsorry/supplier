import { graphql } from '../../generated'

export const GetEntitySupplierById = graphql(`
	query GetEntitySupplierById($entitySupplierId: String!) {
		entitySupplier(id: $entitySupplierId) {
			_id
			certifications {
				_id
				attachments {
					bucket
					key
					location
				}
				createdAt
				entityId
				expiryDate
				isSpecial
				issueDate
				name
				organization
				status
				updatedAt
			}
			coverImages {
				bucket
				key
				location
			}
			createdAt
			deliveryDetail {
				... on DeliverToAimak {
					aimak
					collectionPointId
					deliveryType
				}
				... on DeliverToLocalPort {
					cityProvince
					deliveryType
					duuregSoum
					portId
				}
				... on FullDelivery {
					deliveryType
					stockLocationId
				}
				... on ThirdPartyDelivery {
					deliveryType
				}
			}
			entity {
				_id
				address {
					cityProvince {
						code
						name
						nameMn
					}
					coordinates {
						latitude
						longtitude
					}
					detail
					duuregSoum {
						code
						name
						nameMn
					}
					khorooBag {
						code
						name
						nameMn
					}
				}
				areasOfActivity
				countryCode
				createdAt
				createdUser {
					_id
					countryCode
					createdAt
					email
					firstName
					image
					isEmailVerified
					isoCode
					lastName
					phoneNumber
					updatedAt
				}
				createdUserId
				entityBuyerId
				entityCompany {
					_id
					incorporatedDate
					numberOfEmployees
					description
					createdAt
					updatedAt
				}
				entityCompanyId
				isoCode
				name
				phoneNumber
				registrationNumber
				type
				updatedAt
			}
			entityId
			entitySupplierCompany {
				_id
				stateRegistrationOrder {
					bucket
					key
					location
				}
			}
			entitySupplierCompanyId
			entitySupplierIndividual {
				_id
				createdAt
				entitySupplierId
				identityBack {
					bucket
					key
					location
				}
				identityFront {
					bucket
					key
					location
				}
				identitySelfie {
					bucket
					key
					location
				}
				updatedAt
			}
			entitySupplierIndividualId
			image {
				bucket
				key
				location
			}
			isDomestic
			isInternational
			productTypeIds
			productTypes {
				_id
				code
				createdAt
				name
				parentId
				updatedAt
			}
			reviewId
			status
			stockLocationIds
			stockLocations {
				_id
				address {
					cityProvince {
						code
						name
						nameMn
					}
					coordinates {
						latitude
						longtitude
					}
					detail
					duuregSoum {
						code
						name
						nameMn
					}
					khorooBag {
						code
						name
						nameMn
					}
				}
				branchName
				createdAt
				phoneNumber
				updatedAt
			}
			supplierType {
				_id
				code
				name
			}
			updatedAt
		}
	}
`)
