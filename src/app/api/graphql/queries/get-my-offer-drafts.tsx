import { graphql } from '../../generated'

export const GetMyOfferDrafts = graphql(`
	query GetMyOfferDrafts($where: OfferDraftResolver_Find_FilterInputType) {
		findOfferDrafts(where: $where) {
			docs {
				_id
				productName
				offerStartDate
				offerEndDate
				variants {
					moq
					quantityRangePrices {
						max
						min
						price
					}
					variantId
				}
				warrantyPeriod
				warrantyPeriodType
				createdAt
			}
		}
	}
`)
