import { graphql } from '../../generated'

export const GetMyOffers = graphql(`
	query GetMyOffers($pagination: PaginationDto!, $where: OfferResolver_FindAll_FilterInputType) {
		offers(pagination: $pagination, where: $where) {
			docs {
				_id
				createdAt
				productName
				offerStartDate
				offerEndDate
				code
				variants {
					availableDate
					moq
					quantityRangePrices {
						min
						max
						price
					}
				}
			}
		}
	}
`)
