import { graphql } from '../../generated'

export const GetOfferDraftById = graphql(`
	query GetOfferDraftById($offerDraftId: String!) {
		offerDraft(id: $offerDraftId) {
			_id
			additionalDocuments
			composition
			createdUserId
			deliveryDetail {
				... on DeliverToAimak {
					aimak
					collectionPointId
					deliveryType
				}
				... on DeliverToLocalPort {
					cityProvince
					deliveryType
					duuregSoum
					portId
				}
				... on FullDelivery {
					deliveryType
					stockLocationId
				}
				... on ThirdPartyDelivery {
					deliveryType
				}
			}
			customization {
				color {
					name
					moq
				}
				custom {
					moq
					name
				}
				logo {
					name
					moq
				}
				package {
					moq
					name
				}
			}
			hasTemperatureControl
			isRefundable
			isReturnable
			isSampleDelivery
			isSensitiveToMoisture
			isShippableThroughAir
			marketCountry
			isVerified
			marketType
			offerEndDate
			offerStartDate
			packageDetail
			paymentMethods {
				acountNumber
				icon
				name
			}
			paymentType {
				advance
				final
				interim
			}
			productId
			productName
			supplierId
			tags
			variants {
				availableDate
				moq
				quantity
				quantityRangeLeadTimes {
					min
					max
					days
				}
				quantityRangePrices {
					max
					min
					price
				}
				variantId
			}
			warrantyPeriod
			warrantyPeriodType
		}
	}
`)
