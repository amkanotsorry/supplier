import { graphql } from '../../generated';

export const GetPorts = graphql(`
  query GetPorts {
    ports {
      _id
      name
    }
  }
`);
