import { graphql } from '../../generated'

export const GetProductMetaList = graphql(`
	query GetProductMetaList {
		getProductMetaList {
			_id
			attributes
			categoryId
			createdAt
			isActive
			name
			unit
			unitVolume
			unitWeight
			updatedAt
		}
	}
`)
