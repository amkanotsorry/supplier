import { graphql } from '../../generated'

export const GetSupplierReviewById = graphql(`
	query GetSupplierReviewById($supplierReviewId: String!) {
		supplierReview(id: $supplierReviewId) {
			_id
			certificationIds
			createdAt
			delivery_type_comment
			delivery_type_status
			entity_info_comment
			entity_info_status
			images_comment
			images_status
			product_category_comment
			product_category_status
			payment_method_status
			payment_method_comment
			status
			stock_location_comment
			stock_location_status
			supplier_id
			supplier_type_comment
			supplier_type_status
			updatedAt
			uploaded_document_comment
			uploaded_document_status
		}
	}
`)
