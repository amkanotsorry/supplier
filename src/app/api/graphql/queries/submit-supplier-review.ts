import { graphql } from '../../generated';

export const SubmitSupplierReview = graphql(`
  mutation SubmitSupplierReview(
    $markets: [SUPPLIER_MARKETS!]!
    $supplierId: String!
  ) {
    submitSupplier(markets: $markets, supplierId: $supplierId) {
      _id
    }
  }
`);
