import { graphql } from '../../generated'

export const UserById = graphql(`
	query UserById($userId: String!) {
		user(userId: $userId) {
			_id
			createdAt
			createdEntityIds
			createdEntities {
				_id
				name
				phoneNumber
				registrationNumber
				type
				entityCompanyId
				entitySupplierId
				entityBuyerId
				updatedAt
			}
			currentEntity {
				name
				phoneNumber
				registrationNumber
				type
				createdUserId
				entityBuyerId
				entityCompanyId
				entitySupplier {
					certificationIds
					coverImages {
						bucket
						key
						location
					}
					createdAt
					deliveryDetail {
						... on DeliverToAimak {
							aimak
							collectionPointId
							deliveryType
						}
						... on DeliverToLocalPort {
							cityProvince
							deliveryType
							duuregSoum
							portId
						}
						... on FullDelivery {
							deliveryType
							stockLocationId
						}
						... on ThirdPartyDelivery {
							deliveryType
						}
					}
					entitySupplierCompany {
						_id
						stateRegistrationOrder {
							bucket
							key
							location
						}
					}
					entitySupplierCompanyId
					entitySupplierIndividual {
						_id
						createdAt
						entitySupplierId
						identityBack {
							bucket
							key
							location
						}
						identityFront {
							bucket
							key
							location
						}
						identitySelfie {
							bucket
							key
							location
						}
						updatedAt
					}
					entitySupplierIndividualId
					image {
						bucket
						key
						location
					}
					isDomestic
					isInternational
					productTypeIds
					productTypes {
						_id
						code
						createdAt
						name
						parentId
						updatedAt
					}
					reviewId
					status
					stockLocationIds
					stockLocations {
						_id
						address {
							cityProvince {
								code
								name
								nameMn
							}
							coordinates {
								latitude
								longtitude
							}
							detail
							duuregSoum {
								code
								name
								nameMn
							}
							khorooBag {
								code
								name
								nameMn
							}
						}
						branchName
						createdAt
						phoneNumber
						updatedAt
					}
					supplierType {
						_id
						code
						name
					}
					updatedAt
				}
				entitySupplierId
				_id
			}
			currentEntityId
			currentPlatformId
			email
			firstName
			image
			lastName
			phoneNumber
			refreshToken
			isEmailVerified
			entityIds
		}
	}
`)
