'use server'

import { ApolloClient, ApolloLink, HttpLink, InMemoryCache } from '@apollo/client'
import { setContext } from '@apollo/client/link/context'
import { registerApolloClient } from '@apollo/experimental-nextjs-app-support/rsc'
import { getServerSession } from 'next-auth'
import { authOptions } from '../api/auth/[...nextauth]/authOptions'

const authLink = setContext(async (_, { headers }) => {
	const session = await getServerSession(authOptions)
	const { token } = session?.user || {}

	return {
		headers: {
			...headers,
			authorization: token ? `Bearer ${token}` : '',
			'Apollo-Require-Preflight': true
		}
	}
})

const httpProductMsLink = new HttpLink({
	uri: 'https://product-graphql-dev.agrix.mn/graphql',
	headers: { 'Apollo-Require-Preflight': 'true' }
})

export const { getClient } = registerApolloClient(() => {
	return new ApolloClient({
		cache: new InMemoryCache(),
		link: authLink.concat(httpProductMsLink)
	})
})
