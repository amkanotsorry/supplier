/* eslint-disable */
import * as types from './graphql';
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';

/**
 * Map of all GraphQL operations in the project.
 *
 * This map has several performance disadvantages:
 * 1. It is not tree-shakeable, so it will include all operations in the project.
 * 2. It is not minifiable, so the string of a GraphQL query will be multiple times inside the bundle.
 * 3. It does not support dead code elimination, so it will add unused operations.
 *
 * Therefore it is highly recommended to use the babel or swc plugin for production.
 */
const documents = {
    "\n\tmutation CreateProducts($createProductInput: CreateProductInput!) {\n\t\tcreateProduct(createProductInput: $createProductInput) {\n\t\t\t_id\n\t\t\tvariants {\n\t\t\t\tattributes {\n\t\t\t\t\tisActive\n\t\t\t\t\toption\n\t\t\t\t\tvalue\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\t}\n": types.CreateProductsDocument,
    "\n\tquery GetAllProducts($where: ProductResolver_Find_FilterInputType) {\n\t\tfindProducts(where: $where) {\n\t\t\tdocs {\n\t\t\t\t_id\n\t\t\t\tname\n\t\t\t\tmetaId\n\t\t\t\tmedia\n\t\t\t\tdescription\n\t\t\t\tstatus\n\t\t\t\timages\n\t\t\t\tprice\n\t\t\t\tsku\n\t\t\t\tisPhysical\n\t\t\t\tslug\n\t\t\t\tcondition\n\t\t\t\tavailableDate\n\t\t\t\texpiryDate\n\t\t\t\tisReturnable\n\t\t\t\tweight\n\t\t\t\tweightUnit\n\t\t\t\tvariants {\n\t\t\t\t\t_id\n\t\t\t\t\tprice\n\t\t\t\t\tquantity\n\t\t\t\t\tproductId\n\t\t\t\t\tplaceOfOrigin\n\t\t\t\t\tcertificatesOfOrigin\n\t\t\t\t\tsku\n\t\t\t\t\timages\n\t\t\t\t\tstatus\n\t\t\t\t\tweight\n\t\t\t\t\tweightUnit\n\t\t\t\t\tsellingPrice\n\t\t\t\t\tdiscountPrice\n\t\t\t\t\tavailableOn\n\t\t\t\t\texpiryDate\n\t\t\t\t\tbarcode\n\t\t\t\t\tattributes {\n\t\t\t\t\t\toption\n\t\t\t\t\t\tvalue\n\t\t\t\t\t\tisActive\n\t\t\t\t\t}\n\t\t\t\t\tcreatedAt\n\t\t\t\t\tupdatedAt\n\t\t\t\t}\n\t\t\t\tcreatedAt\n\t\t\t\tupdatedAt\n\t\t\t\tcomposition\n\t\t\t\tvolume\n\t\t\t\tcreatedUserId\n\t\t\t}\n\t\t}\n\t}\n": types.GetAllProductsDocument,
    "\n\tquery GetVariant($getVariantId: String!) {\n\t\tgetVariant(id: $getVariantId) {\n\t\t\t_id\n\t\t\tprice\n\t\t\tquantity\n\t\t\tproductId\n\t\t\tplaceOfOrigin\n\t\t\tcertificatesOfOrigin\n\t\t\tsku\n\t\t\timages\n\t\t\tstatus\n\t\t\tweight\n\t\t\tweightUnit\n\t\t\tsellingPrice\n\t\t\tdiscountPrice\n\t\t\tavailableOn\n\t\t\texpiryDate\n\t\t\tbarcode\n\t\t\tattributes {\n\t\t\t\toption\n\t\t\t\tvalue\n\t\t\t\tisActive\n\t\t\t}\n\t\t\tcreatedAt\n\t\t\tupdatedAt\n\t\t}\n\t}\n": types.GetVariantDocument,
    "\n\tquery GetProduct($getProductId: String!) {\n\t\tgetProduct(id: $getProductId) {\n\t\t\t_id\n\t\t\tname\n\t\t\tmetaId\n\t\t\tmedia\n\t\t\tdescription\n\t\t\tstatus\n\t\t\timages\n\t\t\tprice\n\t\t\tsku\n\t\t\tisPhysical\n\t\t\tslug\n\t\t\tcondition\n\t\t\tavailableDate\n\t\t\texpiryDate\n\t\t\tisReturnable\n\t\t\tweight\n\t\t\tweightUnit\n\t\t\tvariants {\n\t\t\t\t_id\n\t\t\t\tprice\n\t\t\t\tquantity\n\t\t\t\tproductId\n\t\t\t\tplaceOfOrigin\n\t\t\t\tcertificatesOfOrigin\n\t\t\t\tsku\n\t\t\t\timages\n\t\t\t\tstatus\n\t\t\t\tweight\n\t\t\t\tweightUnit\n\t\t\t\tsellingPrice\n\t\t\t\tdiscountPrice\n\t\t\t\tavailableOn\n\t\t\t\texpiryDate\n\t\t\t\tbarcode\n\t\t\t\tattributes {\n\t\t\t\t\toption\n\t\t\t\t\tvalue\n\t\t\t\t\tisActive\n\t\t\t\t}\n\t\t\t\tcreatedAt\n\t\t\t\tupdatedAt\n\t\t\t}\n\t\t\tcomposition\n\t\t\tvolume\n\t\t\tcreatedAt\n\t\t\tupdatedAt\n\t\t}\n\t}\n": types.GetProductDocument,
};

/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 *
 *
 * @example
 * ```ts
 * const query = graphql(`query GetUser($id: ID!) { user(id: $id) { name } }`);
 * ```
 *
 * The query argument is unknown!
 * Please regenerate the types.
 */
export function graphql(source: string): unknown;

/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n\tmutation CreateProducts($createProductInput: CreateProductInput!) {\n\t\tcreateProduct(createProductInput: $createProductInput) {\n\t\t\t_id\n\t\t\tvariants {\n\t\t\t\tattributes {\n\t\t\t\t\tisActive\n\t\t\t\t\toption\n\t\t\t\t\tvalue\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\t}\n"): (typeof documents)["\n\tmutation CreateProducts($createProductInput: CreateProductInput!) {\n\t\tcreateProduct(createProductInput: $createProductInput) {\n\t\t\t_id\n\t\t\tvariants {\n\t\t\t\tattributes {\n\t\t\t\t\tisActive\n\t\t\t\t\toption\n\t\t\t\t\tvalue\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\t}\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n\tquery GetAllProducts($where: ProductResolver_Find_FilterInputType) {\n\t\tfindProducts(where: $where) {\n\t\t\tdocs {\n\t\t\t\t_id\n\t\t\t\tname\n\t\t\t\tmetaId\n\t\t\t\tmedia\n\t\t\t\tdescription\n\t\t\t\tstatus\n\t\t\t\timages\n\t\t\t\tprice\n\t\t\t\tsku\n\t\t\t\tisPhysical\n\t\t\t\tslug\n\t\t\t\tcondition\n\t\t\t\tavailableDate\n\t\t\t\texpiryDate\n\t\t\t\tisReturnable\n\t\t\t\tweight\n\t\t\t\tweightUnit\n\t\t\t\tvariants {\n\t\t\t\t\t_id\n\t\t\t\t\tprice\n\t\t\t\t\tquantity\n\t\t\t\t\tproductId\n\t\t\t\t\tplaceOfOrigin\n\t\t\t\t\tcertificatesOfOrigin\n\t\t\t\t\tsku\n\t\t\t\t\timages\n\t\t\t\t\tstatus\n\t\t\t\t\tweight\n\t\t\t\t\tweightUnit\n\t\t\t\t\tsellingPrice\n\t\t\t\t\tdiscountPrice\n\t\t\t\t\tavailableOn\n\t\t\t\t\texpiryDate\n\t\t\t\t\tbarcode\n\t\t\t\t\tattributes {\n\t\t\t\t\t\toption\n\t\t\t\t\t\tvalue\n\t\t\t\t\t\tisActive\n\t\t\t\t\t}\n\t\t\t\t\tcreatedAt\n\t\t\t\t\tupdatedAt\n\t\t\t\t}\n\t\t\t\tcreatedAt\n\t\t\t\tupdatedAt\n\t\t\t\tcomposition\n\t\t\t\tvolume\n\t\t\t\tcreatedUserId\n\t\t\t}\n\t\t}\n\t}\n"): (typeof documents)["\n\tquery GetAllProducts($where: ProductResolver_Find_FilterInputType) {\n\t\tfindProducts(where: $where) {\n\t\t\tdocs {\n\t\t\t\t_id\n\t\t\t\tname\n\t\t\t\tmetaId\n\t\t\t\tmedia\n\t\t\t\tdescription\n\t\t\t\tstatus\n\t\t\t\timages\n\t\t\t\tprice\n\t\t\t\tsku\n\t\t\t\tisPhysical\n\t\t\t\tslug\n\t\t\t\tcondition\n\t\t\t\tavailableDate\n\t\t\t\texpiryDate\n\t\t\t\tisReturnable\n\t\t\t\tweight\n\t\t\t\tweightUnit\n\t\t\t\tvariants {\n\t\t\t\t\t_id\n\t\t\t\t\tprice\n\t\t\t\t\tquantity\n\t\t\t\t\tproductId\n\t\t\t\t\tplaceOfOrigin\n\t\t\t\t\tcertificatesOfOrigin\n\t\t\t\t\tsku\n\t\t\t\t\timages\n\t\t\t\t\tstatus\n\t\t\t\t\tweight\n\t\t\t\t\tweightUnit\n\t\t\t\t\tsellingPrice\n\t\t\t\t\tdiscountPrice\n\t\t\t\t\tavailableOn\n\t\t\t\t\texpiryDate\n\t\t\t\t\tbarcode\n\t\t\t\t\tattributes {\n\t\t\t\t\t\toption\n\t\t\t\t\t\tvalue\n\t\t\t\t\t\tisActive\n\t\t\t\t\t}\n\t\t\t\t\tcreatedAt\n\t\t\t\t\tupdatedAt\n\t\t\t\t}\n\t\t\t\tcreatedAt\n\t\t\t\tupdatedAt\n\t\t\t\tcomposition\n\t\t\t\tvolume\n\t\t\t\tcreatedUserId\n\t\t\t}\n\t\t}\n\t}\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n\tquery GetVariant($getVariantId: String!) {\n\t\tgetVariant(id: $getVariantId) {\n\t\t\t_id\n\t\t\tprice\n\t\t\tquantity\n\t\t\tproductId\n\t\t\tplaceOfOrigin\n\t\t\tcertificatesOfOrigin\n\t\t\tsku\n\t\t\timages\n\t\t\tstatus\n\t\t\tweight\n\t\t\tweightUnit\n\t\t\tsellingPrice\n\t\t\tdiscountPrice\n\t\t\tavailableOn\n\t\t\texpiryDate\n\t\t\tbarcode\n\t\t\tattributes {\n\t\t\t\toption\n\t\t\t\tvalue\n\t\t\t\tisActive\n\t\t\t}\n\t\t\tcreatedAt\n\t\t\tupdatedAt\n\t\t}\n\t}\n"): (typeof documents)["\n\tquery GetVariant($getVariantId: String!) {\n\t\tgetVariant(id: $getVariantId) {\n\t\t\t_id\n\t\t\tprice\n\t\t\tquantity\n\t\t\tproductId\n\t\t\tplaceOfOrigin\n\t\t\tcertificatesOfOrigin\n\t\t\tsku\n\t\t\timages\n\t\t\tstatus\n\t\t\tweight\n\t\t\tweightUnit\n\t\t\tsellingPrice\n\t\t\tdiscountPrice\n\t\t\tavailableOn\n\t\t\texpiryDate\n\t\t\tbarcode\n\t\t\tattributes {\n\t\t\t\toption\n\t\t\t\tvalue\n\t\t\t\tisActive\n\t\t\t}\n\t\t\tcreatedAt\n\t\t\tupdatedAt\n\t\t}\n\t}\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n\tquery GetProduct($getProductId: String!) {\n\t\tgetProduct(id: $getProductId) {\n\t\t\t_id\n\t\t\tname\n\t\t\tmetaId\n\t\t\tmedia\n\t\t\tdescription\n\t\t\tstatus\n\t\t\timages\n\t\t\tprice\n\t\t\tsku\n\t\t\tisPhysical\n\t\t\tslug\n\t\t\tcondition\n\t\t\tavailableDate\n\t\t\texpiryDate\n\t\t\tisReturnable\n\t\t\tweight\n\t\t\tweightUnit\n\t\t\tvariants {\n\t\t\t\t_id\n\t\t\t\tprice\n\t\t\t\tquantity\n\t\t\t\tproductId\n\t\t\t\tplaceOfOrigin\n\t\t\t\tcertificatesOfOrigin\n\t\t\t\tsku\n\t\t\t\timages\n\t\t\t\tstatus\n\t\t\t\tweight\n\t\t\t\tweightUnit\n\t\t\t\tsellingPrice\n\t\t\t\tdiscountPrice\n\t\t\t\tavailableOn\n\t\t\t\texpiryDate\n\t\t\t\tbarcode\n\t\t\t\tattributes {\n\t\t\t\t\toption\n\t\t\t\t\tvalue\n\t\t\t\t\tisActive\n\t\t\t\t}\n\t\t\t\tcreatedAt\n\t\t\t\tupdatedAt\n\t\t\t}\n\t\t\tcomposition\n\t\t\tvolume\n\t\t\tcreatedAt\n\t\t\tupdatedAt\n\t\t}\n\t}\n"): (typeof documents)["\n\tquery GetProduct($getProductId: String!) {\n\t\tgetProduct(id: $getProductId) {\n\t\t\t_id\n\t\t\tname\n\t\t\tmetaId\n\t\t\tmedia\n\t\t\tdescription\n\t\t\tstatus\n\t\t\timages\n\t\t\tprice\n\t\t\tsku\n\t\t\tisPhysical\n\t\t\tslug\n\t\t\tcondition\n\t\t\tavailableDate\n\t\t\texpiryDate\n\t\t\tisReturnable\n\t\t\tweight\n\t\t\tweightUnit\n\t\t\tvariants {\n\t\t\t\t_id\n\t\t\t\tprice\n\t\t\t\tquantity\n\t\t\t\tproductId\n\t\t\t\tplaceOfOrigin\n\t\t\t\tcertificatesOfOrigin\n\t\t\t\tsku\n\t\t\t\timages\n\t\t\t\tstatus\n\t\t\t\tweight\n\t\t\t\tweightUnit\n\t\t\t\tsellingPrice\n\t\t\t\tdiscountPrice\n\t\t\t\tavailableOn\n\t\t\t\texpiryDate\n\t\t\t\tbarcode\n\t\t\t\tattributes {\n\t\t\t\t\toption\n\t\t\t\t\tvalue\n\t\t\t\t\tisActive\n\t\t\t\t}\n\t\t\t\tcreatedAt\n\t\t\t\tupdatedAt\n\t\t\t}\n\t\t\tcomposition\n\t\t\tvolume\n\t\t\tcreatedAt\n\t\t\tupdatedAt\n\t\t}\n\t}\n"];

export function graphql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> = TDocumentNode extends DocumentNode<  infer TType,  any>  ? TType  : never;