/* eslint-disable */
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  /** A date-time string at UTC, such as 2019-12-03T09:54:33Z, compliant with the date-time format. */
  DateTime: { input: any; output: any; }
};

export type Attribute = {
  __typename?: 'Attribute';
  isActive: Scalars['Boolean']['output'];
  option: Scalars['String']['output'];
  value: Scalars['String']['output'];
};

export type AttributeInput = {
  isActive: Scalars['Boolean']['input'];
  option: Scalars['String']['input'];
  value: Scalars['String']['input'];
};

export type Boolean_PropertyFilterInputType = {
  eq?: InputMaybe<Scalars['Boolean']['input']>;
  gt?: InputMaybe<Scalars['Boolean']['input']>;
  gte?: InputMaybe<Scalars['Boolean']['input']>;
  in?: InputMaybe<Array<Scalars['Boolean']['input']>>;
  lt?: InputMaybe<Scalars['Boolean']['input']>;
  lte?: InputMaybe<Scalars['Boolean']['input']>;
  ne?: InputMaybe<Scalars['Boolean']['input']>;
  nin?: InputMaybe<Array<Scalars['Boolean']['input']>>;
  regex?: InputMaybe<Scalars['Boolean']['input']>;
};

export type CreateCategoryInput = {
  /** Category code */
  code: Scalars['String']['input'];
  /** is active category  */
  isActive: Scalars['Boolean']['input'];
  /** is parent category  */
  isParent: Scalars['Boolean']['input'];
  /** Category name */
  name: Scalars['String']['input'];
  /** Parent category id */
  parentId?: InputMaybe<Scalars['String']['input']>;
};

export type CreateLogInput = {
  /** Employee id */
  employeeId: Scalars['String']['input'];
  /** ip address */
  ipAddress: Scalars['String']['input'];
  /** Product id */
  productId?: InputMaybe<Scalars['String']['input']>;
  /** log type */
  type: Log_Type;
  /** Product id */
  variantId?: InputMaybe<Scalars['String']['input']>;
};

export type CreateProductInput = {
  /** Product available date */
  availableDate: Scalars['DateTime']['input'];
  /** Product composition */
  composition?: InputMaybe<Scalars['String']['input']>;
  /** Product condition */
  condition?: InputMaybe<Product_Condition>;
  createdUserId?: InputMaybe<Scalars['String']['input']>;
  /** Product description */
  description: Scalars['String']['input'];
  /** Product expiry date */
  expiryDate?: InputMaybe<Scalars['DateTime']['input']>;
  /** Product images */
  images?: InputMaybe<Array<Scalars['String']['input']>>;
  /** is physical product */
  isPhysical?: InputMaybe<Scalars['Boolean']['input']>;
  /** is returnable product */
  isReturnable?: InputMaybe<Scalars['Boolean']['input']>;
  /** Key Attributes */
  keyAttributes?: InputMaybe<Array<KeyAttributeInput>>;
  /** Product media url */
  media: Scalars['String']['input'];
  /** meta id */
  metaId?: InputMaybe<Scalars['String']['input']>;
  /** Product name */
  name: Scalars['String']['input'];
  /** Product price */
  price?: InputMaybe<Scalars['Float']['input']>;
  /** Product SKU */
  sku: Scalars['String']['input'];
  /** Product slug */
  slug?: InputMaybe<Scalars['String']['input']>;
  /** Product status ENUM */
  status: Product_Status;
  /** Product test report attachment url */
  testReport?: InputMaybe<Scalars['String']['input']>;
  /** product variants */
  variants: Array<CreateVariantInputProduct>;
  /** Product volume */
  volume?: InputMaybe<Scalars['String']['input']>;
  /** Product weight */
  weight?: InputMaybe<Scalars['Float']['input']>;
  /** Product weight */
  weightUnit?: InputMaybe<Weight_Unit>;
};

export type CreateVariantInput = {
  /** Attributes */
  attributes?: InputMaybe<Array<AttributeInput>>;
  /** Variant available on */
  availableOn: Scalars['DateTime']['input'];
  /** Variant barcode */
  barcode?: InputMaybe<Scalars['String']['input']>;
  /** Variant certificate of origin */
  certificatesOfOrigin?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Variant discount price */
  discountPrice?: InputMaybe<Scalars['Float']['input']>;
  /** Variant expiry date */
  expiryDate?: InputMaybe<Scalars['DateTime']['input']>;
  /** Variant images */
  images?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Variant quantity */
  placeOfOrigin?: InputMaybe<Scalars['String']['input']>;
  /** Variant price */
  price: Scalars['Float']['input'];
  /** Variant price */
  productId: Scalars['String']['input'];
  /** Variant quantity */
  quantity: Scalars['Int']['input'];
  /** Variant selling price */
  sellingPrice?: InputMaybe<Scalars['Float']['input']>;
  /** Variant SKU */
  sku: Scalars['String']['input'];
  /** Variant status */
  status: Variant_Status;
  /** Variant weight */
  weight: Scalars['Float']['input'];
  /** Variant weight unit */
  weightUnit?: InputMaybe<Weight_Unit>;
};

export type CreateVariantInputProduct = {
  /** Attributes */
  attributes?: InputMaybe<Array<AttributeInput>>;
  /** Variant available on */
  availableOn: Scalars['DateTime']['input'];
  /** Variant barcode */
  barcode?: InputMaybe<Scalars['String']['input']>;
  /** Variant certificate of origin */
  certificatesOfOrigin?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Variant discount price */
  discountPrice?: InputMaybe<Scalars['Float']['input']>;
  /** Variant expiry date */
  expiryDate?: InputMaybe<Scalars['DateTime']['input']>;
  /** Variant images */
  images?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Variant quantity */
  placeOfOrigin?: InputMaybe<Scalars['String']['input']>;
  /** Variant price */
  price: Scalars['Float']['input'];
  /** Variant quantity */
  quantity: Scalars['Int']['input'];
  /** Variant selling price */
  sellingPrice?: InputMaybe<Scalars['Float']['input']>;
  /** Variant SKU */
  sku: Scalars['String']['input'];
  /** Variant status */
  status: Variant_Status;
  /** Variant weight */
  weight: Scalars['Float']['input'];
  /** Variant weight unit */
  weightUnit?: InputMaybe<Weight_Unit>;
};

export type Date_PropertyFilterInputType = {
  eq?: InputMaybe<Scalars['DateTime']['input']>;
  gt?: InputMaybe<Scalars['DateTime']['input']>;
  gte?: InputMaybe<Scalars['DateTime']['input']>;
  in?: InputMaybe<Array<Scalars['DateTime']['input']>>;
  lt?: InputMaybe<Scalars['DateTime']['input']>;
  lte?: InputMaybe<Scalars['DateTime']['input']>;
  ne?: InputMaybe<Scalars['DateTime']['input']>;
  nin?: InputMaybe<Array<Scalars['DateTime']['input']>>;
  regex?: InputMaybe<Scalars['DateTime']['input']>;
};

export type Float_PropertyFilterInputType = {
  eq?: InputMaybe<Scalars['Float']['input']>;
  gt?: InputMaybe<Scalars['Float']['input']>;
  gte?: InputMaybe<Scalars['Float']['input']>;
  in?: InputMaybe<Array<Scalars['Float']['input']>>;
  lt?: InputMaybe<Scalars['Float']['input']>;
  lte?: InputMaybe<Scalars['Float']['input']>;
  ne?: InputMaybe<Scalars['Float']['input']>;
  nin?: InputMaybe<Array<Scalars['Float']['input']>>;
  regex?: InputMaybe<Scalars['Float']['input']>;
};

export type KeyAttribute = {
  __typename?: 'KeyAttribute';
  option: Scalars['String']['output'];
  value: Scalars['String']['output'];
};

export type KeyAttributeInput = {
  option: Scalars['String']['input'];
  value: Scalars['String']['input'];
};

export enum Log_Type {
  CreateProduct = 'create_product',
  CreateVariant = 'create_variant',
  DeleteProduct = 'delete_product',
  DeleteVariant = 'delete_variant',
  UpdateProduct = 'update_product',
  UpdateVariant = 'update_variant'
}

export type Mutation = {
  __typename?: 'Mutation';
  createCategory: Categories;
  createLog: Logs;
  createProduct: Products;
  createVariant: Variants;
  removeCategory: Categories;
  removeVariant: Variants;
  updateCategory: Categories;
  updateProduct: Products;
  updateVariant: Variants;
};


export type MutationCreateCategoryArgs = {
  createCategoryInput: CreateCategoryInput;
};


export type MutationCreateLogArgs = {
  createLogInput: CreateLogInput;
};


export type MutationCreateProductArgs = {
  createProductInput: CreateProductInput;
};


export type MutationCreateVariantArgs = {
  createVariantInput: CreateVariantInput;
};


export type MutationRemoveCategoryArgs = {
  id: Scalars['String']['input'];
};


export type MutationRemoveVariantArgs = {
  id: Scalars['String']['input'];
};


export type MutationUpdateCategoryArgs = {
  updateCategoryInput: UpdateCategoryInput;
};


export type MutationUpdateProductArgs = {
  updateProductInput: UpdateProductInput;
};


export type MutationUpdateVariantArgs = {
  updateVariantInput: UpdateVariantInput;
};

export type Productcondition_PropertyFilterInputType = {
  eq?: InputMaybe<Product_Condition>;
  in?: InputMaybe<Array<Product_Condition>>;
  ne?: InputMaybe<Product_Condition>;
  nin?: InputMaybe<Array<Product_Condition>>;
};

export type Productstatus_PropertyFilterInputType = {
  eq?: InputMaybe<Product_Status>;
  in?: InputMaybe<Array<Product_Status>>;
  ne?: InputMaybe<Product_Status>;
  nin?: InputMaybe<Array<Product_Status>>;
};

export enum Product_Condition {
  New = 'new',
  Refurbished = 'refurbished',
  Used = 'used'
}

export enum Product_Status {
  Active = 'active',
  Draft = 'draft'
}

export type PaginationDto = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
};

export type ProductPaginate = {
  __typename?: 'ProductPaginate';
  docs: Array<Products>;
  hasNextPage: Scalars['Boolean']['output'];
  hasPrevPage: Scalars['Boolean']['output'];
  limit: Scalars['Int']['output'];
  nextPage?: Maybe<Scalars['Int']['output']>;
  offset?: Maybe<Scalars['Int']['output']>;
  page?: Maybe<Scalars['Int']['output']>;
  pagingCounter: Scalars['Int']['output'];
  prevPage?: Maybe<Scalars['Int']['output']>;
  totalDocs: Scalars['Int']['output'];
  totalPages: Scalars['Int']['output'];
};

export type ProductResolver_FindFilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  availableDate?: InputMaybe<Date_PropertyFilterInputType>;
  composition?: InputMaybe<String_PropertyFilterInputType>;
  condition?: InputMaybe<Productcondition_PropertyFilterInputType>;
  createdAt?: InputMaybe<Date_PropertyFilterInputType>;
  createdUserId?: InputMaybe<String_PropertyFilterInputType>;
  description?: InputMaybe<String_PropertyFilterInputType>;
  expiryDate?: InputMaybe<Date_PropertyFilterInputType>;
  images?: InputMaybe<String_PropertyFilterInputType>;
  isPhysical?: InputMaybe<Boolean_PropertyFilterInputType>;
  isReturnable?: InputMaybe<Boolean_PropertyFilterInputType>;
  media?: InputMaybe<String_PropertyFilterInputType>;
  metaId?: InputMaybe<String_PropertyFilterInputType>;
  name?: InputMaybe<String_PropertyFilterInputType>;
  price?: InputMaybe<Float_PropertyFilterInputType>;
  sku?: InputMaybe<String_PropertyFilterInputType>;
  slug?: InputMaybe<String_PropertyFilterInputType>;
  status?: InputMaybe<Productstatus_PropertyFilterInputType>;
  testReport?: InputMaybe<String_PropertyFilterInputType>;
  updatedAt?: InputMaybe<Date_PropertyFilterInputType>;
  volume?: InputMaybe<String_PropertyFilterInputType>;
  weight?: InputMaybe<Float_PropertyFilterInputType>;
  weightUnit?: InputMaybe<Weightunit_PropertyFilterInputType>;
};

export type ProductResolver_Find_FilterInputType = {
  _id?: InputMaybe<String_PropertyFilterInputType>;
  and?: InputMaybe<Array<ProductResolver_FindFilterInputType>>;
  availableDate?: InputMaybe<Date_PropertyFilterInputType>;
  composition?: InputMaybe<String_PropertyFilterInputType>;
  condition?: InputMaybe<Productcondition_PropertyFilterInputType>;
  createdAt?: InputMaybe<Date_PropertyFilterInputType>;
  createdUserId?: InputMaybe<String_PropertyFilterInputType>;
  description?: InputMaybe<String_PropertyFilterInputType>;
  expiryDate?: InputMaybe<Date_PropertyFilterInputType>;
  images?: InputMaybe<String_PropertyFilterInputType>;
  isPhysical?: InputMaybe<Boolean_PropertyFilterInputType>;
  isReturnable?: InputMaybe<Boolean_PropertyFilterInputType>;
  media?: InputMaybe<String_PropertyFilterInputType>;
  metaId?: InputMaybe<String_PropertyFilterInputType>;
  name?: InputMaybe<String_PropertyFilterInputType>;
  or?: InputMaybe<Array<ProductResolver_FindFilterInputType>>;
  price?: InputMaybe<Float_PropertyFilterInputType>;
  sku?: InputMaybe<String_PropertyFilterInputType>;
  slug?: InputMaybe<String_PropertyFilterInputType>;
  status?: InputMaybe<Productstatus_PropertyFilterInputType>;
  testReport?: InputMaybe<String_PropertyFilterInputType>;
  updatedAt?: InputMaybe<Date_PropertyFilterInputType>;
  volume?: InputMaybe<String_PropertyFilterInputType>;
  weight?: InputMaybe<Float_PropertyFilterInputType>;
  weightUnit?: InputMaybe<Weightunit_PropertyFilterInputType>;
};

export type Query = {
  __typename?: 'Query';
  countByMetaId: Scalars['Int']['output'];
  findCategoriesByIds: Array<Categories>;
  findCategoriesByParentIds: Array<Categories>;
  findManyProductsByIds: Array<Products>;
  findManyVariantsByIds: Array<Variants>;
  findManyVariantsByProductIds: Array<Variants>;
  findProducts: ProductPaginate;
  getAllCategories: Array<Categories>;
  getAllProductLogs: Array<ProductLogs>;
  getAllVariantLogs: Array<VariantLogs>;
  getAllVariants: Array<Variants>;
  getAllproducts: Array<Products>;
  getCategory: Categories;
  getProduct: Products;
  getProductLog: ProductLogs;
  getVariant: Variants;
  getVariantLog: VariantLogs;
};


export type QueryCountByMetaIdArgs = {
  id: Scalars['String']['input'];
};


export type QueryFindCategoriesByIdsArgs = {
  ids: Array<Scalars['String']['input']>;
};


export type QueryFindCategoriesByParentIdsArgs = {
  ids: Array<Scalars['String']['input']>;
};


export type QueryFindManyProductsByIdsArgs = {
  ids: Array<Scalars['String']['input']>;
};


export type QueryFindManyVariantsByIdsArgs = {
  ids: Array<Scalars['String']['input']>;
};


export type QueryFindManyVariantsByProductIdsArgs = {
  ids: Array<Scalars['String']['input']>;
};


export type QueryFindProductsArgs = {
  pagination?: InputMaybe<PaginationDto>;
  where?: InputMaybe<ProductResolver_Find_FilterInputType>;
};


export type QueryGetCategoryArgs = {
  id: Scalars['String']['input'];
};


export type QueryGetProductArgs = {
  id: Scalars['String']['input'];
};


export type QueryGetProductLogArgs = {
  id: Scalars['String']['input'];
};


export type QueryGetVariantArgs = {
  id: Scalars['String']['input'];
};


export type QueryGetVariantLogArgs = {
  id: Scalars['String']['input'];
};

export type String_PropertyFilterInputType = {
  eq?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  ne?: InputMaybe<Scalars['String']['input']>;
  nin?: InputMaybe<Array<Scalars['String']['input']>>;
  regex?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateCategoryInput = {
  _id: Scalars['String']['input'];
  /** Category code */
  code?: InputMaybe<Scalars['String']['input']>;
  /** is active category  */
  isActive?: InputMaybe<Scalars['Boolean']['input']>;
  /** is parent category  */
  isParent?: InputMaybe<Scalars['Boolean']['input']>;
  /** Category name */
  name?: InputMaybe<Scalars['String']['input']>;
  /** Parent category id */
  parentId?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateProductInput = {
  _id: Scalars['String']['input'];
  /** Product available date */
  availableDate?: InputMaybe<Scalars['DateTime']['input']>;
  /** Product composition */
  composition?: InputMaybe<Scalars['String']['input']>;
  /** Product condition */
  condition?: InputMaybe<Product_Condition>;
  createdUserId?: InputMaybe<Scalars['String']['input']>;
  /** Product description */
  description?: InputMaybe<Scalars['String']['input']>;
  /** Product expiry date */
  expiryDate?: InputMaybe<Scalars['DateTime']['input']>;
  /** Product images */
  images?: InputMaybe<Array<Scalars['String']['input']>>;
  /** is physical product */
  isPhysical?: InputMaybe<Scalars['Boolean']['input']>;
  /** is returnable product */
  isReturnable?: InputMaybe<Scalars['Boolean']['input']>;
  /** Key Attributes */
  keyAttributes?: InputMaybe<Array<KeyAttributeInput>>;
  /** Product media url */
  media?: InputMaybe<Scalars['String']['input']>;
  /** meta id */
  metaId?: InputMaybe<Scalars['String']['input']>;
  /** Product name */
  name?: InputMaybe<Scalars['String']['input']>;
  /** Product price */
  price?: InputMaybe<Scalars['Float']['input']>;
  /** Product SKU */
  sku?: InputMaybe<Scalars['String']['input']>;
  /** Product slug */
  slug?: InputMaybe<Scalars['String']['input']>;
  /** Product status ENUM */
  status?: InputMaybe<Product_Status>;
  /** Product test report attachment url */
  testReport?: InputMaybe<Scalars['String']['input']>;
  /** product variants */
  variants?: InputMaybe<Array<CreateVariantInputProduct>>;
  /** Product volume */
  volume?: InputMaybe<Scalars['String']['input']>;
  /** Product weight */
  weight?: InputMaybe<Scalars['Float']['input']>;
  /** Product weight */
  weightUnit?: InputMaybe<Weight_Unit>;
};

export type UpdateVariantInput = {
  _id: Scalars['String']['input'];
  /** Variant available on */
  availableOn?: InputMaybe<Scalars['DateTime']['input']>;
  /** Variant barcode */
  barcode?: InputMaybe<Scalars['String']['input']>;
  /** Variant certificate of origin */
  certificatesOfOrigin?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Variant discount price */
  discountPrice?: InputMaybe<Scalars['Float']['input']>;
  /** Variant expiry date */
  expiryDate?: InputMaybe<Scalars['DateTime']['input']>;
  /** Variant images */
  images?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Variant quantity */
  placeOfOrigin?: InputMaybe<Scalars['String']['input']>;
  /** Variant price */
  price?: InputMaybe<Scalars['Float']['input']>;
  /** Variant price */
  productId?: InputMaybe<Scalars['String']['input']>;
  /** Variant quantity */
  quantity?: InputMaybe<Scalars['Int']['input']>;
  /** Variant selling price */
  sellingPrice?: InputMaybe<Scalars['Float']['input']>;
  /** Variant SKU */
  sku?: InputMaybe<Scalars['String']['input']>;
  /** Variant status */
  status?: InputMaybe<Variant_Status>;
  /** Variant weight */
  weight?: InputMaybe<Scalars['Float']['input']>;
  /** Variant weight unit */
  weightUnit?: InputMaybe<Weight_Unit>;
};

export enum Variant_Status {
  Available = 'available',
  Unavailable = 'unavailable'
}

export type Weightunit_PropertyFilterInputType = {
  eq?: InputMaybe<Weight_Unit>;
  in?: InputMaybe<Array<Weight_Unit>>;
  ne?: InputMaybe<Weight_Unit>;
  nin?: InputMaybe<Array<Weight_Unit>>;
};

export enum Weight_Unit {
  Gram = 'gram',
  Kilogram = 'kilogram',
  Ounce = 'ounce',
  Pound = 'pound'
}

export type Categories = {
  __typename?: 'categories';
  /** Category id */
  _id: Scalars['String']['output'];
  children?: Maybe<Array<Categories>>;
  /** Category code */
  code: Scalars['String']['output'];
  /** createdAt */
  createdAt: Scalars['DateTime']['output'];
  /** is active category  */
  isActive: Scalars['Boolean']['output'];
  /** is parent category  */
  isParent: Scalars['Boolean']['output'];
  /** Category name */
  name: Scalars['String']['output'];
  /** Parent category id */
  parentId?: Maybe<Scalars['String']['output']>;
  /** updatedAt */
  updatedAt: Scalars['DateTime']['output'];
};

export type Logs = {
  __typename?: 'logs';
  /** Log id */
  _id: Scalars['String']['output'];
  /** createdAt */
  createdAt: Scalars['DateTime']['output'];
  /** Employee id */
  employeeId: Scalars['String']['output'];
  /** ip address */
  ipAddress: Scalars['String']['output'];
  /** Product id */
  productId?: Maybe<Scalars['String']['output']>;
  /** log type */
  type: Log_Type;
  /** updatedAt */
  updatedAt: Scalars['DateTime']['output'];
  /** Variant id */
  variantId?: Maybe<Scalars['String']['output']>;
};

export type ProductLogs = {
  __typename?: 'productLogs';
  /** Log id */
  _id: Scalars['String']['output'];
  /** createdAt */
  createdAt: Scalars['DateTime']['output'];
  /** Employee id */
  employeeId: Scalars['String']['output'];
  /** ip address */
  ipAddress: Scalars['String']['output'];
  /** Product */
  product: Products;
  /** Product after changed */
  productAfter?: Maybe<Products>;
  /** log type */
  type: Log_Type;
  /** updatedAt */
  updatedAt: Scalars['DateTime']['output'];
};

export type Products = {
  __typename?: 'products';
  /** Product id */
  _id: Scalars['String']['output'];
  /** Product available date */
  availableDate: Scalars['DateTime']['output'];
  /** composition */
  composition?: Maybe<Scalars['String']['output']>;
  /** Product condition */
  condition?: Maybe<Product_Condition>;
  /** createdAt */
  createdAt: Scalars['DateTime']['output'];
  createdUserId?: Maybe<Scalars['String']['output']>;
  /** Product description */
  description: Scalars['String']['output'];
  /** Product expiry date */
  expiryDate?: Maybe<Scalars['DateTime']['output']>;
  /** Product images */
  images?: Maybe<Array<Scalars['String']['output']>>;
  /** is physical product */
  isPhysical?: Maybe<Scalars['Boolean']['output']>;
  /** is returnable product */
  isReturnable?: Maybe<Scalars['Boolean']['output']>;
  /** Key Attributes */
  keyAttributes?: Maybe<Array<KeyAttribute>>;
  /** Product media url */
  media?: Maybe<Scalars['String']['output']>;
  /** meta id */
  metaId?: Maybe<Scalars['String']['output']>;
  /** Product name */
  name: Scalars['String']['output'];
  /** Product price */
  price?: Maybe<Scalars['Float']['output']>;
  /** Product SKU */
  sku: Scalars['String']['output'];
  /** Product slug */
  slug?: Maybe<Scalars['String']['output']>;
  /** Product status ENUM */
  status: Product_Status;
  /** test report attachment url */
  testReport?: Maybe<Scalars['String']['output']>;
  /** updatedAt */
  updatedAt: Scalars['DateTime']['output'];
  variants: Array<Variants>;
  /** volume */
  volume?: Maybe<Scalars['String']['output']>;
  /** Product weight */
  weight?: Maybe<Scalars['Float']['output']>;
  /** Product weight unit */
  weightUnit?: Maybe<Weight_Unit>;
};

export type VariantLogs = {
  __typename?: 'variantLogs';
  /** Log id */
  _id: Scalars['String']['output'];
  /** createdAt */
  createdAt: Scalars['DateTime']['output'];
  /** Employee id */
  employeeId: Scalars['String']['output'];
  /** ip address */
  ipAddress: Scalars['String']['output'];
  /** log type */
  type: Log_Type;
  /** updatedAt */
  updatedAt: Scalars['DateTime']['output'];
  /** Variant */
  variant: Variants;
  /** Variant after changed */
  variantAfter?: Maybe<Variants>;
};

export type Variants = {
  __typename?: 'variants';
  /** Variant id */
  _id: Scalars['String']['output'];
  /** Attributes */
  attributes?: Maybe<Array<Attribute>>;
  /** Variant available on */
  availableOn: Scalars['DateTime']['output'];
  /** Variant barcode */
  barcode?: Maybe<Scalars['String']['output']>;
  /** Variant certificate of origin */
  certificatesOfOrigin?: Maybe<Array<Scalars['String']['output']>>;
  /** createdAt */
  createdAt: Scalars['DateTime']['output'];
  /** Variant discount price */
  discountPrice?: Maybe<Scalars['Float']['output']>;
  /** Variant expiry date */
  expiryDate?: Maybe<Scalars['DateTime']['output']>;
  /** Variant images */
  images?: Maybe<Array<Scalars['String']['output']>>;
  /** Variant quantity */
  placeOfOrigin?: Maybe<Scalars['String']['output']>;
  /** Variant price */
  price: Scalars['Float']['output'];
  product?: Maybe<Products>;
  /** productId */
  productId: Scalars['String']['output'];
  /** Variant quantity */
  quantity: Scalars['Int']['output'];
  /** Variant selling price */
  sellingPrice?: Maybe<Scalars['Float']['output']>;
  /** Variant SKU */
  sku: Scalars['String']['output'];
  /** Variant status */
  status: Variant_Status;
  /** updatedAt */
  updatedAt: Scalars['DateTime']['output'];
  /** Variant weight */
  weight: Scalars['Float']['output'];
  /** Variant weight unit */
  weightUnit?: Maybe<Weight_Unit>;
};

export type CreateProductsMutationVariables = Exact<{
  createProductInput: CreateProductInput;
}>;


export type CreateProductsMutation = { __typename?: 'Mutation', createProduct: { __typename?: 'products', _id: string, variants: Array<{ __typename?: 'variants', attributes?: Array<{ __typename?: 'Attribute', isActive: boolean, option: string, value: string }> | null }> } };

export type GetAllProductsQueryVariables = Exact<{
  where?: InputMaybe<ProductResolver_Find_FilterInputType>;
}>;


export type GetAllProductsQuery = { __typename?: 'Query', findProducts: { __typename?: 'ProductPaginate', docs: Array<{ __typename?: 'products', _id: string, name: string, metaId?: string | null, media?: string | null, description: string, status: Product_Status, images?: Array<string> | null, price?: number | null, sku: string, isPhysical?: boolean | null, slug?: string | null, condition?: Product_Condition | null, availableDate: any, expiryDate?: any | null, isReturnable?: boolean | null, weight?: number | null, weightUnit?: Weight_Unit | null, createdAt: any, updatedAt: any, composition?: string | null, volume?: string | null, createdUserId?: string | null, variants: Array<{ __typename?: 'variants', _id: string, price: number, quantity: number, productId: string, placeOfOrigin?: string | null, certificatesOfOrigin?: Array<string> | null, sku: string, images?: Array<string> | null, status: Variant_Status, weight: number, weightUnit?: Weight_Unit | null, sellingPrice?: number | null, discountPrice?: number | null, availableOn: any, expiryDate?: any | null, barcode?: string | null, createdAt: any, updatedAt: any, attributes?: Array<{ __typename?: 'Attribute', option: string, value: string, isActive: boolean }> | null }> }> } };

export type GetVariantQueryVariables = Exact<{
  getVariantId: Scalars['String']['input'];
}>;


export type GetVariantQuery = { __typename?: 'Query', getVariant: { __typename?: 'variants', _id: string, price: number, quantity: number, productId: string, placeOfOrigin?: string | null, certificatesOfOrigin?: Array<string> | null, sku: string, images?: Array<string> | null, status: Variant_Status, weight: number, weightUnit?: Weight_Unit | null, sellingPrice?: number | null, discountPrice?: number | null, availableOn: any, expiryDate?: any | null, barcode?: string | null, createdAt: any, updatedAt: any, attributes?: Array<{ __typename?: 'Attribute', option: string, value: string, isActive: boolean }> | null } };

export type GetProductQueryVariables = Exact<{
  getProductId: Scalars['String']['input'];
}>;


export type GetProductQuery = { __typename?: 'Query', getProduct: { __typename?: 'products', _id: string, name: string, metaId?: string | null, media?: string | null, description: string, status: Product_Status, images?: Array<string> | null, price?: number | null, sku: string, isPhysical?: boolean | null, slug?: string | null, condition?: Product_Condition | null, availableDate: any, expiryDate?: any | null, isReturnable?: boolean | null, weight?: number | null, weightUnit?: Weight_Unit | null, composition?: string | null, volume?: string | null, createdAt: any, updatedAt: any, variants: Array<{ __typename?: 'variants', _id: string, price: number, quantity: number, productId: string, placeOfOrigin?: string | null, certificatesOfOrigin?: Array<string> | null, sku: string, images?: Array<string> | null, status: Variant_Status, weight: number, weightUnit?: Weight_Unit | null, sellingPrice?: number | null, discountPrice?: number | null, availableOn: any, expiryDate?: any | null, barcode?: string | null, createdAt: any, updatedAt: any, attributes?: Array<{ __typename?: 'Attribute', option: string, value: string, isActive: boolean }> | null }> } };


export const CreateProductsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CreateProducts"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"createProductInput"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"CreateProductInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createProduct"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"createProductInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"createProductInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"variants"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"isActive"}},{"kind":"Field","name":{"kind":"Name","value":"option"}},{"kind":"Field","name":{"kind":"Name","value":"value"}}]}}]}}]}}]}}]} as unknown as DocumentNode<CreateProductsMutation, CreateProductsMutationVariables>;
export const GetAllProductsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetAllProducts"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ProductResolver_Find_FilterInputType"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"findProducts"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"docs"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"metaId"}},{"kind":"Field","name":{"kind":"Name","value":"media"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"status"}},{"kind":"Field","name":{"kind":"Name","value":"images"}},{"kind":"Field","name":{"kind":"Name","value":"price"}},{"kind":"Field","name":{"kind":"Name","value":"sku"}},{"kind":"Field","name":{"kind":"Name","value":"isPhysical"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"condition"}},{"kind":"Field","name":{"kind":"Name","value":"availableDate"}},{"kind":"Field","name":{"kind":"Name","value":"expiryDate"}},{"kind":"Field","name":{"kind":"Name","value":"isReturnable"}},{"kind":"Field","name":{"kind":"Name","value":"weight"}},{"kind":"Field","name":{"kind":"Name","value":"weightUnit"}},{"kind":"Field","name":{"kind":"Name","value":"variants"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"price"}},{"kind":"Field","name":{"kind":"Name","value":"quantity"}},{"kind":"Field","name":{"kind":"Name","value":"productId"}},{"kind":"Field","name":{"kind":"Name","value":"placeOfOrigin"}},{"kind":"Field","name":{"kind":"Name","value":"certificatesOfOrigin"}},{"kind":"Field","name":{"kind":"Name","value":"sku"}},{"kind":"Field","name":{"kind":"Name","value":"images"}},{"kind":"Field","name":{"kind":"Name","value":"status"}},{"kind":"Field","name":{"kind":"Name","value":"weight"}},{"kind":"Field","name":{"kind":"Name","value":"weightUnit"}},{"kind":"Field","name":{"kind":"Name","value":"sellingPrice"}},{"kind":"Field","name":{"kind":"Name","value":"discountPrice"}},{"kind":"Field","name":{"kind":"Name","value":"availableOn"}},{"kind":"Field","name":{"kind":"Name","value":"expiryDate"}},{"kind":"Field","name":{"kind":"Name","value":"barcode"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"option"}},{"kind":"Field","name":{"kind":"Name","value":"value"}},{"kind":"Field","name":{"kind":"Name","value":"isActive"}}]}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}},{"kind":"Field","name":{"kind":"Name","value":"composition"}},{"kind":"Field","name":{"kind":"Name","value":"volume"}},{"kind":"Field","name":{"kind":"Name","value":"createdUserId"}}]}}]}}]}}]} as unknown as DocumentNode<GetAllProductsQuery, GetAllProductsQueryVariables>;
export const GetVariantDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetVariant"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"getVariantId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"getVariant"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"getVariantId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"price"}},{"kind":"Field","name":{"kind":"Name","value":"quantity"}},{"kind":"Field","name":{"kind":"Name","value":"productId"}},{"kind":"Field","name":{"kind":"Name","value":"placeOfOrigin"}},{"kind":"Field","name":{"kind":"Name","value":"certificatesOfOrigin"}},{"kind":"Field","name":{"kind":"Name","value":"sku"}},{"kind":"Field","name":{"kind":"Name","value":"images"}},{"kind":"Field","name":{"kind":"Name","value":"status"}},{"kind":"Field","name":{"kind":"Name","value":"weight"}},{"kind":"Field","name":{"kind":"Name","value":"weightUnit"}},{"kind":"Field","name":{"kind":"Name","value":"sellingPrice"}},{"kind":"Field","name":{"kind":"Name","value":"discountPrice"}},{"kind":"Field","name":{"kind":"Name","value":"availableOn"}},{"kind":"Field","name":{"kind":"Name","value":"expiryDate"}},{"kind":"Field","name":{"kind":"Name","value":"barcode"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"option"}},{"kind":"Field","name":{"kind":"Name","value":"value"}},{"kind":"Field","name":{"kind":"Name","value":"isActive"}}]}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}}]}}]} as unknown as DocumentNode<GetVariantQuery, GetVariantQueryVariables>;
export const GetProductDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetProduct"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"getProductId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"getProduct"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"getProductId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"metaId"}},{"kind":"Field","name":{"kind":"Name","value":"media"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"status"}},{"kind":"Field","name":{"kind":"Name","value":"images"}},{"kind":"Field","name":{"kind":"Name","value":"price"}},{"kind":"Field","name":{"kind":"Name","value":"sku"}},{"kind":"Field","name":{"kind":"Name","value":"isPhysical"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"condition"}},{"kind":"Field","name":{"kind":"Name","value":"availableDate"}},{"kind":"Field","name":{"kind":"Name","value":"expiryDate"}},{"kind":"Field","name":{"kind":"Name","value":"isReturnable"}},{"kind":"Field","name":{"kind":"Name","value":"weight"}},{"kind":"Field","name":{"kind":"Name","value":"weightUnit"}},{"kind":"Field","name":{"kind":"Name","value":"variants"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"price"}},{"kind":"Field","name":{"kind":"Name","value":"quantity"}},{"kind":"Field","name":{"kind":"Name","value":"productId"}},{"kind":"Field","name":{"kind":"Name","value":"placeOfOrigin"}},{"kind":"Field","name":{"kind":"Name","value":"certificatesOfOrigin"}},{"kind":"Field","name":{"kind":"Name","value":"sku"}},{"kind":"Field","name":{"kind":"Name","value":"images"}},{"kind":"Field","name":{"kind":"Name","value":"status"}},{"kind":"Field","name":{"kind":"Name","value":"weight"}},{"kind":"Field","name":{"kind":"Name","value":"weightUnit"}},{"kind":"Field","name":{"kind":"Name","value":"sellingPrice"}},{"kind":"Field","name":{"kind":"Name","value":"discountPrice"}},{"kind":"Field","name":{"kind":"Name","value":"availableOn"}},{"kind":"Field","name":{"kind":"Name","value":"expiryDate"}},{"kind":"Field","name":{"kind":"Name","value":"barcode"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"option"}},{"kind":"Field","name":{"kind":"Name","value":"value"}},{"kind":"Field","name":{"kind":"Name","value":"isActive"}}]}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"composition"}},{"kind":"Field","name":{"kind":"Name","value":"volume"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}}]}}]} as unknown as DocumentNode<GetProductQuery, GetProductQueryVariables>;