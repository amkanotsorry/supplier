import { graphql } from '../../generated'

export const CreateProductMutation = graphql(`
	mutation CreateProducts($createProductInput: CreateProductInput!) {
		createProduct(createProductInput: $createProductInput) {
			_id
			variants {
				attributes {
					isActive
					option
					value
				}
			}
		}
	}
`)
