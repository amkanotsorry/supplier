import { graphql } from '../../generated'

export const GetAllProducts = graphql(`
	query GetAllProducts($where: ProductResolver_Find_FilterInputType) {
		findProducts(where: $where) {
			docs {
				_id
				name
				metaId
				media
				description
				status
				images
				price
				sku
				isPhysical
				slug
				condition
				availableDate
				expiryDate
				isReturnable
				weight
				weightUnit
				variants {
					_id
					price
					quantity
					productId
					placeOfOrigin
					certificatesOfOrigin
					sku
					images
					status
					weight
					weightUnit
					sellingPrice
					discountPrice
					availableOn
					expiryDate
					barcode
					attributes {
						option
						value
						isActive
					}
					createdAt
					updatedAt
				}
				createdAt
				updatedAt
				composition
				volume
				createdUserId
			}
		}
	}
`)
