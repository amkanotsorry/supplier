import { graphql } from '../../generated'

export const GetVariantById = graphql(`
	query GetVariant($getVariantId: String!) {
		getVariant(id: $getVariantId) {
			_id
			price
			quantity
			productId
			placeOfOrigin
			certificatesOfOrigin
			sku
			images
			status
			weight
			weightUnit
			sellingPrice
			discountPrice
			availableOn
			expiryDate
			barcode
			attributes {
				option
				value
				isActive
			}
			createdAt
			updatedAt
		}
	}
`)

export const GetProductById = graphql(`
	query GetProduct($getProductId: String!) {
		getProduct(id: $getProductId) {
			_id
			name
			metaId
			media
			description
			status
			images
			price
			sku
			isPhysical
			slug
			condition
			availableDate
			expiryDate
			isReturnable
			weight
			weightUnit
			variants {
				_id
				price
				quantity
				productId
				placeOfOrigin
				certificatesOfOrigin
				sku
				images
				status
				weight
				weightUnit
				sellingPrice
				discountPrice
				availableOn
				expiryDate
				barcode
				attributes {
					option
					value
					isActive
				}
				createdAt
				updatedAt
			}
			composition
			volume
			createdAt
			updatedAt
		}
	}
`)
