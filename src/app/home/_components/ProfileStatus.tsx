import { CheckContainedIcon, Star02Icon, StarfillIcon } from '@/src/assets/icons'
import Text from '@/src/components/text'
import React from 'react'
import { Review_Status } from '../../api/generated/graphql'
import { cn } from '@/src/lib/utils'
import { Check, RefreshCwIcon } from 'lucide-react'

interface Props {
	status: Review_Status
}

const ProfileStatus: React.FC<Props> = ({ status }) => {
	let title = ''
	let textColor = ''
	let backgroundColor = ''
	let Icon = <></>

	switch (status) {
		case Review_Status.Approved:
			title = 'Verified'
			textColor = 'text-success-700'
			backgroundColor = 'bg-success-300'
			Icon = <CheckContainedIcon className={cn('h-4 w-4', textColor)} />
			break
		case Review_Status.Pending:
			title = 'Pending'
			textColor = 'text-warning-400'
			backgroundColor = 'bg-warning-100'
			Icon = <CheckContainedIcon className={cn('h-4 w-4', textColor)} />
			break
		case Review_Status.Declined:
			title = 'Returned'
			textColor = 'text-error-700'
			backgroundColor = 'bg-error-300'
			Icon = <RefreshCwIcon className={cn('h-4 w-4', textColor)} />
			break
		case Review_Status.Updated:
			title = 'Updated'
			textColor = 'text-info-700'
			backgroundColor = 'bg-info-300'
			Icon = <Check className={cn('h-4 w-4', textColor)} />
	}

	return (
		<div className={cn('flex rounded-[32px] gap-1 px-2 items-center', backgroundColor)}>
			{Icon}
			<Text variant="smMedium" className={cn(textColor)}>
				{title}
			</Text>
		</div>
	)
}

export default ProfileStatus
