import { Star02Icon, StarfillIcon } from '@/src/assets/icons'
import React from 'react'

interface Props {
	rating: Number
}

const RatingStars: React.FC<Props> = ({ rating = 0 }) => {
	rating = Math.min(Number(rating), 5)

	return (
		<div className="inline-flex gap-1 items-center">
			{Array.from(Array(rating), (e, i) => {
				return <StarfillIcon key={`StarfillIcon${i}`} className="h-4 w-4 text-warning-700" />
			})}
			{Array.from(Array(5 - Number(rating)), (e, i) => {
				return <Star02Icon key={`Star02Icon${i}`} className="h-4 w-4 text-support" />
			})}
		</div>
	)
}

export default RatingStars
