import { EditContainer } from '@/src/assets/icons'

import locationImage from '@/src/assets/images/location.png'
import Text from '@/src/components/text'
import { Image } from '@nextui-org/react'
import { FC } from 'react'

type Props = {}

const BranchItem: FC = () => {
	return (
		<div className="flex flex-row rounded-[12px] p-4 gap-4 bg-lightgray-100">
			<div className="flex justify-center items-center">
				<Image src={locationImage.src} alt="Profile imaaage" width={92} height={92} />
			</div>

			<div className="flex flex-col gap-2 flex-1">
				<Text variant="mdMedium">Branch 1</Text>
				<div className="flex flex-col gap-1">
					<Text variant="smRegular" color="brandSupportDarkGrey700">
						Ulaanbaatar, Sukhbaatar district, 1-r khoroo, Central Park Mongolia
					</Text>
					<Text variant="smRegular" color="brandSupportDarkGrey700">
						Phone number: (+976) 90095611
					</Text>
				</div>
			</div>

			<div className="flex justify-center">
				<EditContainer className="text-primary h-[20px] w-[20px]" />
			</div>
		</div>
	)
}

export default BranchItem
