import ProfileImage from '@/src/assets/images/avatar.png'
import { getServerSession } from 'next-auth'
import Image from 'next/image'
import React from 'react'
import LogoutButton from './logout-button'
import PageHeaderLeft from './page-header-left'
import { authOptions } from '@/src/app/api/auth/[...nextauth]/authOptions'

interface Props {}

const PageHeader: React.FC<Props> = async () => {
	const session = await getServerSession(authOptions)

	return (
		<div className="w-full h-[56px] flex flex-shrink-0 justify-between items-center px-4 border-b">
			<PageHeaderLeft />

			<div className="flex gap-2 items-center">
				<div className="w-9 h-9 bg-primary rounded-full flex items-center justify-center text-white">{session?.user.name?.slice(0, 1)}</div>

				<div>
					<div className="text-sm font-medium leading-5">{session?.user.name || 'Nameless User'}</div>
					<div className="text-xs text-support">Admin</div>
				</div>

				<LogoutButton />
			</div>
		</div>
	)
}

export default PageHeader
