'use client'

import useEntity from '@/src/app/hooks/use-entity'
import { Button, Dropdown, DropdownItem, DropdownMenu, DropdownSection, DropdownTrigger, User } from '@nextui-org/react'
import { ChevronDown, PlusIcon } from 'lucide-react'
import { signOut } from 'next-auth/react'
import Link from 'next/link'
import React from 'react'

interface Props {}

const LogoutButton: React.FC<Props> = () => {
	const { entity } = useEntity({})

	return (
		<Dropdown
			showArrow
			radius="sm"
			classNames={{
				base: 'before:bg-default-200', // change arrow background
				content: 'p-0 border-small border-divider bg-background'
			}}>
			<DropdownTrigger>
				<button>
					<ChevronDown />
				</button>
			</DropdownTrigger>
			<DropdownMenu
				aria-label="Custom item styles"
				disabledKeys={['profile']}
				className="p-3"
				itemClasses={{
					base: [
						'rounded-md',
						'text-default-500',
						'transition-opacity',
						'data-[hover=true]:text-foreground',
						'data-[hover=true]:bg-default-100',
						'dark:data-[hover=true]:bg-default-50',
						'data-[selectable=true]:focus:bg-default-50',
						'data-[pressed=true]:opacity-70',
						'data-[focus-visible=true]:ring-default-500'
					]
				}}>
				<DropdownSection aria-label="Profile & Actions" showDivider>
					<DropdownItem key="dashboard">
						{entity?.entitySupplierId ? (
							<Link href={`/home/profile/${entity.entitySupplierId}`} className={`cursor-pointer whitespace-nowrap transition duration-300`}>
								Business profile
							</Link>
						) : (
							<div className={` whitespace-nowrap transition duration-300`}>Business profile</div>
						)}
					</DropdownItem>
					<DropdownItem key="quick_search" shortcut="⌘K">
						Quick search
					</DropdownItem>
				</DropdownSection>

				<DropdownSection aria-label="Help & Feedback">
					<DropdownItem className="text-danger-400" onClick={() => signOut()} key="logout">
						Log Out
					</DropdownItem>
				</DropdownSection>
			</DropdownMenu>
		</Dropdown>
	)
}

export default LogoutButton
