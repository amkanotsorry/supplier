'use client'

import { usePathname } from 'next/navigation'
import React from 'react'

const PageHeaderLeft = () => {
	const pathname = usePathname()

	switch (pathname) {
		case '/home/product/create':
			return <div className="font-semibold">Product Create</div>
		default:
			return <div className="text-lg font-semibold leading-6">Business profile</div>
	}
}

export default PageHeaderLeft
