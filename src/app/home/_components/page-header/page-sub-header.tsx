'use client'

import { cn } from '@/src/lib/utils'
import { ArrowLeftIcon } from 'lucide-react'
import { useRouter } from 'next/navigation'
import React from 'react'
import { ClassNameValue } from 'tailwind-merge'

interface Props {
	title: string
	wrapper?: ClassNameValue
}

const PageSubHeader: React.FC<Props> = ({ title, wrapper }) => {
	const router = useRouter()
	return (
		<div className={cn('flex h-14 gap-4 p-4 items-center border-b', wrapper)}>
			<ArrowLeftIcon className="cursor-pointer" width={20} height={20} onClick={router.back} />
			<span className="text-base font-semibold">{title}</span>
		</div>
	)
}

export default PageSubHeader
