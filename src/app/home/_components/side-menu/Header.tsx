'use client'
import { LogoIcon } from '@/src/assets/icons'
import { FC } from 'react'

type Props = {
	onClick?: () => void
	isOpen?: Boolean
}

const SideMenuHeader: FC<Props> = ({ onClick, isOpen = true }) => {
	return (
		<div className="flex items-center justify-between p-4">
			{isOpen && <LogoIcon />}

			<div onClick={onClick} className={`${!isOpen && 'rotate-180'} transition-all duration-300 cursor-pointer p-2 bg-primary-foreground rounded-lg`}>
				<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" fill="none">
					<path
						d="M9.9 15.4001L5.5 11.0001L9.9 6.6001M16.5 15.4001L12.1 11.0001L16.5 6.6001"
						stroke={'#22577A'}
						stroke-width="2"
						stroke-linecap="round"
						stroke-linejoin="round"
					/>
				</svg>
			</div>
		</div>
	)
}

export default SideMenuHeader
