import { Home02Icon } from '@/src/assets/icons'
import { ActionTooltip } from '@/src/components/ui/action-tooltip'
import React from 'react'

interface Props {
	open: boolean
	title: string
	selected: boolean
	Icon: any
}

const MenuItem: React.FC<Props> = ({ open, title, selected, Icon }) => {
	return (
		<ActionTooltip disabled={open} label={title} side="right" align="start">
			<div className={`cursor-pointer flex items-center p-2 gap-3 rounded-xl ${selected && 'bg-primary-foreground'}`}>
				<Icon className={`${selected ? 'text-primary' : 'text-black'} h-[22px] w-[22px]`} />

				{open && <div className={`${selected && 'text-primary'} text-sm font-semibold leading-5`}>{title}</div>}
			</div>
		</ActionTooltip>
	)
}

export default MenuItem
