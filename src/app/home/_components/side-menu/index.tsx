'use client'

import { useState } from 'react'
import SideMenuHeader from './Header'
import EntityInfo from './EntityInfo'
import MenuSearch from './MenuSearch'
import MenuItem from './MenuItem'
import { BarChartSquareUp01Icon, BarGroup03Icon, Bell02Icon, BoxIcon, Home02Icon, MessageText01Icon, UsersProfiles02Icon } from '@/src/assets/icons'

const SideMenu = () => {
	const [open, setOpen] = useState(true)

	return (
		<div className={`duration-300 ${open ? 'w-[300px]' : 'w-[70px]'}`}>
			<div className="h-screen flex flex-col border-r">
				<SideMenuHeader isOpen={open} onClick={() => setOpen(!open)} />

				<EntityInfo open={open} />

				<MenuSearch open={open} />

				<div className="flex flex-col px-4 py-2 gap-2">
					<MenuItem Icon={Home02Icon} open={open} title="Get started" selected={true} />
					<MenuItem Icon={BarGroup03Icon} open={open} title="Dashboard" selected={false} />
					<MenuItem Icon={BoxIcon} open={open} title="Commodity Management" selected={false} />
					<MenuItem Icon={BarChartSquareUp01Icon} open={open} title="Trades" selected={false} />
					<MenuItem Icon={Bell02Icon} open={open} title="Notification" selected={false} />
					<MenuItem Icon={UsersProfiles02Icon} open={open} title="Customers" selected={false} />
					<MenuItem Icon={MessageText01Icon} open={open} title="BID room" selected={false} />
				</div>
			</div>
		</div>
	)
}

export default SideMenu
