'use server'

import { revalidatePath } from 'next/cache'
import {
	CreateOfferDocument,
	CreateOfferDraftDocument,
	CreateOfferDraftInput,
	CreateOfferInput,
	GetEntityByIdDocument,
	GetEntityByIdQuery
} from '../api/generated/graphql'
import { getClient } from '../api/graphql/client'

const client = getClient()

export type QueryEntityType = Extract<GetEntityByIdQuery['entity'], any>

export const getEntityById = async (entityId: string): Promise<{ entity: QueryEntityType | null; error: string | null }> => {
	try {
		const { data } = await client.query({
			query: GetEntityByIdDocument,
			variables: {
				entityId
			}
		})

		return { entity: data.entity, error: null }
	} catch (error) {
		return { entity: null, error: JSON.stringify(error, null, 2) }
	}
}

export const createOffer = async (variables: { createOfferInput: CreateOfferInput }) => {
	try {
		const { data } = await client.mutate({
			mutation: CreateOfferDocument,
			variables
		})

		revalidatePath('/home/place-offer', 'page')
		return { offer: data?.createOffer, error: null }
	} catch (error) {
		return { offer: null, error: JSON.stringify(error, null, 2) }
	}
}

export const draftOffer = async (variables: { createOfferInput: CreateOfferDraftInput }) => {
	try {
		const { data } = await client.mutate({
			mutation: CreateOfferDraftDocument,
			variables: {
				createOfferDraftInput: variables.createOfferInput
			}
		})

		revalidatePath('/home/place-offer', 'page')
		return { offer: data?.createOfferDraft, error: null }
	} catch (error) {
		return { offer: null, error: JSON.stringify(error, null, 2) }
	}
}
