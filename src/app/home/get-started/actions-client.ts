'use server'

import { GetAllCategoriesDocument, GetAllCategoriesQuery } from '../../api/generated/graphql'
import { getClient } from '../../api/graphql/client'

const client = getClient()

type QueryCategoryListType = Extract<GetAllCategoriesQuery['productTypes'], any>

export type QueryCategoryType = QueryCategoryListType[number]

export const getCategories = async (): Promise<{ categories: QueryCategoryType[]; error: string | null }> => {
	try {
		const { data } = await client.query({
			query: GetAllCategoriesDocument
		})

		return { categories: data.productTypes, error: null }
	} catch (error) {
		return { categories: [], error: JSON.stringify(error, null, 2) }
	}
}
