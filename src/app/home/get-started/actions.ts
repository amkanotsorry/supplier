'use server'

import prisma from '@/src/prisma/client'
import {
	CreateCertificationDocument,
	CreateCertificationInput,
	CreateEntitySupplierInput,
	CreateEntitySupplierMutationDocument,
	Entity_Type,
	GetAllSupplierCategoriesDocument,
	GetAllSupplierCategoriesQuery,
	GetCertificatesDocument,
	GetCertificatesQuery,
	GetCollectionPointsDocument,
	GetEntitySupplierByIdDocument,
	GetPortsDocument,
	UserByIdDocument
} from '../../api/generated/graphql'
import { getClient } from '../../api/graphql/client'

const client = getClient()

export const getProfile = async (id: string) => {
	try {
		const { data } = await client.query({
			query: UserByIdDocument,
			variables: { userId: id },
			fetchPolicy: 'network-only'
		})

		return { profile: data.user, error: null }
	} catch (error) {
		return { profile: null, error: JSON.stringify(error, null, 2) }
	}
}

export const getEntitySupplierById = async (id: string) => {
	try {
		const { data } = await client.query({
			query: GetEntitySupplierByIdDocument,
			variables: {
				entitySupplierId: id
			}
		})

		return data.entitySupplier
	} catch (e) {
		console.log('Supplier query error ====>', e)
		return
	}
}

export const getProductTypes = async () => {
	try {
		return await prisma.producttypes.findMany()
	} catch (e) {
		console.log(e)
		return
	}
}

export const getCerticateSource = async () => {
	try {
		return await prisma.certificate_sources.findMany()
	} catch (error) {
		console.log('error', error)
	}
}

type QueryCertificates = Extract<GetCertificatesQuery['certificates'], any>

export type QueryCertificate = QueryCertificates[number]

export const getCerticateTypes = async () => {
	try {
		const { data } = await client.query({
			query: GetCertificatesDocument
		})
		return { certificates: data.certificates, error: null }
	} catch (error) {
		return { certificates: [], error: JSON.stringify(error, null, 2) }
	}
}

export const getPorts = async () => {
	try {
		const { data } = await client.query({
			query: GetPortsDocument
		})

		return { ports: data.ports, error: null }
	} catch (error) {
		return { ports: [], error: JSON.stringify(error) }
	}
}

export const getCollectionPoints = async () => {
	try {
		const { data } = await client.query({
			query: GetCollectionPointsDocument
		})

		return { collectionPoints: data.collectionPoints, error: null }
	} catch (error) {
		return { collectionPoints: [], error: JSON.stringify(error) }
	}
}

export const createCertification = async (createCertificationInput: CreateCertificationInput) => {
	try {
		const { data } = await client.mutate({
			mutation: CreateCertificationDocument,
			variables: {
				createCertificationInput
			}
		})

		return { createCertification: data?.createCertification, error: null }
	} catch (error) {
		return { createCertification: null, error: JSON.stringify(error, null, 2) }
	}
}

export const createEntitySupplier = async (variables: {
	createEntitySupplierInput: CreateEntitySupplierInput
	entityId: string
	entityType: Entity_Type
}) => {
	try {
		const response = await client.mutate({
			mutation: CreateEntitySupplierMutationDocument,
			variables
		})

		return { response, error: null }
	} catch (error) {
		return { response: null, error: JSON.stringify(error, null, 2) }
	}
}

export type QuerySupplierCategory = Extract<GetAllSupplierCategoriesQuery['getAllSupplierCategories'], any>[number]

export const getSupplierCategories = async () => {
	try {
		const { data } = await client.query({
			query: GetAllSupplierCategoriesDocument
		})
		return { supplierCategories: data.getAllSupplierCategories, error: null }
	} catch (error) {
		return { supplierCategories: [], error: JSON.stringify(error, null, 2) }
	}
}
