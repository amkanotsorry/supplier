import React from 'react'
import { ApolloWrapper } from '../../api/graphql/apollo-wrapper'
import { getProductTypes, getCerticateSource, getCerticateTypes, getPorts, getCollectionPoints, getSupplierCategories } from './actions'
import GetStartedSteps from './steps'
import { TreeNodeProp } from '@/src/components/tree-select/types'
import { producttypes } from '@/src/prisma/generated/client'

const buildTree = (node: producttypes, productTypes: producttypes[]): TreeNodeProp => {
	const newNode: TreeNodeProp = {
		title: node.name,
		value: node.code,
		children: productTypes?.filter(type => type.parentId === node.id).map(node1 => buildTree(node1, productTypes))
	}
	return newNode
}

const GetStarted = async () => {
	const productTypes = await getProductTypes()
	const parentNodes = productTypes?.filter(type => type.isParent && !type.parentId)
	const productTypeOptions = productTypes ? parentNodes?.map(node => buildTree(node, productTypes)) : []

	const sources = await getCerticateSource()
	const { certificates } = await getCerticateTypes()

	const { supplierCategories } = await getSupplierCategories()

	const { ports } = await getPorts()
	const { collectionPoints } = await getCollectionPoints()

	return (
		<ApolloWrapper>
			<GetStartedSteps
				ports={ports}
				collectionPoints={collectionPoints}
				productTypeOptions={productTypeOptions}
				sources={sources}
				certificates={certificates}
				supplierCategories={supplierCategories}
			/>
		</ApolloWrapper>
	)
}

export default GetStarted
