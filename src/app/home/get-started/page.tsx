import { getServerSession } from 'next-auth'
import { authOptions } from '../../api/auth/[...nextauth]/authOptions'
import { Entity_Status, Review_Status } from '../../api/generated/graphql'
import { getEntityById } from '../actions'
import { getReviewById } from '../profile/[profile_id]/profile-actions'
import GetStarted from './get-started'
import Result from './result'
import { getProfile } from './actions'

const GetStartedPage = async () => {
	const session = await getServerSession(authOptions)
	const { profile } = await getProfile(session?.user.id || '')
	const { entity } = await getEntityById(profile?.currentEntityId || '')
	if (!entity) return <div> entity not found</div>

	const { supplierReview } = await getReviewById(entity.entitySupplier?.reviewId || '')

	if (entity.entitySupplier?._id) {
		if (entity.entitySupplier.status === Entity_Status.Approved) {
			return <Result status={'success'} entitySupplierId={entity.entitySupplier._id} />
		}

		switch (supplierReview?.status) {
			case Review_Status.Declined:
				return <Result status={'error'} entitySupplierId={entity.entitySupplier._id} />
			default:
				return <Result status={'review'} entitySupplierId={entity.entitySupplier._id} />
		}
	}

	return <GetStarted />
}

export default GetStartedPage
