'use client'

import TreeSelect from '@/src/components/tree-select'
import { TreeNodeProp } from '@/src/components/tree-select/types'
import { Form, FormControl, FormField, FormItem, FormMessage } from '@/src/components/ui/form'
import { zodResolver } from '@hookform/resolvers/zod'
import { Button, Spinner } from '@nextui-org/react'
import { useContext, useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import * as z from 'zod'
import { QueryCategoryType, getCategories } from '../actions-client'
import { GetStartedContext } from '../steps'

const buildTree = (node: QueryCategoryType, productTypes: QueryCategoryType[]): TreeNodeProp => {
	const newNode: TreeNodeProp = {
		title: node.name,
		value: node.code,
		children: productTypes?.filter(type => type.parentId === node._id).map(node1 => buildTree(node1, productTypes))
	}
	return newNode
}

const FormSchema = z.object({
	productTypes: z.array(z.string())
})

type Props = {
	handleClickNext: () => void
}

export default function ProductType(props: Props) {
	const { updateEntitySupplier } = useContext(GetStartedContext)
	const [options, setOptions] = useState<TreeNodeProp[]>()
	const [loading, setLoading] = useState(false)

	useEffect(() => {
		fetchProductType()
	}, [])

	const fetchProductType = async () => {
		setLoading(true)
		const { categories, error } = await getCategories()
		setLoading(false)
		const parentNodes = categories?.filter(type => !type.parentId)
		const productTypeOptions = categories ? parentNodes?.map(node => buildTree(node, categories)) : []

		setOptions(productTypeOptions)
	}

	const { handleClickNext } = props
	const form = useForm<z.infer<typeof FormSchema>>({
		resolver: zodResolver(FormSchema)
	})
	const productTypes = form.watch('productTypes')

	async function onSubmit(data: z.infer<typeof FormSchema>) {
		updateEntitySupplier?.(data)
		handleClickNext()
	}

	return (
		<Form {...form}>
			<form onSubmit={form.handleSubmit(onSubmit)} className="w-100 h-full">
				<div className="w-100 h-full flex flex-col gap-4">
					<div className="w-full h-full overflow-y-auto">
						<div className="text-black text-lg font-semibold leading-7">Product type</div>
						<div className="text-gray-400 text-sm font-normal leading-relaxed">This will categorize your products and set your preferences</div>

						{loading ? (
							<div className="w-full h-full flex items-center justify-center gap-4 shrink-0">
								Fetching Product Types... <Spinner color="primary" />
							</div>
						) : (
							<FormField
								control={form.control}
								name="productTypes"
								render={() => (
									<FormItem className="mt-6">
										<FormControl>
											<TreeSelect data={options ?? []} onChange={(val: string[]) => form.setValue('productTypes', val)} />
										</FormControl>
										<FormMessage />
									</FormItem>
								)}
							/>
						)}
					</div>
					<div className="flex gap-4">
						<Button className="w-full border-[1px]" variant="bordered" onClick={handleClickNext}>
							Skip
						</Button>
						<Button className="w-full" color="primary" disabled={productTypes?.length < 1} onClick={() => form.handleSubmit(onSubmit)()}>
							Continue
						</Button>
					</div>
				</div>
			</form>
		</Form>
	)
}

// export default ProductType
