'use client'
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from '@/src/components/ui/form'
import { RadioGroup, RadioGroupItem } from '@/src/components/ui/radio-group'
// import { useQuery } from '@apollo/client'
import { zodResolver } from '@hookform/resolvers/zod'
import { Button } from '@nextui-org/react'
import { useContext } from 'react'
import { useForm } from 'react-hook-form'
import * as z from 'zod'
import { GetStartedContext } from '../steps'

export const dynamic = 'force-dynamic'

const FormSchema = z.object({
	supplierType: z.string()
})

type Props = {
	handleClickNext: () => void
}

const SupplierType = ({ handleClickNext }: Props) => {
	const { createEntitySupplierInput, updateEntitySupplier, supplierCategories } = useContext(GetStartedContext)

	const form = useForm<z.infer<typeof FormSchema>>({
		resolver: zodResolver(FormSchema),
		defaultValues: {
			supplierType: createEntitySupplierInput?.supplierType ?? undefined
		}
	})
	const supplierType = form.watch('supplierType')

	async function onSubmit(data: z.infer<typeof FormSchema>) {
		console.log('suppliertype', data)
		updateEntitySupplier?.(data)
		handleClickNext()
	}

	return (
		<Form {...form}>
			<form onSubmit={form.handleSubmit(onSubmit)} className="w-100 h-full">
				<div className="w-100 h-full flex flex-col justify-between">
					<div>
						<div className="text-black text-lg font-semibold leading-7">Choose supplier type</div>
						<div className="text-gray-400 text-sm font-normal leading-relaxed">This option will set your further preference.</div>

						<FormField
							control={form.control}
							name="supplierType"
							render={({ field }) => (
								<FormItem className="mt-4">
									<FormControl>
										<RadioGroup onValueChange={field.onChange} defaultValue={field.value} className="flex flex-col space-y-4">
											{(supplierCategories || []).map(category => (
												<FormItem key={category._id} className="flex items-center px-4 pb-2 bg-lightgray-100 rounded-xl h-10">
													<FormControl className="mt-2">
														<RadioGroupItem value={category._id} />
													</FormControl>
													<FormLabel className="ml-4 text-zinc-900 text-sm font-medium leading-relaxed">{category.name}</FormLabel>
												</FormItem>
											))}
										</RadioGroup>
									</FormControl>
									<FormMessage />
								</FormItem>
							)}
						/>
					</div>
					<Button variant="solid" color="primary" className="w-full h-10" disabled={!supplierType} onClick={form.handleSubmit(onSubmit)}>
						Continue
					</Button>
				</div>
			</form>
		</Form>
	)
}

export default SupplierType
