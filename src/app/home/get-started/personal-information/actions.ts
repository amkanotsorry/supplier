'use server'

import {
	CreateStockLocationsDocument,
	StockLocationInput,
	UpdateStockLocationDocument,
	UpdateStockLocationInput
} from '@/src/app/api/generated/graphql'
import { getClient } from '@/src/app/api/graphql/client'
import { StockLocationType } from '../steps'

const client = getClient()

export const createStockLocation = async (createStockLocationsInput: StockLocationInput) => {
	try {
		console.log('createStockLocatinsInput', JSON.stringify(createStockLocationsInput))

		const { data } = await client.mutate({
			mutation: CreateStockLocationsDocument,
			variables: {
				createStockLocationsInput
			}
		})

		return { stockLocation: data?.createStockLocations, error: null }
	} catch (error) {
		return { stockLocation: null, error: JSON.stringify(error, null, 2) }
	}
}

export const updateStockLocation = async (updateStockLocationsInput: UpdateStockLocationInput) => {
	try {
		const { data } = await client.mutate({
			mutation: UpdateStockLocationDocument,
			variables: {
				updateStockLocationsInput
			}
		})

		return { stockLocation: data?.updateStockLocation, error: null }
	} catch (error) {
		return { stockLocation: null, error: JSON.stringify(error, null, 2) }
	}
}
