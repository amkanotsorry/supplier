import { StockLocationInput } from '@/src/app/api/generated/graphql'
import { DMapContainer, DMarker, DTileLayer } from '@/src/components/leaflet/'
import StockLocationModal from '@/src/components/modals/location-modals/stock-location-modal'
import 'leaflet/dist/leaflet.css'
import { Edit, XIcon } from 'lucide-react'
import { StockLocationType } from '../../steps'
import { useState } from 'react'
import GoogleMap from '@/src/components/map-position-selector/google-maps'

interface Props {
	branch: StockLocationType
	onDelete: () => void
	onUpdate: (stockLocation: StockLocationType) => void
}

const BranchCard = ({ branch, onDelete, onUpdate }: Props) => {
	const { address, phoneNumber, branchName } = branch

	const [isOpen, setIsOpen] = useState(false)

	const openModal = () => setIsOpen(true)
	const closeModal = () => setIsOpen(false)

	return (
		<div className="p-4 rounded-lg bg-gray-50 flex gap-4 z-0 cursor-pointer">
			<div className="h-24 w-24 z-0">
				<GoogleMap className="h-24" position={[branch.address.coordinates.latitude, branch.address.coordinates.longtitude]} />
			</div>
			<div className="grow">
				<div className="text-black text-base font-medium leading-relaxed">{branchName}</div>
				<div className="mt-2 flex-col gap-1 text-gray-500 text-sm font-normal leading-snug">
					<div className="">{`${address.cityProvince.name}, ${address.duuregSoum.name}, ${address.khorooBag.name}`}</div>
					<div className="">{`Phone number: ${phoneNumber}`}</div>
					<div className="">{`Detailed Address: ${address.detail}`}</div>
				</div>
			</div>

			<XIcon onClick={onDelete} />

			<Edit onClick={() => openModal()} />

			<StockLocationModal open={isOpen} initialData={branch} onOk={onUpdate} onOpenChange={closeModal} />
		</div>
	)
}

export default BranchCard
