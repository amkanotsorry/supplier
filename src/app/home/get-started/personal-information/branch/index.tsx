'use client'

import { PlusIcon } from '@/src/assets/icons'
import StockLocationModal from '@/src/components/modals/location-modals/stock-location-modal'
import { toast } from '@/src/components/ui/use-toast'
import { Button, Spinner } from '@nextui-org/react'
import { useContext, useState } from 'react'
import { GetStartedContext, StockLocationType } from '../../steps'
import { createStockLocation, updateStockLocation } from '../actions'
import BranchCard from './BranchCard'
import { StockLocationInput } from '@/src/app/api/generated/graphql'

type Props = {
	handleClickNext: () => void
}
const Branch = ({ handleClickNext }: Props) => {
	const { createEntitySupplierInput, addBranch, editBranch, deleteBranch } = useContext(GetStartedContext)
	const branches = createEntitySupplierInput?.stockLocations

	const [isOpen, setIsOpen] = useState(false)
	const [pending, setPending] = useState(false)

	const openModal = () => {
		setIsOpen(true)
	}

	const closeModal = () => setIsOpen(false)

	const onSave = async (stockLocation: StockLocationType) => {
		setPending(true)

		if (stockLocation._id) {
			console.log('editing', stockLocation)
			editBranch?.(stockLocation)
			updateStockLocation(stockLocation)
			setPending(false)
			return
		}

		const { stockLocation: newStockLocation, error } = await createStockLocation(stockLocation)

		setPending(false)

		if (error) {
			console.log('stock locaiton', error)
			toast({
				title: error,
				variant: 'destructive'
			})
		}

		if (newStockLocation?.[0]._id) {
			addBranch?.({ ...stockLocation, _id: newStockLocation[0]._id })
		}
	}

	return (
		<>
			<div className="w-100 h-full flex flex-col justify-between">
				<div className="h-full overflow-y-auto">
					<div className="text-black text-lg font-semibold leading-7">Branch</div>
					<div className="text-gray-400 text-base font-normal leading-relaxed">At least one branch is required.</div>
					<div className="flex flex-col gap-y-6 mt-4">
						{pending && (
							<div className="p-4 rounded-lg h-32 bg-gray-50 flex gap-4 z-0 cursor-pointer">
								<Spinner className="m-auto" />
							</div>
						)}
						{branches?.map((branch, index) => (
							<div key={index}>
								<BranchCard onUpdate={onSave} onDelete={() => deleteBranch?.(branch.branchName)} branch={branch} />
							</div>
						))}
					</div>
					<Button
						color="primary"
						variant="bordered"
						startContent={<PlusIcon className="w-6 h-6" />}
						className="my-4 border-[1px]"
						onClick={() => {
							openModal()
						}}>
						Add Branch
					</Button>
				</div>
				<Button isDisabled={pending} color="primary" className="w-full" onClick={handleClickNext} disabled={!branches?.length}>
					Continue
				</Button>

				<StockLocationModal open={isOpen} initialData={undefined} onOk={onSave} onOpenChange={closeModal} />
			</div>
		</>
	)
}

export default Branch
