import { DeliverToAimak, DeliverToLocalPort, DeliveryDetailUnion, Delivery_Type, FullDelivery } from '@/src/app/api/generated/graphql'
import Button from '@/src/components/Button'
import { Form, FormControl, FormField, FormItem, FormMessage } from '@/src/components/ui/form'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '@/src/components/ui/select'
import { enumToArray } from '@/src/lib/utils'
import { useContext } from 'react'
import { useForm } from 'react-hook-form'
import { GetStartedContext } from '../../steps'
import ToAimagCollectionPointInputs from './to-aimag-collection-point'
import ToLocalPortInputs from './to-local-port-inputs'

export function isDeliverToAimak(obj: any): obj is DeliverToAimak {
	return obj.deliveryType === Delivery_Type.ToAimagCollectionPoint
}

export function isDeliverToLocalPort(obj: any): obj is DeliverToLocalPort {
	return obj.deliveryType === Delivery_Type.ToLocalPort
}

export function isFullDelivery(obj: any): obj is FullDelivery {
	return obj.deliveryType === Delivery_Type.Full
}

type Props = {
	handleClickNext: () => void
}

const DeliveryType = (props: Props) => {
	const { updateEntitySupplier, createEntitySupplierInput } = useContext(GetStartedContext)
	const { nextStep } = useContext(GetStartedContext)

	const form = useForm<DeliveryDetailUnion>({})

	const { watch } = form
	const deliveryType = watch('deliveryType')

	function onSubmit(data: DeliveryDetailUnion) {
		const deliveryDetail: any = {
			deliveryType: data.deliveryType
		}

		if (isFullDelivery(data)) {
			deliveryDetail.stockLocationId = data.stockLocationId
		}

		if (isDeliverToAimak(data)) {
			deliveryDetail.aimak = data.aimak
			deliveryDetail.collectionPointId = data.collectionPointId
		}

		if (isDeliverToLocalPort(data)) {
			deliveryDetail.cityProvince = data.cityProvince
			deliveryDetail.duuregSoum = data.duuregSoum
			deliveryDetail.portId = data.portId
		}

		updateEntitySupplier?.({ deliveryDetail })
		nextStep?.()
	}

	return (
		<Form {...form}>
			<form onSubmit={form.handleSubmit(onSubmit)} className="w-100 h-full">
				<div className="w-100 h-full flex flex-col justify-between">
					<div>
						<div className="text-black text-lg font-semibold leading-7">Delivery type</div>
						<div className="text-gray-400 text-base font-normal leading-relaxed">Can be changed later.</div>

						<div className="mt-6 flex flex-col gap-y-6">
							<FormField
								control={form.control}
								name="deliveryType"
								render={({ field }) => (
									<FormItem>
										<Select onValueChange={field.onChange} defaultValue={field.value}>
											<FormControl className="">
												<SelectTrigger className="bg-gray-50 border-0 h-14">
													<SelectValue className="::placeholder:text-gray-400" placeholder="Choose type" />
												</SelectTrigger>
											</FormControl>
											<SelectContent>
												{enumToArray(Delivery_Type).map(({ key, value }) => (
													<SelectItem key={key} value={value?.toString()}>
														{key}
													</SelectItem>
												))}
											</SelectContent>
										</Select>
										<FormMessage />
									</FormItem>
								)}
							/>

							{watch('deliveryType') === Delivery_Type.ToLocalPort && <ToLocalPortInputs form={form} />}

							{watch('deliveryType') === Delivery_Type.ToAimagCollectionPoint && <ToAimagCollectionPointInputs form={form} />}

							{watch('deliveryType') === Delivery_Type.Full && (
								<FormField
									control={form.control}
									name="stockLocationId"
									render={({ field }) => (
										<FormItem>
											<Select onValueChange={field.onChange} defaultValue={field.value}>
												<FormControl className="">
													<SelectTrigger className="bg-gray-50 border-0 h-14">
														<SelectValue className="::placeholder:text-gray-400" placeholder="Choose Stock Location" />
													</SelectTrigger>
												</FormControl>
												<SelectContent>
													{(createEntitySupplierInput?.stockLocations || []).map((location, index) => (
														<SelectItem key={index} value={location._id}>
															{location.branchName}
														</SelectItem>
													))}
												</SelectContent>
											</Select>
											<FormMessage />
										</FormItem>
									)}
								/>
							)}
						</div>
					</div>
					<div className="flex gap-4">
						<Button
							text="Skip"
							className="w-full"
							type="secondary"
							onClick={() => {
								nextStep?.()
							}}
						/>
						<Button text="Save" className="w-full" onClick={() => form.handleSubmit(onSubmit)()} disabled={!deliveryType} />
					</div>
				</div>
			</form>
		</Form>
	)
}

export default DeliveryType
