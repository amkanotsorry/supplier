import React, { useContext } from 'react'
import { FormControl, FormField, FormItem, FormMessage } from '@/src/components/ui/form'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '@/src/components/ui/select'
import { useAddress } from '@/src/lib/address-helper'
import { UseFormReturn } from 'react-hook-form'
import { GetStartedContext } from '../../steps'
import { DeliveryDetailUnion } from '@/src/app/api/generated/graphql'

interface Props {
	form: UseFormReturn<DeliveryDetailUnion>
}

const ToAimagCollectionPointInputs: React.FC<Props> = ({ form }) => {
	const { aimagHotOptions } = useAddress({ form, aimagFormKey: 'cityProvince' })
	const { collectionPoints } = useContext(GetStartedContext)

	return (
		<div className="flex gap-4">
			<div className="flex-1">
				<FormField
					control={form.control}
					name="aimak"
					render={({ field }) => (
						<FormItem>
							<Select onValueChange={field.onChange} defaultValue={field.value}>
								<FormControl className="">
									<SelectTrigger className="bg-gray-50 border-0 h-14 flex-1">
										<SelectValue className="::placeholder:text-gray-400" placeholder="Choose City Provice" />
									</SelectTrigger>
								</FormControl>
								<SelectContent>
									{aimagHotOptions.map((aimag, index) => (
										<SelectItem key={index} value={aimag.code.toString()}>
											{aimag.name}
										</SelectItem>
									))}
								</SelectContent>
							</Select>
							<FormMessage />
						</FormItem>
					)}
				/>
			</div>
			<div className="flex-1">
				<FormField
					control={form.control}
					name={'collectionPointId'}
					render={({ field }) => (
						<FormItem>
							<Select onValueChange={field.onChange} defaultValue={field.value}>
								<FormControl className="">
									<SelectTrigger className="bg-gray-50 border-0 h-14 flex-1">
										<SelectValue className="::placeholder:text-gray-400" placeholder="Choose collection point" />
									</SelectTrigger>
								</FormControl>
								<SelectContent>
									{(collectionPoints || []).map((point, index) => (
										<SelectItem key={index} value={point._id.toString()}>
											{point.name}
										</SelectItem>
									))}
								</SelectContent>
							</Select>
							<FormMessage />
						</FormItem>
					)}
				/>
			</div>
		</div>
	)
}

export default ToAimagCollectionPointInputs
