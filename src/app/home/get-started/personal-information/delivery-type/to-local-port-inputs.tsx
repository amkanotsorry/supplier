import { FormControl, FormField, FormItem, FormMessage } from '@/src/components/ui/form'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '@/src/components/ui/select'
import { useAddress } from '@/src/lib/address-helper'
import React, { useContext } from 'react'
import { UseFormReturn } from 'react-hook-form'
import { GetStartedContext } from '../../steps'
import { DeliveryDetailUnion } from '@/src/app/api/generated/graphql'

interface Props {
	form: UseFormReturn<DeliveryDetailUnion>
}

const ToLocalPortInputs: React.FC<Props> = ({ form }) => {
	const { aimagHotOptions, sumDuuregOpns, address } = useAddress({ form, aimagFormKey: 'cityProvince', sumFormKey: 'duuregSoum' })
	const { ports } = useContext(GetStartedContext)

	return (
		<div className="flex flex-col gap-4">
			<div className="flex gap-4">
				<div className="flex-1">
					<FormField
						control={form.control}
						name="cityProvince"
						render={({ field }) => (
							<FormItem>
								<Select onValueChange={field.onChange} defaultValue={field.value}>
									<FormControl className="">
										<SelectTrigger className="bg-gray-50 border-0 h-14 flex-1">
											<SelectValue className="::placeholder:text-gray-400" placeholder="Choose City/Provice" />
										</SelectTrigger>
									</FormControl>
									<SelectContent>
										{aimagHotOptions.map((aimag, index) => (
											<SelectItem key={index} value={aimag.code.toString()}>
												{aimag.name}
											</SelectItem>
										))}
									</SelectContent>
								</Select>
								<FormMessage />
							</FormItem>
						)}
					/>
				</div>
				<div className="flex-1">
					<FormField
						control={form.control}
						name="duuregSoum"
						render={({ field }) => (
							<FormItem>
								<Select onValueChange={field.onChange} defaultValue={field.value}>
									<FormControl className="">
										<SelectTrigger className="bg-gray-50 border-0 h-14 flex-1">
											<SelectValue className="::placeholder:text-gray-400" placeholder="Choose Duureg/Sum" />
										</SelectTrigger>
									</FormControl>
									<SelectContent>
										{sumDuuregOpns.map((sum, index) => (
											<SelectItem key={index} value={sum.code.toString()}>
												{sum.name}
											</SelectItem>
										))}
									</SelectContent>
								</Select>
								<FormMessage />
							</FormItem>
						)}
					/>
				</div>
			</div>
			<div className="flex-1">
				<FormField
					control={form.control}
					name="portId"
					render={({ field }) => (
						<FormItem>
							<Select onValueChange={field.onChange} defaultValue={field.value}>
								<FormControl className="">
									<SelectTrigger className="bg-gray-50 border-0 h-14 flex-1">
										<SelectValue className="::placeholder:text-gray-400" placeholder="Choose Port" />
									</SelectTrigger>
								</FormControl>
								<SelectContent>
									{(ports || []).map((port, index) => (
										<SelectItem key={index} value={port._id}>
											{port.name}
										</SelectItem>
									))}
								</SelectContent>
							</Select>
							<FormMessage />
						</FormItem>
					)}
				/>
			</div>
		</div>
	)
}

export default ToLocalPortInputs
