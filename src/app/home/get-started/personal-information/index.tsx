'use client'

import Steps from '@/src/components/steps'
import React, { useState } from 'react'
import SupplierType from './SupplierType'
import ProductType from './ProductType'
import Branch from './branch'
import DeliveryType from './delivery-type'

interface Props {}

const PersonalInformation: React.FC<Props> = () => {
	const [current, setCurrent] = useState(0)

	const handleClickNext = () => setCurrent(current + 1)

	return (
		<div className="mt-6 w-full h-full grow p-4 bg-white rounded-2xl shadow-sm">
			<Steps
				onStepClick={setCurrent}
				direction="vertical"
				items={[
					{
						title: 'Supplier type',
						content: <SupplierType handleClickNext={handleClickNext} />
					},
					{
						title: 'Product type',
						content: <ProductType handleClickNext={handleClickNext} />
					},
					{
						title: 'Branch',
						content: <Branch handleClickNext={handleClickNext} />
					},
					{
						title: 'Delivery type',
						content: <DeliveryType handleClickNext={handleClickNext} />
					}
				]}
				current={current}
			/>
		</div>
	)
}

export default PersonalInformation
