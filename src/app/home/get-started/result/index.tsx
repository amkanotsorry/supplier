'use client'

import { DocIcon } from '@/src/assets/icons'
import { Button } from '@nextui-org/react'
import { AlertCircle, Check, InfoIcon } from 'lucide-react'
import { useRouter } from 'next/navigation'
import React from 'react'

type Props = {
	status: 'success' | 'error' | 'review'
	entitySupplierId?: string
}

const Result = ({ status, entitySupplierId }: Props) => {
	const router = useRouter()
	const statusCircles = {
		success: (
			<div className="h-11 w-11 bg-green-500 rounded-full text-white flex justify-center items-center">
				<Check />
			</div>
		),

		review: (
			<div className="h-11 w-11 bg-warning-700 rounded-full text-white flex justify-center items-center">
				<InfoIcon />
			</div>
		),

		error: (
			<div className="h-11 w-11 bg-red-500 rounded-full text-white flex justify-center items-center">
				<AlertCircle />
			</div>
		)
	}

	const statusInfo = {
		success: {
			text: 'Your profile has been approved!',
			description:
				'Congratulations! Your profile has been successfully approved, you can now enjoy full access to our services. Thank you for choosing us.'
		},

		review: {
			text: 'Your profile is under review.',
			description:
				'Thank you for your submission. Our team is currently reviewing your profile, and we will notify you once the review process is complete.'
		},

		error: {
			text: 'Your profile has not been approved.',
			description:
				'We regret to inform you that your profile has not been approved; kindly review our terms and conditions, make any necessary corrections to your information, and resubmit your profile for consideration, or contact our support team for further assistance."'
		}
	}

	const actionButton = {
		success: (
			<Button onClick={() => router.push(`/home/product`)} color="primary" className="mt-8">
				Get started
			</Button>
		),

		review: (
			<Button variant="bordered" className="mt-8 border-[1px] bg-white">
				Contact Support
			</Button>
		),

		error: (
			<Button onClick={() => router.push(`/home/profile/${entitySupplierId}`)} color="primary" className="mt-8">
				More
			</Button>
		)
	}

	return (
		<div className="bg-gray-50 flex flex-col w-full h-full justify-center items-center">
			<div className="relative h-20 p-4 bg-white rounded-2xl shadow border border-gray-100 justify-start items-start gap-3 inline-flex">
				<DocIcon />
				<div className="flex-col justify-start items-start gap-2 inline-flex">
					<div className="w-24 h-3 bg-zinc-200 rounded" />
					<div className="w-16 h-3 bg-zinc-200 rounded" />
				</div>
				<div className="absolute -right-4 -bottom-4 h-11 w-11">{statusCircles[status]}</div>
			</div>
			<div className="mt-8 text-zinc-900 text-lg font-medium leading-7">{statusInfo[status].text}</div>
			<div className="mt-4 max-w-[512px] text-center text-gray-400 text-sm font-normal leading-snug">{statusInfo[status].description}</div>
			{actionButton[status]}
		</div>
	)
}

export default Result
