import React from 'react'
import Result from '../result'

interface Props {}

const ReviewPendingPage: React.FC<Props> = () => {
	return <Result status={'success'} />
}

export default ReviewPendingPage
