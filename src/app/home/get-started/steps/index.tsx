'use client'

import {
	DeliveryDetailUnion,
	GetCollectionPointsQuery,
	GetEntityByIdQuery,
	GetPortsQuery,
	Scalars,
	StockLocationInput
} from '@/src/app/api/generated/graphql'
import { ApolloFileUploadWrapper } from '@/src/app/api/graphql/apollo-wrapper'
import useEntity from '@/src/app/hooks/use-entity'
import Steps from '@/src/components/steps'
import { TreeNodeProp } from '@/src/components/tree-select/types'
import { certificate_sources, certificate_types } from '@/src/prisma/generated/client'
import { Dispatch, SetStateAction, createContext, useState } from 'react'
import { UploadFileResponseType } from '../../upload-file'
import PersonalInformation from '../personal-information'
import TermsAndConditions from '../terms-and-conditions'
import UploadDocuments from '../upload-documents'
import { QueryCertificate, QuerySupplierCategory } from '../actions'

type CreateEntitySupplierProps = {
	certificationIds?: Array<string>
	stateRegistrationOrder?: UploadFileResponseType
	coverImages?: Array<UploadFileResponseType>
	/** Хүргэлийн төрөл */
	deliveryDetail?: DeliveryDetailUnion
	entityId?: Scalars['String']['input']
	image?: UploadFileResponseType
	isDomestic?: Scalars['Boolean']['input']
	isInternational?: Scalars['Boolean']['input']
	/** Барааны төрлүүд */
	productTypes?: Array<Scalars['String']['input']>
	stockLocations?: Array<StockLocationType>
	/** Нийлүүлэгчийн төрөл */
	supplierType?: string
	status?: 'warning' | 'success' | 'error'
}

export interface StockLocationType extends StockLocationInput {
	_id: string
}

interface ContextData {
	entity?: GetEntityByIdQuery['entity']
	createEntitySupplierInput?: CreateEntitySupplierProps
	updateEntitySupplier?: (val: CreateEntitySupplierProps) => void
	productTypeOptions?: TreeNodeProp[]
	addBranch?: (branch: StockLocationType) => void
	editBranch?: (branch: StockLocationType) => void
	deleteBranch?: (branchName: string) => void
	currentStep?: number
	nextStep?: () => void
	setCurrentStep?: Dispatch<SetStateAction<number>>
	supplierCategories?: QuerySupplierCategory[]
	sources?: certificate_sources[]
	certificates?: QueryCertificate[]
	ports?: Extract<GetPortsQuery['ports'], any>
	collectionPoints?: Extract<GetCollectionPointsQuery['collectionPoints'], any>
}

const initialVal: ContextData = {}

export const GetStartedContext = createContext(initialVal)

type Props = {
	productTypeOptions?: TreeNodeProp[]
	sources?: certificate_sources[]
	supplierCategories?: QuerySupplierCategory[]
	certificates?: QueryCertificate[]
	ports?: Extract<GetPortsQuery['ports'], any>
	collectionPoints?: Extract<GetCollectionPointsQuery['collectionPoints'], any>
}

const GetStartedSteps = ({ productTypeOptions, sources, supplierCategories, certificates, ports, collectionPoints }: Props) => {
	const { entity } = useEntity({})

	const [createEntitySupplierInput, setCreateEntitySupplierInput] = useState<CreateEntitySupplierProps>()
	const [currentStep, setCurrentStep] = useState(0)

	const updateEntitySupplier = (val: CreateEntitySupplierProps) => {
		const newInputs = { ...createEntitySupplierInput, ...val }
		setCreateEntitySupplierInput(newInputs)
	}

	const addBranch = (branch: StockLocationType) => {
		const newInputs = { ...createEntitySupplierInput }
		newInputs.stockLocations = [...(newInputs.stockLocations ?? []), branch]
		setCreateEntitySupplierInput(newInputs)
	}

	const editBranch = (branch: StockLocationType) => {
		const newInputs = { ...createEntitySupplierInput }
		if (!newInputs.stockLocations) return
		const branchIndexToEdit = newInputs.stockLocations?.findIndex(location => location._id === branch._id) || 0
		newInputs.stockLocations[branchIndexToEdit] = branch

		setCreateEntitySupplierInput(newInputs)
	}

	const deleteBranch = (branchName: string) => {
		const newInputs = { ...createEntitySupplierInput }
		if (!newInputs.stockLocations) return

		const branchIndexToEdit = newInputs.stockLocations?.findIndex(location => location.branchName === branchName) || 0
		newInputs.stockLocations.splice(branchIndexToEdit, 1)

		setCreateEntitySupplierInput(newInputs)
	}

	const nextStep = () => {
		setCurrentStep(currentStep + 1)
	}

	const stepItems = [
		{
			title: 'Personal information',
			content: <PersonalInformation />
		},
		{
			title: 'Upload documents',
			content: (
				<ApolloFileUploadWrapper>
					<UploadDocuments />
				</ApolloFileUploadWrapper>
			)
		},
		{
			title: 'Terms & Condition',
			content: <TermsAndConditions />
		}
	]

	return (
		<GetStartedContext.Provider
			value={{
				entity,
				createEntitySupplierInput,
				updateEntitySupplier,
				productTypeOptions,
				addBranch,
				editBranch,
				deleteBranch,
				currentStep,
				nextStep,
				setCurrentStep,
				supplierCategories,
				sources,
				certificates,
				ports,
				collectionPoints
			}}>
			<div className="h-full overflow-hidden p-12 bg-lightgray-100">
				<div className="flex flex-col w-full h-full overflow-hidden">
					<div className="flex justify-between items-center">
						<div className="text-zinc-900 text-3xl font-semibold leading-[44px] mb-6">Setup guide</div>
					</div>
					<Steps onStepClick={setCurrentStep} items={stepItems} current={currentStep} />
				</div>
			</div>
		</GetStartedContext.Provider>
	)
}

export default GetStartedSteps
