'use client'
import Steps from '@/src/components/steps'
import React, { useState } from 'react'
import TermStep1 from './term-step-1'

type Props = {}

const TermsAndConditions = (props: Props) => {
	const [current, setCurrent] = useState(0)

	const handleClickNext = () => setCurrent(current + 1)
	const handleClickPrev = () => setCurrent(current - 1)
	return (
		<div className="mt-12 w-full h-full overflow-hidden grow p-4 bg-white rounded-2xl ">
			<Steps
				direction="vertical"
				items={[
					{
						title: 'Terms & Condition',
						content: <TermStep1 />
					}
				]}
				current={current}
			/>
		</div>
	)
}

export default TermsAndConditions
