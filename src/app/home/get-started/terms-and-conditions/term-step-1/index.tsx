import { DeliveryDetailUnion, Delivery_Type, Entity_Type } from '@/src/app/api/generated/graphql'
import { Checkbox } from '@/src/components/ui/checkbox'
import { Form, FormControl, FormField, FormItem, FormLabel } from '@/src/components/ui/form'
import { toast } from '@/src/components/ui/use-toast'
import { zodResolver } from '@hookform/resolvers/zod'
import { Button, Spinner } from '@nextui-org/react'
import { useRouter } from 'next/navigation'
import { useContext, useState } from 'react'
import { useForm } from 'react-hook-form'
import * as z from 'zod'
import { createEntitySupplier } from '../../actions'
import { GetStartedContext } from '../../steps'

const FormSchema = z.object({
	agree: z.boolean().default(false).optional()
})

const TermStep1 = () => {
	const router = useRouter()
	const [hasAgreed, setHasAgreed] = useState(false)
	const [loading, setLoading] = useState(false)

	const { entity, createEntitySupplierInput, updateEntitySupplier } = useContext(GetStartedContext)
	const form = useForm<z.infer<typeof FormSchema>>({
		resolver: zodResolver(FormSchema),
		defaultValues: {
			agree: false
		}
	})

	async function onSubmit(data: z.infer<typeof FormSchema>) {
		const { image, coverImages, deliveryDetail, productTypes, stockLocations, supplierType, stateRegistrationOrder, certificationIds } =
			createEntitySupplierInput ?? {}

		if (entity?._id && supplierType && deliveryDetail && stockLocations) {
			setLoading(true)

			const { response, error } = await createEntitySupplier({
				entityId: entity?._id,
				entityType: Entity_Type.Company,
				createEntitySupplierInput: {
					entityType: Entity_Type.Company,
					supplierTypeId: supplierType,
					productTypeIds: [entity?._id],
					deliveryDetail,
					image: {
						bucket: '',
						key: '',
						location: `https://agrix-supplier.s3.ap-southeast-1.amazonaws.com/${image?.files[0].url}`
					},
					coverImages: (coverImages || []).map(image => {
						return {
							location: `https://agrix-supplier.s3.ap-southeast-1.amazonaws.com/${image?.files[0].url}`,
							bucket: '',
							key: ''
						}
					}),
					certificationIds: certificationIds || [],
					stockLocationIds: stockLocations.map(location => location._id),
					isDomestic: false,
					isInternational: false,
					entitySupplierCompany: {
						stateRegistrationOrder: {
							bucket: '',
							key: '',
							location: `https://agrix-supplier.s3.ap-southeast-1.amazonaws.com/${stateRegistrationOrder?.files[0].url}`
						}
					}
				}
			})

			setLoading(false)

			if (error) {
				toast({
					title: error,
					variant: 'destructive'
				})
			}

			if (response) {
				router.push(`/home/profile/${response.data?.createEntitySupplier._id}`)
				setLoading(false)
			}
		} else {
			toast({
				variant: 'destructive',
				title: 'Required fields missing'
			})
		}
	}

	return (
		<div className="overflow-y-auto h-full">
			<Form {...form}>
				<form onSubmit={form.handleSubmit(onSubmit)} className="space-y-6">
					<div className="h-full">
						<div className="text-black text-lg font-semibold leading-7">Generel terms & condition</div>
						<div className="text-zinc-800 text-sm font-medium leading-snug mt-3">Summary:</div>
						<div className="text-zinc-800 text-sm font-normal leading-snug text-justify">
							Lorem ipsum dolor sit amet consectetur. Eget ut semper adipiscing quam euismod nunc suspendisse odio. Id sem leo nisi lacus. Mi odio
							etiam ultrices arcu in proin consequat sed. Aliquam dignissim purus feugiat cursus maecenas sapien.
						</div>

						<div className="text-zinc-800 text-sm font-normal leading-snug text-justify mt-6">
							Lorem ipsum dolor sit amet consectetur. Eget ut semper adipiscing quam euismod nunc suspendisse odio. Id sem leo nisi lacus. Mi odio
							etiam ultrices arcu in proin consequat sed. Aliquam dignissim purus feugiat cursus maecenas sapien.
						</div>

						<div className="mt-6 text-black text-lg font-semibold leading-7">Acceptable Use Policy</div>
						<div className="text-zinc-800 text-sm font-medium leading-snug mt-3">Summary:</div>
						<div className="text-zinc-800 text-sm font-normal leading-snug text-justify">
							Lorem ipsum dolor sit amet consectetur. Eget ut semper adipiscing quam euismod nunc suspendisse odio. Id sem leo nisi lacus. Mi odio
							etiam ultrices arcu in proin consequat sed. Aliquam dignissim purus feugiat cursus maecenas sapien.
						</div>

						<div className="text-zinc-800 text-sm font-normal leading-snug text-justify mt-6">
							Lorem ipsum dolor sit amet consectetur. Eget ut semper adipiscing quam euismod nunc suspendisse odio. Id sem leo nisi lacus. Mi odio
							etiam ultrices arcu in proin consequat sed. Aliquam dignissim purus feugiat cursus maecenas sapien.
						</div>

						<div className="mt-6 text-black text-lg font-semibold leading-7">Cancellation Policy</div>
						<div className="text-zinc-800 text-sm font-medium leading-snug mt-3">Summary:</div>
						<div className="text-zinc-800 text-sm font-normal leading-snug text-justify">
							Lorem ipsum dolor sit amet consectetur. Eget ut semper adipiscing quam euismod nunc suspendisse odio. Id sem leo nisi lacus. Mi odio
							etiam ultrices arcu in proin consequat sed. Aliquam dignissim purus feugiat cursus maecenas sapien.
						</div>

						<div className="text-zinc-800 text-sm font-normal leading-snug text-justify mt-6">
							Lorem ipsum dolor sit amet consectetur. Eget ut semper adipiscing quam euismod nunc suspendisse odio. Id sem leo nisi lacus. Mi odio
							etiam ultrices arcu in proin consequat sed. Aliquam dignissim purus feugiat cursus maecenas sapien.
						</div>

						<FormField
							control={form.control}
							name="agree"
							render={({ field }) => (
								<FormItem className="my-10 flex items-center space-y-0">
									<FormControl>
										<Checkbox
											checked={hasAgreed}
											onCheckedChange={() => setHasAgreed(agreed => !agreed)}
											className="data-[state=checked]:bg-[#57CC99] [state=checked]:text-[#57CC99]-foreground data-[state=checked]:border-[#57CC99]"
										/>
									</FormControl>
									<FormLabel className="mt-0 ml-2.5">I have read and agreed to the Terms and Conditions</FormLabel>
								</FormItem>
							)}
						/>

						<Button color="primary" disabled={!hasAgreed || loading} onClick={() => form.handleSubmit(onSubmit)()}>
							{loading ? <Spinner size="sm" color="white" /> : 'Submit'}
						</Button>
					</div>
				</form>
			</Form>
		</div>
	)
}

export default TermStep1
