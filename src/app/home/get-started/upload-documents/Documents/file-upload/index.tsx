'use client'

import { cn } from '@/src/lib/utils'
import React from 'react'
import { ClassNameValue } from 'tailwind-merge'

type Props = {
	value?: File
	onChange: (value?: File) => void
	name: string
	disabled?: boolean
	className?: ClassNameValue
}

const FileUpload = React.forwardRef<HTMLDivElement, Props>(({ ...props }, ref) => {
	const { value, onChange, name, disabled, className } = props

	const handleClickUpload = () => {
		if (disabled) return
		document.getElementById(name)?.click()
	}

	const onChangeFile = (e: React.ChangeEvent<HTMLInputElement>) => {
		const file = e.target.files?.[0]
		onChange(file)
	}

	return (
		<>
			<div ref={ref} className={cn('h-10 rounded-xl border border-zinc-200 flex', className)}>
				<div className="px-4 w-full flex items-center flex-grow-0 overflow-hidden">
					<span className="truncate grow text-sm">
						{value ? <span>{value.name}</span> : <span className="text-gray-400">File. * jpg, png, pdf</span>}
					</span>
					{value && (
						<svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28" fill="none">
							<path
								d="M2.7998 14C2.7998 7.81441 7.81422 2.8 13.9998 2.8C20.1854 2.8 25.1998 7.81441 25.1998 14C25.1998 20.1856 20.1854 25.2 13.9998 25.2C7.81422 25.2 2.7998 20.1856 2.7998 14Z"
								fill="#17B26A"
							/>
							<path
								d="M17.9099 11.49L12.5998 16.8L10.7898 14.99M13.9998 2.8C7.81422 2.8 2.7998 7.81441 2.7998 14C2.7998 20.1856 7.81422 25.2 13.9998 25.2C20.1854 25.2 25.1998 20.1856 25.1998 14C25.1998 7.81441 20.1854 2.8 13.9998 2.8Z"
								stroke="white"
								strokeWidth="2"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
						</svg>
					)}
				</div>
				<div
					className={cn(
						'bg-gray-50 h-full text-sm rounded-r-xl px-4 flex items-center border-l border-zinc-200 cursor-pointer',
						disabled && 'cursor-not-allowed'
					)}
					onClick={handleClickUpload}>
					Browse
				</div>
			</div>

			<input id={name} name={name} type="file" accept="application/pdf,image/*" required onChange={onChangeFile} className="hidden" />
		</>
	)
})

FileUpload.displayName = 'File upload'

export default FileUpload
