'use client'

import { Form, FormControl, FormField, FormItem, FormMessage } from '@/src/components/ui/form'
import { toast } from '@/src/components/ui/use-toast'
import { zodResolver } from '@hookform/resolvers/zod'
import { Button, Spinner, Table, TableBody, TableCell, TableColumn, TableHeader, TableRow, getKeyValue } from '@nextui-org/react'
import { Plus, XIcon } from 'lucide-react'
import { useContext, useState } from 'react'
import { useForm } from 'react-hook-form'
import * as z from 'zod'
import { uploadFile } from '../../../upload-file'
import { getCerticateSource } from '../../actions'
import { GetStartedContext } from '../../steps'
import FileUpload from './file-upload'
import SpecialCertificationModal from '@/src/components/modals/special-certification-modal'
import moment from 'moment'

type Column = {
	key: 'owning' | 'certificate' | 'delete' | 'issueDate' | 'expiryDate'
	label: string
}

const columns: Column[] = [
	{
		key: 'owning',
		label: 'Document name'
	},
	{
		key: 'certificate',
		label: 'Certificate'
	},
	{
		key: 'issueDate',
		label: 'Issue Date'
	},
	{
		key: 'expiryDate',
		label: 'Expiry Date'
	},
	{
		key: 'delete',
		label: ''
	}
]

const FormSchema = z.object({
	stateRegistrationOrder: z.instanceof(File),
	specialCertificates: z.array(
		z.object({
			owning: z.string(),
			certificate: z.string(),
			issueDate: z.date(),
			expiryDate: z.date(),
			attachment: z.string()
		})
	)
})

export type SpecialCertificateType = z.infer<typeof FormSchema>['specialCertificates'][number]

const Documents = () => {
	const { nextStep, updateEntitySupplier } = useContext(GetStartedContext)
	const [isOpen, setIsOpen] = useState(false)
	const [loading, setLoading] = useState(false)

	const form = useForm<z.infer<typeof FormSchema>>({
		resolver: zodResolver(FormSchema)
	})

	async function onSubmit(data: z.infer<typeof FormSchema>) {
		setLoading(true)
		const response = await uploadFile(data.stateRegistrationOrder)

		setLoading(false)

		if (!response) {
			toast({
				title: 'Error uploading doc',
				variant: 'destructive'
			})
			return
		}

		updateEntitySupplier!({ stateRegistrationOrder: response! })
		nextStep?.()
	}

	function handleClickContinue() {
		form.handleSubmit(onSubmit)()
	}

	const getColumnData = (item: any, dataColumns: Column['key']) => {
		const onClickRemove = () => {
			const certs = form.watch('specialCertificates') || []
			const filteredFilesArray = certs.filter(function (data) {
				return data.owning !== item.owning || data.certificate !== item.certificate || data.attachment !== item.attachment
			})
			form.setValue('specialCertificates', filteredFilesArray)
		}

		switch (dataColumns) {
			case 'delete':
				return <XIcon onClick={() => onClickRemove()} className="cursor-pointer text-danger-400" color={'red'} />
			case 'issueDate':
				return moment(getKeyValue(item, 'expiryDate')).format('yyyy/MM/DD')
			case 'expiryDate':
				return moment(getKeyValue(item, 'expiryDate')).format('yyyy/MM/DD')
			case 'owning':
				return getKeyValue(item, 'owning')
			case 'certificate':
				return getKeyValue(item, 'certificate')
		}
	}

	const onSave = (newCert: SpecialCertificateType) => {
		const cert = form.watch('specialCertificates') || []
		cert.push(newCert)
		form.setValue('specialCertificates', cert)
	}

	return (
		<>
			<Form {...form}>
				<form className="w-full h-full flex flex-col" onSubmit={form.handleSubmit(onSubmit)}>
					<div className="h-full overflow-hidden">
						<div className="text-black text-lg font-semibold leading-7">State registration order</div>
						<div className="mt-2 text-gray-400 text-sm font-normal leading-snug">PDF, Image files, Photos are acceptable.</div>
						<div className="mt-4">
							<FormField
								control={form.control}
								name="stateRegistrationOrder"
								render={({ field }) => (
									<FormItem>
										<FormControl>
											<FileUpload {...field} />
										</FormControl>
										<FormMessage />
									</FormItem>
								)}
							/>
						</div>
						<div className="w-full h-px border border-gray-100 my-4" />

						<div className="flex">
							<div className="grow">
								<div className="text-black text-lg font-semibold leading-7">Special certificates</div>
								<div className="mt-2 text-gray-400 text-sm font-normal leading-snug">Include any certifcate that validates your company.</div>
							</div>
							<div>
								<Button onClick={() => setIsOpen(true)} color="primary" size="sm" startContent={<Plus size={22} />}>
									Add File
								</Button>

								<SpecialCertificationModal open={isOpen} onOpenChange={setIsOpen} onSave={onSave} />
							</div>
						</div>

						<div className="h-full">
							<FormField
								control={form.control}
								name="specialCertificates"
								render={({ field }) => {
									return (
										<FormItem>
											<FormControl>
												<>
													<Table aria-label="Documents" removeWrapper className="mt-4 h-40 overflow-y-auto">
														<TableHeader columns={columns}>{column => <TableColumn key={column.key}>{column.label}</TableColumn>}</TableHeader>
														<TableBody items={field.value || []}>
															{item => (
																<TableRow key={item.owning}>
																	{columnKey => <TableCell>{getColumnData(item, columnKey as Column['key'])}</TableCell>}
																</TableRow>
															)}
														</TableBody>
													</Table>
												</>
											</FormControl>
											<FormMessage />
										</FormItem>
									)
								}}
							/>
						</div>
					</div>

					<Button disabled={loading} color="primary" className="w-full mt-6 shrink-0" onClick={handleClickContinue}>
						{loading ? <Spinner color="white" size="sm" /> : 'Save'}
					</Button>
				</form>
			</Form>
		</>
	)
}

export default Documents
