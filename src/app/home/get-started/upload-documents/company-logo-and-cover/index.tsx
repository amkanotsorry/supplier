'use client'
import { Form, FormControl, FormField, FormItem, FormMessage } from '@/src/components/ui/form'
import UploadImage from '@/src/components/upload-image'
import DragAndDropImageUpload from '@/src/components/upload-image/DragAndDropImageUpload'
import { zodResolver } from '@hookform/resolvers/zod'
import { useForm } from 'react-hook-form'
import * as z from 'zod'
import { useContext, useState } from 'react'
import { GetStartedContext } from '../../steps'
import { Button, Spinner } from '@nextui-org/react'
import Seperator from '@/src/components/seperator'
import { uploadFile } from '../../../upload-file'

export const dynamic = 'force-dynamic'

const FormSchema = z.object({
	image: z.instanceof(File),
	coverImages: z.array(z.instanceof(File))
})

type Props = {
	handleClickNext: () => void
}

const CompanyLogoAndCover = (props: Props) => {
	const { updateEntitySupplier } = useContext(GetStartedContext)
	const [loading, setLoading] = useState(false)

	const { handleClickNext } = props
	const form = useForm<z.infer<typeof FormSchema>>({
		resolver: zodResolver(FormSchema),
		defaultValues: {
			coverImages: []
		}
	})

	const image = form.watch('image')
	const coverImages = form.watch('coverImages')
	const isDisabled = !image || coverImages.length < 1

	async function onSubmit(data: z.infer<typeof FormSchema>) {
		setLoading(true)
		const imageUrl = await uploadFile(data.image)
		const coverImagesUrl: any[] = []

		for (const coverImage of data.coverImages) {
			let response = await uploadFile(coverImage)
			coverImagesUrl.push(response)
		}
		setLoading(false)
		updateEntitySupplier!({ image: imageUrl!, coverImages: coverImagesUrl })
		handleClickNext()
		return
	}

	function handleClickContinue() {
		form.handleSubmit(onSubmit)()
	}

	return (
		<>
			<Form {...form}>
				<form className="w-full h-full overflow-y-auto" onSubmit={form.handleSubmit(onSubmit)}>
					<div>
						<div className="text-black text-lg font-semibold leading-7">Company Logo </div>

						<div className="flex justify-between items-center">
							<div className="mt-2 flex flex-col gap-1.5">
								<div className="flex text-gray-400 text-sm font-normal">
									<div className="w-36 ">Ratio:</div>
									<div className="">1x1</div>
								</div>

								<div className="flex text-gray-400 text-sm font-normal">
									<div className="w-36 ">Maximum file size:</div>
									<div className="">5120 KB</div>
								</div>

								<div className="flex text-gray-400 text-sm font-normal">
									<div className="w-36 ">Recommended size:</div>
									<div className="">1000x1000 px</div>
								</div>

								<div className="flex text-gray-400 text-sm font-normal">
									<div className="w-36 ">File format:</div>
									<div className="">JPG, JPEG, PNG</div>
								</div>
							</div>

							<div className="mr-4">
								<FormField
									control={form.control}
									name="image"
									render={({ field }) => (
										<FormItem>
											<FormControl>
												<UploadImage {...field} />
											</FormControl>
											<FormMessage />
										</FormItem>
									)}
								/>
							</div>
						</div>

						<Seperator className={'my-6'} />

						<div className="text-black text-lg font-semibold leading-7">{`Cover image - ${coverImages.length}/5`}</div>

						<div className="mt-2 flex flex-col gap-1.5">
							<div className="flex text-gray-400 text-sm font-normal">
								<div className="w-36 ">Ratio:</div>
								<div className="">8x1</div>
							</div>

							<div className="flex text-gray-400 text-sm font-normal">
								<div className="w-36 ">Maximum file size:</div>
								<div className="">5120 KB</div>
							</div>

							<div className="flex text-gray-400 text-sm font-normal">
								<div className="w-36 ">Recommended size:</div>
								<div className="">1600x200 pxd</div>
							</div>

							<div className="flex text-gray-400 text-sm font-normal">
								<div className="w-36 ">File format:</div>
								<div className="">JPG, JPEG, PNG</div>
							</div>
						</div>

						<div className="mt-4">
							<FormField
								control={form.control}
								name="coverImages"
								render={({ field }) => (
									<FormItem>
										<FormControl>
											<DragAndDropImageUpload {...field} />
										</FormControl>
										<FormMessage />
									</FormItem>
								)}
							/>
						</div>
					</div>

					<Button color="primary" className="w-full mt-6" onClick={handleClickContinue} disabled={isDisabled || loading}>
						{loading ? <Spinner size="sm" color="white" /> : 'Continue'}
					</Button>
				</form>
			</Form>
		</>
	)
}

export default CompanyLogoAndCover
