'use client'
import Steps from '@/src/components/steps'
import React, { useState } from 'react'
import CompanyLogoAndCover from './company-logo-and-cover'
import { ApolloFileUploadWrapper } from '@/src/app/api/graphql/apollo-wrapper'
import Documents from './Documents'

interface Props {}

const UploadDocuments: React.FC<Props> = () => {
	const [current, setCurrent] = useState(0)

	const handleClickNext = () => setCurrent(current + 1)
	// const handleClickPrev = () => setCurrent(current - 1)

	return (
		<div className="mt-8 w-full h-full overflow-hidden p-4 bg-white rounded-2xl">
			<Steps
				direction="vertical"
				items={[
					{
						title: 'Company logo & cover',
						content: (
							<ApolloFileUploadWrapper>
								<CompanyLogoAndCover handleClickNext={handleClickNext} />
							</ApolloFileUploadWrapper>
						)
					},
					{
						title: 'Documents',
						content: <Documents />
					}
				]}
				current={current}
			/>
		</div>
	)
}

export default UploadDocuments
