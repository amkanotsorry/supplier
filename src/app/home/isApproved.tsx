'use client'

import { redirect } from 'next/navigation'
import { useLayoutEffect } from 'react'
import { Entity_Status, Review_Status } from '../api/generated/graphql'
import useEntity from '../hooks/use-entity'

export default function isApproved(Component: any) {
	return function IsApproved(props: any) {
		const { entity, review } = useEntity({})

		useLayoutEffect(() => {
			if (!entity.entitySupplierId) return redirect('/home/get-started')
		}, [entity.entitySupplier?.status, entity.entitySupplierId, review?.status])

		return <Component {...props} />
	}
}
