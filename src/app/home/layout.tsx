import { getServerSession } from 'next-auth'
import { redirect } from 'next/navigation'
import React from 'react'
import { authOptions } from '../api/auth/[...nextauth]/authOptions'
import { QueryEntityType } from './actions'
import PageHeader from './_components/page-header'
import { getProfile } from './get-started/actions'
import SideMenu from './side-menu'

export default async function HomeLayout({ children }: { children: React.ReactNode }) {
	const session = await getServerSession(authOptions)
	if (!session?.user) return redirect('/login')

	const { profile } = await getProfile(session.user.id)

	if (!profile?.isEmailVerified) return redirect(`/register/verify?email=${profile?.email}`)
	if (!profile?.currentEntityId) return redirect('/register/entity-info')

	return (
		<div className="flex h-full">
			<SideMenu entity={profile?.currentEntity as QueryEntityType} />

			<div className="w-full max-h-screen flex flex-col flex-1 overflow-hidden">
				<PageHeader />

				{children}
			</div>
		</div>
	)
}
