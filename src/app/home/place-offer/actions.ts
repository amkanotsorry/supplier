'use server'

import {
	GetMyOfferDraftsDocument,
	GetMyOfferDraftsQuery,
	GetMyOffersDocument,
	GetMyOffersQuery,
	GetOfferDraftByIdDocument
} from '../../api/generated/graphql'
import { getClient } from '../../api/graphql/client'

type MyOffers = Extract<GetMyOffersQuery['offers']['docs'], any[]>

export type QueryOfferType = MyOffers[number]

export const getMyOffers = async (createUserId: string) => {
	const client = getClient()
	try {
		const { data } = await client.query({
			query: GetMyOffersDocument,
			variables: {
				pagination: {
					limit: 100,
					page: 1
				},
				where: {
					createdUserId: {
						eq: createUserId
					}
				}
			},
			fetchPolicy: 'network-only'
		})

		return { offers: data.offers.docs, error: null }
	} catch (error) {
		return { offers: [], error: JSON.stringify(error, null, 2) }
	}
}

type MyOfferDrafts = Extract<GetMyOfferDraftsQuery['findOfferDrafts']['docs'], any[]>

export type QueryOfferDraftType = MyOfferDrafts[number]

export const getMyOfferDrafts = async (createdUserId: string) => {
	const client = getClient()
	try {
		const { data } = await client.query({
			query: GetMyOfferDraftsDocument,
			variables: {
				where: {
					createdUserId: {
						eq: createdUserId
					}
				}
			}
		})

		return { offerDrafts: data.findOfferDrafts.docs, error: null }
	} catch (error) {
		return { offerDrafts: [], error: JSON.stringify(error, null, 2) }
	}
}

export const getOfferDraftById = async (id: string) => {
	const client = getClient()
	try {
		const { data } = await client.query({
			query: GetOfferDraftByIdDocument,
			variables: {
				offerDraftId: id
			}
		})

		return data.offerDraft
	} catch (error) {
		console.log(error)
	}
}
