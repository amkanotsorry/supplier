'use client'

import React, { createContext, useState } from 'react'
import { QueryProductType } from '../../product/actions/product'

export type CurrentStepType = 1 | 2 | 3 | 4

type AllVariantQuerytype = Extract<QueryProductType['variants'], any[]>
type QueryVariantType = AllVariantQuerytype[number]

type LeadTime = {
	min?: string
	max?: string
	days?: string
}

type PriceList = {
	min?: string
	max?: string
	price?: string
}

export type CustomVariant = {
	variant: QueryVariantType
	quantity?: number
	minimumOrderQuantity?: number
	availableDate?: Date
	leadTimes?: LeadTime[]
	priceList?: PriceList[]
	hasProductTestReport?: boolean
	hasPlaceOfOrigin?: boolean
	hasCertificateOfOrigin?: boolean
}

type ContextValueType = {
	currentStep: CurrentStepType
	nextStep: () => void
	previousStep: () => void
	products: QueryProductType[]
	selectedProduct: QueryProductType | null
	setSelectedProduct: React.Dispatch<React.SetStateAction<QueryProductType | null>>
	selectedVariants: CustomVariant[]
	setSelectedVariants: React.Dispatch<React.SetStateAction<CustomVariant[]>>
	sheetVariant: CustomVariant
	setSheetVariant: React.Dispatch<React.SetStateAction<CustomVariant>>
	isSheetOpen: boolean
	setIsSheetOpen: React.Dispatch<React.SetStateAction<boolean>>
	onSelectVariant: (variant: CustomVariant) => void
	onUpdateOfferVariant: (variantId: string, updateValues: CustomVariant) => void
}

const initialValue: ContextValueType = {
	currentStep: 1,
	nextStep: () => {},
	previousStep: () => {},
	products: [],
	selectedProduct: null,
	setSelectedProduct: () => {},
	selectedVariants: [],
	setSelectedVariants: () => {},
	sheetVariant: {} as CustomVariant,
	setSheetVariant: () => {},
	isSheetOpen: false,
	setIsSheetOpen: () => {},
	onSelectVariant: () => {},
	onUpdateOfferVariant: () => {}
}

export const CreateOfferContext = createContext(initialValue)

const CreateOfferContextProvider: React.FC<{
	children: React.ReactNode
	products: QueryProductType[]
	initialSelectedVariants: CustomVariant[]
}> = ({ children, products, initialSelectedVariants }) => {
	const [currentStep, setCurrentStep] = useState<CurrentStepType>(1)
	const [selectedProduct, setSelectedProduct] = useState<QueryProductType | null>(
		initialSelectedVariants.length > 0 ? products.find(product => product._id === initialSelectedVariants[0].variant.productId) || null : null
	)
	const [selectedVariants, setSelectedVariants] = useState<CustomVariant[]>(initialSelectedVariants)
	const [sheetVariant, setSheetVariant] = useState<CustomVariant>({} as CustomVariant)
	const [isSheetOpen, setIsSheetOpen] = useState<boolean>(false)

	const nextStep = () => {
		if (currentStep === 4) return // TODO show terms pop up
		setCurrentStep(prev => (prev + 1) as CurrentStepType)
	}

	const previousStep = () => {
		if (currentStep === 1) return
		setCurrentStep(prev => (prev - 1) as CurrentStepType)
	}

	const onSelectVariant = (variant: CustomVariant) => {
		setSelectedVariants(prev => {
			if (selectedVariants.find(selectedVariant => selectedVariant.variant._id === variant.variant._id)) {
				return prev.filter(prevVariant => prevVariant.variant._id !== variant.variant._id)
			}
			const temp = [...prev]
			temp.push(variant)
			return temp
		})
	}

	const onUpdateOfferVariant = (variantId: string, updatedValues: CustomVariant) => {
		setSelectedVariants(prevVariants => {
			const index = prevVariants.findIndex(variant => variant.variant._id === variantId)

			if (index === -1) {
				console.error(`Variant with ID ${variantId} not found.`)
				return prevVariants
			}

			const updatedVariants = [...prevVariants]
			updatedVariants[index] = { ...updatedVariants[index], ...updatedValues }

			return updatedVariants
		})
	}

	return (
		<CreateOfferContext.Provider
			value={{
				currentStep,
				nextStep,
				previousStep,
				products,
				selectedProduct,
				setSelectedProduct,
				selectedVariants,
				setSelectedVariants,
				sheetVariant,
				setSheetVariant,
				isSheetOpen,
				setIsSheetOpen,
				onSelectVariant,
				onUpdateOfferVariant
			}}>
			{children}
		</CreateOfferContext.Provider>
	)
}

export default CreateOfferContextProvider
