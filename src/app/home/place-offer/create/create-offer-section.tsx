'use client'

import React, { useContext, useState } from 'react'

import {
	CreateOfferCustomizationInput,
	CreateOfferDraftInput,
	CreateOfferInput,
	Delivery_Type,
	GetCollectionPointsQuery,
	GetOfferDraftByIdQuery,
	GetPortsQuery,
	Market_Country,
	Market_Type,
	Warranty_Period_Type
} from '@/src/app/api/generated/graphql'
import { toast } from '@/src/components/ui/use-toast'
import { zodResolver } from '@hookform/resolvers/zod'
import { useRouter } from 'next/navigation'
import { FormProvider, useForm } from 'react-hook-form'
import { z } from 'zod'
import { createOffer, draftOffer } from '../../actions'
import { CreateOfferContext } from './context'
import Footer from './footer'
import DeliveryPackagingSection from './sections/delivery-packaging-section'
import MarketPaymentSection from './sections/market-payment-section'
import ProductSection from './sections/product-section'
import EditOfferVariantSheet from './sections/product-section/edit-offer-variant-sheet'
import ProductSpecSection from './sections/product-spec-section'
import StepsHeader from './steps-header'
import isApproved from '../../isApproved'

export const CreateOfferFormSchema = z.object({
	// Product Specifications
	startDate: z.date().optional(),
	validUntilDate: z.date().optional(),
	tags: z.string().array().optional(),
	warrantyType: z.nativeEnum(Warranty_Period_Type).optional(),
	warrantyValue: z.string().optional(),
	colorAvailable: z.boolean().optional(),
	colorValue: z.string().optional(),
	packageAvailable: z.boolean().optional(),
	packageValue: z.string().optional(),
	logoAvailable: z.boolean().optional(),
	logoValue: z.string().optional(),
	hasSampleDelivery: z.boolean().optional(),
	isReturnable: z.boolean().optional(),
	isRefundable: z.boolean().optional(),
	composition: z.string().optional(),

	// Delivery & Packaging
	deliveryDetail: z
		.object({
			deliveryType: z.nativeEnum(Delivery_Type),
			stockLocationId: z.string().optional(),
			aimak: z.string().optional(),
			collectionPointId: z.string().optional(),
			cityProvince: z.string().optional(),
			duuregSoum: z.string().optional(),
			portId: z.string().optional()
		})
		.optional(),

	//Delivery type toAimak
	aimak: z.string().optional(),
	collectionPoint: z.string().optional(),

	//Delivery type DeliverToLocalPort
	cityProvince: z.string().optional(),
	duuregSoum: z.string().optional(),
	portName: z.string().optional(),

	//Delivery type FullDelivery
	stockLocation: z.string().optional(),

	isTempControl: z.boolean().optional(),
	isCantShippedAir: z.boolean().optional(),
	isSensitiveToMoist: z.boolean().optional(),
	packagingDetail: z.string().optional(),

	// Market & Payment
	marketType: z.nativeEnum(Market_Type).array(),
	advancePayment: z.string().optional(),
	interimPayment: z.string().optional(),
	finalPayment: z.string().optional(),
	accountNumber: z.string().optional(),

	additionalDocuments: z.string().array().optional()
})

export type CreateOfferType = z.infer<typeof CreateOfferFormSchema>

interface Props {
	offerDraft?: GetOfferDraftByIdQuery['offerDraft']
	ports?: Extract<GetPortsQuery['ports'], any>
	collectionPoints?: Extract<GetCollectionPointsQuery['collectionPoints'], any>
}

const CreateOfferSection: React.FC<Props> = ({ offerDraft, ports, collectionPoints }) => {
	const router = useRouter()
	const { currentStep, selectedProduct, selectedVariants } = useContext(CreateOfferContext)
	const [loading, setLoading] = useState(false)

	const colorVal = offerDraft?.customization?.color?.moq
	const packageVal = offerDraft?.customization?.package?.moq
	const logoVal = offerDraft?.customization?.logo?.moq

	const methods = useForm<CreateOfferType>({
		resolver: zodResolver(CreateOfferFormSchema),
		defaultValues: {
			// Product Specifications
			startDate: offerDraft?.offerStartDate && new Date(offerDraft.offerStartDate),
			validUntilDate: offerDraft?.offerEndDate && new Date(offerDraft.offerEndDate),
			tags: [], // TODO
			warrantyType: offerDraft?.warrantyPeriodType || Warranty_Period_Type.Days,
			warrantyValue: offerDraft?.warrantyPeriod?.toString() || '',
			colorAvailable: Boolean(colorVal),
			colorValue: colorVal?.toString(),
			packageAvailable: Boolean(packageVal),
			packageValue: packageVal?.toString(),
			logoAvailable: Boolean(logoVal),
			logoValue: logoVal?.toString(),
			hasSampleDelivery: !!offerDraft?.isSampleDelivery,
			isReturnable: !!offerDraft?.isReturnable,
			isRefundable: !!offerDraft?.isRefundable,
			composition: offerDraft?.composition || '',

			// Delivery & Packaging
			...offerDraft?.deliveryDetail,

			isTempControl: !!offerDraft?.hasTemperatureControl,
			isCantShippedAir: !!offerDraft?.isShippableThroughAir,
			isSensitiveToMoist: !!offerDraft?.isSensitiveToMoisture,
			packagingDetail: offerDraft?.packageDetail || '',

			// Market & Payment
			marketType: offerDraft?.marketType || [],
			advancePayment: offerDraft?.paymentType?.advance.toString() || '0',
			interimPayment: offerDraft?.paymentType?.interim.toString() || '0',
			finalPayment: offerDraft?.paymentType?.final.toString() || '0',
			accountNumber: offerDraft?.paymentMethods?.[0]?.acountNumber?.toString(),

			additionalDocuments: []
		}
	})

	const onSubmit = async (data: CreateOfferType, action: 'save' | 'draft') => {
		console.log('÷÷÷÷÷/')
		if (!selectedProduct) {
			toast({ title: 'Product is required', variant: 'destructive' })
			return
		}

		console.log('drafting')

		const offerData: CreateOfferInput | CreateOfferDraftInput = {
			additionalDocuments: ['http://lorem.putsum/1243'],
			composition: data.composition,
			customization: undefined,
			deliveryDetail: {
				deliveryType: data.deliveryDetail?.deliveryType || Delivery_Type.ThirdParty
			},
			hasTemperatureControl: data.isTempControl || false,
			isRefundable: data.isRefundable || false,
			isReturnable: data.isReturnable || false,
			isSampleDelivery: data.hasSampleDelivery || false,
			isSensitiveToMoisture: data.isSensitiveToMoist || false,
			isShippableThroughAir: data.isCantShippedAir || false,
			isVerified: false,
			marketCountry: Market_Country.Montenegro,
			marketType: data.marketType?.length === 0 ? [Market_Type.Local] : data.marketType,
			offerEndDate: data.validUntilDate || new Date(),
			offerStartDate: data.startDate || new Date(),
			packageDetail: data.packagingDetail,
			paymentMethods: [
				{
					acountNumber: parseInt(data.accountNumber || '0', 10),
					icon: 'http://lorem.putsum/1243',
					name: ''
				}
			],
			paymentType: {
				advance: parseInt(data.advancePayment || '0', 10),
				final: parseInt(data.finalPayment || '0', 10),
				interim: parseInt(data.interimPayment || '0', 10)
			},
			productId: selectedProduct._id,
			tags: [],
			variants: selectedVariants.map(v => {
				return {
					availableDate: v.availableDate || new Date(),
					discloseCertificateOfOrigin: v.hasCertificateOfOrigin,
					disclosePlaceOfOrigin: v.hasPlaceOfOrigin,
					discloseTestReport: v.hasProductTestReport,
					moq: v.minimumOrderQuantity || 0,
					quantity: v.quantity || 0,
					quantityRangeLeadTimes: (v.leadTimes || []).map(leadTime => {
						return {
							days: parseInt(leadTime.days || '0', 10),
							max: parseInt(leadTime.max || '0', 10),
							min: parseInt(leadTime.min || '0', 10)
						}
					}),
					quantityRangePrices: (v.priceList || []).map(priceList => {
						return {
							price: parseInt(priceList.price || '0', 10),
							max: parseInt(priceList.max || '0', 10),
							min: parseInt(priceList.min || '0', 10)
						}
					}),
					variantId: v.variant._id
				}
			}),
			warrantyPeriod: parseInt(data.warrantyValue || '0', 10),
			warrantyPeriodType: data.warrantyType || Warranty_Period_Type.Days
		}

		if (data.colorAvailable || data.packageAvailable || data.logoAvailable) {
			let custom: CreateOfferCustomizationInput = {}
			if (data.colorAvailable) custom.color = { name: 'Custom color', moq: parseInt(data.colorValue || '0') }
			if (data.packageAvailable) custom.package = { name: 'Custom package', moq: parseInt(data.packageValue || '0') }
			if (data.logoAvailable) custom.logo = { name: 'Custom logo', moq: parseInt(data.logoValue || '0') }
			offerData.customization = custom
		}

		const deliveryDetail: CreateOfferInput['deliveryDetail'] = {
			deliveryType: data.deliveryDetail?.deliveryType || Delivery_Type.ThirdParty
		}

		switch (data.deliveryDetail?.deliveryType) {
			case Delivery_Type.Full:
				deliveryDetail.stockLocationId = data.deliveryDetail.stockLocationId
				break

			case Delivery_Type.ToAimagCollectionPoint:
				deliveryDetail.aimak = data.deliveryDetail.aimak
				deliveryDetail.collectionPointId = data.deliveryDetail.collectionPointId
				break

			case Delivery_Type.ToLocalPort:
				deliveryDetail.cityProvince = data.deliveryDetail.cityProvince
				deliveryDetail.duuregSoum = data.deliveryDetail.duuregSoum
				deliveryDetail.portId = data.deliveryDetail.portId
				break
			default:
				break
		}

		offerData.deliveryDetail = deliveryDetail

		if (!data.composition) delete offerData.composition

		setLoading(true)

		try {
			let response
			if (action === 'save') {
				const createOfferInput: CreateOfferInput = { ...offerData, numberOfOrderRequest: 0, numberOfCounterOffer: 0 } as CreateOfferInput

				if (offerDraft) {
					createOfferInput.offerDraftId = offerDraft._id
				}

				const res = await createOffer({
					createOfferInput
				})

				response = res.offer
			} else {
				console.log('on draft')
				const res = await draftOffer({
					createOfferInput: offerData
				})

				response = res.offer
			}
			console.log('wtf', response)

			setLoading(false)

			if (response?._id) {
				toast({ title: `Offer ${action === 'save' ? 'created' : 'drafted'} Successfully` })
				router.replace('/home/place-offer')
				return
			}

			toast({ title: 'Something went wrong', variant: 'destructive' })
		} catch (error) {
			toast({ title: 'Error creating offer. Please check required fields', variant: 'destructive' })
		}
	}

	const CurrentStepSection: React.FC = () => {
		switch (currentStep) {
			case 1:
				return <ProductSection />
		}
		switch (currentStep) {
			case 2:
				return <ProductSpecSection />
		}
		switch (currentStep) {
			case 3:
				return <DeliveryPackagingSection ports={ports} collectionPoints={collectionPoints} />
		}
		switch (currentStep) {
			case 4:
				return <MarketPaymentSection />
		}
	}

	return (
		<>
			<FormProvider {...methods}>
				<StepsHeader />

				<div className="h-full overflow-y-auto bg-lightgray-100">
					<form onSubmit={methods.handleSubmit(data => onSubmit(data, 'save'))}>
						<CurrentStepSection />
					</form>
				</div>

				<Footer
					loading={loading}
					onDraft={methods.handleSubmit(
						data => onSubmit(data, 'draft'),
						err => console.log('error', err)
					)}
					onSave={methods.handleSubmit(data => onSubmit(data, 'save'))}
				/>
			</FormProvider>

			<EditOfferVariantSheet />
		</>
	)
}

export default isApproved(CreateOfferSection)
