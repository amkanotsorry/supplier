import { Button, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, Spinner, useDisclosure } from '@nextui-org/react'
import React, { useContext } from 'react'
import { CreateOfferContext } from './context'
import { useFormContext } from 'react-hook-form'
import { z } from 'zod'
import { CreateOfferFormSchema } from './create-offer-section'
import { toast } from '@/src/components/ui/use-toast'

interface Props {
	onDraft: () => void
	onSave: () => void
	loading: boolean
}

const Footer: React.FC<Props> = ({ onSave, onDraft, loading }) => {
	const { currentStep, nextStep, previousStep } = useContext(CreateOfferContext)
	const { isOpen, onOpen, onOpenChange } = useDisclosure()
	const { getValues } = useFormContext<z.infer<typeof CreateOfferFormSchema>>()

	const validateFourthStep = () => {
		const advance = getValues('advancePayment')
		const interim = getValues('interimPayment')
		const final = getValues('finalPayment')

		if (parseInt(advance || '0', 10) + parseInt(interim || '0', 10) + parseInt(final || '0', 10) === 100) {
			onSave()
		} else {
			toast({ title: 'All payments should add up to 100%! Please re-enter payment amounts', variant: 'destructive' })
		}
	}

	const onClickContinue = () => {
		switch (currentStep) {
			case 4:
				validateFourthStep()
				break
			default:
				nextStep()
		}
	}

	return (
		<div className="shrink-0">
			<div className="bg-white border-t flex items-center p-4 gap-4">
				{currentStep > 1 && (
					<Button onClick={previousStep} className="border-[1px]" variant="bordered">
						Back
					</Button>
				)}
				<div className="flex-1" />

				<Button disabled={loading} onClick={onOpen} className="border-[1px]" variant="bordered" color="primary">
					{loading ? <Spinner size="sm" color="white" /> : 'Save Draft'}
				</Button>
				<Button disabled={loading} onClick={onClickContinue} className="disabled:bg-lightgray-200 disabled:text-lightgray-800" color="primary">
					{loading ? <Spinner size="sm" color="white" /> : currentStep === 4 ? 'Create Offer' : 'Continue'}
				</Button>
			</div>

			<Modal isOpen={isOpen} onOpenChange={onOpenChange}>
				<ModalContent>
					{onClose => (
						<>
							<ModalHeader className="flex flex-col gap-1">Would you like to draft this offer?</ModalHeader>
							<ModalBody>
								<p>You can edit and publish this offer whenever the time is right.</p>
							</ModalBody>
							<ModalFooter>
								<Button color="danger" variant="light" onPress={onClose}>
									Cancel
								</Button>
								<Button
									disabled={loading}
									onPress={() => {
										console.log('wtffffff')
										onDraft()
										onClose()
									}}
									color="primary">
									{loading ? <Spinner size="sm" color="white" /> : 'Save Draft'}
								</Button>
							</ModalFooter>
						</>
					)}
				</ModalContent>
			</Modal>
		</div>
	)
}

export default Footer
