import PageLoading from '@/src/components/page-loading'

export default function Loading() {
	return <PageLoading />
}
