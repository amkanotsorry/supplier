import moment from 'moment'
import PageSubHeader from '../../_components/page-header/page-sub-header'
import { getAllProducts } from '../../product/actions/product'
import CreateOfferContextProvider from './context'
import CreateOfferSection from './create-offer-section'
import { getCollectionPoints, getPorts } from '../../get-started/actions'
import { getEntitySupplierId } from '@/src/app/utils'

const OfferCreatePage = async () => {
	const id = await getEntitySupplierId()
	const { products } = await getAllProducts(id || '')

	const { ports } = await getPorts()
	const { collectionPoints } = await getCollectionPoints()

	const sortedProducts = [...products].sort((a, b) => moment(b.createdAt).unix() - moment(a.createdAt).unix())

	return (
		<>
			<PageSubHeader title="Create new offer" />

			<CreateOfferContextProvider initialSelectedVariants={[]} products={sortedProducts || []}>
				<CreateOfferSection ports={ports} collectionPoints={collectionPoints} />
			</CreateOfferContextProvider>
		</>
	)
}

export default OfferCreatePage
