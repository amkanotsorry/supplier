'use client'

import CheckboxField from '@/src/components/checkbox-field'
import Seperator from '@/src/components/seperator'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '@/src/components/ui/select'
import { Input, Textarea } from '@nextui-org/react'
import { DropletIcon, PlaneIcon } from 'lucide-react'
import React from 'react'
import { useFormContext } from 'react-hook-form'
import { z } from 'zod'
import { CreateOfferFormSchema } from '../create-offer-section'
import { Delivery_Type, GetCollectionPointsQuery, GetPortsQuery } from '@/src/app/api/generated/graphql'
import useEntity from '@/src/app/hooks/use-entity'
import { useAddress } from '@/src/lib/address-helper'

interface Props {
	ports?: Extract<GetPortsQuery['ports'], any>
	collectionPoints?: Extract<GetCollectionPointsQuery['collectionPoints'], any>
}

const DeliveryPackagingSection: React.FC<Props> = ({ ports, collectionPoints }) => {
	const form = useFormContext<z.infer<typeof CreateOfferFormSchema>>()
	const { setValue, getValues, watch } = form

	const { entity } = useEntity({})
	const { aimagHotOptions, sumDuuregOpns } = useAddress({
		form,
		aimagFormKey: 'deliveryDetail.cityProvince',
		sumFormKey: 'deliveryDetail.duuregSoum'
	})

	return (
		<div className="flex flex-col p-6 gap-6 max-w-[1080px] m-auto">
			<div className="flex flex-col border rounded-xl p-6 gap-6 bg-white w-full">
				<div className="flex gap-6">
					<div className="flex flex-1 flex-col">
						<div className="text-sm font-semibold mb-1">
							Delivery type <span className="text-danger-400">*</span>
						</div>

						<div className="text-neutral-700 text-xs font-medium">Select delivery type. </div>
					</div>

					<div className="flex flex-1 flex-col">
						<div className="text-xs text-lightgray-1000 font-semibold mb-2">Type</div>
						<Select
							value={watch('deliveryDetail.deliveryType')}
							onValueChange={value => setValue('deliveryDetail.deliveryType', value as Delivery_Type)}
							defaultValue={watch('deliveryDetail.deliveryType')}>
							<SelectTrigger className="w-full">
								<SelectValue placeholder="Select delivery type" />
							</SelectTrigger>
							<SelectContent>
								{[
									{ title: 'Full delivery type', value: Delivery_Type.Full },
									{ title: 'Deliver to local port', value: Delivery_Type.ToLocalPort },
									{ title: 'Deliver to Aimak collection point', value: Delivery_Type.ToAimagCollectionPoint },
									{ title: 'Third-party Delivery', value: Delivery_Type.ThirdParty }
								].map((deliveryType, index) => (
									<SelectItem className="py-3" key={index} value={deliveryType.value}>
										{deliveryType.title}
									</SelectItem>
								))}
							</SelectContent>
						</Select>

						{[Delivery_Type.Full, Delivery_Type.ToLocalPort, Delivery_Type.ToAimagCollectionPoint].includes(watch('deliveryDetail.deliveryType')) && (
							<div className="text-xs text-lightgray-1000 font-semibold mt-6 mb-2">Delivery detail</div>
						)}

						{watch('deliveryDetail.deliveryType') === Delivery_Type.ToLocalPort && (
							<div className="flex flex-col gap-4">
								<div className="flex gap-4">
									<Select
										value={watch('deliveryDetail.cityProvince')}
										onValueChange={value => setValue('deliveryDetail.cityProvince', value as Delivery_Type)}
										defaultValue={watch('deliveryDetail.cityProvince')}>
										<SelectTrigger className="bg-gray-50 border-0 h-10 flex-1">
											<SelectValue className="::placeholder:text-gray-400" placeholder="Choose City/Provice" />
										</SelectTrigger>
										<SelectContent>
											{aimagHotOptions.map((aimag, index) => (
												<SelectItem key={index} value={aimag.code.toString()}>
													{aimag.name}
												</SelectItem>
											))}
										</SelectContent>
									</Select>

									<Select
										value={watch('deliveryDetail.duuregSoum')}
										onValueChange={value => setValue('deliveryDetail.duuregSoum', value as Delivery_Type)}
										defaultValue={watch('deliveryDetail.duuregSoum')}>
										<SelectTrigger className="bg-gray-50 border-0 h-10 flex-1">
											<SelectValue className="::placeholder:text-gray-400" placeholder="Choose Duureg/Sum" />
										</SelectTrigger>
										<SelectContent>
											{sumDuuregOpns.map((sum, index) => (
												<SelectItem key={index} value={sum.code.toString()}>
													{sum.name}
												</SelectItem>
											))}
										</SelectContent>
									</Select>
								</div>

								<Select
									value={watch('deliveryDetail.portId')}
									onValueChange={value => setValue('deliveryDetail.portId', value as Delivery_Type)}
									defaultValue={watch('deliveryDetail.portId')}>
									<SelectTrigger className="bg-gray-50 border-0 h-10 flex-1">
										<SelectValue className="::placeholder:text-gray-400" placeholder="Choose Port" />
									</SelectTrigger>
									<SelectContent>
										{(ports || []).map((port, index) => (
											<SelectItem key={index} value={port._id}>
												{port.name}
											</SelectItem>
										))}
									</SelectContent>
								</Select>
							</div>
						)}

						{watch('deliveryDetail.deliveryType') === Delivery_Type.ToAimagCollectionPoint && (
							<div className="flex gap-4">
								<Select
									value={watch('deliveryDetail.aimak')}
									onValueChange={value => setValue('deliveryDetail.aimak', value as Delivery_Type)}
									defaultValue={watch('deliveryDetail.aimak')}>
									<SelectTrigger className="bg-gray-50 border-0 h-10 flex-1">
										<SelectValue className="::placeholder:text-gray-400" placeholder="Choose Aimak" />
									</SelectTrigger>
									<SelectContent>
										{aimagHotOptions.map((aimag, index) => (
											<SelectItem key={index} value={aimag.code.toString()}>
												{aimag.name}
											</SelectItem>
										))}
									</SelectContent>
								</Select>

								<Select
									value={watch('deliveryDetail.collectionPointId')}
									onValueChange={value => setValue('deliveryDetail.collectionPointId', value as Delivery_Type)}
									defaultValue={watch('deliveryDetail.collectionPointId')}>
									<SelectTrigger className="bg-gray-50 border-0 h-10 flex-1">
										<SelectValue className="::placeholder:text-gray-400" placeholder="Choose Duureg/Sum" />
									</SelectTrigger>
									<SelectContent>
										{(collectionPoints || []).map((point, index) => (
											<SelectItem key={index} value={point._id}>
												{point.name}
											</SelectItem>
										))}
									</SelectContent>
								</Select>
							</div>
						)}

						{watch('deliveryDetail.deliveryType') === Delivery_Type.Full && (
							<Select
								value={watch('deliveryDetail.stockLocationId')}
								onValueChange={value => setValue('deliveryDetail.stockLocationId', value as Delivery_Type)}
								defaultValue={watch('deliveryDetail.stockLocationId')}>
								<SelectTrigger className="bg-gray-50 border-0 h-10">
									<SelectValue className="::placeholder:text-gray-400" placeholder="Choose Stock Location" />
								</SelectTrigger>
								<SelectContent>
									{(entity?.entitySupplier?.stockLocations || []).map((location, index) => (
										<SelectItem key={index} value={location._id}>
											{location.branchName}
										</SelectItem>
									))}
								</SelectContent>
							</Select>
						)}
					</div>
				</div>

				<Seperator />

				<div className="flex gap-6">
					<div className="flex flex-1 flex-col">
						<div className="text-sm font-semibold mb-1">
							Packaging <span className="text-danger-400">*</span>
						</div>

						<div className="text-neutral-700 text-xs font-medium">Fill in packaging details. </div>
					</div>

					<div className="flex flex-1 flex-col">
						<div className="text-xs text-lightgray-1000 font-semibold mb-2">Special handling specification</div>

						<div className="flex flex-col gap-4 mb-6">
							<CheckboxField
								isSelected={watch('isTempControl')}
								onChange={() => setValue('isTempControl', !watch('isTempControl'))}
								Icon={PlaneIcon}
								className="h-12"
								title="Temperature control"
							/>
							<CheckboxField
								isSelected={watch('isCantShippedAir')}
								onChange={() => setValue('isCantShippedAir', !watch('isCantShippedAir'))}
								Icon={PlaneIcon}
								className="h-12"
								title="Cannot be shipped through air"
							/>
							<CheckboxField
								isSelected={watch('isSensitiveToMoist')}
								onChange={() => setValue('isSensitiveToMoist', !watch('isSensitiveToMoist'))}
								Icon={DropletIcon}
								className="h-12"
								title="Sensitive to moisture"
							/>
						</div>

						<div className="text-xs text-lightgray-1000 font-semibold mb-2">Packaging detail</div>

						<Textarea
							defaultValue={getValues('packagingDetail')}
							onChange={e => setValue('packagingDetail', e.target.value)}
							placeholder="Enter packaging details"
						/>
					</div>
				</div>
			</div>
		</div>
	)
}

export default DeliveryPackagingSection
