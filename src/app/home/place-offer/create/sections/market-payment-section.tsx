import { GolomtBankIcon, KhanBankIcon } from '@/src/assets/icons'
import CheckboxField from '@/src/components/checkbox-field'
import Seperator from '@/src/components/seperator'
import { Button, Input } from '@nextui-org/react'
import { Edit3Icon } from 'lucide-react'
import React, { useState } from 'react'
import { useFormContext } from 'react-hook-form'
import { z } from 'zod'
import { CreateOfferFormSchema } from '../create-offer-section'
import { Market_Type } from '@/src/app/api/generated/graphql'
import PercentInput from '@/src/components/percent-input'

interface Props {}

const MarketPaymentSection: React.FC<Props> = () => {
	const { register, getValues, setValue, watch } = useFormContext<z.infer<typeof CreateOfferFormSchema>>()
	const [paymentError, setPaymentError] = useState('')

	return (
		<div className="flex flex-col p-6 gap-6 max-w-[1080px] m-auto">
			<div className="flex flex-col border rounded-xl p-6 gap-6 bg-white w-full">
				<div className="flex gap-6">
					<div className="flex flex-1 flex-col">
						<div className="text-sm font-semibold mb-1">
							Market type <span className="text-danger-400">*</span>
						</div>

						<div className="text-neutral-700 text-xs font-medium">Select market type. </div>
					</div>

					<div className="flex flex-1 gap-4">
						<CheckboxField
							isSelected={watch('marketType').includes(Market_Type.Local)}
							onChange={() => {
								let temp = watch('marketType')
								if (temp.includes(Market_Type.Local)) {
									temp = temp.filter(data => data !== Market_Type.Local)
								} else {
									temp.push(Market_Type.Local)
								}
								setValue('marketType', temp)
							}}
							className="w-1/2"
							title="Local"
						/>
						<CheckboxField
							isSelected={watch('marketType').includes(Market_Type.Global)}
							onChange={() => {
								let temp = watch('marketType')
								if (temp.includes(Market_Type.Global)) {
									temp = temp.filter(data => data !== Market_Type.Global)
								} else {
									temp.push(Market_Type.Global)
								}
								setValue('marketType', temp)
							}}
							className="w-1/2"
							title="Global"
						/>
					</div>
				</div>

				<Seperator />

				<div className="flex gap-6">
					<div className="flex flex-1 flex-col">
						<div className="text-sm font-semibold mb-1">
							Payment type <span className="text-danger-400">*</span>
						</div>

						<div className="text-neutral-700 text-xs font-medium">Numbers should add up to 100%</div>
					</div>

					<div className="flex flex-1 gap-4">
						<PercentInput
							defaultValue={getValues('advancePayment')}
							onValueChange={value => setValue('advancePayment', value)}
							label="Advance payment"
							className={paymentError && 'border-danger-400'}
						/>
						<PercentInput
							defaultValue={getValues('interimPayment')}
							onValueChange={value => setValue('interimPayment', value)}
							label="Interim payment"
							className={paymentError && 'border-danger-400'}
						/>
						<PercentInput
							defaultValue={getValues('finalPayment')}
							onValueChange={value => setValue('finalPayment', value)}
							label="Final payment"
							className={paymentError && 'border-danger-400'}
						/>
					</div>

					{paymentError && <span className="text-sm text-danger-400">{paymentError}</span>}
				</div>

				<Seperator />

				<div className="flex gap-6">
					<div className="flex flex-1 flex-col">
						<div className="text-sm font-semibold mb-1">
							Payment method <span className="text-danger-400">*</span>
						</div>

						<div className="text-neutral-700 text-xs font-medium">Select payment method. </div>
					</div>

					<div className="flex flex-1 flex-col gap-4">
						<div className="flex flex-col flex-1 gap-4">
							<CheckboxField
								isSelected={watch('accountNumber') === '5041'}
								onChange={() => setValue('accountNumber', '5041')}
								iconStyle="w-8 h-8"
								Icon={KhanBankIcon}
								className="w-full h-14"
								title="5041372577"
								label="Хаан банк"
							/>
							<CheckboxField
								isSelected={watch('accountNumber') === '2015'}
								onChange={() => setValue('accountNumber', '2015')}
								iconStyle="w-8 h-8"
								Icon={GolomtBankIcon}
								className="w-full h-14"
								title="2015163740"
								label="Голомт банк"
							/>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default MarketPaymentSection
