import CheckboxField from '@/src/components/checkbox-field'
import React from 'react'
import { UseFormReturn, useFormContext } from 'react-hook-form'
import { z } from 'zod'
import { CreateOfferFormSchema } from '../../create-offer-section'
import { FormSchema } from './edit-offer-variant-sheet'

interface Props {
	form: UseFormReturn<z.infer<typeof FormSchema>, any, undefined>
}

const DocumentsTab: React.FC<Props> = ({ form }) => {
	const { setValue, watch } = useFormContext<z.infer<typeof CreateOfferFormSchema>>()

	const documents = watch('additionalDocuments') ?? []
	const fields = [
		{ title: 'Product test report', value: 'Product test report' },
		{ title: 'Place of origin', value: 'Place of origin' },
		{ title: 'Certificate of origin', value: 'Certificate of origin' }
	]

	const onChange = (value: string) => {
		let temp = [...documents]
		if (temp.includes(value)) {
			temp = temp.filter(data => data !== value)
		} else {
			temp.push(value)
		}
		setValue('additionalDocuments', temp)
	}
	return (
		<div className="flex flex-col gap-4 p-2 h-full overflow-y-auto">
			<div className="text-sm font-semibold text-lightgray-1200">Document Disclosure</div>
			<div className="text-xs font-semibold text-lightgray-1000">Check documents to be disclosed to public.</div>
			{fields.map(({ title, value }, i) => (
				<CheckboxField key={value} title={title} isSelected={documents.includes(value)} onChange={() => onChange(value)} />
			))}
		</div>
	)
}

export default DocumentsTab
