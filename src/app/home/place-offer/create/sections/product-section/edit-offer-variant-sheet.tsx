'use client'

import { Form } from '@/src/components/ui/form'
import { Sheet, SheetClose, SheetContent, SheetFooter, SheetHeader, SheetTitle } from '@/src/components/ui/sheet'
import { zodResolver } from '@hookform/resolvers/zod'
import { Button, Tab, Tabs } from '@nextui-org/react'
import React, { useContext } from 'react'
import { useForm } from 'react-hook-form'
import { z } from 'zod'
import { CreateOfferContext } from '../../context'
import DocumentsTab from './documents-tab'
import GeneralTab from './general-tab'
import PriceTab from './price-tab'

export const FormSchema = z.object({
	quantity: z.number().optional(),
	minimumOrderQuantity: z.number().optional(),
	availableDate: z.date().optional(),
	leadTimes: z.object({ min: z.string().optional(), max: z.string().optional(), days: z.string().optional() }).array(),
	priceList: z.object({ min: z.string().optional(), max: z.string().optional(), price: z.string().optional() }).array(),
	hasProductTestReport: z.boolean().optional(),
	hasPlaceOfOrigin: z.boolean().optional(),
	hasCertificateOfOrigin: z.boolean().optional()
})

interface Props {}

const EditOfferVariantSheet: React.FC<Props> = () => {
	const { onUpdateOfferVariant, sheetVariant, isSheetOpen, setIsSheetOpen } = useContext(CreateOfferContext)

	const { quantity, minimumOrderQuantity, availableDate, leadTimes, priceList, hasCertificateOfOrigin, hasPlaceOfOrigin, hasProductTestReport } =
		sheetVariant

	const title = `${sheetVariant?.variant?.attributes?.map(attr => attr.value).join(' / ')}`

	const form = useForm<z.infer<typeof FormSchema>>({
		resolver: zodResolver(FormSchema),
		defaultValues: {
			quantity: quantity,
			minimumOrderQuantity: minimumOrderQuantity,
			availableDate: availableDate ? new Date(availableDate) : new Date(),
			leadTimes: leadTimes && leadTimes.length > 0 ? leadTimes : [{}],
			priceList: priceList && priceList.length > 0 ? priceList : [{}],
			hasProductTestReport: !!hasProductTestReport,
			hasPlaceOfOrigin: !!hasPlaceOfOrigin,
			hasCertificateOfOrigin: !!hasCertificateOfOrigin
		}
	})

	function onSubmit(data: z.infer<typeof FormSchema>) {
		onUpdateOfferVariant(sheetVariant.variant._id, { ...data, variant: sheetVariant.variant })
	}

	const onInvalid = (errors: any) => console.error(errors)

	return (
		<Sheet open={isSheetOpen} onOpenChange={setIsSheetOpen}>
			<SheetContent className="p-0 flex flex-col justify-between gap-0 w-[400px] sm:w-[400px] max-w-none sm:max-w-none">
				<Form {...form}>
					<form aria-label="my-form" name="my-form" className="flex flex-col h-full" onSubmit={form.handleSubmit(onSubmit, onInvalid)}>
						<SheetHeader className="border-b p-4">
							<SheetTitle>Edit variant</SheetTitle>
						</SheetHeader>
						<Tabs
							color="primary"
							variant="underlined"
							classNames={{
								tabList: 'gap-6 w-full relative rounded-none py-0 border-b border-divider px-4',
								cursor: 'w-full bg-primary',
								tab: 'max-w-fit px-0 h-12',
								tabContent: 'group-data-[selected=true]:text-primary'
							}}>
							<Tab
								key="general"
								className="h-full p-2"
								title={
									<div className="flex items-center space-x-2 h-full">
										<span className="text-sm font-semibold">General</span>
									</div>
								}>
								<GeneralTab title={title} form={form} />
							</Tab>
							<Tab
								key="price"
								className="h-full p-2"
								title={
									<div className="flex items-center space-x-2 h-full">
										<span className="text-sm font-semibold">Price</span>
									</div>
								}>
								<PriceTab form={form} />
							</Tab>
							<Tab
								key="documents"
								className="h-full p-2"
								title={
									<div className="flex items-center space-x-2 h-full">
										<span className="text-sm font-semibold">Documents</span>
									</div>
								}>
								<DocumentsTab form={form} />
							</Tab>
						</Tabs>

						<SheetFooter className="border-t p-4 gap-4 flex">
							<SheetClose asChild>
								<Button className="border-[1px]" variant="bordered">
									Cancel
								</Button>
							</SheetClose>

							<SheetClose asChild>
								<Button type="submit" className="disabled:bg-lightgray-200 disabled:text-lightgray-800" color="primary">
									Save
								</Button>
							</SheetClose>
						</SheetFooter>
					</form>
				</Form>
			</SheetContent>
		</Sheet>
	)
}

export default EditOfferVariantSheet
