import DatePicker from '@/src/components/datepicker'
import Seperator from '@/src/components/seperator'
import { FormField, FormItem, FormControl, FormLabel } from '@/src/components/ui/form'
import { Button, Input } from '@nextui-org/react'
import { DollarSign, Plus, XIcon } from 'lucide-react'
import React from 'react'
import { UseFormReturn, useFieldArray } from 'react-hook-form'
import { FormSchema } from './edit-offer-variant-sheet'
import { z } from 'zod'
import useWindowSize from '@/src/app/hooks/use-window-size'

interface Props {
	form: UseFormReturn<z.infer<typeof FormSchema>, any, undefined>
	title: string
}

const GeneralTab: React.FC<Props> = ({ form, title }) => {
	const { fields, append, remove } = useFieldArray({
		control: form.control,
		name: 'leadTimes'
	})

	const size = useWindowSize()
	const tabHeight = (size.height || 0) - 61 - 37 - 89

	const onClickAdd = () => {
		const leadTimes = form.watch('leadTimes')
		const maxQuantity = parseInt(leadTimes[leadTimes.length - 1].max || '0', 10)

		append({ min: (maxQuantity + 1).toString() })
	}

	return (
		<div style={{ height: tabHeight }} className="overflow-y-auto">
			<div className={`flex flex-col shrink-0 gap-4 p-2 `}>
				<div className="text-sm font-semibold text-lightgray-1200">Variant Info</div>

				<FormField
					name="variant"
					render={() => (
						<FormItem className="w-full">
							<FormControl>
								<Input classNames={{ inputWrapper: 'h-10' }} disabled value={title} />
							</FormControl>
						</FormItem>
					)}
				/>

				<div className="flex gap-4">
					<FormField
						control={form.control}
						name="quantity"
						render={({ field }) => (
							<FormItem className="w-full">
								<FormLabel>
									Quantity <span className="text-danger-400">*</span>
								</FormLabel>
								<FormControl>
									<Input
										type="number"
										classNames={{ inputWrapper: 'h-10' }}
										value={field.value?.toString()}
										onChange={e => field.onChange(parseInt(e.target.value, 10))}
										placeholder="0"
									/>
								</FormControl>
							</FormItem>
						)}
					/>

					<FormField
						control={form.control}
						name="minimumOrderQuantity"
						render={({ field }) => (
							<FormItem className="w-full">
								<FormLabel>
									Minimum Order Quantity <span className="text-danger-400">*</span>
								</FormLabel>
								<FormControl>
									<Input
										type="number"
										classNames={{ inputWrapper: 'h-10' }}
										value={field.value?.toString()}
										onChange={e => field.onChange(parseInt(e.target.value, 10))}
										placeholder="0"
									/>
								</FormControl>
							</FormItem>
						)}
					/>
				</div>

				<FormField
					control={form.control}
					name="availableDate"
					render={({ field }) => (
						<FormItem className="w-full flex-col flex">
							<FormLabel>Available date</FormLabel>
							<FormControl>
								<DatePicker className="h-10" {...field} placeholder="0000/00/00" />
							</FormControl>
						</FormItem>
					)}
				/>

				<Seperator className="border-t" />

				<div className="text-sm font-semibold text-lightgray-1200">Lead Time</div>

				<div className="flex flex-col gap-4">
					{fields.map((field, index) => (
						<div key={field.id} className="flex gap-4">
							<div className="flex flex-col w-1/2 gap-2">
								<FormLabel>
									Quantity Range <span className="text-danger-400">*</span>
								</FormLabel>

								<div className="flex items-center gap-1">
									<FormControl>
										<Input
											type="number"
											value={form.watch(`leadTimes.${index}.min`)?.toString()}
											onChange={e => form.setValue(`leadTimes.${index}.min`, e.target.value)}
											classNames={{ inputWrapper: 'h-10' }}
											placeholder="Min"
										/>
									</FormControl>
									-
									<FormControl>
										<Input
											type="number"
											value={form.watch(`leadTimes.${index}.max`)?.toString()}
											onChange={e => form.setValue(`leadTimes.${index}.max`, e.target.value)}
											classNames={{ inputWrapper: 'h-10' }}
											placeholder="Max"
										/>
									</FormControl>
								</div>
							</div>

							<div className="w-1/2 flex flex-col gap-2">
								<FormLabel className="flex justify-between">
									<div>
										Days <span className="text-danger-400">*</span>
									</div>
									{index !== 0 && (
										<div
											onClick={() => remove(index)}
											className="cursor-pointer w-4 h-4 rounded-full border border-danger-400 flex items-center justify-center">
											<XIcon className="text-danger-400" width={12} height={12} />
										</div>
									)}
								</FormLabel>
								<FormControl>
									<Input
										type="number"
										value={form.watch(`leadTimes.${index}.days`)?.toString()}
										onChange={e => form.setValue(`leadTimes.${index}.days`, e.target.value)}
										classNames={{ inputWrapper: 'h-10' }}
										placeholder="0"
									/>
								</FormControl>
							</div>
						</div>
					))}

					<Button onClick={onClickAdd} size="sm" color="primary" variant="bordered" className="border-[1px] w-min" startContent={<Plus />}>
						Add
					</Button>
				</div>
			</div>
		</div>
	)
}

export default GeneralTab
