import React, { useContext, useState } from 'react'

import CheckboxField from '@/src/components/checkbox-field'
import Seperator from '@/src/components/seperator'
import { Checkbox, Select, SelectItem, Table, TableBody, TableCell, TableColumn, TableHeader, TableRow } from '@nextui-org/react'
import moment from 'moment'
import { CreateOfferContext } from '../../context'
import OfferVariantCard from './offer-variant-card'
import Image from 'next/image'
import { convertCurrency } from '@/src/lib/currency'

interface Props {}

const ProductSection: React.FC<Props> = () => {
	const { products, selectedProduct, setSelectedProduct, selectedVariants, onSelectVariant } = useContext(CreateOfferContext)
	const [isDiff, setIsDiff] = useState(false)

	return (
		<div className="flex p-6 gap-6">
			<div className="flex flex-col w-full max-w-[360px] gap-6">
				<div className="flex flex-col border rounded-xl p-6 bg-white h-min">
					<div className="text-sm font-semibold mb-1">
						Product <span className="text-danger-400">*</span>
					</div>

					<div className="text-neutral-700 text-xs font-medium mb-6">Select product from your entity listing</div>

					<Select
						classNames={{
							mainWrapper: 'h-10',
							label: 'text-sm'
						}}
						selectedKeys={selectedProduct ? [selectedProduct?._id] : undefined}
						onChange={event => {
							const currentProduct = products.find(product => product._id === event.target.value)
							setSelectedProduct(currentProduct!)
						}}
						label="Select a product">
						{products.map(product => (
							<SelectItem className="py-3" key={product._id} value={product._id}>
								{product.name}
							</SelectItem>
						))}
					</Select>
				</div>

				<div className="flex flex-col border rounded-xl p-6 bg-white h-min">
					<div className="text-sm font-semibold mb-1">
						Price <span className="text-danger-400">*</span>
					</div>

					<div className="text-neutral-700 text-xs font-medium mb-6">Select how you would like your variant to be priced. </div>

					<div className="mb-2 text-xs font-semibold text-lightgray-1000">Each variant is priced differently?</div>

					<div className="flex gap-4">
						<CheckboxField isSelected={isDiff} onChange={() => setIsDiff(true)} className="w-1/2" title="Yes" />
						<CheckboxField isSelected={!isDiff} onChange={() => setIsDiff(false)} className="w-1/2" title="No" />
					</div>
				</div>
			</div>

			<div className="flex flex-col border rounded-xl p-6 gap-6 bg-white w-full">
				<div className="flex flex-col">
					<div className="text-sm font-semibold mb-1">
						Product variants <span className="text-danger-400">*</span>
					</div>

					<div className="text-neutral-700 text-xs font-medium">Please choose variants to be included in this offer.</div>
				</div>

				<Table
					classNames={{
						wrapper: 'shadow-none p-0 rounded-none',
						thead: '!h-[34px] [&>*:nth-child(2)]:hidden',
						th: '!rounded-none !h-[34px] border-r last:border-none',
						tr: 'border-t border-b',
						td: 'border-r last:border-none'
					}}>
					<TableHeader>
						<TableColumn>Variant</TableColumn>
						<TableColumn>Cost per unit</TableColumn>
						<TableColumn>Place of origin</TableColumn>
						<TableColumn>Product Images</TableColumn>
						<TableColumn>Expiry date</TableColumn>
					</TableHeader>
					<TableBody>
						{(selectedProduct?.variants || []).map(variant => (
							<TableRow key={variant._id}>
								<TableCell>
									<Checkbox
										isSelected={!!selectedVariants.find(selectedVariant => selectedVariant.variant._id === variant._id)}
										onChange={() => onSelectVariant({ variant })}
									/>
									{variant.attributes?.map(attr => attr.value).join(' / ')}
								</TableCell>
								<TableCell>{convertCurrency(variant.price, 'tugrik')}</TableCell>
								<TableCell>{variant.placeOfOrigin}</TableCell>
								<TableCell>
									<div className="flex gap-2">
										{(variant.images || []).map((image, index) => (
											<div key={index} className="overflow-hidden relative border rounded-md w-8 h-8">
												<Image fill objectFit="cover" src={image} alt={'product Image' + index} />
											</div>
										))}
									</div>
								</TableCell>
								<TableCell>{moment(variant.expiryDate).format('yyyy/MM/DD')}</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>

				<Seperator />

				<div className="flex flex-col">
					<div className="text-sm font-semibold mb-1">
						Offer variants <span className="text-danger-400">*</span>
					</div>

					<div className="text-neutral-700 text-xs font-medium">Fill in offer details. </div>
				</div>

				{selectedVariants.map(variant => (
					<OfferVariantCard key={variant.variant._id} productName={selectedProduct?.name || ''} variant={variant} />
				))}
			</div>
		</div>
	)
}

export default ProductSection
