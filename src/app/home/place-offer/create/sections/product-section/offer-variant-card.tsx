import { Table, TableBody, TableCell, TableColumn, TableHeader, TableRow } from '@nextui-org/react'
import { EditIcon } from 'lucide-react'
import moment from 'moment'
import React, { useContext } from 'react'
import { CreateOfferContext, CustomVariant } from '../../context'
import EditOfferVariantSheet from './edit-offer-variant-sheet'
import { convertCurrency } from '@/src/lib/currency'

interface Props {
	variant: CustomVariant
	productName: string
}

const OfferVariantCard: React.FC<Props> = ({ variant, productName }) => {
	const { setIsSheetOpen, setSheetVariant } = useContext(CreateOfferContext)
	const title = `${productName} - ${variant.variant.attributes?.map(attr => attr.value).join(' / ')}`
	return (
		<div className="border rounded-lg overflow-hidden">
			<div className="border-b flex h-10 px-3 justify-between items-center bg-lightgray-100">
				<span className="text-sm font-medium">{title}</span>

				<div
					onClick={() => {
						setSheetVariant(variant)
						setIsSheetOpen(true)
					}}
					className="flex gap-2 items-center cursor-pointer">
					<EditIcon className="w-4 h-4 text-primary" />
					<span className="text-sm text-primary font-medium">Edit</span>
				</div>
			</div>

			<div className="flex flex-col gap-3 p-3">
				<div className="flex">
					<div className="flex flex-1 flex-col gap-1 text-sm font-medium">
						<span className="text-lightgray-1100">Quantity</span>
						<span>{variant.quantity || '-'}</span>
					</div>

					<div className="flex flex-1 flex-col gap-1 text-sm font-medium">
						<span className="text-lightgray-1100">Minimum Order Quantity</span>
						<span>{variant.minimumOrderQuantity || '-'}</span>
					</div>

					<div className="flex flex-1 flex-col gap-1 text-sm font-medium">
						<span className="text-lightgray-1100">Available date</span>
						<span>{variant.availableDate ? moment(variant.availableDate).format('yyyy/MM/DD') : '-'}</span>
					</div>
				</div>

				<div className="flex gap-6">
					<div className="flex flex-col gap-3 flex-1">
						<span className="text-sm font-medium">Lead Time</span>

						<Table
							layout="fixed"
							classNames={{
								wrapper: 'shadow-none p-0 rounded-none w-full',
								thead: '!h-[34px] [&>*:nth-child(2)]:hidden w-full',
								th: '!rounded-none !h-[34px] border-r last:border-none w-full',
								tr: 'border-t w-full',
								td: 'border-r last:border-none'
							}}>
							<TableHeader>
								<TableColumn>Quantity Range</TableColumn>
								<TableColumn>Days</TableColumn>
							</TableHeader>
							<TableBody>
								{(variant.leadTimes || [{}]).map((lead, index) => (
									<TableRow key={index}>
										<TableCell>
											{lead.min} - {lead.max}
										</TableCell>
										<TableCell>{lead.days || ' - '}</TableCell>
									</TableRow>
								))}
							</TableBody>
						</Table>
					</div>

					<div className="flex flex-col gap-3 flex-1">
						<span className="text-sm font-medium">Price</span>

						<Table
							layout="fixed"
							classNames={{
								wrapper: 'shadow-none p-0 rounded-none',
								thead: '!h-[34px] [&>*:nth-child(2)]:hidden',
								th: '!rounded-none !h-[34px] border-r last:border-none',
								tr: 'border-t',
								td: 'border-r last:border-none'
							}}>
							<TableHeader>
								<TableColumn>Quantity Range</TableColumn>
								<TableColumn>Price</TableColumn>
							</TableHeader>
							<TableBody>
								{(variant.priceList || [{}]).map((prices, index) => (
									<TableRow key={index}>
										<TableCell>
											{prices.min} - {prices.max}
										</TableCell>
										<TableCell>{prices.price ? convertCurrency(parseInt(prices.price, 10)) : ' - '}</TableCell>
									</TableRow>
								))}
							</TableBody>
						</Table>
					</div>
				</div>
			</div>
		</div>
	)
}

export default OfferVariantCard
