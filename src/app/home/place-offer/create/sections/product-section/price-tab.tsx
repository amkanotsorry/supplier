import { FormControl, FormLabel } from '@/src/components/ui/form'
import { Button, Input } from '@nextui-org/react'
import { Plus, XIcon } from 'lucide-react'
import React from 'react'
import { UseFormReturn, useFieldArray } from 'react-hook-form'
import { z } from 'zod'
import { FormSchema } from './edit-offer-variant-sheet'
import useWindowSize from '@/src/app/hooks/use-window-size'

interface Props {
	form: UseFormReturn<z.infer<typeof FormSchema>, any, undefined>
}

const PriceTab: React.FC<Props> = ({ form }) => {
	const { fields, append, remove } = useFieldArray({
		control: form.control,
		name: 'priceList'
	})

	const size = useWindowSize()
	const tabHeight = (size.height || 0) - 61 - 37 - 89

	const onClickAdd = () => {
		const priceList = form.watch('priceList')
		const maxQuantity = parseInt(priceList[priceList.length - 1].max || '0', 10)

		append({ min: (maxQuantity + 1).toString() })
	}

	return (
		<div style={{ height: tabHeight }} className="overflow-y-auto">
			<div className="flex flex-col gap-4 p-2 shrink-0">
				{fields.map((field, index) => (
					<div key={field.id} className="flex gap-4">
						<div className="flex flex-col w-1/2 gap-2">
							<FormLabel>
								Quantity Range <span className="text-danger-400">*</span>
							</FormLabel>

							<div className="flex items-center gap-1">
								<FormControl>
									<Input
										type="number"
										value={form.watch(`priceList.${index}.min`)?.toString()}
										onChange={e => form.setValue(`priceList.${index}.min`, e.target.value)}
										classNames={{ inputWrapper: 'h-10' }}
										placeholder="Min"
									/>
								</FormControl>
								-
								<FormControl>
									<Input
										type="number"
										value={form.watch(`priceList.${index}.max`)?.toString()}
										onChange={e => form.setValue(`priceList.${index}.max`, e.target.value)}
										classNames={{ inputWrapper: 'h-10' }}
										placeholder="Max"
									/>
								</FormControl>
							</div>
						</div>

						<div className="w-1/2 flex flex-col gap-2">
							<FormLabel className="flex justify-between">
								<div>
									Price <span className="text-danger-400">*</span>
								</div>
								{index !== 0 && (
									<div
										onClick={() => remove(index)}
										className="cursor-pointer w-4 h-4 rounded-full border border-danger-400 flex items-center justify-center">
										<XIcon className="text-danger-400" width={12} height={12} />
									</div>
								)}
							</FormLabel>
							<FormControl>
								<Input
									type="number"
									value={form.watch(`priceList.${index}.price`)?.toString()}
									onChange={e => form.setValue(`priceList.${index}.price`, e.target.value)}
									classNames={{ inputWrapper: 'h-10' }}
									placeholder="0"
								/>
							</FormControl>
						</div>
					</div>
				))}

				<Button onClick={onClickAdd} size="sm" color="primary" variant="bordered" className="border-[1px] w-min" startContent={<Plus />}>
					Add
				</Button>
			</div>
		</div>
	)
}

export default PriceTab
