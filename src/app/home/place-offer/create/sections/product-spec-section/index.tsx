import CheckboxField from '@/src/components/checkbox-field'
import DatePicker from '@/src/components/datepicker'
import Seperator from '@/src/components/seperator'
import { FormLabel } from '@/src/components/ui/form'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '@/src/components/ui/select'
import { AxeIcon, BanknoteIcon, RotateCwIcon } from 'lucide-react'
import React, { useState } from 'react'
import { useFormContext } from 'react-hook-form'
import { z } from 'zod'
import { CreateOfferFormSchema } from '../../create-offer-section'
import { Warranty_Period_Type } from '@/src/app/api/generated/graphql'
import { Select as MultiSelect, SelectItem as MultiSelectItem, Avatar, Chip, Input } from '@nextui-org/react'

interface Props {}

const tags = ['Cattle', 'Cow', 'Sheep', 'Goats', 'Horse']

const ProductSpecSection: React.FC<Props> = () => {
	const { getValues, setValue, watch } = useFormContext<z.infer<typeof CreateOfferFormSchema>>()

	return (
		<div className="flex flex-col p-6 gap-6 m-auto">
			<div className="flex flex-col border rounded-xl p-6 gap-6 bg-white w-full">
				<div className="flex gap-6">
					<div className="flex flex-1 flex-col">
						<div className="text-sm font-semibold mb-1">
							Offer period <span className="text-danger-400">*</span>
						</div>

						<div className="text-neutral-700 text-xs font-medium">Select offer period. </div>
					</div>

					<div className="flex gap-4 flex-1">
						<div className="flex flex-col gap-1 flex-1">
							<FormLabel>
								Start date <span className="text-danger-400">*</span>
							</FormLabel>

							<DatePicker className="h-10" value={watch('startDate')} onChange={value => setValue('startDate', value)} placeholder="0000/00/00" />
						</div>

						<div className="flex flex-col gap-1 flex-1">
							<FormLabel>
								Valid until date <span className="text-danger-400">*</span>
							</FormLabel>

							<DatePicker
								className="h-10"
								value={watch('validUntilDate')}
								onChange={value => setValue('validUntilDate', value)}
								placeholder="0000/00/00"
							/>
						</div>
					</div>
				</div>

				<Seperator />

				<div className="flex gap-6">
					<div className="flex flex-1 flex-col">
						<div className="text-sm font-semibold mb-1">
							Tag <span className="text-danger-400">*</span>
						</div>

						<div className="text-neutral-700 text-xs font-medium">Select product tag. </div>
					</div>

					<div className="flex flex-1 flex-col">
						<div className="text-xs text-lightgray-1000 font-semibold mb-2">
							Tag <span className="text-danger-400">*</span>
						</div>

						<MultiSelect
							items={tags}
							selectedKeys={watch('tags')}
							isMultiline={true}
							selectionMode="multiple"
							placeholder="Select a tag"
							labelPlacement="outside"
							classNames={{
								mainWrapper: 'h-10',
								base: 'max-w-xs',
								trigger: 'min-h-unit-12 py-2'
							}}
							onChange={data => {
								setValue('tags', data.target.value.split(','))
							}}
							renderValue={items => {
								return (
									<div className="flex flex-wrap gap-2">
										{items.map(item => (
											<Chip className="bg-white rounded-md shadow-sm" key={item.key}>
												{item.textValue}
											</Chip>
										))}
									</div>
								)
							}}>
							{tags.map(tag => (
								<MultiSelectItem key={tag} textValue={tag}>
									<div className="flex gap-2 items-center">{tag}</div>
								</MultiSelectItem>
							))}
						</MultiSelect>
					</div>
				</div>
			</div>

			<div className="flex flex-col border rounded-xl p-6 gap-6 bg-white w-full">
				<div className="flex gap-6">
					<div className="flex flex-1 flex-col">
						<div className="text-sm font-semibold mb-1">Warranty period</div>

						<div className="text-neutral-700 text-xs font-medium">Select product warranty type and period. </div>
					</div>

					<div className="flex flex-1 gap-4">
						<div className="flex flex-1 flex-col">
							<div className="text-xs text-lightgray-1000 font-semibold mb-2">Type</div>

							<Select onValueChange={value => setValue('warrantyType', value as Warranty_Period_Type)} value={getValues('warrantyType')}>
								<SelectTrigger className="w-full h-10">
									<SelectValue placeholder="Select place" />
								</SelectTrigger>
								<SelectContent>
									{[
										{ title: 'Days', value: Warranty_Period_Type.Days },
										{ title: 'Months', value: Warranty_Period_Type.Months },
										{ title: 'Years', value: Warranty_Period_Type.Years }
									].map((warrantyType, index) => (
										<SelectItem className="py-3" key={index} value={warrantyType.value}>
											{warrantyType.title}
										</SelectItem>
									))}
								</SelectContent>
							</Select>
						</div>

						<div className="flex flex-1 flex-col">
							<div className="text-xs text-lightgray-1000 font-semibold mb-2">Number</div>

							<Input
								classNames={{
									inputWrapper: 'h-10'
								}}
								defaultValue={getValues('warrantyValue')}
								onChange={e => setValue('warrantyValue', e.target.value)}
								placeholder="0"
							/>
						</div>
					</div>
				</div>

				<Seperator />

				<div className="flex gap-6">
					<div className="flex flex-1 flex-col">
						<div className="text-sm font-semibold mb-1">Customization availability</div>

						<div className="text-neutral-700 text-xs font-medium">Select product customization and fill in MOQ.</div>
					</div>

					<div className="flex flex-1 flex-col gap-4">
						<div className="flex gap-4">
							<CheckboxField
								isSelected={watch('colorAvailable')}
								onChange={() => setValue('colorAvailable', !watch('colorAvailable'))}
								Icon={AxeIcon}
								className="w-1/2"
								title="Color"
							/>

							<div className="w-1/2">
								{watch('colorAvailable') && (
									<Input
										classNames={{
											inputWrapper: 'h-10'
										}}
										defaultValue={getValues('colorValue')}
										onChange={e => setValue('colorValue', e.target.value)}
										placeholder="Minimum Order Quantity"
									/>
								)}
							</div>
						</div>

						<div className="flex gap-4">
							<CheckboxField
								isSelected={watch('packageAvailable')}
								onChange={() => setValue('packageAvailable', !watch('packageAvailable'))}
								Icon={AxeIcon}
								className="w-1/2"
								title="Package"
							/>

							<div className="w-1/2">
								{watch('packageAvailable') && (
									<Input
										classNames={{
											inputWrapper: 'h-10'
										}}
										defaultValue={getValues('packageValue')}
										onChange={e => setValue('packageValue', e.target.value)}
										placeholder="Minimum Order Quantity"
									/>
								)}
							</div>
						</div>

						<div className="flex gap-4">
							<CheckboxField
								isSelected={watch('logoAvailable')}
								onChange={() => setValue('logoAvailable', !watch('logoAvailable'))}
								Icon={AxeIcon}
								className="w-1/2"
								title="Logo"
							/>

							<div className="w-1/2">
								{watch('logoAvailable') && (
									<Input
										classNames={{
											inputWrapper: 'h-10'
										}}
										defaultValue={getValues('logoValue')}
										onChange={e => setValue('logoValue', e.target.value)}
										placeholder="Minimum Order Quantity"
									/>
								)}
							</div>
						</div>
					</div>
				</div>

				<Seperator />

				<div className="flex gap-6">
					<div className="flex flex-1 flex-col">
						<div className="text-sm font-semibold mb-1">Other</div>

						<div className="text-neutral-700 text-xs font-medium">Select offer details. </div>
					</div>

					<div className="flex flex-col flex-1 gap-4">
						<div className="flex flex-col gap-2">
							<div className="text-xs text-lightgray-1000 font-semibold">Sample delivery</div>

							<div className="flex gap-4">
								<CheckboxField
									isSelected={watch('hasSampleDelivery')}
									onChange={() => setValue('hasSampleDelivery', true)}
									className="w-1/2"
									title="Yes"
								/>
								<CheckboxField
									isSelected={!watch('hasSampleDelivery')}
									onChange={() => setValue('hasSampleDelivery', false)}
									className="w-1/2"
									title="No"
								/>
							</div>
						</div>

						<div className="flex flex-col gap-2">
							<div className="text-xs text-lightgray-1000 font-semibold">Return or refund available</div>

							<div className="flex gap-4">
								<CheckboxField
									isSelected={watch('isReturnable')}
									onChange={() => setValue('isReturnable', !watch('isReturnable'))}
									Icon={RotateCwIcon}
									className="w-1/2"
									title="Return"
								/>
								<CheckboxField
									isSelected={watch('isRefundable')}
									onChange={() => setValue('isRefundable', !watch('isRefundable'))}
									Icon={BanknoteIcon}
									className="w-1/2"
									title="Refund"
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default ProductSpecSection
