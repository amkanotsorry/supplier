import { CheckIcon } from 'lucide-react'
import React, { useContext } from 'react'
import { CreateOfferContext } from './context'

const StepsHeader: React.FC = () => {
	const { currentStep } = useContext(CreateOfferContext)

	const getStatus = (index: 1 | 2 | 3 | 4): ProgressStepStatus => {
		if (index === currentStep) return 'current'
		if (index < currentStep) return 'completed'
		return 'disabled'
	}

	return (
		<div className="flex p-4 gap-3 items-center border-b">
			<ProgressStep status={getStatus(1)} title="Product" index={1} />
			<StepSeperator />
			<ProgressStep status={getStatus(2)} title="Product specification" index={2} />
			<StepSeperator />
			<ProgressStep status={getStatus(3)} title="Delivery & Packaging" index={3} />
			<StepSeperator />
			<ProgressStep status={getStatus(4)} title="Market & Payment" index={4} />
		</div>
	)
}

export default StepsHeader

type ProgressStepStatus = 'completed' | 'current' | 'disabled'

const ProgressStep: React.FC<{ status: ProgressStepStatus; title: string; index: number }> = ({ status, title, index }) => {
	const StatusIcon: React.FC = () => {
		switch (status) {
			case 'completed':
				return (
					<div className="w-6 h-6 bg-success-400 rounded-full flex items-center justify-center">
						<CheckIcon color="white" className="w-4 h-4" />
					</div>
				)
			case 'current':
				return <div className="w-6 h-6 text-sm border border-black rounded-full flex items-center justify-center">{index}</div>
			case 'disabled':
				return <div className="w-6 h-6 text-sm bg-lightgray-200 rounded-full flex items-center justify-center text-lightgray-900">{index}</div>
			default:
				break
		}
	}

	return (
		<div className="flex gap-2 items-center">
			<StatusIcon />
			<span className={`${status === 'disabled' && 'text-lightgray-900'} text-sm font-medium`}>{title}</span>
		</div>
	)
}

const StepSeperator = () => <div className="border-t w-6" />
