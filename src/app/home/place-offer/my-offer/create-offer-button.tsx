'use client'

import { Button } from '@nextui-org/react'
import { PlusIcon } from 'lucide-react'
import { useRouter } from 'next/navigation'
import React from 'react'

interface Props {}

const CreateOfferButton: React.FC<Props> = () => {
	const router = useRouter()
	return (
		<Button onClick={() => router.push('/home/place-offer/create')} color="primary">
			<PlusIcon />
			Create New Offer
		</Button>
	)
}

export default CreateOfferButton
