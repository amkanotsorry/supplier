import SearchBar from '@/src/components/search'
import React from 'react'
import CreateOfferButton from './create-offer-button'
import OfferColumn from './offer-column'
import { QueryOfferType } from '../actions'

interface Props {
	offers: QueryOfferType[]
}

export type OfferStatus = 'Listed' | 'SampleDelivered' | 'Payment' | 'Shipped' | 'Draft'

const MyOffer: React.FC<Props> = ({ offers }) => {
	return (
		<div className="p-2 w-full flex flex-col gap-4 h-full">
			<div className="flex gap-4 shrink-0 items-center justify-between">
				<SearchBar label="Search offers" />

				<CreateOfferButton />
			</div>

			<div className="w-full h-full flex gap-4 pt-0">
				<OfferColumn offerStatus={'Listed'} offers={offers} />
			</div>
		</div>
	)
}

export default MyOffer
