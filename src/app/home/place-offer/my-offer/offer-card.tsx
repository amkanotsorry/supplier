import { AgrixVerifiedIcon, LinkExternalIcon } from '@/src/assets/icons'
import React from 'react'
import { QueryOfferType } from '../actions'
import moment from 'moment'
import { timeAgo } from '@/src/lib/utils'
import { convertCurrency } from '@/src/lib/currency'

interface Props {
	offer: QueryOfferType
}

const OfferCard: React.FC<Props> = ({ offer }) => {
	const getPriceRange = () => {
		if (!offer.variants || offer.variants.length === 0) return 'No Data'
		if (!offer.variants[0].quantityRangePrices || offer.variants[0].quantityRangePrices?.length === 0) return 'No Data'

		const prices = offer.variants[0].quantityRangePrices.map(range => range.price)
		const minPrice = Math.min(...prices)
		const maxPrice = Math.max(...prices)

		if (minPrice === maxPrice) return convertCurrency(minPrice)

		return `${convertCurrency(minPrice)} - ${convertCurrency(maxPrice)}`
	}

	const getMoq = () => {
		if (!offer.variants || offer.variants.length === 0) return 'No Data'

		return offer.variants[0].moq
	}

	return (
		<div className="border rounded-lg shadow">
			<div className="border-b flex items-center justify-between p-3">
				<div className="flex items-center gap-2">
					<span className="text-xs font-medium">#{offer.code}</span>

					<div className="rounded-full w-1 h-1 bg-lightgray-500" />

					<span className="text-xs font-medium text-lightgray-1000">{timeAgo(offer.createdAt)}</span>
				</div>

				<LinkExternalIcon />
			</div>

			<div className="flex flex-col p-[6px]">
				<div className="flex gap-3 flex-wrap p-[6px]">
					<div className="flex flex-col gap-1 flex-1">
						<span className="text-xs font-semibold text-lightgray-1000">Product name</span>
						<span className="text-sm font-medium">{offer.productName}</span>
					</div>

					<div className="flex flex-col gap-1 flex-1">
						<span className="text-xs font-semibold text-lightgray-1000">Price range</span>
						<span className="text-sm font-medium">{getPriceRange()}</span>
					</div>
				</div>

				<div className="flex gap-3 flex-wrap p-[6px]">
					<div className="flex flex-col gap-1 flex-1">
						<span className="text-xs font-semibold text-lightgray-1000">Valid date</span>
						<span className="text-sm font-medium">{moment(offer.offerEndDate).format('yyyy/MM/DD')}</span>
					</div>

					<div className="flex flex-col gap-1 flex-1">
						<span className="text-xs font-semibold text-lightgray-1000">Min order quantity</span>
						<span className="text-sm font-medium">{getMoq()}</span>
					</div>
				</div>

				<div className="flex gap-3 flex-wrap p-[6px]">
					<div className="flex flex-col gap-1 flex-1">
						<span className="text-xs font-semibold text-lightgray-1000">Available date</span>
						<span className="text-sm font-medium">{moment(offer.variants[0].availableDate).format('yyyy/MM/DD')}</span>
					</div>

					<div className="flex flex-col gap-1 flex-1">
						<span className="text-xs font-semibold text-lightgray-1000">Verified</span>
						<AgrixVerifiedIcon />
					</div>
				</div>
			</div>
		</div>
	)
}

export default OfferCard
