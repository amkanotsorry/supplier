import React from 'react'
import { OfferStatus } from '.'

interface Props {
	offerStatus: OfferStatus
	count: number
}

const getStatusColor = (status: OfferStatus): string => {
	switch (status) {
		case 'Listed':
			return 'border-primary'
		case 'SampleDelivered':
			return 'border-success'
		case 'Payment':
			return 'border-pinegreen-700'
		case 'Shipped':
			return 'border-warmyellow-700'
		default:
			return 'border-warmyellow-700'
	}
}

const OfferColumnHeader: React.FC<Props> = ({ offerStatus, count }) => {
	return (
		<div className={`flex items-center rounded-lg border-t-3 ${getStatusColor(offerStatus)} p-3 w-[360px] gap-2 shadow`}>
			<span className="text-sm">{offerStatus}</span>
			<div className="flex justify-center items-center rounded-full bg-lightgray-200 w-4 h-4 text-[10px] px-0 text-lightgray-1000 font-semibold">
				{count}
			</div>
		</div>
	)
}

export default OfferColumnHeader
