import React from 'react'
import OfferColumnHeader from './offer-column-header'
import OfferCard from './offer-card'
import { OfferStatus } from '.'
import { QueryOfferType } from '../actions'

interface Props {
	offerStatus: OfferStatus
	offers: QueryOfferType[]
}

const OfferColumn: React.FC<Props> = ({ offerStatus, offers }) => {
	return (
		<div className="overflow-x-hidden">
			<OfferColumnHeader offerStatus={offerStatus} count={offers.length} />

			<div className="flex my-4 flex-col gap-4 overflow-y-auto">
				{offers
					.slice(0)
					.reverse()
					.map(offer => (
						<OfferCard key={offer._id} offer={offer} />
					))}
			</div>
		</div>
	)
}

export default OfferColumn
