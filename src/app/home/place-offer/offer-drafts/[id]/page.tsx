import React from 'react'
import PageSubHeader from '../../../_components/page-header/page-sub-header'
import { getAllProducts, getVariantById } from '../../../product/actions/product'
import { getOfferDraftById } from '../../actions'
import CreateOfferContextProvider, { CustomVariant } from '../../create/context'
import CreateOfferSection from '../../create/create-offer-section'
import { getEntitySupplierId } from '@/src/app/utils'

interface Props {
	params: { id: string }
}

const OfferDraftPage: React.FC<Props> = async ({ params }) => {
	const id = await getEntitySupplierId()
	const { products } = await getAllProducts(id || '')
	const offerDraft = await getOfferDraftById(params.id)

	if (!offerDraft) return <div>no offerdraft with id {params.id}</div>

	const selectedVariants: CustomVariant[] = []

	if (offerDraft.variants && offerDraft.variants[0]?.variantId) {
		for (let i = 0; i < offerDraft.variants.length; i++) {
			const currentVariant = offerDraft.variants[i]
			const response = await getVariantById(currentVariant?.variantId)
			if (response) {
				selectedVariants.push({
					variant: response,
					quantity: currentVariant.quantity,
					minimumOrderQuantity: currentVariant.moq,
					availableDate: currentVariant.availableDate,
					leadTimes: currentVariant.quantityRangeLeadTimes.map(times => {
						return {
							min: times.min.toString(),
							max: times.max.toString(),
							days: times.days.toString()
						}
					}),
					priceList: currentVariant.quantityRangePrices.map(times => {
						return {
							min: times.min.toString(),
							max: times.max.toString(),
							price: times.price.toString()
						}
					}),
					hasProductTestReport: false,
					hasPlaceOfOrigin: false,
					hasCertificateOfOrigin: false
				})
			}
		}
	}

	return (
		<>
			<PageSubHeader title="Edit offer draft" />

			<CreateOfferContextProvider initialSelectedVariants={selectedVariants} products={products || []}>
				<CreateOfferSection offerDraft={offerDraft} />
			</CreateOfferContextProvider>
		</>
	)
}

export default OfferDraftPage
