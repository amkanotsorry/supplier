import { AgrixVerifiedIcon, LinkExternalIcon } from '@/src/assets/icons'
import React from 'react'
import { QueryOfferDraftType } from '../actions'
import Link from 'next/link'
import moment from 'moment'
import { timeAgo } from '@/src/lib/utils'

interface Props {
	offerDraft: QueryOfferDraftType
}

const OfferDraftCard: React.FC<Props> = ({ offerDraft }) => {
	const getPriceRange = () => {
		if (!offerDraft.variants || offerDraft.variants.length === 0) return 'No Data'
		if (!offerDraft.variants[0].quantityRangePrices || offerDraft.variants[0].quantityRangePrices?.length === 0) return 'No Data'

		const prices = offerDraft.variants[0].quantityRangePrices.map(range => range.price)
		const minPrice = Math.min(...prices)
		const maxPrice = Math.max(...prices)

		if (minPrice === maxPrice) return `${minPrice}$`

		return `${minPrice} - ${maxPrice}$`
	}

	const getMoq = () => {
		if (!offerDraft.variants || offerDraft.variants.length === 0) return 'No Data'

		return offerDraft.variants[0].moq
	}

	return (
		<Link href={`/home/place-offer/offer-drafts/${offerDraft._id}`} className="border rounded-lg shadow">
			<div className="border-b flex items-center justify-between p-3">
				<div className="flex items-center gap-2">
					<span className="text-xs font-medium">Drafted at: {moment(offerDraft.createdAt).format('yyyy/MM/DD HH:mm')}</span>

					<div className="rounded-full w-1 h-1 bg-lightgray-500" />

					<span className="text-xs font-medium text-lightgray-1000">{timeAgo(offerDraft.createdAt)}</span>
				</div>

				<LinkExternalIcon />
			</div>

			<div className="flex flex-col p-[6px]">
				<div className="flex gap-3 flex-wrap p-[6px]">
					<div className="flex flex-col gap-1 flex-1">
						<span className="text-xs font-semibold text-lightgray-1000">Product name</span>
						<span className="text-sm font-medium">{offerDraft.productName || 'No data'}</span>
					</div>

					<div className="flex flex-col gap-1 flex-1">
						<span className="text-xs font-semibold text-lightgray-1000">Price range</span>
						<span className="text-sm font-medium">{getPriceRange()}</span>
					</div>
				</div>

				<div className="flex gap-3 flex-wrap p-[6px]">
					<div className="flex flex-col gap-1 flex-1">
						<span className="text-xs font-semibold text-lightgray-1000">Valid date</span>
						<span className="text-sm font-medium">{offerDraft.offerEndDate ? moment(offerDraft.offerEndDate).format('yyyy/MM/DD') : 'No Data'}</span>
					</div>

					<div className="flex flex-col gap-1 flex-1">
						<span className="text-xs font-semibold text-lightgray-1000">Min order quantity</span>
						<span className="text-sm font-medium">{getMoq()}</span>
					</div>
				</div>

				<div className="flex gap-3 flex-wrap p-[6px]">
					<div className="flex flex-col gap-1 flex-1">
						<span className="text-xs font-semibold text-lightgray-1000">Available date</span>
						<span className="text-sm font-medium">
							{offerDraft.offerStartDate ? moment(offerDraft.offerStartDate).format('yyyy/MM/DD') : 'No Data'}
						</span>
					</div>

					<div className="flex flex-col gap-1 flex-1">
						<span className="text-xs font-semibold text-lightgray-1000">Verified</span>
						<AgrixVerifiedIcon />
					</div>
				</div>
			</div>
		</Link>
	)
}

export default OfferDraftCard
