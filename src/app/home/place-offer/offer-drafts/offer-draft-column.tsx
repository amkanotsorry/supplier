import React from 'react'
import { QueryOfferDraftType } from '../actions'
import { OfferStatus } from '../my-offer'
import OfferColumnHeader from '../my-offer/offer-column-header'
import OfferDraftCard from './offer-draft-card'

interface Props {
	offerStatus: OfferStatus
	offerDrafts: QueryOfferDraftType[]
}

const OfferDraftColumn: React.FC<Props> = ({ offerStatus, offerDrafts }) => {
	return (
		<div className="overflow-x-hidden">
			<OfferColumnHeader offerStatus={offerStatus} count={offerDrafts.length} />

			<div className="flex my-4 flex-col gap-4 overflow-y-auto">
				{offerDrafts
					.slice(0)
					.reverse()
					.map(offer => (
						<OfferDraftCard key={offer._id} offerDraft={offer} />
					))}
			</div>
		</div>
	)
}

export default OfferDraftColumn
