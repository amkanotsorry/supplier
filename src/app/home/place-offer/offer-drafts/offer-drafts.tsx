import SearchBar from '@/src/components/search'
import React from 'react'
import { QueryOfferDraftType } from '../actions'
import OfferDraftColumn from './offer-draft-column'

interface Props {
	offerDrafts: QueryOfferDraftType[]
}

const OfferDrafts: React.FC<Props> = ({ offerDrafts }) => {
	return (
		<div className="p-2 w-full flex flex-col gap-4 h-full">
			<div className="flex gap-4 shrink-0 items-center justify-between">
				<SearchBar label="Search offers" />
			</div>

			<div className="w-full h-full flex gap-4 pt-0">
				<OfferDraftColumn offerStatus={'Draft'} offerDrafts={offerDrafts} />
			</div>
		</div>
	)
}

export default OfferDrafts
