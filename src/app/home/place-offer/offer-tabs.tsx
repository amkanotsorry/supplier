'use client'

import { Chip, Tab, Tabs } from '@nextui-org/react'
import React from 'react'
import MyOffer from './my-offer'
import { QueryOfferDraftType, QueryOfferType } from './actions'
import OfferDrafts from './offer-drafts/offer-drafts'
import isApproved from '../isApproved'

interface Props {
	offers: QueryOfferType[]
	offerDrafts: QueryOfferDraftType[]
}

const OfferTabs: React.FC<Props> = ({ offers, offerDrafts }) => {
	return (
		<div className="flex w-full h-full flex-col">
			<Tabs
				aria-label="Options"
				color="primary"
				variant="underlined"
				classNames={{
					tabList: 'gap-6 w-full relative rounded-none py-0 border-b border-divider px-4 ',
					cursor: 'w-full bg-primary',
					tab: 'max-w-fit px-0 h-12 ',
					tabContent: 'group-data-[selected=true]:text-primary',
					base: 'shrink-0'
				}}>
				<Tab
					key="livestock"
					className="p-2 custom-tab"
					title={
						<div className="flex items-center space-x-2 h-full">
							<span className="text-sm font-semibold">My Offer</span>
							<Chip className="text-xs border-none rounded-md" size="sm" variant="faded">
								{offers.length}
							</Chip>
						</div>
					}>
					<MyOffer offers={offers} />
				</Tab>
				<Tab
					key="final_product"
					className="p-2 custom-tab"
					title={
						<div className="flex items-center space-x-2 h-full">
							<span className="text-sm font-semibold">Draft</span>
							<Chip className="text-xs border-none rounded-md" size="sm" variant="faded">
								{offerDrafts.length}
							</Chip>
						</div>
					}>
					<OfferDrafts offerDrafts={offerDrafts} />
				</Tab>
			</Tabs>
		</div>
	)
}

export default isApproved(OfferTabs)
