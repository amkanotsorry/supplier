import { getServerSession } from 'next-auth'
import { authOptions } from '../../api/auth/[...nextauth]/authOptions'
import { getMyOfferDrafts, getMyOffers } from './actions'
import OfferTabs from './offer-tabs'

const OfferPage = async () => {
	const session = await getServerSession(authOptions)
	const { offers } = await getMyOffers(session?.user.id || '')
	const { offerDrafts } = await getMyOfferDrafts(session?.user.id || '')

	return <OfferTabs offers={offers || []} offerDrafts={offerDrafts || []} />
}

export default OfferPage
