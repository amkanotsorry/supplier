import React from 'react'
import { getProductById } from '../actions/product'
import PageSubHeader from '../../_components/page-header/page-sub-header'
import ProductDetailTabs from './product-detail-tabs'
import { getProductMetas } from '../actions/product-metas'

interface Props {
	params: { id: string }
}

const ProductDetailsPage: React.FC<Props> = async ({ params }) => {
	const product = await getProductById(params.id)
	const { getProductMetaList } = await getProductMetas()

	// console.log('product getProductById', JSON.stringify(product))
	if (!product) return <div>no product with Id {params.id}</div>

	return (
		<>
			<PageSubHeader title={product.name} wrapper={'border-b-0'} />

			<ProductDetailTabs product={product} metas={getProductMetaList || []} />
		</>
	)
}

export default ProductDetailsPage
