'use client'

import { GetProductQuery } from '@/src/app/apiProductMs/generated/graphql'
import { BoxIcon, ClockForwardIcon } from '@/src/assets/icons'
import { Tab, Tabs } from '@nextui-org/react'
import React from 'react'
import { QueryProductMetaType } from '../actions/product-metas'
import CreateProductContextProvider from '../create/context'
import CreateProductSection from '../create/create-product-section'

interface Props {
	product: GetProductQuery['getProduct']
	metas: QueryProductMetaType[]
}

const ProductDetailTabs: React.FC<Props> = ({ product, metas }) => {
	return (
		<div className="h-full overflow-y-scroll">
			<Tabs
				aria-label="Options"
				color="primary"
				variant="underlined"
				className="w-full"
				classNames={{
					tabList: 'gap-6 w-full relative rounded-none py-0 border-b border-divider px-4',
					cursor: 'w-full bg-primary',
					tab: 'max-w-fit px-0 h-8',
					tabContent: 'group-data-[selected=true]:text-primary'
				}}>
				<Tab
					key="livestock"
					className="p-0"
					title={
						<div className="flex items-center space-x-2 px-2">
							<BoxIcon className={` h-[18px] w-[18px]`} />
							<span className="text-sm font-semibold">Product information</span>
						</div>
					}>
					<CreateProductContextProvider product={product}>
						<CreateProductSection product={product} metas={metas} />
					</CreateProductContextProvider>
				</Tab>
				<Tab
					key="final_product"
					className="p-0"
					title={
						<div className="flex items-center space-x-2 px-2">
							<ClockForwardIcon className={` h-[18px] w-[18px]`} />
							<span className="text-sm font-semibold">Product log</span>
						</div>
					}>
					<div>log</div>
				</Tab>
			</Tabs>
		</div>
	)
}

export default ProductDetailTabs
