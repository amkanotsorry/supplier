import { GetProductMetaListQuery, GetProductMetaListDocument } from '@/src/app/api/generated/graphql'
import { getClient } from '@/src/app/api/graphql/client'

type QueryProductMetaListType = Extract<GetProductMetaListQuery['getProductMetaList'], any[]>
export type QueryProductMetaType = QueryProductMetaListType[number]

const client = getClient()

export const getProductMetas = async (): Promise<{ getProductMetaList: QueryProductMetaType[]; error: string | null }> => {
	try {
		const { data } = await client.query({
			query: GetProductMetaListDocument
		})

		return { getProductMetaList: data.getProductMetaList, error: null }
	} catch (error) {
		return { getProductMetaList: [], error: JSON.stringify(error, null, 2) }
	}
}
