'use server'

import { getClient } from '@/src/app/apiProductMs/clientMs'
import { CreateProductInput, GetAllProductsDocument, GetAllProductsQuery } from '@/src/app/apiProductMs/generated/graphql'
import { CreateProductMutation } from '@/src/app/apiProductMs/graphql/mutations/create-product'
import { GetProductById, GetVariantById } from '@/src/app/apiProductMs/graphql/queries/get-product-by-id'
import { revalidatePath } from 'next/cache'

type AllProductsQueryType = Extract<GetAllProductsQuery['findProducts']['docs'], any[]>
export type QueryProductType = AllProductsQueryType[number]

const client = getClient()

export const getAllProducts = async (createdUserId: string): Promise<{ products: QueryProductType[]; error: string | null }> => {
	try {
		const { data } = await client.query({
			query: GetAllProductsDocument,
			variables: {
				where: {
					createdUserId: {
						eq: createdUserId
					}
				}
			},
			fetchPolicy: 'network-only'
		})

		return { products: data.findProducts.docs, error: null }
	} catch (error) {
		return { products: [], error: JSON.stringify(error, null, 2) }
	}
}

export const getProductById = async (id: string) => {
	try {
		const { data } = await client.query({
			query: GetProductById,
			variables: {
				getProductId: id
			}
		})

		return data.getProduct
	} catch (error) {
		console.log(error)
	}
}

export const getVariantById = async (id: string) => {
	try {
		const { data } = await client.query({
			query: GetVariantById,
			variables: {
				getVariantId: id
			}
		})

		return data.getVariant
	} catch (error) {
		console.log(error)
	}
}

export const createProduct = async (createProductInput: CreateProductInput) => {
	try {
		const response = await client.mutate({
			mutation: CreateProductMutation,
			variables: {
				createProductInput
			}
		})

		revalidatePath('/home/product')
		return response.data?.createProduct
	} catch (error) {
		console.log(error)
	}
}
