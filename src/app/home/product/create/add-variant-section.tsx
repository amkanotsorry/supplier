'use client'

import { Drag1Icon } from '@/src/assets/icons'
import Seperator from '@/src/components/seperator'
import { Button, Input, Select, SelectItem } from '@nextui-org/react'
import { PlusIcon, Trash2Icon } from 'lucide-react'
import { useContext, useEffect } from 'react'
import { CreateProductContext, initialVariantValues } from './context'
import VariantsTable from './variants-table'
import { useParams } from 'next/navigation'
import { Variant_Enum } from '@/src/app/api/generated/graphql'

interface Props {
	variantOptions: Variant_Enum[]
}

const AddVariantSection = ({ variantOptions }: Props) => {
	const { id } = useParams()
	const { variants, setVariants } = useContext(CreateProductContext)

	const addVariants = () => {
		setVariants(prev => [...prev, { optionName: '', optionValues: [{ ...initialVariantValues }] }])
	}

	return (
		<div className="flex flex-col p-6 border rounded-xl bg-white w-full">
			<div className="flex gap-6">
				<div className="flex flex-col flex-1">
					<div className="text-sm font-semibold mb-1">
						Variants <span className="text-danger-400">*</span>
					</div>

					<div className="text-neutral-700 text-xs font-medium">Add product variant(s). </div>
				</div>

				<div className="flex flex-col gap-4 flex-1">
					{variants.map((variant, index) => (
						<div className="flex flex-col gap-3" key={index}>
							<div className="flex flex-col w-full gap-2">
								<span className="text-xs font-semibold ml-8 text-lightgray-1000">Option name</span>

								<div className="flex gap-4 items-center">
									<Drag1Icon className="w-6" />

									<Select
										classNames={{
											mainWrapper: 'h-12',
											label: 'text-sm',
											innerWrapper: 'capitalize'
										}}
										value={variant.optionName}
										selectedKeys={[variant.optionName]}
										onChange={event =>
											setVariants(prev => {
												const temp = [...prev]
												temp[index].optionName = event.target.value
												return temp
											})
										}
										label="Select option">
										{variantOptions.map(option => (
											<SelectItem className="py-3 capitalize" key={option} value={option}>
												{option}
											</SelectItem>
										))}
									</Select>

									<Trash2Icon
										className="cursor-pointer"
										onClick={() => {
											const filteredVariants = variants.filter((_, idx) => idx !== index)
											setVariants(filteredVariants)
										}}
									/>
								</div>

								<div className="pl-8 mt-1">
									<div className="flex flex-col gap-3">
										<div className="flex flex-col w-full gap-2">
											<span className="text-xs font-semibold ml-9 text-lightgray-1000">Option values</span>

											{variant.optionValues.map((optionValue, optionValueIndex) => {
												if (variant.optionValues[variant.optionValues.length - 1].variant !== '') {
													const temp = [...variants]
													temp[index].optionValues.push({ ...initialVariantValues })
													setVariants(temp)
												}

												const showTools = variant.optionValues.length - 1 !== optionValueIndex

												return (
													<div key={optionValueIndex} className="flex items-center">
														<div className="w-8">{showTools && <Drag1Icon className="m-auto" />}</div>

														<Input
															value={optionValue.variant}
															onChange={e => {
																const temp = [...variants]
																temp[index].optionValues[optionValueIndex].variant = e.target.value
																setVariants(temp)
															}}
															placeholder="Option Value"
														/>

														<div className="w-8">
															{showTools && (
																<Trash2Icon
																	className="cursor-pointer w-4 m-auto"
																	onClick={() => {
																		const temp = [...variants]
																		const filteredOptionValues = temp[index].optionValues.filter((_, idx) => idx !== optionValueIndex)
																		temp[index].optionValues = filteredOptionValues
																		setVariants(temp)
																	}}
																/>
															)}
														</div>
													</div>
												)
											})}
										</div>
									</div>
								</div>
							</div>
						</div>
					))}

					<div>
						<Button onClick={addVariants} className="border-[1px] border-lightgray-200 text-primary font-medium" variant="bordered">
							<PlusIcon />
							Add
						</Button>
					</div>
				</div>
			</div>

			<Seperator className="my-6" />

			<VariantsTable />
		</div>
	)
}

export default AddVariantSection
