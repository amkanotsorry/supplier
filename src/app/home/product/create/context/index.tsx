'use client'

import React, { createContext, useEffect, useState } from 'react'
import { ConvertedVariant } from '../variants-table'
import { GetProductQuery } from '@/src/app/apiProductMs/generated/graphql'

export const initialVariantValues = {
	variant: ''
}

export type VariantType = {
	optionName: string
	optionValues: (typeof initialVariantValues)[]
}

type ContextValues = {
	variants: VariantType[]
	setVariants: React.Dispatch<React.SetStateAction<VariantType[]>>
	activeVariants: ConvertedVariant[]
	onSelectVariant: (variant: ConvertedVariant) => void
	product: GetProductQuery['getProduct'] | undefined
	setActiveVariants: React.Dispatch<React.SetStateAction<ConvertedVariant[]>>
}

const defaultValue: ContextValues = {
	variants: [],
	setVariants: () => {},
	activeVariants: [],
	onSelectVariant: () => {},
	product: undefined,
	setActiveVariants: () => {}
}

export const CreateProductContext = createContext(defaultValue)

interface Props {
	children: React.ReactNode
	product?: GetProductQuery['getProduct']
}

const CreateProductContextProvider: React.FC<Props> = ({ children, product }) => {
	const [variants, setVariants] = useState<VariantType[]>([])
	const [activeVariants, setActiveVariants] = useState<ConvertedVariant[]>([])

	const onSelectVariant = (variant: ConvertedVariant) => {
		let temp = [...activeVariants]

		if (activeVariants.find(v => v.displayValue === variant.displayValue)) {
			temp = activeVariants.filter(v => v.displayValue !== variant.displayValue)
		} else {
			temp.push(variant)
		}

		setActiveVariants(temp)
	}

	useEffect(() => {
		if (product?.variants?.length) {
			let variant: VariantType = { optionName: '', optionValues: [] }
			variant.optionValues = []
			product.variants.forEach(el => {
				const attr = el.attributes?.[0]
				variant.optionName = attr?.option || ''
				variant.optionValues.push({ variant: attr?.value || '' })
			})
			setVariants([variant])
		}
		// console.log('product?.variants', product?.variants)
	}, [product])

	return (
		<CreateProductContext.Provider value={{ variants, setVariants, activeVariants, onSelectVariant, product, setActiveVariants }}>
			{children}
		</CreateProductContext.Provider>
	)
}

export default CreateProductContextProvider
