import { Button, Spinner } from '@nextui-org/react'
import { useParams } from 'next/navigation'
import React from 'react'

interface Props {
	isSaveDisabled: boolean
	loading: boolean
	onSave: () => void
	onCancel: () => void
}

const CreateProductFooter: React.FC<Props> = ({ isSaveDisabled, loading, onSave, onCancel }) => {
	const { id } = useParams()

	return (
		<div className="bg-white border-t flex flex-shrink-0 justify-end items-center h-16 px-4 gap-4">
			<Button onClick={onCancel} className="border-[1px]" variant="bordered">
				Cancel
			</Button>
			<Button
				onClick={onSave}
				className="disabled:bg-lightgray-200 disabled:text-lightgray-800"
				color="primary"
				disabled={loading || isSaveDisabled || Boolean(id)}>
				{loading ? <Spinner color="white" size="sm" /> : 'Save'}
			</Button>
		</div>
	)
}

export default CreateProductFooter
