'use client'

import { GetProductQuery, Product_Condition, Product_Status, Variant_Status, Weight_Unit } from '@/src/app/apiProductMs/generated/graphql'
import useEntity from '@/src/app/hooks/use-entity'
import { toast } from '@/src/components/ui/use-toast'
import { cn } from '@/src/lib/utils'
import { Input, Select, SelectItem } from '@nextui-org/react'
import { useRouter } from 'next/navigation'
import React, { useContext, useState } from 'react'
import FileUpload from '../../get-started/upload-documents/Documents/file-upload'
import isApproved from '../../isApproved'
import { createProduct } from '../actions/product'
import { QueryProductMetaType } from '../actions/product-metas'
import AddVariantSection from './add-variant-section'
import { CreateProductContext } from './context'
import CreateProductFooter from './create-product-footer'
import ProductMetaInfo from './product-meta-info'

interface Props {
	product?: GetProductQuery['getProduct']
	metas: QueryProductMetaType[]
}

const CreateProductSection: React.FC<Props> = ({ product, metas }) => {
	const { activeVariants } = useContext(CreateProductContext)
	const router = useRouter()
	const { entity } = useEntity({})
	const [selectedProductMeta, setSelectedProductMeta] = useState<string | undefined>(product?.metaId || undefined)
	const [name, setName] = useState(product?.name || '')
	const [weight, setWeight] = useState(product?.weight?.toString() || '')
	const [volume, setVolume] = useState(product?.volume || '')
	const [composition, setComposition] = useState(product?.composition || '')
	const [productTestReportFile, setProductTestReportFile] = useState<File | undefined>()
	const [loading, setLoading] = useState(false)

	const onSave = async () => {
		try {
			if (activeVariants.length) {
				for (const variant of activeVariants) {
					if (!variant.availableDate) {
						toast({ title: 'Variant available date required!', variant: 'destructive' })
						return
					}
				}
			}

			setLoading(true)

			// const testReportFile = productTestReportFile ? await uploadFile(productTestReportFile) : undefined

			const response = await createProduct({
				metaId: selectedProductMeta,
				availableDate: '2023/11/20',
				condition: Product_Condition.New,
				description: composition,
				expiryDate: null,
				images: [],
				isPhysical: true,
				isReturnable: false,
				media: '',
				name: name,
				price: 8000,
				sku: '03457239',
				slug: name,
				status: Product_Status.Active,
				createdUserId: entity.entitySupplierId,
				variants: activeVariants.map(variant => {
					const price = parseInt(variant.costPerUnit || '0', 10)
					return {
						attributes: variant.attributes.map(attr => {
							return {
								option: attr.option,
								value: attr.value,
								isActive: true
							}
						}),
						availableOn: variant.availableDate,
						barcode: '240931',
						certificates_of_origin: variant.certificate,
						discountPrice: price,
						sellingPrice: price,
						price: price,
						expiryDate: variant.expiryDate,
						images: variant.productImages || [],
						placeOfOrigin: variant.placeOfOrigin,
						sku: '329847',
						status: variant.isReady ? Variant_Status.Available : Variant_Status.Unavailable,
						quantity: parseInt((variant.quantity || '').toString(), 10) || 0,
						weight: 0,
						weightUnit: Weight_Unit.Gram
					}
				}),
				weight: parseInt(weight, 10),
				weightUnit: Weight_Unit.Kilogram
			})

			setLoading(false)

			if (response?._id) {
				toast({
					title: 'Product created successfully!'
				})
				router.replace('/home/product')
				return
			}

			toast({
				title: 'Error creating a product',
				variant: 'destructive'
			})
		} catch (error) {
			console.log(error)
		}
	}

	const selectedMeta = metas?.find(m => m._id === selectedProductMeta)

	return (
		<>
			<div className="h-full overflow-y-scroll">
				<div className="flex p-6 gap-6 bg-lightgray-100">
					<div className="flex flex-col w-full max-w-[360px]">
						<div className="flex flex-col border rounded-xl p-6 bg-white h-min">
							<div className="text-sm font-semibold mb-1">
								Product meta <span className="text-danger-400">*</span>
							</div>

							<div className="text-neutral-700 text-xs font-medium mb-6">Select product meta. </div>

							<Select
								selectedKeys={selectedProductMeta ? [selectedProductMeta] : undefined}
								classNames={{
									mainWrapper: 'h-10',
									label: 'text-sm'
								}}
								onChange={event => setSelectedProductMeta(event.target.value)}
								label="Select a product meta">
								{Array.isArray(metas) ? (
									metas.map(meta => (
										<SelectItem className="py-3" key={meta._id} value={meta._id}>
											{meta.name}
										</SelectItem>
									))
								) : (
									<SelectItem isReadOnly key={''}>
										No meta found.
									</SelectItem>
								)}
							</Select>

							{selectedMeta && <ProductMetaInfo meta={selectedMeta} />}
						</div>
					</div>

					<div className={cn('flex flex-col gap-6 w-full opacity-50', selectedProductMeta && 'opacity-100')}>
						<div className="flex border rounded-xl p-6 gap-6 bg-white w-full">
							<div className="flex flex-col flex-1">
								<div className="text-sm font-semibold mb-1">
									Product <span className="text-danger-400">*</span>
								</div>

								<div className="text-neutral-700 text-xs font-medium">Fill in product information. </div>
							</div>

							<div className="flex flex-1 flex-col gap-4">
								<div className="flex flex-col gap-2">
									<div className="text-xs font-semibold mb-1 text-lightgray-1000">
										Name <span className="text-danger-400">*</span>
									</div>

									<Input
										value={name}
										classNames={{ inputWrapper: 'h-10' }}
										disabled={!selectedProductMeta}
										onChange={e => setName(e.target.value)}
										placeholder="Product name"
									/>
								</div>

								<div className="flex gap-4">
									<div className="flex flex-col gap-2">
										<div className="text-xs font-semibold mb-1 text-lightgray-1000">
											Weight <span className="text-danger-400">*</span>
										</div>

										<Input
											value={weight}
											classNames={{ inputWrapper: 'h-10' }}
											disabled={!selectedProductMeta}
											onChange={e => setWeight(e.target.value)}
											placeholder="Weight"
											endContent={<div className="text-sm text-lightgray-800">Kг</div>}
										/>
									</div>

									<div className="flex flex-col gap-2">
										<div className="text-xs font-semibold mb-1 text-lightgray-1000">Volume</div>

										<Input
											value={volume}
											classNames={{ inputWrapper: 'h-10' }}
											disabled={!selectedProductMeta}
											onChange={e => setVolume(e.target.value)}
											placeholder="Volume"
											endContent={<div className="text-sm text-lightgray-800">&#13220;</div>}
										/>
									</div>
								</div>

								<div className="flex flex-col gap-2">
									<div className="text-xs font-semibold mb-1 text-lightgray-1000">Attach product test report</div>

									<FileUpload
										disabled={!selectedProductMeta}
										value={productTestReportFile}
										name="file-upload-1"
										onChange={file => {
											setProductTestReportFile(file)
										}}
									/>
								</div>

								<div className="flex flex-col gap-2">
									<div className="text-xs font-semibold mb-1 text-lightgray-1000">Composition</div>

									<Input
										classNames={{ inputWrapper: 'h-10' }}
										disabled={!selectedProductMeta}
										onChange={e => setComposition(e.target.value)}
										placeholder="Please Input"
									/>
								</div>
							</div>
						</div>

						<AddVariantSection variantOptions={selectedMeta?.attributes || []} />
					</div>
				</div>
			</div>

			<CreateProductFooter loading={loading} isSaveDisabled={false} onSave={onSave} onCancel={() => {}} />
		</>
	)
}

export default isApproved(CreateProductSection)
