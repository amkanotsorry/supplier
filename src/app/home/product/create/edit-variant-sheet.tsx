'use client'

import DatePicker from '@/src/components/datepicker'
import Seperator from '@/src/components/seperator'
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from '@/src/components/ui/form'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '@/src/components/ui/select'
import { Sheet, SheetClose, SheetContent, SheetFooter, SheetHeader, SheetTitle, SheetTrigger } from '@/src/components/ui/sheet'
import { zodResolver } from '@hookform/resolvers/zod'
import { Button, Input, Spinner, Switch } from '@nextui-org/react'
import { Eye, Trash } from 'lucide-react'
import Image from 'next/image'
import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { z } from 'zod'
import { ConvertedVariant } from './variants-table'
import { countries } from '@/src/lib/countries'
import useEntity from '@/src/app/hooks/use-entity'
import UploadImage from '@/src/components/upload-image'
import { toast } from '@/src/components/ui/use-toast'
import { uploadFile } from '../../upload-file'
import { useAddress } from '@/src/lib/address-helper'

const FormSchema = z.object({
	costPerUnit: z.string(),
	placeOfOrigin: z.string().optional(),
	productImages: z.array(z.string()),
	certificate: z.instanceof(File).optional(),
	expiryDate: z.date().optional(),
	quantity: z.string(),
	availableDate: z.date(),
	isReady: z.boolean().optional(),
	stockLocation: z.string()
})

interface Props {
	triggerComponent: React.ReactNode
	variant: ConvertedVariant
	onSave: (input: any) => void
}

const EditVariantSheet: React.FC<Props> = ({ triggerComponent, variant, onSave }) => {
	const [sheetIsOpen, setSheetIsOpen] = useState<boolean>(false)
	const { entity } = useEntity({})
	const [uploading, setUploading] = useState(false)

	const form = useForm<z.infer<typeof FormSchema>>({
		resolver: zodResolver(FormSchema),
		defaultValues: {
			costPerUnit: variant.costPerUnit || variant.price || '0',
			placeOfOrigin: variant.placeOfOrigin || '',
			certificate: undefined,
			expiryDate: variant.expiryDate ? new Date(variant.expiryDate) : new Date(),
			quantity: variant.quantity?.toString() || '0',
			availableDate: variant.availableDate ? new Date(variant.availableDate) : new Date(),
			isReady: variant.isReady || false,
			productImages: variant.productImages || []
		}
	})

	const { aimagHotOptions, address } = useAddress({ form, aimagFormKey: 'placeOfOrigin' })

	function onSubmit(data: z.infer<typeof FormSchema>) {
		data.placeOfOrigin = address?.cityProvince.name
		onSave(data)
		setSheetIsOpen(false)
	}

	const addProfileImage = async (file?: File) => {
		if (!file) return
		if (form.watch('productImages').length > 4) {
			toast({
				title: 'Cannot add images more than 5.',
				variant: 'destructive'
			})
			return
		}

		setUploading(true)
		const fileUrl = await uploadFile(file)
		setUploading(false)

		const temp = [...form.watch('productImages')]
		if (fileUrl?.files[0].url) temp.push(`https://agrix-supplier.s3.ap-southeast-1.amazonaws.com/${fileUrl?.files[0].url}`)
		form.setValue('productImages', temp)
	}

	const onRemoveImage = (index: number) => {
		const temp = [...form.watch('productImages')]
		temp.splice(index, 1)
		form.setValue('productImages', temp)
	}

	return (
		<Sheet open={sheetIsOpen} onOpenChange={open => setSheetIsOpen(open)}>
			<SheetTrigger asChild>{triggerComponent}</SheetTrigger>
			<SheetContent className="p-0 flex flex-col justify-between gap-0 w-[400px] sm:w-[400px] max-w-none sm:max-w-none">
				<Form {...form}>
					<form className="flex flex-col h-full" onSubmit={form.handleSubmit(onSubmit)}>
						<SheetHeader className="border-b p-4">
							<SheetTitle>Edit profile</SheetTitle>
						</SheetHeader>
						<div className="h-full overflow-y-auto">
							<div className="flex flex-col gap-4 p-4 h-full shrink-0">
								<div className="text-sm shrink-0 font-semibold text-lightgray-1200">Variant Info</div>

								<FormField
									name="variant"
									render={() => (
										<FormItem className="w-full shrink-0">
											<FormLabel>Variant</FormLabel>
											<FormControl>
												<Input classNames={{ inputWrapper: 'h-10' }} disabled value={variant.displayValue} />
											</FormControl>
											<FormMessage />
										</FormItem>
									)}
								/>

								<div className="text-xs font-semibold shrink-0 mb-1 text-lightgray-1000">
									Product picture - {form.watch('productImages').length}/5
								</div>

								<div className="shrink-0 overflow-x-auto">
									<div className="flex gap-3 shrink-0">
										<div className="w-28 shrink-0">
											<UploadImage name="image-upload" onChange={addProfileImage} />
										</div>

										{uploading && (
											<div className="flex shrink-0 items-center justify-center w-28 h-28 border border-zinc-200 rounded-xl">
												<Spinner size="sm" className="text-lightgray-1000" color="current" />
											</div>
										)}

										{form.watch('productImages').map((image, index) => (
											<ProductImage key={index} image={image} handleRemove={() => onRemoveImage(index)} />
										))}
									</div>
								</div>

								<div className="flex gap-4 shrink-0">
									<FormField
										control={form.control}
										name="costPerUnit"
										render={({ field }) => (
											<FormItem className="w-full">
												<FormLabel>
													Input cost per one unit <span className="text-danger-400">*</span>
												</FormLabel>
												<FormControl>
													<Input classNames={{ inputWrapper: 'h-10' }} {...field} placeholder="0.00" endContent={<div className="text-md">₮</div>} />
												</FormControl>
												<FormMessage />
											</FormItem>
										)}
									/>

									<FormField
										control={form.control}
										name="placeOfOrigin"
										render={({ field }) => (
											<FormItem className="w-full">
												<FormLabel>Place of origin</FormLabel>
												<Select value={field.value} onValueChange={field.onChange} defaultValue={field.value}>
													<FormControl>
														<SelectTrigger className="w-[180px] h-10">
															<SelectValue placeholder="Select place" />
														</SelectTrigger>
													</FormControl>
													<SelectContent className="overflow-y-auto">
														{aimagHotOptions.map((country, index) => (
															<SelectItem className="py-3" key={index} value={country.code.toString()}>
																<div className="flex gap-2">{country.name}</div>
															</SelectItem>
														))}
													</SelectContent>
												</Select>
												<FormMessage />
											</FormItem>
										)}
									/>
								</div>

								<Seperator className="border-t" />

								<div className="text-sm shrink-0 font-semibold text-lightgray-1200">Stock Info</div>

								<div className="flex shrink-0 gap-4">
									<FormField
										control={form.control}
										name="expiryDate"
										render={({ field }) => (
											<FormItem className="w-full">
												<FormLabel>Expiry date</FormLabel>
												<FormControl>
													<DatePicker className="h-10" {...field} placeholder="0000/00/00" />
												</FormControl>
												<FormMessage />
											</FormItem>
										)}
									/>

									<FormField
										control={form.control}
										name="quantity"
										render={({ field }) => (
											<FormItem className="w-full">
												<FormLabel>
													Quantity <span className="text-danger-400">*</span>
												</FormLabel>
												<FormControl>
													<Input classNames={{ inputWrapper: 'h-10' }} {...field} type="number" placeholder="0" />
												</FormControl>
												<FormMessage />
											</FormItem>
										)}
									/>
								</div>

								<div className="shrink-0">
									<FormField
										control={form.control}
										name="stockLocation"
										render={({ field }) => (
											<FormItem className="w-full">
												<FormLabel>
													Stock location <span className="text-danger-400">*</span>
												</FormLabel>
												<Select onValueChange={field.onChange} defaultValue={field.value} {...field}>
													<FormControl>
														<SelectTrigger>
															<SelectValue placeholder="Select place" />
														</SelectTrigger>
													</FormControl>
													<SelectContent>
														{(entity.entitySupplier?.stockLocations || []).map((stockLocation, index) => (
															<SelectItem className="py-3" key={index} value={stockLocation._id}>
																{stockLocation.branchName}
															</SelectItem>
														))}
													</SelectContent>
												</Select>
												<FormMessage />
											</FormItem>
										)}
									/>
								</div>

								<div className="flex shrink-0 gap-4 pb-4">
									<FormField
										control={form.control}
										name="isReady"
										render={({ field }) => (
											<FormItem className="w-full">
												<FormLabel>
													Status <span className="text-danger-400">*</span>
												</FormLabel>
												<FormControl>
													<Input
														classNames={{ inputWrapper: 'h-10' }}
														value={'Ready'}
														endContent={<Switch color="success" size="sm" className="w-11 h-6" />}
													/>
												</FormControl>
												<FormMessage />
											</FormItem>
										)}
									/>

									<FormField
										control={form.control}
										name="availableDate"
										render={({ field }) => (
											<FormItem className="w-full">
												<FormLabel>
													Available date <span className="text-danger-400">*</span>
												</FormLabel>
												<FormControl>
													<DatePicker className="h-10" {...field} placeholder="0000/00/00" />
												</FormControl>
												<FormMessage />
											</FormItem>
										)}
									/>
								</div>
							</div>
						</div>

						<SheetFooter className="border-t p-4 gap-4 flex">
							<SheetClose asChild>
								<Button className="border-[1px]" variant="bordered">
									Cancel
								</Button>
							</SheetClose>

							<Button disabled={uploading} type="submit" className="disabled:bg-lightgray-200 disabled:text-lightgray-800" color="primary">
								Save
							</Button>
						</SheetFooter>
					</form>
				</Form>
			</SheetContent>
		</Sheet>
	)
}

export default EditVariantSheet

const ProductImage: React.FC<{ image: string; handleRemove: () => void }> = ({ image, handleRemove }) => {
	const [isHovered, setIsHovered] = useState(false)

	return (
		<div
			onMouseEnter={() => setIsHovered(true)}
			onMouseLeave={() => setIsHovered(false)}
			className="border w-28 h-28 rounded-xl relative overflow-hidden object-cover shrink-0">
			<Image alt="img" className="object-cover absolute w-28 h-28 rounded-xl" width={112} height={112} src={image} />

			{isHovered && (
				<div className="absolute z-20 flex justify-center items-center gap-3 w-28 h-28 rounded-xl bg-gray-500/[.4] text-white">
					<Eye className="cursor-pointer" />
					<Trash className="cursor-pointer" onClick={handleRemove} />
				</div>
			)}
		</div>
	)
}
