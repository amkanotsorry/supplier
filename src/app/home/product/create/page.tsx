import PageSubHeader from '../../_components/page-header/page-sub-header'
import { getProductMetas } from '../actions/product-metas'
import CreateProductContextProvider from './context'
import CreateProductSection from './create-product-section'

const ProductCreatePage = async () => {
	const { getProductMetaList } = await getProductMetas()

	return (
		<>
			<PageSubHeader title="Create product" />

			<CreateProductContextProvider>
				<CreateProductSection metas={getProductMetaList} />
			</CreateProductContextProvider>
		</>
	)
}

export default ProductCreatePage
