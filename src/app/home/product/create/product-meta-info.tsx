import { Input } from '@nextui-org/react'
import React, { useContext } from 'react'
import { CreateProductContext } from './context'

interface Props {
	meta: any
}

const ProductMetaInfo: React.FC<Props> = ({ meta }) => {
	return (
		<div className="flex flex-col gap-4 mt-4">
			<div className="flex gap-4">
				<div className="flex flex-1 flex-col gap-2">
					<div className="text-xs font-semibold text-lightgray-1000">Unit</div>

					<Input classNames={{ inputWrapper: 'h-10' }} disabled value={meta.unit} />
				</div>

				<div className="flex flex-1 flex-col gap-2">
					<div className="text-xs font-semibold text-lightgray-1000">unitWeight</div>

					<Input classNames={{ inputWrapper: 'h-10' }} disabled value={meta.unitWeight.toString()} />
				</div>

				<div className="flex flex-1 flex-col gap-2">
					<div className="text-xs font-semibold text-lightgray-1000">unitVolume</div>

					<Input classNames={{ inputWrapper: 'h-10' }} disabled value={meta.unitVolume.toString()} />
				</div>
			</div>

			<div className="flex flex-1 flex-col gap-2">
				<div className="text-xs font-semibold text-lightgray-1000">Variants</div>

				<div className="flex gap-2 bg-default-100 py-[5px] px-4 rounded-xl h-14 items-center">
					{meta.attributes.map((attr: any) => (
						<div key={attr} className="py-1 px-2 bg-white rounded-lg shadow-sm shadow-lightgray-300 text-sm">
							{attr}
						</div>
					))}
				</div>
			</div>
		</div>
	)
}

export default ProductMetaInfo
