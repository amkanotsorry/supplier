'use client'

import { Checkbox, Table, TableBody, TableCell, TableColumn, TableHeader, TableRow } from '@nextui-org/react'
import moment from 'moment'
import React, { useContext, useEffect, useState } from 'react'
import { CreateProductContext, VariantType } from './context'
import EditVariantSheet from './edit-variant-sheet'
import Image from 'next/image'

type VariantAttribute = {
	option: string
	value: string
}

export type ConvertedVariant = {
	displayValue: string
	attributes: VariantAttribute[]
	productImages?: string[]
	costPerUnit?: string
	price?: string
	placeOfOrigin?: string
	certificate?: string
	expiryDate?: Date
	quantity?: number
	stockLocation?: string
	isReady?: boolean
	availableDate?: Date
	availableOn?: Date
}

const convertVariants = (variants: VariantType[]): ConvertedVariant[] => {
	const convertedVariants: ConvertedVariant[] = []

	function combineOptions(currentIndex: number, currentCombination: { option: string; value: string }[]): void {
		if (currentIndex === variants.length) {
			convertedVariants.push({ displayValue: currentCombination.map(data => data.value).join(' / '), attributes: currentCombination })
			return
		}

		const currentOptionValues = variants[currentIndex].optionValues
		const currentOptionName = variants[currentIndex].optionName

		for (const optionValue of currentOptionValues) {
			if (optionValue.variant !== '') {
				combineOptions(currentIndex + 1, [...currentCombination, { option: currentOptionName, value: optionValue.variant }])
			}
		}
	}

	combineOptions(0, [])

	return convertedVariants
}

interface Props {}

const VariantsTable: React.FC<Props> = () => {
	const { variants, product } = useContext(CreateProductContext)
	let convertedVariants = convertVariants(variants)

	if (product?.variants)
		convertedVariants = product?.variants.map((el, i) => {
			return {
				...el,
				availableDate: el.availableOn,
				placeOfOrigin: el.placeOfOrigin,
				displayValue: el.attributes?.[0]?.value || '',
				...(variants?.[i] ?? {})
			} as unknown as ConvertedVariant
		})

	return <RenderTable convertedVariants={convertedVariants} />
}

export default VariantsTable

type RenderTableProps = {
	convertedVariants: ConvertedVariant[]
}
const RenderTable = React.forwardRef<HTMLDivElement, RenderTableProps>(({ convertedVariants }, ref) => {
	const { onSelectVariant, activeVariants, setActiveVariants } = useContext(CreateProductContext)
	const [matrixVariants, setMatrixVariants] = useState<ConvertedVariant[]>(convertedVariants)

	const onSave = (values: ConvertedVariant, index: number) => {
		const temp = [...matrixVariants]
		temp[index] = { ...values, displayValue: temp[index].displayValue, attributes: temp[index].attributes }
		setMatrixVariants(temp)
		const activeDisplayValues = activeVariants.map(a => a.displayValue)
		const activeTemps = temp.filter(el => activeDisplayValues.includes(el.displayValue))
		if (activeTemps.length) setActiveVariants(activeTemps)
	}

	useEffect(() => {
		const temp = [...matrixVariants]
		const newMatrixVariants: ConvertedVariant[] = convertedVariants.map((v, index) => {
			return {
				...temp[index],
				displayValue: v.displayValue,
				attributes: v.attributes
			}
		})

		setMatrixVariants(newMatrixVariants)
	}, [convertedVariants])

	return (
		<Table
			classNames={{
				wrapper: 'shadow-none p-0 rounded-none overflow-x-scroll',
				thead: '!h-[34px] [&>*:nth-child(2)]:hidden',
				th: '!rounded-none !h-[34px] border-r last:border-none',
				tr: 'border-t',
				td: 'border-r last:border-none'
			}}>
			<TableHeader>
				<TableColumn>Variant</TableColumn>
				<TableColumn>Price</TableColumn>
				<TableColumn>Expiry Date</TableColumn>
				<TableColumn>Product Images</TableColumn>
				<TableColumn isRowHeader>Action</TableColumn>
			</TableHeader>
			<TableBody>
				{matrixVariants.map((variant, index) => (
					<TableRow key={index}>
						<TableCell>
							<Checkbox
								checked={Boolean(activeVariants.find(el => el.displayValue === variant.displayValue))}
								onChange={() => onSelectVariant(matrixVariants[index])}
							/>
							{variant.displayValue || '-'}
						</TableCell>
						<TableCell>{variant.costPerUnit || variant.price || '-'}</TableCell>
						<TableCell>{variant.expiryDate ? moment(variant.expiryDate).format('yyyy/MM/DD') : '-'}</TableCell>
						<TableCell>
							<div className="flex gap-2">
								{(variant.productImages || []).map((image, index) => (
									<div key={index} className="overflow-hidden relative border rounded-md w-8 h-8">
										<Image fill objectFit="cover" src={image} alt={'product Image' + index} />
									</div>
								))}
							</div>
						</TableCell>
						<TableCell onSelect={() => null}>
							<EditVariantSheet
								onSave={values => onSave(values, index)}
								variant={variant}
								triggerComponent={<div className="cursor-pointer text-primary font-semibold">Edit</div>}
							/>
						</TableCell>
					</TableRow>
				))}
			</TableBody>
		</Table>
	)
})

RenderTable.displayName = 'RenderTable'
