'use client'

import { FilterIcon, UpDownIcon } from '@/src/assets/icons'
import { ColumnDef } from '@tanstack/react-table'
import React from 'react'
import { QueryProductType } from '../actions/product'

export const finalProductTableColumns: ColumnDef<QueryProductType>[] = [
	{
		accessorKey: 'id',
		header: ({ column }) => {
			return <HeaderComponent column={column} title="Id" />
		}
	},
	{
		accessorKey: 'name',
		header: ({ column }) => {
			return <HeaderComponent column={column} title="Name" />
		},
		cell: ({ row }) => <div className="lowercase">{row.getValue('name')}</div>
	},
	{
		accessorKey: 'price',
		header: ({ column }) => {
			return <HeaderComponent column={column} title="Price" />
		},
		cell: ({ row }) => <div className="lowercase">{row.getValue('price')}</div>
	},
	{
		accessorKey: 'sku',
		header: ({ column }) => {
			return <HeaderComponent column={column} title="Sku" />
		},
		cell: ({ row }) => <div className="lowercase">{row.getValue('sku')}</div>
	},
	{
		accessorKey: 'status',
		header: ({ column }) => {
			return <HeaderComponent column={column} title="Status" />
		},
		cell: ({ row }) => <div className="lowercase">{row.getValue('status')}</div>
	},
	{
		accessorKey: 'description',
		header: ({ column }) => {
			return <HeaderComponent column={column} title="Description" />
		},
		cell: ({ row }) => <div className="lowercase">{row.getValue('description')}</div>
	}
]

const HeaderComponent: React.FC<{ title: string; column: any }> = ({ title, column }) => {
	return (
		<div className="flex items-center justify-between mb-2" onClick={() => column.toggleSorting(column.getIsSorted() === 'asc')}>
			<div className="flex items-center cursor-pointer">
				<span className="mx-2 text-xs font-semibold">{title}</span>
				<UpDownIcon />
			</div>

			<FilterIcon width={16} />
		</div>
	)
}
