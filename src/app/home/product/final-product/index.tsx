import { Download01Icon, FilterIcon, SettingsIcon, SortVertical02Icon } from '@/src/assets/icons'
import SearchBar from '@/src/components/search'
import { DataTable } from '@/src/components/table/DataTable'
import React from 'react'
import { QueryProductType } from '../actions/product'
import { finalProductTableColumns } from './final-product-table-columns'

interface Props {
	products: QueryProductType[]
}

const FinalProduct: React.FC<Props> = ({ products }) => {
	return (
		<div className="p-2 flex flex-col gap-4">
			<div className="flex justify-between">
				<SearchBar label="Search products" />

				{/* <CreateProductButton /> */}
			</div>

			{/* <div className="flex justify-between flex-wrap gap-4">
				<div className="flex gap-4">
					<div className="flex items-center gap-2 px-4 py-[9px] border rounded-xl cursor-pointer">
						<FilterIcon className="w-[18px] h-[18px]" />
						<span className="text-xs font-semibold whitespace-nowrap">Filter</span>
					</div>

					<div className="flex items-center gap-2 px-4 py-[9px] border rounded-xl cursor-pointer">
						<SortVertical02Icon className="w-[18px] h-[18px]" />
						<span className="text-xs font-semibold whitespace-nowrap">Sort by</span>
					</div>
				</div>

				<div className="flex gap-4">
					<div className="flex items-center gap-2 px-4 py-[9px] border rounded-xl cursor-pointer">
						<SettingsIcon className="w-[18px] h-[18px]" />
						<span className="text-xs font-semibold whitespace-nowrap">View Setting</span>
					</div>

					<div className="flex items-center gap-2 px-4 py-[9px] border rounded-xl cursor-pointer">
						<Download01Icon className="w-[18px] h-[18px]" />
						<span className="text-xs font-semibold whitespace-nowrap">Export</span>
					</div>
				</div>
			</div> */}

			<DataTable onClickRow={() => {}} columns={finalProductTableColumns} data={products} />
		</div>
	)
}

export default FinalProduct
