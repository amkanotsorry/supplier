'use client'

import { Button } from '@nextui-org/react'
import { PlusIcon } from 'lucide-react'
import { useRouter } from 'next/navigation'
import React from 'react'

interface Props {}

const CreateProductButton: React.FC<Props> = () => {
	const router = useRouter()
	return (
		<Button onClick={() => router.push('/home/product/create')} color="primary">
			<PlusIcon />
			Create Product
		</Button>
	)
}

export default CreateProductButton
