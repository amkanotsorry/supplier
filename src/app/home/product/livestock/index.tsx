import SearchBar from '@/src/components/search'
import { DataTable } from '@/src/components/table/DataTable'
import moment from 'moment'
import React from 'react'
import { QueryProductType } from '../actions/product'
import CreateProductButton from './create-product-button'
import { livestockTableColumns } from './livestock-table-columns'
import { redirect, useRouter } from 'next/navigation'
import { RefreshCcw } from 'lucide-react'
import { Spinner } from '@nextui-org/react'

interface Props {
	products: QueryProductType[]
	refetch: () => void
	loading: boolean
}

const Livestock: React.FC<Props> = ({ products, refetch, loading }) => {
	const router = useRouter()
	const sortedProducts = products.sort((a, b) => moment(b.createdAt).valueOf() - moment(a.createdAt).valueOf())

	return (
		<div className="p-2 w-full flex flex-col gap-4 h-full overflow-y-auto">
			<div className="flex gap-4 shrink-0 items-center">
				<SearchBar label="Search products" />

				<RefreshCcw className="cursor-pointer" onClick={refetch} />

				<div className="ml-auto">
					<CreateProductButton />
				</div>
			</div>

			{loading ? (
				<div className="w-full h-full flex justify-center">
					<Spinner color="primary" />
				</div>
			) : (
				<DataTable
					onClickRow={data => {
						router.push(`/home/product/${data.original._id}`)
					}}
					columns={livestockTableColumns}
					data={sortedProducts}
				/>
			)}
		</div>
	)
}

export default Livestock
