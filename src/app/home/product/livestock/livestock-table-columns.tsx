'use client'

import { FilterIcon, UpDownIcon } from '@/src/assets/icons'
import { ColumnDef } from '@tanstack/react-table'
import React from 'react'
import { QueryProductType } from '../actions/product'
import moment from 'moment'
import Image from 'next/image'
import { convertCurrency } from '@/src/lib/currency'
import { Product_Status } from '@/src/app/apiProductMs/generated/graphql'
import { cn } from '@/src/lib/utils'

export const livestockTableColumns: ColumnDef<QueryProductType>[] = [
	{
		accessorKey: 'name',
		header: ({ column }) => {
			return <HeaderComponent column={column} title="Name" />
		},
		cell: ({ row }) => <div className="lowercase">{row.getValue('name')}</div>
	},
	{
		accessorKey: 'variants',
		header: ({ column }) => {
			return <HeaderComponent column={column} title="Images" />
		},
		cell: ({ row }) => {
			const variants: QueryProductType['variants'] = row.getValue('variants')
			const { images } = variants[0] || {}
			return (
				<div className="flex gap-2">
					{(images || []).map(image => (
						<Image className="w-5 h-5 rounded-sm" src={image} width={20} height={20} alt={'product image' + image} />
					))}
				</div>
			)
		}
	},
	{
		accessorKey: 'price',
		header: ({ column }) => {
			return <HeaderComponent column={column} title="Price" />
		},
		cell: ({ row }) => {
			const price: number = row.getValue('price') || 0
			return <div className="lowercase">{convertCurrency(price)}</div>
		}
	},
	{
		accessorKey: 'sku',
		header: ({ column }) => {
			return <HeaderComponent column={column} title="Sku" />
		},
		cell: ({ row }) => <div className="lowercase">{row.getValue('sku')}</div>
	},
	{
		accessorKey: 'status',
		header: ({ column }) => {
			return <HeaderComponent column={column} title="Status" />
		},
		cell: ({ row }) => {
			const status: Product_Status = row.getValue('status')
			return <ProductStatusTag status={status} />
		}
	},
	{
		accessorKey: 'description',
		header: ({ column }) => {
			return <HeaderComponent column={column} title="Description" />
		},
		cell: ({ row }) => <div className="lowercase">{row.getValue('description')}</div>
	},
	{
		accessorKey: 'createdAt',
		header: ({ column }) => {
			return <HeaderComponent column={column} title="Created At" />
		},
		cell: ({ row }) => <div className="lowercase">{moment(row.getValue('createdAt')).format('yyyy/MM/DD HH:mm')}</div>
	}
]

const HeaderComponent: React.FC<{ title: string; column: any }> = ({ title, column }) => {
	return (
		<div className="flex items-center justify-between mb-2" onClick={() => column.toggleSorting(column.getIsSorted() === 'asc')}>
			<div className="flex items-center cursor-pointer">
				<span className="mx-2 text-xs font-semibold">{title}</span>
				<UpDownIcon />
			</div>

			<FilterIcon width={16} />
		</div>
	)
}

const ProductStatusTag = ({ status }: { status: Product_Status }) => {
	const StatusContainer = ({ children, className }: any) => {
		return (
			<div className={cn('py-1 px-2 rounded-md text-xs w-fit flex items-center gap-1', className)}>
				<div style={{ lineHeight: 0, marginBottom: 3 }} className="text-2xl flex-0">
					•
				</div>
				{children}
			</div>
		)
	}

	switch (status) {
		case Product_Status.Active:
			return <StatusContainer className="text-success-400 bg-success-100">Active</StatusContainer>
		case Product_Status.Draft:
			return <StatusContainer class="text-warning-400 bg-warning-100">Draft</StatusContainer>
	}
}
