import { getEntitySupplierId } from '../../utils'
import { getAllProducts } from './actions/product'
import ProductTabs from './product-list-tabs'

const ProductPage = async () => {
	const id = await getEntitySupplierId()
	const { products } = await getAllProducts(id || '')

	return <ProductTabs products={products || []} />
}

export default ProductPage
