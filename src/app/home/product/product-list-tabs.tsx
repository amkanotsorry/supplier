'use client'

import { Chip, Tab, Tabs } from '@nextui-org/react'
import React from 'react'
import useProducts from '../../hooks/use-products'
import isApproved from '../isApproved'
import { QueryProductType } from './actions/product'
import FinalProduct from './final-product'
import Livestock from './livestock'
import './tab.css'

interface Props {
	products: QueryProductType[]
}

const ProductListTabs: React.FC<Props> = ({ products: initialData }) => {
	const { products, refetch, loading } = useProducts({ initialData })

	return (
		<div className="flex w-full h-full flex-col">
			<Tabs
				aria-label="Options"
				color="primary"
				variant="underlined"
				classNames={{
					tabList: 'gap-6 w-full relative rounded-none py-0 border-b border-divider px-4',
					cursor: 'w-full bg-primary',
					tab: 'max-w-fit px-0 h-12',
					tabContent: 'group-data-[selected=true]:text-primary shrink-0',
					base: 'shrink-0'
				}}>
				<Tab
					key="livestock"
					className="p-2 custom-tab"
					title={
						<div className="flex items-center space-x-2 h-full">
							<span className="text-sm font-semibold">Animal based</span>
							<Chip className="text-xs border-none rounded-md" size="sm" variant="faded">
								{products.length}
							</Chip>
						</div>
					}>
					<Livestock loading={loading} refetch={refetch} products={products} />
				</Tab>
				<Tab
					key="final_product"
					className="p-2 custom-tab"
					title={
						<div className="flex items-center space-x-2">
							<span className="text-sm font-semibold">Plant based</span>
							<Chip className="text-xs border-none rounded-md" size="sm" variant="faded">
								0
							</Chip>
						</div>
					}>
					<FinalProduct products={[]} />
				</Tab>
			</Tabs>
		</div>
	)
}

export default isApproved(ProductListTabs)
