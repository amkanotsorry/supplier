'use client'
import Button from '@/src/components/Button'
import { Dialog, DialogContent, DialogFooter, DialogHeader, DialogTrigger } from '@/src/components/ui/dialog'
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from '@/src/components/ui/form'
import { Input } from '@/src/components/ui/input'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '@/src/components/ui/select'
import { zodResolver } from '@hookform/resolvers/zod'
import React from 'react'
import { useForm } from 'react-hook-form'
import * as z from 'zod'

const FormSchema = z.object({
	cityProvince: z.string(),
	detailAddress: z.string(),
	branchName: z.string(),
	position: z.array(z.number()).min(2, { message: 'min 2' }).max(2, { message: 'max 2' })
})

type Props = {
	triggerComponent: React.ReactNode
	isOpen: boolean
	onCancel: () => void
	onOk?: () => void
}

const ModalAddEmployee = (props: Props) => {
	const { triggerComponent } = props

	const form = useForm<z.infer<typeof FormSchema>>({
		resolver: zodResolver(FormSchema)
	})

	function onSubmit(data: z.infer<typeof FormSchema>) {
		console.log('form data: ', data)
	}

	return (
		<Dialog>
			<DialogTrigger>{triggerComponent}</DialogTrigger>
			<DialogContent className="max-w-[544px]">
				<DialogHeader className="w-full">
					<div className="text-black text-lg font-semibold leading-7">Add Employee</div>
					{/* <div className="w-100 text-gray-400 text-sm font-medium leading-snug">
						Lorem ipsum dolor sit amet consectetur. Nibh mauris sed in ultricies ac sed ultricies amet iaculis.
					</div> */}
				</DialogHeader>
				<DialogContent>
					<Form {...form}>
						<form onSubmit={form.handleSubmit(onSubmit)}>
							<div className="p-4 flex flex-col gap-y-4">
								<FormField
									control={form.control}
									name="branchName"
									render={({ field }) => (
										<FormItem>
											<FormLabel>Invite by email</FormLabel>
											<FormControl>
												<Input className="bg-gray-50 border-0" placeholder="Email" {...field} />
											</FormControl>
											<FormMessage />
										</FormItem>
									)}
								/>

								<FormField
									control={form.control}
									name="cityProvince"
									render={({ field }) => (
										<FormItem>
											<FormLabel>Role</FormLabel>
											<Select onValueChange={field.onChange} defaultValue={field.value}>
												<FormControl>
													<SelectTrigger className="bg-gray-50 border-0">
														<SelectValue placeholder="Choose role" />
													</SelectTrigger>
												</FormControl>
												<SelectContent>
													<SelectItem value="Ulaanbaatar">Ulaanbaatar</SelectItem>
													<SelectItem value="Bayankhongor">Bayankhongor</SelectItem>
												</SelectContent>
											</Select>
											<FormMessage />
										</FormItem>
									)}
								/>
							</div>
						</form>
					</Form>
				</DialogContent>
				<DialogFooter className="flex justify-end p-4 border-t border-gray-100">
					<div className="flex gap-x-4">
						<Button text="Cancel" type="secondary" size="sm" />
						<Button text="Send invite" size="sm" />
					</div>
				</DialogFooter>
			</DialogContent>
		</Dialog>
	)
}

export default ModalAddEmployee
