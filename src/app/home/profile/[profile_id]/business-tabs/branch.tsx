import { Plus01Icon } from '@/src/assets/icons'

import Button from '@/src/components/Button'
import Text from '@/src/components/text'
import { FC, useState } from 'react'
import BranchItem from '../../../_components/branch-item'

type Props = {}

const Branch: FC = () => {
	const [isOpen, setIsOpen] = useState(false)

	const openModal = () => setIsOpen(true)
	const closeModal = () => setIsOpen(false)

	return (
		<div className="flex flex-col">
			<div className="flex flex-col py-4">
				<Text variant="xlSemibold">Branch</Text>
				<Text variant="mdRegular">Lorem ipsum dolor sit amet consectetur. Nibh mauris sed in ultricies ac sed ultricies amet iaculis.</Text>
			</div>
			<div className="flex flex-col py-4 gap-4">
				<BranchItem />
				<BranchItem />
				<Button type="secondary" text="Add location" icon={<Plus01Icon className="h-[22px] w-[22px]" />} onClick={openModal} />
			</div>
		</div>
	)
}

export default Branch
