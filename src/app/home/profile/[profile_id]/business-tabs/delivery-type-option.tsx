import { CheckIcon } from '@/src/assets/icons'
import { cn } from '@/src/lib/utils'
import React from 'react'

interface Props {
	title: string
	selected?: boolean
	disabled?: boolean
	onClick?: () => void
}

const DeliveryTypeOption: React.FC<Props> = ({ title, selected = false, disabled = true, onClick }) => {
	return (
		<div
			onClick={() => {
				if (!disabled) onClick?.()
			}}
			className={cn(
				`${selected || !disabled ? 'opacity-100' : 'opacity-50'} flex flex-1 flex-grow gap-4 p-4 items-center bg-lightgray-100 rounded-2xl`,
				!disabled ? 'cursor-pointer' : 'cursor-default'
			)}>
			<div className="flex flex-grow flex-col gap-1">
				<span className="text-sm font-medium">{title}</span>
				{/* <span className="text-xs text-lightgray-1000">Lorem ipsum dolor sit amet consectetur.</span> */}
			</div>

			{selected && (
				<div className="flex w-[18px] h-[18px] bg-primary rounded-full self-start">
					<CheckIcon className="text-white" />
				</div>
			)}
		</div>
	)
}

export default DeliveryTypeOption
