import { cn } from '@/src/lib/utils'
import { Input } from '@nextui-org/react'
import React from 'react'
import { ClassNameValue } from 'tailwind-merge'

interface Props {
	label: string
	value: string
	className?: ClassNameValue
}

const DetailField: React.FC<Props> = ({ label, value, className }) => {
	return (
		<div className="flex flex-col gap-1 w-full">
			<span className="text-sm font-semibold text-lightgray-1000">{label}</span>

			<Input
				classNames={{
					inputWrapper: 'h-10'
				}}
				readOnly
				className={cn('cursor-default', className)}
				value={value}
			/>
		</div>
	)
}

export default DetailField
