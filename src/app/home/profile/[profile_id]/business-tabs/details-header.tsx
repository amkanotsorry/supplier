import ProfileImage from '@/src/assets/images/avatar.png'
import { ChevronRight } from 'lucide-react'
import { getServerSession } from 'next-auth'
import Image from 'next/image'
import Link from 'next/link'
import LogoutButton from '../../../_components/page-header/logout-button'
import { authOptions } from '@/src/app/api/auth/[...nextauth]/authOptions'

const DetailsHeader = async () => {
	const session = await getServerSession(authOptions)

	return (
		<div className="w-full h-14 py-2 flex justify-between items-center px-4 border-b">
			<div className="flex items-center h-[26px]">
				<Link href={'/supplier-management'} className="text-xs font-semibold leading-6 text-lightgray-1000 p-1">
					Supplier management
				</Link>

				<div className="p-[3px]">
					<ChevronRight width={18} height={18} className="text-lightgray-800" />
				</div>

				<Link href={'/supplier-management'} className="text-xs font-semibold leading-6 text-lightgray-1000 p-1">
					New supplier request
				</Link>

				<div className="p-[3px]">
					<ChevronRight width={18} height={18} className="text-lightgray-800" />
				</div>

				<div className="text-xs leading-[18px] py-1 px-2 font-semibold bg-lightgray-200 rounded-md text-lightgray-1200">TechPack LLC</div>
			</div>

			<div className="flex gap-2">
				<Image src={ProfileImage} alt="Profile image" width={38} height={38} />

				<div>
					<div className="text-sm font-medium leading-5">{session?.user.email || 'Nameless User'}</div>
					<div className="text-xs text-support">Admin</div>
				</div>

				<LogoutButton />
			</div>
		</div>
	)
}

export default DetailsHeader
