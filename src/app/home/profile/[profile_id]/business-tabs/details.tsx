import { Edit04Icon, Factory01Icon, Luggage04Icon, Sheep01Icon } from '@/src/assets/icons'

import Button from '@/src/components/Button'
import Map from '@/src/components/map'
import Seperator from '@/src/components/seperator'
import Text from '@/src/components/text'
import { FC } from 'react'
import DetailField from './detail-field'
import SupplierTypeOption from './supplier-type-option'
import GoogleMaps from '@/src/components/map-position-selector/google-maps'
import { DEFAULT_LAT, DEFAULT_LNG } from '@/src/lib/constants'

const ProfileDetails: FC = () => {
	return (
		<div className="flex flex-col py-4 gap-9">
			<div className="flex flex-col py-4 gap-4">
				<div className="flex flex-col gap-2">
					<Text variant="xlSemibold">Entity info</Text>
					<Text variant="mdRegular">
						The segment is non-editable. If changes are needed, kindly forward your request to <Text variant="mdSemibold">support@agrix.mn</Text>
					</Text>
				</div>

				<div className="flex flex-col gap-4">
					<div className="flex flex-row gap-4">
						<DetailField label="Company name" value="Techpack LLC" />
						<DetailField label="Company registration number" value="232132" />
					</div>

					<div className="flex flex-row gap-4">
						<DetailField label="Phone number" value="Techpack LLC" />
						<DetailField label="Areas of Activity" value="232132" />
					</div>

					<div className="flex flex-col gap-4">
						<Text variant="smSemibold" color="brandMossGreen900">
							Registered Address
						</Text>
						<div className="overflow-hidden rounded-xl">
							<GoogleMaps markerActionDisabled position={[DEFAULT_LAT, DEFAULT_LNG]} />
						</div>

						<div className="flex flex-row gap-4">
							<DetailField label="City/Province" value="Techpack LLC" />
							<DetailField label="District/Sum" value="232132" />
						</div>

						<div className="flex flex-row gap-4">
							<DetailField label="City/Province" value="Techpack LLC" />
							<div className="w-full" />
						</div>
					</div>

					<Seperator />
				</div>
			</div>

			<div className="flex flex-col gap-4">
				<div className="flex flex-row justify-between gap-4">
					<Text variant="xlSemibold">Supplier type</Text>
					<Button text="Edit" icon={<Edit04Icon className="w-[22px] h-[22px]" />} type="secondary" size="sm" />
				</div>

				<div className="flex flex-row gap-4">
					<SupplierTypeOption selected title="Herder" Icon={Sheep01Icon} />
					<SupplierTypeOption title="Cooperative" Icon={Luggage04Icon} />
					<SupplierTypeOption title="Manufacturer" Icon={Factory01Icon} />
				</div>
			</div>

			<div className="flex flex-col gap-4">
				<div className="flex flex-row justify-between gap-4">
					<Text variant="xlSemibold">Product type</Text>
					<div className="flex flex-row gap-4">
						<Button text="Cancel" type="secondary" size="sm" className="w-[80px]" />
						<Button text="Save" size="sm" className="w-[80px]" />
					</div>
				</div>

				<div className="flex flex-row gap-4">
					<SupplierTypeOption selected title="Herder" detail=" " Icon={Sheep01Icon} check />
					<SupplierTypeOption title="Cooperative" detail=" " Icon={Luggage04Icon} check />
					<SupplierTypeOption title="Manufacturer" detail=" " Icon={Factory01Icon} check />
				</div>
			</div>

			<div className="flex flex-col gap-4">
				<div className="flex flex-row justify-between gap-4">
					<Text variant="xlSemibold">Delivery type</Text>
					<Button text="Edit" icon={<Edit04Icon className="w-[22px] h-[22px]" />} type="secondary" size="sm" />
				</div>

				<div className="flex flex-col gap-4">
					<div className="flex flex-row gap-4">
						<SupplierTypeOption selected title="Herder" check />
						<SupplierTypeOption title="Cooperative" check />
					</div>
					<div className="flex flex-row gap-4">
						<SupplierTypeOption title="Herder" check />
						<SupplierTypeOption title="Cooperative" check />
					</div>
				</div>
			</div>

			{/* 
			<div>
				<span className="text-black text-base font-normal font-['Inter'] leading-relaxed">
					The segment is non-editable. If changes are needed, kindly forward your request to{' '}
				</span>
				<span className="text-black text-base font-semibold font-['Inter'] leading-relaxed">isupport@agrix.mn</span>
			</div> */}

			{/* <div className="flex gap-4">
				<DetailField label="Company name" value="Techpack LLC" />
				<DetailField label="Company registration number" value="232132" />
			</div> */}

			{/* <div className="flex gap-4">
				<DetailField label="Phone number" value="+976 88118812" />
				<DetailField label="Areas of Activity" value="Farming" />
			</div>

			<span className="text-sm font-semibold text-lightgray-1000">Registered Address</span> */}

			{/* <div className="overflow-hidden h-[338px] rounded-xl">
				<Map />
			</div>

			<div className="flex gap-4">
				<DetailField label="City/Province" value="Ulaanbaatar" />
				<DetailField label="District/Sum" value="Bayangol" />
			</div> */}
			{/* 
			<div className="flex gap-4">
				<DetailField label="City/Province" value="1st Khoroo" />
				<div className="w-full" />
			</div> */}

			{/* <Seperator /> */}

			{/* <div className="w-96 text-gray-900 text-xl font-semibold font-['Inter'] leading-7">Supplier type</div>

			<div className="flex gap-4">
				<SupplierTypeOption selected title="Herder" Icon={Sheep01Icon} />

				<SupplierTypeOption title="Cooperative" Icon={Luggage04Icon} />

				<SupplierTypeOption title="Manufacturer" Icon={Factory01Icon} />
			</div>

			<div className="inline-flex justify-between">
				<div className="w-96 text-gray-900 text-xl font-semibold font-['Inter'] leading-7">Product type</div>
				<div className="inline-flex gap-4">
					<Button text="Cancel" type="secondary" size="sm" className="w-[80px]" />
					<Button text="Save" size="sm" className="w-[80px]" />
				</div>
			</div>

			<div className="flex gap-4">
				<SupplierTypeOption selected title="Herder" Icon={Sheep01Icon} />

				<SupplierTypeOption title="Cooperative" Icon={Luggage04Icon} />

				<SupplierTypeOption title="Manufacturer" Icon={Factory01Icon} />
			</div>

			<div className="inline-flex justify-between">
				<div className="w-96 text-gray-900 text-xl font-semibold font-['Inter'] leading-7">Delivery type</div>
				<Button text="Edit" icon={<Edit04Icon className="w-[22px] h-[22px]" />} type="secondary" size="sm" />
			</div>

			<div className="flex gap-4 flex-col">
				<div className="flex gap-4 flex-row">
					<SupplierTypeOption selected title="Herder" Icon={Sheep01Icon} />
					<SupplierTypeOption title="Cooperative" Icon={Luggage04Icon} />
				</div>
				<div className="flex gap-4 flex-row">
					<SupplierTypeOption title="Manufacturer" Icon={Factory01Icon} />
					<SupplierTypeOption title="Manufacturer" Icon={Factory01Icon} />
				</div>
			</div> */}
		</div>
	)
}

export default ProfileDetails
