import { PlusIcon, Search01Icon } from '@/src/assets/icons'

import Button from '@/src/components/Button'
import Text from '@/src/components/text'
import { Input } from '@nextui-org/react'
import { FC } from 'react'

type Props = {}

const Documents: FC = () => {
	return (
		<div className="flex flex-col">
			<div className="flex flex-col py-4">
				<Text variant="xlSemibold">Documents</Text>
				<Text variant="mdRegular">
					The segment is non-editable. If changes are needed, kindly forward your request to isupport@agrix.mn{' '}
					<Text variant="mdSemibold">upport@agrix.mn</Text>
				</Text>
			</div>
			<div className="flex flex-col py-4 gap-4">
				<div className="flex flex-row justify-between	">
					<Input
						className="w-[312px]"
						type="search"
						placeholder="Search Documents"
						startContent={<Search01Icon className="w-5 h-5 text-support" />}
					/>
					<Button icon={<PlusIcon className="text-white w-[22px] h-[22px]" />} text="Documents" />
				</div>
				<div className="flex flex-row">
					<div className="flex flex-1" />
				</div>
				<div className="">{/*  */}</div>
			</div>
		</div>
	)
}

export default Documents
