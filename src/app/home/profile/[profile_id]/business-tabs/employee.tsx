import { PlusIcon, Search01Icon } from '@/src/assets/icons'

import Button from '@/src/components/Button'
import Text from '@/src/components/text'
import { FC, useState } from 'react'
import { Input } from '@nextui-org/react'
import ModalAddEmployee from './ModalAddEmployee'

type Props = {}

const Employee: FC = () => {
	const [isOpen, setIsOpen] = useState(false)

	const openModal = () => setIsOpen(true)
	const closeModal = () => setIsOpen(false)

	return (
		<div className="flex flex-col">
			<div className="flex flex-col py-4">
				<Text variant="xlSemibold">Employee list</Text>
			</div>
			<div className="flex flex-col py-4 gap-4">
				<div className="flex flex-row justify-between	">
					<Input
						classNames={{ inputWrapper: 'h-10' }}
						className="w-[312px]"
						type="search"
						placeholder="Search Employee"
						startContent={<Search01Icon className="w-5 h-5 text-support" />}
					/>
					<Button size="sm" icon={<PlusIcon className="text-white w-[22px] h-[22px]" />} text="Add Employee" onClick={openModal} />
				</div>
				<div className=""></div>
			</div>
			{/* <ModalAddEmployee isOpen={isOpen} onCancel={closeModal} /> */}
		</div>
	)
}

export default Employee
