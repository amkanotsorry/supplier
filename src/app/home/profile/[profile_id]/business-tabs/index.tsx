'use client'

import { File05, FileBranch, FolderOpenIcon, UsersProfiles02Icon } from '@/src/assets/icons'
import { Tab, Tabs } from '@nextui-org/react'
import ProfileDetails from './details'
import Employee from './employee'
import Documents from './documents'
import Branch from './branch'
import Payments from './payments'
import Text from '@/src/components/text'

type Props = {}

const BusinessTabs = () => {
	return (
		<div className="h-[500px]">
			<Tabs variant={'underlined'} aria-label="Tabs variants" color="primary">
				<Tab
					key="Details"
					title={
						<div className="flex items-center space-x-2">
							<FolderOpenIcon className=" h-[18px] w-[18px]" />
							<Text variant="smMedium">Details</Text>
						</div>
					}>
					<ProfileDetails />
				</Tab>
				<Tab
					key="Employees"
					title={
						<div className="flex items-center space-x-2">
							<UsersProfiles02Icon className=" h-[18px] w-[18px]" />
							<Text variant="smSemibold">Employees</Text>
						</div>
					}>
					<Employee />
				</Tab>
				<Tab
					key="Documents"
					title={
						<div className="flex items-center space-x-2">
							<File05 className=" h-[18px] w-[18px]" />
							<Text variant="smMedium">Documents</Text>
						</div>
					}>
					<Documents />
				</Tab>
				<Tab
					key="Branch"
					title={
						<div className="flex items-center space-x-2">
							<FileBranch className=" h-[18px] w-[18px]" />
							<Text variant="smMedium">Branch</Text>
						</div>
					}>
					<Branch />
				</Tab>
				<Tab
					key="Payments"
					title={
						<div className="flex items-center space-x-2">
							<UsersProfiles02Icon className=" h-[18px] w-[18px]" />
							<Text variant="smMedium">Payments</Text>
						</div>
					}>
					<Payments />
				</Tab>
			</Tabs>
		</div>
	)
}

export default BusinessTabs
