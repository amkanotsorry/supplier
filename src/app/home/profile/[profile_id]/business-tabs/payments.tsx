import Text from '@/src/components/text'
import { FC } from 'react'
import DetailField from './detail-field'

type Props = {}

const Payments: FC = () => {
	return (
		<div className="flex flex-col">
			<div className="flex flex-col py-4">
				<Text variant="xlSemibold">Payment method</Text>
				<Text variant="mdRegular">Lorem ipsum dolor sit amet consectetur. Nibh mauris sed in ultricies ac sed ultricies amet iaculis.</Text>
			</div>
			<div className="flex flex-col gap-4">
				<div className="flex flex-row gap-4">
					<DetailField label="Дансны дугаар" value="55 23 23 23 23" />
					<DetailField label="Данс эзэмшигчийн нэр" value="Lhagva-ochir" />
				</div>
				<div className="flex flex-col gap-2">
					<div className="flex flex-col gap-2">
						<Text variant="mdMedium">Payment settings</Text>
						<Text variant="smRegular" color="brandSupportLightGrey1000">
							Lorem ipsum dolor sit amet consectetur.
						</Text>
					</div>
					<div className="flex flex-row gap-4">
						<DetailField label="Advance payment" value="40%" />
						<DetailField label="Interim payment" value="20%" />
						<DetailField label="Final payment" value="40%" />
					</div>
				</div>
			</div>
		</div>
	)
}

export default Payments
