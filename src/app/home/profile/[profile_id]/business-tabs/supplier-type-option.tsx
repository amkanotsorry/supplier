import CheckBox from '@/src/components/selector/checkbox'
import Radio from '@/src/components/selector/radio'
import Text from '@/src/components/text'
import { cn } from '@/src/lib/utils'
import React from 'react'

interface Props {
	Icon?: any
	title?: string
	detail?: string
	selected?: boolean
	disabled?: boolean
	onClick?: () => void
	radio?: boolean
	check?: boolean
}

const SupplierTypeOption: React.FC<Props> = ({ Icon, title, detail, selected = false, check, disabled = true, onClick }) => {
	return (
		<div
			onClick={() => {
				if (!disabled) onClick?.()
			}}
			className={cn(
				'flex flex-1 flex-row bg-lightgray-100 rounded-[12px] gap-4 p-4 justify-between items-center cursor-pointer',
				!disabled ? 'cursor-pointer' : 'cursor-default',
				selected || !disabled ? 'opacity-100' : 'opacity-50'
			)}>
			<div className="flex flex-row gap-4 items-center">
				{Icon && (
					<div className="flex rounded-[8px] bg-white p-2">
						<Icon />
					</div>
				)}
				<div className="flex flex-col">
					<Text variant="smMedium">{title}</Text>
					<Text variant="xsRegular" color="brandSupportLightGrey1000">
						{detail}
					</Text>
				</div>
			</div>
			{check ? <CheckBox size="LG" selected={selected} /> : <Radio size="LG" selected={selected} />}
		</div>
	)
}

export default SupplierTypeOption
