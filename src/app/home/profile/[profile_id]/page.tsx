import { Calendar03Icon } from '@/src/assets/icons'

import { Entity_Status, Review_Status } from '@/src/app/api/generated/graphql'
import Text from '@/src/components/text'
import Image from 'next/image'
import ProfileStatus from '../../_components/ProfileStatus'
import BusinessTabs from './business-tabs'
import { QuerySupplierReviewByIdType, getReviewById, getSupplierRequestDetails } from './profile-actions'
import ProfileCarousel from './profile-carousel'
import ReviewSection from './review-section'
import moment from 'moment'
import { getSupplierCategories } from '../../get-started/actions'

interface Props {
	params: {
		profile_id: string
	}
}

const ProfileDetails = async ({ params }: Props) => {
	const { entitySupplier } = await getSupplierRequestDetails(params.profile_id)
	const { supplierCategories } = await getSupplierCategories()

	let review: QuerySupplierReviewByIdType | undefined

	if (entitySupplier?.reviewId) {
		const { supplierReview } = await getReviewById(entitySupplier.reviewId)

		if (supplierReview) review = supplierReview
	}

	return (
		<div className="flex flex-col w-full items-center overflow-x-hidden">
			<ProfileCarousel images={entitySupplier?.coverImages.map((images: any) => images.location) || []} />

			<div className="max-w-[978px] w-full p-4 -translate-y-20">
				<div className="flex flex-col gap-8 pb-4">
					<div className="flex flex-col gap-4">
						<div className="w-32 h-32 rounded-lg border p-2 bg-white">
							<Image
								className="rounded-md w-full h-full object-contain"
								src={entitySupplier?.image?.location || ''}
								alt="Company Logo"
								width={112}
								height={112}
							/>
						</div>

						<div className="flex flex-col gap-2">
							<div className="inline-flex gap-4">
								<Text variant="displayxsSemibold">{entitySupplier?.entity?.name}</Text>
								<ProfileStatus
									status={entitySupplier?.status === Entity_Status.Approved ? Review_Status.Approved : review?.status || Review_Status.Pending}
								/>
							</div>

							<div className="inline-flex gap-2">
								<div className="inline-flex items-center rounded-lg bg-lightgray-100 py-1 px-2 gap-2">
									<Calendar03Icon />
									<Text variant="smMedium">Incorporated date :</Text>
									<Text variant="smMedium">{moment(entitySupplier?.entity?.entityCompany?.incorporatedDate).format('yyyy/MM/DD')}</Text>
								</div>
							</div>
						</div>
					</div>

					<div className="flex flex-col gap-2">
						<Text variant="smSemibold" className="text-lightgray-1000">
							Company description
						</Text>

						<div className="p-4 rounded-xl bg-lightgray-100">
							<Text variant="mdRegular">{entitySupplier?.entity?.entityCompany?.description}</Text>
						</div>
					</div>
				</div>

				<div className="flex flex-col pt-4 gap-4">
					{entitySupplier?.status === Entity_Status.Approved ? (
						<BusinessTabs />
					) : (
						<ReviewSection supplierCategories={supplierCategories} entitySupplier={entitySupplier} review={review} />
					)}
				</div>
			</div>
		</div>
	)
}

export default ProfileDetails
