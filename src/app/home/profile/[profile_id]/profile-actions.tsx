'use server'

import {
	GetEntitySupplierByIdDocument,
	GetEntitySupplierByIdQuery,
	GetSupplierReviewByIdDocument,
	GetSupplierReviewByIdQuery,
	SubmitSupplierReviewDocument,
	SubmitSupplierReviewMutation,
	Supplier_Markets,
	UpdateEntityReviewInput,
	UpdateEntitySupplierDocument,
	UpdateEntitySupplierInput,
	UpdateEntitySupplierMutation,
	UpdateSupplierReviewDocument,
	UpdateSupplierReviewMutation
} from '@/src/app/api/generated/graphql'
import { getClient } from '@/src/app/api/graphql/client'

const client = getClient()

export type QueryEntitySupplierByIdType = Extract<GetEntitySupplierByIdQuery['entitySupplier'], any>

export const getSupplierRequestDetails = async (
	id: string
): Promise<{
	entitySupplier: QueryEntitySupplierByIdType | null
	error: string | null
}> => {
	try {
		const { data } = await client.query({
			query: GetEntitySupplierByIdDocument,
			variables: {
				entitySupplierId: id
			}
		})

		return { entitySupplier: data.entitySupplier, error: null }
	} catch (error) {
		return { entitySupplier: null, error: JSON.stringify(error, null, 2) }
	}
}

export type QuerySupplierReviewByIdType = Extract<GetSupplierReviewByIdQuery['supplierReview'], any>

export const getReviewById = async (
	reviewId: string
): Promise<{
	supplierReview: QuerySupplierReviewByIdType | null
	error: string | null
}> => {
	try {
		const { data } = await client.query({
			query: GetSupplierReviewByIdDocument,
			variables: {
				supplierReviewId: reviewId
			},
			fetchPolicy: 'network-only'
		})

		return { supplierReview: data.supplierReview, error: null }
	} catch (error) {
		return { supplierReview: null, error: JSON.stringify(error, null, 2) }
	}
}

export type MutationEntitySupplierType = Extract<UpdateEntitySupplierMutation['updateEntitySupplier'], any>

export const updateEntitySupplier = async (
	supplierId: string,
	updateEntitySupplierInput: UpdateEntitySupplierInput
): Promise<{
	updateEntitySupplier?: MutationEntitySupplierType | null
	error: string | null
}> => {
	try {
		const { data } = await client.mutate({
			mutation: UpdateEntitySupplierDocument,
			variables: {
				updateEntitySupplierId: supplierId,
				updateEntitySupplierInput
			}
		})

		return { updateEntitySupplier: data?.updateEntitySupplier, error: null }
	} catch (error) {
		return { updateEntitySupplier: null, error: JSON.stringify(error, null, 2) }
	}
}

export type QuerySubmitReviewType = Extract<UpdateSupplierReviewMutation['updateSupplierReview'], any>

export const submitEntityReview = async (
	id: string,
	updateSupplierReviewInput: UpdateEntityReviewInput
): Promise<{
	updateSupplierReview: QuerySubmitReviewType | null | undefined
	error: string | null
}> => {
	try {
		const { data } = await client.mutate({
			mutation: UpdateSupplierReviewDocument,
			variables: {
				updateSupplierReviewId: id,
				updateSupplierReviewInput
			}
		})

		return { updateSupplierReview: data?.updateSupplierReview, error: null }
	} catch (error) {
		return {
			updateSupplierReview: null,
			error: JSON.stringify(error, null, 2)
		}
	}
}

export type QuerySubmitSupplierReviewType = Extract<SubmitSupplierReviewMutation['submitSupplier'], any>

export const submitSupplierReview = async (
	supplierId: string
): Promise<{
	submitSupplier: QuerySubmitSupplierReviewType | null | undefined
	error: string | null
}> => {
	try {
		const { data } = await client.mutate({
			mutation: SubmitSupplierReviewDocument,
			variables: {
				supplierId,
				markets: Supplier_Markets.Domestic
			}
		})

		return { submitSupplier: data?.submitSupplier, error: null }
	} catch (error) {
		return {
			submitSupplier: null,
			error: JSON.stringify(error, null, 2)
		}
	}
}
