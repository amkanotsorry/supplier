'use client'

import useEmblaCarousel from 'embla-carousel-react'
import Image from 'next/image'
import React from 'react'

interface Props {
	images: string[]
}

const ProfileCarousel: React.FC<Props> = ({ images }) => {
	const [emblaRef] = useEmblaCarousel()

	return (
		<div className="embla h-[228px] w-full shrink-0" ref={emblaRef}>
			<div className="embla__container flex h-[228px]">
				{images.map((image, index) => (
					<div key={index} className="embla__slide h-[228px]" style={{ flex: '0 0 100%' }}>
						<Image className="h-[228px] w-full object-contain" src={image} width={1000} height={228} alt="img" />
					</div>
				))}
			</div>
		</div>
	)
}

export default ProfileCarousel
