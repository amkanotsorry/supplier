'use client'

import { Review_Status, UpdateEntitySupplierInput } from '@/src/app/api/generated/graphql'
import Seperator from '@/src/components/seperator'
import { toast } from '@/src/components/ui/use-toast'
import { Button, Spinner } from '@nextui-org/react'
import { useRouter } from 'next/navigation'
import React, { useState } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { QuerySupplierCategory } from '../../../get-started/actions'
import { QueryEntitySupplierByIdType, QuerySupplierReviewByIdType, updateEntitySupplier } from '../profile-actions'
import DeliveryTypeSection from '../section-list/delivery-type'
import ProductInfoSection from '../section-list/product-info'
import SpecialCertificationSection from '../section-list/special-certification'
import StockLocationSection from '../section-list/stock-location'
import SupplierTypeSection from '../section-list/supplier-type'
import UserInfoSection from '../section-list/user-info'

interface Props {
	review?: QuerySupplierReviewByIdType
	entitySupplier: QueryEntitySupplierByIdType | null
	supplierCategories: QuerySupplierCategory[]
}

const ReviewSection: React.FC<Props> = ({ review, entitySupplier, supplierCategories }) => {
	const [loading, setLoading] = useState(false)
	const router = useRouter()
	const form = useForm<UpdateEntitySupplierInput>({
		defaultValues: {}
	})

	if (!review) return <div>Review has not been found. Please try again</div>

	const onClickResend = async (data: UpdateEntitySupplierInput) => {
		setLoading(true)
		const { error } = await updateEntitySupplier(entitySupplier!._id, data)
		setLoading(false)

		if (error) {
			console.log('err', error)
			return
		}

		router.push('/home/get-started')

		toast({
			title: 'Resend successful'
		})
	}

	return (
		<FormProvider {...form}>
			<div className="flex flex-col py-4 gap-4">
				<UserInfoSection entitySupplier={entitySupplier} review={review} />

				<SupplierTypeSection entitySupplier={entitySupplier} review={review} supplierCategories={supplierCategories} />

				<ProductInfoSection entitySupplier={entitySupplier} review={review} />

				<DeliveryTypeSection entitySupplier={entitySupplier} review={review} />

				<StockLocationSection entitySupplier={entitySupplier} review={review} />

				<SpecialCertificationSection entitySupplier={entitySupplier} review={review} />

				<Seperator />

				{review.status === Review_Status.Declined && (
					<Button disabled={loading} onClick={form.handleSubmit(onClickResend)} className="self-end" color="primary">
						{loading ? <Spinner color="white" size="sm" /> : 'Resend'}
					</Button>
				)}
			</div>
		</FormProvider>
	)
}

export default ReviewSection
