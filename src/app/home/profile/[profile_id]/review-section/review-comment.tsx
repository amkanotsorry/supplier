import { DotVerticalIcon } from '@/src/assets/icons'
import Image from 'next/image'
import React from 'react'
import ProfileImage from '@/src/assets/images/avatar.png'

interface Props {
	comment: string
}

const ReviewComment: React.FC<Props> = ({ comment }) => {
	return (
		<div className="flex flex-col gap-4 bg-lightgray-100 px-4 py-2 rounded-xl">
			<div className="flex items-center justify-between">
				<div className="flex items-center gap-2">
					<Image className="w-8 h-8" src={ProfileImage} width={32} height={32} alt="avatar" />
					<div className="text-sm font-medium">Lkhagvaochir Od</div>
				</div>

				<DotVerticalIcon className="cursor-pointer" />
			</div>

			<div>{comment}</div>
		</div>
	)
}

export default ReviewComment
