import { UpdateEntityReviewInput } from '@/src/app/api/generated/graphql'
import { create } from 'zustand'

type Action = {
	update_delivery_type_comment: (data: UpdateEntityReviewInput['delivery_type_comment']) => void
	update_delivery_type_status: (data: UpdateEntityReviewInput['delivery_type_status']) => void
	update_entity_info_comment: (data: UpdateEntityReviewInput['entity_info_comment']) => void
	update_entity_info_status: (data: UpdateEntityReviewInput['delivery_type_status']) => void
	update_images_comment: (data: UpdateEntityReviewInput['images_comment']) => void
	update_images_status: (data: UpdateEntityReviewInput['images_status']) => void
	update_payment_method_comment: (data: UpdateEntityReviewInput['payment_method_comment']) => void
	update_payment_method_status: (data: UpdateEntityReviewInput['payment_method_status']) => void
	update_product_category_comment: (data: UpdateEntityReviewInput['product_category_comment']) => void
	update_product_category_status: (data: UpdateEntityReviewInput['product_category_status']) => void
	update_stock_location_comment: (data: UpdateEntityReviewInput['stock_location_comment']) => void
	update_stock_location_status: (data: UpdateEntityReviewInput['stock_location_status']) => void
	update_supplier_type_comment: (data: UpdateEntityReviewInput['supplier_type_comment']) => void
	update_supplier_type_status: (data: UpdateEntityReviewInput['supplier_type_status']) => void
	update_uploaded_document_comment: (data: UpdateEntityReviewInput['uploaded_document_comment']) => void
	update_uploaded_document_status: (data: UpdateEntityReviewInput['uploaded_document_status']) => void
}

export const useReviewStore = create<UpdateEntityReviewInput & Action>(set => ({
	update_delivery_type_comment: data => set(() => ({ delivery_type_comment: data })),
	update_delivery_type_status: data => set(() => ({ delivery_type_status: data })),
	update_entity_info_comment: data => set(() => ({ entity_info_comment: data })),
	update_entity_info_status: data => set(() => ({ entity_info_status: data })),
	update_images_comment: data => set(() => ({ images_comment: data })),
	update_images_status: data => set(() => ({ images_status: data })),
	update_payment_method_comment: data => set(() => ({ payment_method_comment: data })),
	update_payment_method_status: data => set(() => ({ payment_method_status: data })),
	update_product_category_comment: data => set(() => ({ product_category_comment: data })),
	update_product_category_status: data => set(() => ({ product_category_status: data })),
	update_stock_location_comment: data => set(() => ({ stock_location_comment: data })),
	update_stock_location_status: data => set(() => ({ stock_location_status: data })),
	update_supplier_type_comment: data => set(() => ({ supplier_type_comment: data })),
	update_supplier_type_status: data => set(() => ({ supplier_type_status: data })),
	update_uploaded_document_comment: data => set(() => ({ uploaded_document_comment: data })),
	update_uploaded_document_status: data => set(() => ({ uploaded_document_status: data }))
}))
