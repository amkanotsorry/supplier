'use client'

import { Delivery_Type, Review_Status, UpdateEntitySupplierInput } from '@/src/app/api/generated/graphql'
import Seperator from '@/src/components/seperator'
import React, { useState } from 'react'
import DeliveryTypeOption from '../business-tabs/delivery-type-option'
import { QueryEntitySupplierByIdType, QuerySupplierReviewByIdType } from '../profile-actions'
import ReviewComment from '../review-section/review-comment'
import { useFormContext } from 'react-hook-form'
import { Button } from '@nextui-org/react'
import { Edit, Save } from 'lucide-react'

interface Props {
	review: QuerySupplierReviewByIdType
	entitySupplier: QueryEntitySupplierByIdType | null
}

const DeliveryTypeSection: React.FC<Props> = ({ review, entitySupplier }) => {
	const { getValues, setValue } = useFormContext<UpdateEntitySupplierInput>()
	const [editting, setEditting] = useState(false)
	const [selectedType, setSelectedType] = useState<Delivery_Type>(
		getValues('deliveryDetail.deliveryType') || entitySupplier?.deliveryDetail?.deliveryType
	)

	const onSave = () => {
		setValue('deliveryDetail.deliveryType', selectedType)
		setEditting(false)
	}

	if (review.delivery_type_status === Review_Status.Approved) return <></>

	return (
		<>
			<Seperator />

			<div className="flex flex-col gap-2">
				<div className="flex justify-between">
					<span className="text-xl font-semibold leading-7">Delivery Type</span>
					{editting ? (
						<Button onClick={onSave} className="border-[1px]" color="primary" size="sm" startContent={<Save width={14} height={14} />}>
							Save
						</Button>
					) : (
						<Button
							onClick={() => setEditting(true)}
							className="border-[1px]"
							color="primary"
							size="sm"
							variant="bordered"
							startContent={<Edit width={14} height={14} />}>
							Edit
						</Button>
					)}
				</div>
			</div>

			<div className="flex gap-4">
				<DeliveryTypeOption
					onClick={() => setSelectedType(Delivery_Type.Full)}
					disabled={!editting}
					selected={selectedType === Delivery_Type.Full}
					title="Full Delivery"
				/>

				<DeliveryTypeOption
					onClick={() => setSelectedType(Delivery_Type.ToLocalPort)}
					disabled={!editting}
					selected={selectedType === Delivery_Type.ToLocalPort}
					title="Delivery to Local port"
				/>
			</div>

			<div className="flex gap-4">
				<DeliveryTypeOption
					onClick={() => setSelectedType(Delivery_Type.ToAimagCollectionPoint)}
					disabled={!editting}
					selected={selectedType === Delivery_Type.ToAimagCollectionPoint}
					title="Delivery to Aimak collection point"
				/>

				<DeliveryTypeOption
					onClick={() => setSelectedType(Delivery_Type.ThirdParty)}
					disabled={!editting}
					selected={selectedType === Delivery_Type.ThirdParty}
					title="Third Party Delivery"
				/>
			</div>

			{review.status === Review_Status.Declined && (
				<>
					<Seperator />
					<ReviewComment comment={review.delivery_type_comment || ''} />
				</>
			)}
		</>
	)
}

export default DeliveryTypeSection
