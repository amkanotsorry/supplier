'use client'

import { Review_Status } from '@/src/app/api/generated/graphql'
import { GolomtBankIcon } from '@/src/assets/icons'
import PercentInput from '@/src/components/percent-input'
import Seperator from '@/src/components/seperator'
import React from 'react'
import { QueryEntitySupplierByIdType, QuerySupplierReviewByIdType } from '../profile-actions'
import ReviewComment from '../review-section/review-comment'

interface Props {
	review: QuerySupplierReviewByIdType
	entitySupplier: QueryEntitySupplierByIdType | null
}

const PaymentMethodSection: React.FC<Props> = ({ review, entitySupplier }) => {
	if (review.payment_method_status === Review_Status.Approved) return <></>

	return (
		<>
			<Seperator />

			<div className="flex flex-col gap-2">
				<span className="text-xl font-semibold leading-7">Payment Method</span>
			</div>

			<div className="flex gap-4 w-full">
				<div className="flex flex-row gap-4 w-full">
					<div
						className={`flex flex-1 text-sm h-14 px-2 cursor-pointer items-center bg-lightgray-100 
				hover:bg-lightgray-200 rounded-xl shadow-sm transition-background duration-150`}>
						<GolomtBankIcon className={'w-10 h-10 mr-3'} />

						<div className="flex flex-col">
							<span className="text-xs text-lightgray-800">Дансны дугаар</span>
							55 23 23 23 23
						</div>
					</div>

					<div
						className={`flex flex-1 text-sm h-14 px-2 cursor-pointer items-center bg-lightgray-100 
				hover:bg-lightgray-200 rounded-xl shadow-sm transition-background duration-150`}>
						<div className="flex flex-col">
							<span className="text-xs text-lightgray-800">Данс эзэмшигчийн нэр</span>
							Lhagva-ochir
						</div>
					</div>
				</div>
			</div>

			<div className="flex flex-col gap-2">
				<span className="text-lg font-medium leading-7">Payment settings</span>
			</div>

			<div className="flex flex-1 gap-4">
				<PercentInput readOnly defaultValue={'40'} label="Advance payment" />
				<PercentInput readOnly defaultValue={'20'} label="Interim payment" />
				<PercentInput readOnly defaultValue={'40'} label="Final payment" />
			</div>

			{review.status === Review_Status.Declined && (
				<>
					<Seperator />
					<ReviewComment comment={review.payment_method_comment || ''} />
				</>
			)}
		</>
	)
}

export default PaymentMethodSection
