'use client'

import { Review_Status, UpdateEntitySupplierInput } from '@/src/app/api/generated/graphql'
import { Factory01Icon, Luggage04Icon, Sheep01Icon } from '@/src/assets/icons'
import Seperator from '@/src/components/seperator'
import React, { useState } from 'react'
import SupplierTypeOption from '../business-tabs/supplier-type-option'
import { QueryEntitySupplierByIdType, QuerySupplierReviewByIdType } from '../profile-actions'
import ReviewComment from '../review-section/review-comment'
import { useFormContext } from 'react-hook-form'
import { Product_Status } from '@/src/app/apiProductMs/generated/graphql'
import { Button } from '@nextui-org/react'
import { Edit, Save } from 'lucide-react'

interface Props {
	review: QuerySupplierReviewByIdType
	entitySupplier: QueryEntitySupplierByIdType | null
}

const ProductInfoSection: React.FC<Props> = ({ review, entitySupplier }) => {
	const { getValues, setValue } = useFormContext<UpdateEntitySupplierInput>()
	const [editting, setEditting] = useState(false)
	// const [selectedTypes, setSelectedTypes] = useState<Delivery_Type>(getValues('productTypeIds') || entitySupplier?.deliveryDetail?.deliveryType)

	// const onSave = () => {
	// 	setValue('deliveryDetail.deliveryType', selectedType)
	// 	setEditting(false)
	// }

	if (review.product_category_status === Review_Status.Approved) return <></>

	return (
		<>
			<Seperator />

			<div className="flex flex-col gap-2">
				<div className="flex justify-between">
					<span className="text-xl font-semibold leading-7">Product Info</span>
					{editting ? (
						<Button onClick={() => {}} className="border-[1px]" color="primary" size="sm" startContent={<Save width={14} height={14} />}>
							Save
						</Button>
					) : (
						<Button
							onClick={() => setEditting(true)}
							className="border-[1px]"
							color="primary"
							size="sm"
							variant="bordered"
							startContent={<Edit width={14} height={14} />}>
							Edit
						</Button>
					)}
				</div>
			</div>

			<div className="flex gap-4">
				<SupplierTypeOption
					disabled={!editting}
					selected={!!entitySupplier?.productTypes?.find(type => type.name === 'livestock')}
					title="Livestock"
					Icon={Sheep01Icon}
				/>

				<SupplierTypeOption
					disabled={!editting}
					selected={!!entitySupplier?.productTypes?.find(type => type.name === 'raw material')}
					title="Raw material"
					Icon={Luggage04Icon}
				/>

				<SupplierTypeOption
					disabled={!editting}
					selected={!!entitySupplier?.productTypes?.find(type => type.name === 'final product')}
					title="Final product"
					Icon={Factory01Icon}
				/>
			</div>

			{review.status === Review_Status.Declined && (
				<>
					<Seperator />
					<ReviewComment comment={review.product_category_comment || ''} />
				</>
			)}
		</>
	)
}

export default ProductInfoSection
