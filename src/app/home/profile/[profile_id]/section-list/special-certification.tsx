'use client'

import { Review_Status } from '@/src/app/api/generated/graphql'
import Seperator from '@/src/components/seperator'
import React from 'react'
import { QueryEntitySupplierByIdType, QuerySupplierReviewByIdType } from '../profile-actions'
import ReviewComment from '../review-section/review-comment'
import { Table, TableBody, TableCell, TableColumn, TableHeader, TableRow } from '@nextui-org/react'
import Link from 'next/link'
import moment from 'moment'

type CertificationType = Extract<QueryEntitySupplierByIdType['certifications'], any[]>[number]

interface Props {
	review: QuerySupplierReviewByIdType
	entitySupplier: QueryEntitySupplierByIdType | null
}

const columns: {
	key: keyof CertificationType
	label: string
}[] = [
	{
		key: 'organization',
		label: 'Owning org'
	},
	{
		key: 'name',
		label: 'Certifcation'
	},
	{
		key: 'issueDate',
		label: 'Issue Date'
	},
	{
		key: 'expiryDate',
		label: 'Expiry Date'
	},
	{
		key: 'attachments',
		label: 'Attachment'
	}
]

const SpecialCertificationSection: React.FC<Props> = ({ review, entitySupplier }) => {
	if (review.delivery_type_status === Review_Status.Approved) return <></>

	const renderCell = React.useCallback((data: CertificationType, columnKey: keyof CertificationType) => {
		const cellValue = data[columnKey]

		switch (columnKey) {
			case 'attachments':
				return (
					<Link href={cellValue[0].location} target="_blank" rel="noopener noreferrer">
						<span className="text-sm text-info underline">{cellValue[0].location.split('_')[1]}</span>
					</Link>
				)
			case 'issueDate':
			case 'expiryDate':
				return moment(cellValue).format('yyyy/MM/DD')

			default:
				return cellValue
		}
	}, [])

	return (
		<>
			<Seperator />

			<div className="flex flex-col gap-2">
				<span className="text-lg font-medium leading-7">Special certification</span>
			</div>

			<Table aria-label="Example table with dynamic content">
				<TableHeader columns={columns}>{column => <TableColumn key={column.key}>{column.label}</TableColumn>}</TableHeader>
				<TableBody items={entitySupplier?.certifications || []}>
					{item => <TableRow key={item._id}>{columnKey => <TableCell>{renderCell(item, columnKey as keyof CertificationType)}</TableCell>}</TableRow>}
				</TableBody>
			</Table>

			{review.status === Review_Status.Declined && (
				<>
					<Seperator />
					<ReviewComment comment={review.uploaded_document_comment || ''} />
				</>
			)}
		</>
	)
}

export default SpecialCertificationSection
