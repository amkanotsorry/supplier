'use client'

import { Review_Status, UpdateEntitySupplierInput } from '@/src/app/api/generated/graphql'
import Map from '@/src/components/map'
import Seperator from '@/src/components/seperator'
import { Button, Textarea } from '@nextui-org/react'
import { Edit } from 'lucide-react'
import React, { useState } from 'react'
import { useFormContext } from 'react-hook-form'
import DetailField from '../business-tabs/detail-field'
import { QueryEntitySupplierByIdType, QuerySupplierReviewByIdType } from '../profile-actions'
import ReviewComment from '../review-section/review-comment'
import StockLocationModal from '../../../../../components/modals/location-modals/stock-location-modal'
import GoogleMap from '@/src/components/map-position-selector/google-maps'

interface Props {
	review: QuerySupplierReviewByIdType
	entitySupplier: QueryEntitySupplierByIdType | null
}

const StockLocationSection: React.FC<Props> = ({ review, entitySupplier }) => {
	const { getValues, setValue } = useFormContext<UpdateEntitySupplierInput>()
	const [stockLocation, setStockLocation] = useState(
		getValues('stockLocations') ? getValues('stockLocations')![0] : entitySupplier?.stockLocations?.[0]
	)

	const onSave = (data: any) => {
		setValue('stockLocations', [data])
		setStockLocation(data)
	}

	if (review.stock_location_status === Review_Status.Approved) return <></>

	return (
		<>
			<Seperator />

			<div className="flex flex-col gap-2">
				<div className="flex justify-between">
					<span className="text-lg font-medium leading-7">Stock Location</span>

					{/* <StockLocationModal
						initialData={entitySupplier?.stockLocations?.[0]}
						onOk={onSave}
						triggerComponent={
							<Button className="border-[1px]" color="primary" size="sm" variant="bordered" startContent={<Edit width={14} height={14} />}>
								Edit
							</Button>
						}
					/> */}
				</div>
			</div>

			<div className="overflow-hidden h-[338px] rounded-xl">
				<GoogleMap
					className="h-[338px]"
					position={[stockLocation?.address?.coordinates.latitude || 47.9134256, stockLocation?.address?.coordinates.longtitude || 106.9156777]}
				/>
			</div>

			<div className="flex gap-4">
				<DetailField label="Branch name" value={stockLocation?.branchName || ''} />

				<DetailField label="Phone number" value={stockLocation?.phoneNumber || ''} />
			</div>

			<div className="flex gap-4">
				<DetailField label="City/Province" value={stockLocation?.address?.cityProvince.name || ''} />

				<DetailField label="District/Sum" value={stockLocation?.address?.duuregSoum.name || ''} />
			</div>

			<div className="flex gap-4">
				<DetailField label="Khoroo/Bag" value={stockLocation?.address?.khorooBag.name || ''} />

				<DetailField label="Phone Number" value={stockLocation?.address?.khorooBag.name || ''} />
			</div>

			<div className="flex flex-col gap-1 w-full">
				<span className="text-sm font-semibold text-lightgray-1000">Detailed Address</span>

				<Textarea value={stockLocation?.address?.detail} readOnly placeholder="0/200" />
			</div>

			{review.status === Review_Status.Declined && (
				<>
					<Seperator />
					<ReviewComment comment={review.stock_location_comment || ''} />
				</>
			)}
		</>
	)
}

export default StockLocationSection
