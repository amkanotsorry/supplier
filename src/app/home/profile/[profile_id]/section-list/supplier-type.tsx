'use client'

import { Review_Status, UpdateEntitySupplierInput } from '@/src/app/api/generated/graphql'
import { Factory01Icon, Luggage04Icon, Sheep01Icon } from '@/src/assets/icons'
import Seperator from '@/src/components/seperator'
import React, { useState } from 'react'
import SupplierTypeOption from '../business-tabs/supplier-type-option'
import { QueryEntitySupplierByIdType, QuerySupplierReviewByIdType } from '../profile-actions'
import ReviewComment from '../review-section/review-comment'
import { useFormContext } from 'react-hook-form'
import { Button } from '@nextui-org/react'
import { Edit, Save } from 'lucide-react'
import { QuerySupplierCategory } from '../../../get-started/actions'

interface Props {
	review: QuerySupplierReviewByIdType
	entitySupplier: QueryEntitySupplierByIdType | null
	supplierCategories: QuerySupplierCategory[]
}

const SupplierTypeSection: React.FC<Props> = ({ review, entitySupplier, supplierCategories }) => {
	const { getValues, setValue } = useFormContext<UpdateEntitySupplierInput>()
	const [editting, setEditting] = useState(false)
	const [selectedType, setSelectedType] = useState<string | undefined>(getValues('supplierTypeId') || entitySupplier?.supplierType?._id)

	const onSave = () => {
		setValue('supplierTypeId', selectedType)
		setEditting(false)
	}

	if (review.supplier_type_status === Review_Status.Approved) return <></>

	return (
		<>
			<Seperator />

			<div className="flex flex-col gap-2">
				<div className="flex justify-between">
					<span className="text-xl font-semibold leading-7">Supplier Type</span>
					{editting ? (
						<Button onClick={onSave} className="border-[1px]" color="primary" size="sm" startContent={<Save width={14} height={14} />}>
							Save
						</Button>
					) : (
						<Button
							onClick={() => setEditting(true)}
							className="border-[1px]"
							color="primary"
							size="sm"
							variant="bordered"
							startContent={<Edit width={14} height={14} />}>
							Edit
						</Button>
					)}
				</div>
			</div>

			<div className="flex gap-4">
				{supplierCategories.map(supplierCategory => {
					return (
						<SupplierTypeOption
							onClick={() => setSelectedType(supplierCategory._id)}
							disabled={!editting}
							selected={selectedType === supplierCategory._id}
							title={supplierCategory.name}
							Icon={Sheep01Icon}
						/>
					)
				})}
			</div>

			{review.status === Review_Status.Declined && (
				<>
					<Seperator />
					<ReviewComment comment={review.supplier_type_comment || ''} />
				</>
			)}
		</>
	)
}

export default SupplierTypeSection
