'use client'

import { Review_Status, UpdateEntitySupplierInput } from '@/src/app/api/generated/graphql'
import { LinkExternalIcon, PdfFileIcon } from '@/src/assets/icons'
import Map from '@/src/components/map'
import Seperator from '@/src/components/seperator'
import React, { useState } from 'react'
import DetailField from '../business-tabs/detail-field'
import { QueryEntitySupplierByIdType, QuerySupplierReviewByIdType } from '../profile-actions'
import ReviewComment from '../review-section/review-comment'
import Link from 'next/link'
import StockLocationModal from '../../../../../components/modals/location-modals/stock-location-modal'
import { Button } from '@nextui-org/react'
import { Edit } from 'lucide-react'
import { useFormContext } from 'react-hook-form'
import AddressModal from '../../../../../components/modals/location-modals/address-modal'
import GoogleMap from '@/src/components/map-position-selector/google-maps'

interface Props {
	review: QuerySupplierReviewByIdType
	entitySupplier: QueryEntitySupplierByIdType | null
}

const UserInfoSection: React.FC<Props> = ({ review, entitySupplier }) => {
	const { getValues, setValue } = useFormContext<UpdateEntitySupplierInput>()
	const [stockLocation, setStockLocation] = useState(
		getValues('stockLocations') ? getValues('stockLocations')![0] : entitySupplier?.stockLocations?.[0]
	)

	const onSave = (data: any) => {
		setValue('stockLocations', [data])
		setStockLocation(data)
	}

	if (review.entity_info_status === Review_Status.Approved) return <></>

	const stateRegistrationOrderFile = entitySupplier?.entitySupplierCompany?.stateRegistrationOrder?.location

	return (
		<>
			<div className="flex flex-col gap-2">
				<span className="text-xl font-semibold leading-7">User info</span>
			</div>

			<div className="flex gap-4">
				<DetailField label="First Name" value={entitySupplier?.entity?.createdUser?.firstName || ''} />

				<DetailField label="Last Name" value={entitySupplier?.entity?.createdUser?.lastName || ''} />
			</div>

			<div className="flex gap-4">
				<DetailField label="Phone number" value={entitySupplier?.entity?.createdUser?.phoneNumber || ''} />

				<DetailField label="Email address" value={entitySupplier?.entity?.createdUser?.email || ''} />
			</div>

			<Seperator />

			<div className="flex flex-col gap-2">
				<span className="text-xl font-semibold leading-7">Company profile</span>
			</div>

			<div className="flex gap-4">
				<DetailField label="Company name" value={entitySupplier?.entity?.name || ''} />

				<DetailField label="Entity type" className="capitalize" value={entitySupplier?.entity?.type || ''} />
			</div>

			<div className="flex gap-4">
				<DetailField label="Company Register Number" value={entitySupplier?.entity?.registrationNumber || ''} />

				<DetailField label="Phone number" value={entitySupplier?.entity?.phoneNumber || ''} />
			</div>

			<div className="flex gap-4">
				<DetailField label="Areas of Activity" value={entitySupplier?.entity?.areasOfActivity || ''} />

				<div className="w-full" />
			</div>

			<div className="flex flex-col gap-4">
				<span className="text-sm font-semibold text-lightgray-1000">State registration certification</span>

				<div className="flex gap-4 h-[76px] items-center p-4 bg-lightgray-100 rounded-2xl">
					<div className="w-10 h-10 p-2 bg-white">
						<PdfFileIcon />
					</div>

					{stateRegistrationOrderFile ? (
						<>
							<div className="flex flex-col gap-1 flex-1">
								<Link href={stateRegistrationOrderFile} target="_blank" rel="noopener noreferrer">
									<span className="text-sm font-medium">{stateRegistrationOrderFile.split('_')[1]}</span>
								</Link>
								<span className="text-xs color-lightgray-1000">3.14 MB</span>
							</div>

							<Link href={stateRegistrationOrderFile} target="_blank" rel="noopener noreferrer">
								<LinkExternalIcon />
							</Link>
						</>
					) : (
						<div>No file attached.</div>
					)}
				</div>
			</div>

			<div className="flex justify-between items-center">
				<span className="text-sm font-semibold text-lightgray-1000">Registered Address</span>
				<AddressModal
					initialData={entitySupplier?.entity?.address}
					onOk={onSave}
					triggerComponent={
						<Button className="border-[1px]" color="primary" size="sm" variant="bordered" startContent={<Edit width={14} height={14} />}>
							Edit
						</Button>
					}
				/>
			</div>

			<div className="overflow-hidden h-[338px] rounded-xl">
				<GoogleMap
					className="h-[338px]"
					position={[
						entitySupplier?.entity?.address.coordinates.latitude || 47.9134256,
						entitySupplier?.entity?.address.coordinates.longtitude || 106.9156777
					]}
				/>
			</div>

			<div className="flex gap-4">
				<DetailField label="City/Province" value={entitySupplier?.entity?.address.cityProvince.name || ''} />

				<DetailField label="District/Sum" value={entitySupplier?.entity?.address.duuregSoum.name || ''} />
			</div>

			<div className="flex gap-4">
				<DetailField label="Khoroo/Bag" value={entitySupplier?.entity?.address.khorooBag.name || ''} />

				<DetailField label="Khoroo/Bag" value={entitySupplier?.entity?.address.detail || ''} />
			</div>

			{review.status === Review_Status.Declined && (
				<>
					<Seperator />
					<ReviewComment comment={review.entity_info_comment || ''} />
				</>
			)}
		</>
	)
}

export default UserInfoSection
