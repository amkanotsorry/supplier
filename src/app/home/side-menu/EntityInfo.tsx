import { ArrowRefreshIcon, Camera01Icon } from '@/src/assets/icons'
import Radio from '@/src/components/selector/radio'
import Text from '@/src/components/text'
import { Dropdown, DropdownItem, DropdownMenu, DropdownSection, DropdownTrigger } from '@nextui-org/react'
import { AddNoteBulkIcon } from '@nextui-org/shared-icons'
import React from 'react'
import { GetEntityByIdQuery } from '../../api/generated/graphql'
import Link from 'next/link'

const iconClasses = 'text-2xl text-default-500 pointer-events-none flex-shrink-0'
interface Props {
	open: boolean
	selected: boolean
	entity: GetEntityByIdQuery['entity']
}

const EntityInfo: React.FC<Props> = ({ open, selected, entity }) => {
	return (
		<div className="px-4 py-2">
			<Dropdown className="shadow-xl" placement="bottom-end">
				<div className={`flex items-center justify-between overflow-hidden rounded-xl p-2 ${selected && 'bg-primary-foreground'}`}>
					<div className="flex items-center gap-2 h-9">
						<div className={`${open && 'px-[10px]'}`}>
							<Camera01Icon className="w-[18px] h-[18px] text-darkGrey-600" />
						</div>

						{entity?.entitySupplierId ? (
							<Link
								href={`/home/profile/${entity.entitySupplierId}`}
								className={`${!open && 'hidden opacity-0'} ${
									selected && 'text-primary'
								} cursor-pointer whitespace-nowrap transition duration-300  font-semibold`}>
								{entity?.name || 'No entity'}
							</Link>
						) : (
							<div
								className={`${!open && 'hidden opacity-0'} ${selected && 'text-primary'} whitespace-nowrap transition duration-300  font-semibold`}>
								{entity?.name || 'No entity'}
							</div>
						)}
					</div>

					{open && (
						<DropdownTrigger>
							<button>
								<ArrowRefreshIcon className={`${selected && 'text-primary'} h-[20px] w-[20px]`} />
							</button>
						</DropdownTrigger>
					)}
				</div>
				<DropdownMenu closeOnSelect aria-label="Actions" color="default" variant="flat">
					<DropdownSection>
						<DropdownItem
							startContent={
								<div className="flex flex-row gap-3 items-center w-[230px] h-[40px]">
									<AddNoteBulkIcon className={iconClasses} />
									<Text variant="smSemibold">{entity?.name}</Text>
									<div className="flex flex-1" />
									<Radio selected />
								</div>
							}
						/>
					</DropdownSection>
				</DropdownMenu>
			</Dropdown>
		</div>
	)
}

export default EntityInfo
