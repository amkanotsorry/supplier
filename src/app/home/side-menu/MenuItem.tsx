import { ActionTooltip } from '@/src/components/ui/action-tooltip'
import Link from 'next/link'
import React from 'react'

interface Props {
	open: boolean
	title: string
	selected: boolean
	Icon: any
	url: string
}

const MenuItem: React.FC<Props> = ({ open, title, selected, Icon, url }) => {
	return (
		<ActionTooltip disabled={open} label={title} side="right" align="start">
			<Link href={`/home/${url}`} className={`cursor-pointer flex items-center p-2 gap-3 rounded-xl ${selected && 'bg-primary-foreground'}`}>
				<div>
					<Icon className={`${selected ? 'text-primary' : 'text-black'} h-[20px] w-[20px]`} />
				</div>

				<div
					className={`${
						selected ? 'text-primary font-semibold' : 'font-medium'
					} text-xs whitespace-nowrap leading-5 transition ease-out duration-300 overflow-hidden ${
						open ? 'w-[217px] ml-2 opacity-1' : 'w-[0px] ml-0 opacity-0'
					}`}>
					{title}
				</div>
			</Link>
		</ActionTooltip>
	)
}
export default MenuItem
