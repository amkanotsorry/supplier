'use client'

import { ActionTooltip } from '@/src/components/ui/action-tooltip'
import { CommandDialog, CommandEmpty, CommandInput, CommandList } from '@/src/components/ui/command'
import React, { useEffect, useState } from 'react'

interface Props {
	open: boolean
}

function determineOS(userAgent: string) {
	if (userAgent.indexOf('Mac') !== -1) {
		return 'Mac'
	} else if (userAgent.indexOf('Windows') !== -1) {
		return 'Windows'
	}
	return 'Unknown'
}

const MenuSearch: React.FC<Props> = ({ open }) => {
	const [searchOpen, setSearchOpen] = useState(false)
	const [userOS, setUserOS] = useState('Unknown')

	useEffect(() => {
		const userAgent = window.navigator.userAgent
		const os = determineOS(userAgent)
		setUserOS(os)

		const keyboardListener = (event: KeyboardEvent) => {
			if (event.key === 'k' && (event.metaKey || event.ctrlKey)) {
				event.preventDefault()
				setSearchOpen(searchOpen => !searchOpen)
			}
		}

		document.addEventListener('keydown', keyboardListener)

		return () => document.removeEventListener('keydown', keyboardListener)
	}, [])

	return (
		<>
			<ActionTooltip disabled={open} align="center" side="right" label="Search">
				<div onClick={() => setSearchOpen(true)} className="px-4 py-2 cursor-pointer">
					<div className={`flex items-center py-1 ${open ? 'px-[6px]' : 'p-[2px]'} bg-[#f8f9fa] rounded-xl overflow-hidden h-[45px]`}>
						<div className="p-[6px]">
							<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
								<path
									d="M14.1057 14.2L17 17M16.0667 9.53333C16.0667 13.1416 13.1416 16.0667 9.53333 16.0667C5.92507 16.0667 3 13.1416 3 9.53333C3 5.92507 5.92507 3 9.53333 3C13.1416 3 16.0667 5.92507 16.0667 9.53333Z"
									stroke="#7F868C"
									strokeWidth="2"
									strokeLinecap="round"
								/>
							</svg>
						</div>

						{open && (
							<>
								<div className="flex flex-1 px-[2px]">
									<input className="outline-none bg-transparent text-sm leading-5" placeholder="Search" />
								</div>

								<div className="p-[6px] flex gap-[2px]">
									<div className="text-[10px] border border-[#E2E5E9] bg-[#fff] rounded-md px-[6px] py-1">{userOS === 'Mac' ? '⌘' : 'Ctrl'}</div>
									<div className="text-[10px] border border-[#E2E5E9] bg-[#fff] rounded-md px-[6px] py-1">K</div>
								</div>
							</>
						)}
					</div>
				</div>
			</ActionTooltip>

			<CommandDialog open={searchOpen} onOpenChange={setSearchOpen}>
				<CommandInput placeholder="Search" />
				<CommandList>
					<CommandEmpty>No Result Found</CommandEmpty>
				</CommandList>
			</CommandDialog>
		</>
	)
}

export default MenuSearch
