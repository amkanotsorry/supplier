'use client'

import { BarChartSquareUp01Icon, BoxIcon, Home02Icon } from '@/src/assets/icons'
import { usePathname } from 'next/navigation'
import React, { useState } from 'react'
import { Entity_Status } from '../../api/generated/graphql'
import useEntity from '../../hooks/use-entity'
import { QueryEntityType } from '../actions'
import EntityInfo from './EntityInfo'
import SideMenuHeader from './Header'
import MenuItem from './MenuItem'
import MenuSearch from './MenuSearch'

const SideMenu: React.FC<{ entity: QueryEntityType }> = ({ entity: initialEntity }) => {
	const pathname = usePathname()
	const { entity } = useEntity({ initialEntity })
	const [open, setOpen] = useState(true)

	return (
		<div className={`duration-300 inline-block ${open ? 'w-[300px]' : 'w-[70px]'}`}>
			<div className="h-screen flex flex-col border-r">
				<SideMenuHeader isOpen={open} onClick={() => setOpen(!open)} />

				<EntityInfo entity={entity} selected={pathname.includes('/profile')} open={open} />

				<MenuSearch open={open} />

				<div className="flex flex-col px-4 py-2 gap-2">
					{entity.entitySupplier?.status === Entity_Status.Approved ? (
						<>
							{/* <MenuItem Icon={BarGroup03Icon} open={open} title="Dashboard" url="dashboard" selected={pathname.includes(`/home/dashboard`)} /> */}
							<MenuItem Icon={BoxIcon} open={open} title="Product" url="product" selected={pathname.includes(`/home/product`)} />
							<MenuItem
								Icon={BarChartSquareUp01Icon}
								open={open}
								title="Place offer"
								url="place-offer"
								selected={pathname.includes(`/home/place-offer`)}
							/>
							{/* <MenuItem Icon={Bell02Icon} open={open} title="Notification" url="notification" selected={pathname.includes(`/home/notification`)} /> */}
							{/* <MenuItem Icon={UsersProfiles02Icon} open={open} title="Customers" url="customers" selected={pathname.includes(`/home/customers`)} /> */}
							{/* <MenuItem Icon={MessageText01Icon} open={open} title="BID room" url="bid-room" selected={pathname.includes(`/home/bid-room`)} /> */}
						</>
					) : (
						<MenuItem Icon={Home02Icon} open={open} title="Get started" url="get-started" selected={pathname.includes(`/home/get-started`)} />
					)}
				</div>
			</div>
		</div>
	)
}

export default SideMenu
