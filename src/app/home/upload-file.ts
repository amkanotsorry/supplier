export type UploadFileResponseType = { files: { thumbnail: string; url: string }[]; path: string; status: boolean }

export const uploadFile = async (file: File) => {
	const fileUploadInput = new FormData()

	fileUploadInput.append('path', 'products')
	fileUploadInput.append('bucketName', 'agrix-supplier')
	fileUploadInput.append('files', file as File)

	try {
		return fetch('https://storage-dev.agrix.mn/upload', {
			method: 'POST',
			body: fileUploadInput
		})
			.then(res => res.json())
			.then((data: UploadFileResponseType) => {
				if (data.status) {
					return data
				}
			})
	} catch (error) {
		console.log(error)
	}
}
