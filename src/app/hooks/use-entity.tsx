import { toast } from '@/src/components/ui/use-toast'
import { useCallback, useEffect, useState } from 'react'
import { create } from 'zustand'
import { QueryEntityType, getEntityById } from '../home/actions'
import { QuerySupplierReviewByIdType, getReviewById } from '../home/profile/[profile_id]/profile-actions'

interface Props {
	initialEntity?: QueryEntityType | null
}

const useEntity = ({ initialEntity }: Props) => {
	const { entity, review, updateEntity, updateReview } = useEntityStore()

	const refetchEntity = useCallback(async () => {
		if (!initialEntity?._id)
			return toast({
				title: 'No entity found',
				variant: 'destructive'
			})

		// This assumes that the initial entity is passed everytime, which is flawed.
		// To fix this, create a userhook, acquire entity id from the hook, so that way there will always be an entity.
		// Ex: const {entityId} = useUser()
		const { entity: newEntity } = await getEntityById(initialEntity._id)
		if (newEntity) {
			updateEntity(newEntity)
		}
	}, [initialEntity, updateEntity])

	const refetchReview = useCallback(async () => {
		if (!entity.entitySupplier?.reviewId)
			return toast({
				title: 'No review found',
				variant: 'destructive'
			})

		const { supplierReview } = await getReviewById(entity.entitySupplier.reviewId)
		if (supplierReview) updateReview(supplierReview)
	}, [entity.entitySupplier?.reviewId, updateReview])

	// Saves the default entity value, so we don't need to refetch this.
	// Usually the initial entity value comes from server side requests.
	useEffect(() => {
		if (initialEntity) updateEntity(initialEntity)
	}, [initialEntity, updateEntity])

	// Everytime entity changes, we refetch the reviews.
	useEffect(() => {
		if (entity.entitySupplier?.reviewId) refetchReview()
	}, [entity, refetchReview])

	return { entity, review, refetchEntity, refetchReview }
}

export default useEntity

type State = {
	entity: QueryEntityType
	review: QuerySupplierReviewByIdType
}

type Action = {
	updateEntity: (data: QueryEntityType) => void
	updateReview: (data: QuerySupplierReviewByIdType) => void
}

export const useEntityStore = create<State & Action>(set => ({
	entity: {} as QueryEntityType,
	review: {} as QuerySupplierReviewByIdType,
	updateEntity: data => set(() => ({ entity: data })),
	updateReview: data => set(() => ({ review: data }))
}))
