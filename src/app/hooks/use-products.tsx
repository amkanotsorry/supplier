import React, { useState } from 'react'
import { QueryProductType, getAllProducts } from '../home/product/actions/product'
import { toast } from '@/src/components/ui/use-toast'
import useEntity from './use-entity'

interface Props {
	initialData: QueryProductType[]
}

const useProducts = ({ initialData }: Props) => {
	const [products, setProducts] = useState<QueryProductType[]>(initialData || [])
	const [loading, setLoading] = useState(false)
	const { entity } = useEntity({})

	// Do product related functions, refetch, revalidate data here.

	// Fetch products client side here
	const refetch = async () => {
		setLoading(true)
		const { products: newProducts, error } = await getAllProducts(entity.entitySupplierId || '')
		setLoading(false)
		if (error) {
			toast({
				title: 'Error fetching products'
			})
			return
		}

		setProducts(newProducts)
	}

	return { products, refetch, loading }
}

export default useProducts
