import React, { useState } from 'react'
import { QueryProductType } from '../home/product/actions/product'

interface Props {
	initialData: QueryProductType[]
}

const useStockLocations = ({ initialData }: Props) => {
	const [stockLocations, setStockLocations] = useState<QueryProductType[]>(initialData)
	// Do stock location related functions, refetch, revalidate data here.

	// Fetch stock locations client side here
	const refetch = () => {
		setStockLocations(initialData)
	}

	return { stockLocations, refetch }
}

export default useStockLocations
