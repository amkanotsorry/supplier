import React from 'react'
import { Toaster } from '../components/ui/toaster'
import '../i18n/i18n'
import './globals.scss'
import AuthProvider from '../providers/auth_provider'

export const metadata = {
	title: 'AgriX',
	description: 'Powered by Techpack'
}

export default function RootLayout({ children }: { children: React.ReactNode }) {
	return (
		<html>
			<head>
				<title>Agri-X</title>
				<meta name="description" content="" />
				<meta name="viewport" content="width=device-width, initial-scale=1" />
				<link rel="icon" href="/favicon-32x32.ico" />
				{/* <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet" /> */}
			</head>

			<body>
				<AuthProvider>
					{children}
					<Toaster />
				</AuthProvider>
			</body>
		</html>
	)
}
