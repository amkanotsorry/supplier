import { getServerSession } from 'next-auth'
import { redirect } from 'next/navigation'
import '../i18n/i18n'
import { authOptions } from './api/auth/[...nextauth]/authOptions'
import { loadErrorMessages, loadDevMessages } from '@apollo/client/dev'

if (process.env.NODE_ENV !== 'production') {
	loadDevMessages()
	loadErrorMessages()
}

export default async function app() {
	const session = await getServerSession(authOptions)
	if (session?.user) {
		redirect('/home/product')
	} else {
		redirect('/login')
	}
}
