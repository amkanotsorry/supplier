import { getServerSession } from 'next-auth'
import { authOptions } from './api/auth/[...nextauth]/authOptions'
import { getEntityById } from './home/actions'
import { getProfile } from './home/get-started/actions'

export const getEntitySupplierId = async () => {
	const session = await getServerSession(authOptions)
	const { profile } = await getProfile(session?.user.id || '')
	const { entity } = await getEntityById(profile?.currentEntityId || '')
	return entity?.entitySupplierId
}
