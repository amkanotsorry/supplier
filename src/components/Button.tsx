import { StaticImport } from 'next/dist/shared/lib/get-img-props'
import Image from 'next/image'
import { FC, MouseEventHandler, ReactElement } from 'react'

export type ButtonSizes = 'lg' | 'md' | 'sm' | 'xs'

export type ButtonTypes = 'primary' | 'secondary'
export type ButtonVariants = 'filled' | 'outlined'

type Props = {
	icon?: ReactElement
	text?: string
	size?: ButtonSizes
	type?: ButtonTypes
	className?: string
	disabled?: boolean
	variant?: ButtonVariants
	onClick?: MouseEventHandler
}

const Button: FC<Props> = ({ text = '', className = '', size = 'md', type = 'primary', disabled, icon, onClick, variant = 'filled' }) => {
	let btnClassNames = {
		primary: 'bg-primary text-white cursor-pointer',
		secondary: 'bg-white border border-gray-100 cursor-pointer',
		disabled: 'bg-gray-50 text-gray-400 cursor-not-allowed',
		primaryoutlined: 'border border-primary text-primary cursor-pointer',
		secondaryoutlined: 'border border-cyan-900'
	}

	let btnSizeClassNames = {
		lg: 'px-[28px] py-[14px]',
		md: 'px-[22px] py-[11px]',
		sm: 'px-[18px] py-[9px] text-sm font-medium',
		xs: 'px-[14px] py-[7px]'
	}

	return (
		<div
			onClick={onClick}
			className={
				className +
				' ' +
				`${btnSizeClassNames[size]} flex gap-x-2 align-center justify-center items-center rounded-xl   
				${!disabled ? btnClassNames[`${type}${variant === 'filled' ? '' : variant}`] : btnClassNames.disabled}`
			}>
			{icon ? icon : null}
			<span>{text}</span>
		</div>
	)
}

// const TextVariant: any = (size: ButtonSizes) => {
// 	switch (size) {
// 		case 'LG':
// 			return 'lgMedium'
// 		case 'MD':
// 			return 'mdMedium'
// 		case 'SM':
// 			return 'smMedium'
// 		case 'MA':
// 			return 'xsMedium'
// 		default:
// 			return 'mdMedium'
// 	}
// }

export default Button
