import { Checkbox } from '@nextui-org/react'
import React from 'react'
import { ClassNameValue } from 'tailwind-merge'
import { cn } from '../lib/utils'

interface Props {
	title: string
	label?: string
	Icon?: React.ElementType
	className?: ClassNameValue
	iconStyle?: ClassNameValue
	isSelected?: boolean
	onChange?: () => void
}

const CheckboxField: React.FC<Props> = ({ title, label, Icon, className, iconStyle, isSelected, onChange }) => {
	return (
		<div
			onClick={onChange}
			className={cn(
				`flex text-sm h-10 px-2 cursor-pointer items-center bg-lightgray-100 
				hover:bg-lightgray-200 rounded-xl shadow-sm transition-background duration-150`,
				className
			)}>
			{Icon ? <Icon className={cn('w-5 h-5 mr-3', iconStyle)} /> : <div className="w-1" />}
			<div className="flex flex-col">
				{label && <span className="text-xs text-lightgray-800">{label}</span>}
				{title}
			</div>

			<Checkbox isSelected={isSelected} onChange={onChange} className="ml-auto" />
		</div>
	)
}

export default CheckboxField
