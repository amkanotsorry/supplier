'use client'

import { Calendar as CalendarIcon } from 'lucide-react'

import { Calendar } from '@/src/components/ui/calendar'
import { cn } from '@/src/lib/utils'
import { Popover, PopoverContent, PopoverTrigger } from '../ui/popover'
import { ClassNameValue } from 'tailwind-merge'
import { Input } from '@nextui-org/react'
import moment from 'moment'

type Props = {
	value?: Date
	onChange: (value?: Date) => void
	placeholder?: string
	disabled?: boolean
	className?: ClassNameValue
	label?: string
}

const DatePicker = ({ value: date, onChange: setDate, placeholder = '', className, label = '' }: Props) => {
	return (
		<Popover>
			<PopoverTrigger asChild>
				<button>
					<Input
						classNames={{
							input: 'text-start',
							inputWrapper: className
						}}
						placeholder={placeholder}
						label={label}
						value={moment(date || new Date()).format('yyyy/MM/DD')}
						endContent={<CalendarIcon className="w-[14px] text-black" />}
					/>
				</button>
			</PopoverTrigger>
			<PopoverContent className="w-auto p-0" align="start">
				<Calendar mode="single" selected={date} onSelect={setDate} />
			</PopoverContent>
		</Popover>
	)
}

export default DatePicker
