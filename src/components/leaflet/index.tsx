'use client'
import Leaflet from 'leaflet'
import dynamic from 'next/dynamic'
import 'leaflet/dist/leaflet.css'
import { useEffect } from 'react'
// import 'leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.webpack.css' // Re-uses images from ~leaflet package
// import * as L from 'leaflet'
// import 'leaflet-defaulticon-compatibility'

const DMapContainer = dynamic(() => import('react-leaflet').then(module => module.MapContainer), {
	ssr: false
})

const DTileLayer = dynamic(() => import('react-leaflet').then(module => module.TileLayer), {
	ssr: false
})

const DMarker = dynamic(() => import('react-leaflet').then(module => module.Marker), {
	ssr: false
})

// const CustomMapContainer = () => {
// 	useEffect(() => {
// 		;(async function init() {
// 			Leaflet.Icon.Default.mergeOptions({
// 				iconRetinaUrl: 'leaflet/images/marker-icon-2x.png',
// 				iconUrl: 'leaflet/images/marker-icon.png',
// 				shadowUrl: 'leaflet/images/marker-shadow.png'
// 			})
// 		})()
// 	}, [])
// 	return <DMapContainer />
// }

export { DMapContainer, DMarker, DTileLayer }
