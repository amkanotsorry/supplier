'use client'

import { DEFAULT_LAT, DEFAULT_LNG } from '@/src/lib/constants'
import { cn } from '@/src/lib/utils'
import { APIProvider, Map, MapMouseEvent, Marker } from '@vis.gl/react-google-maps'
import { useState } from 'react'
import { ClassNameValue } from 'tailwind-merge'

interface Props {
	position?: [number, number]
	setPosition?: (position: [number, number]) => void
	markerActionDisabled?: boolean
	className?: ClassNameValue
}

const GoogleMap = ({ position, setPosition, markerActionDisabled, className }: Props) => {
	const [positionData, setPositionData] = useState<google.maps.LatLngLiteral | null>(position ? { lat: position[0], lng: position[1] } : null)

	const onClickMap = (mapData: MapMouseEvent) => {
		const { latLng } = mapData.detail || {}
		if (!markerActionDisabled && latLng) {
			setPositionData({ lat: latLng.lat, lng: latLng.lng })
			setPosition?.([latLng.lat, latLng.lng])
		}
	}

	return (
		<APIProvider apiKey={'AIzaSyDhpy8ujvJEs5FcqxNypstHJJ78Rn9Xo9w'}>
			<Map
				onClick={onClickMap}
				className={cn('w-full h-40', className)}
				defaultCenter={{ lat: DEFAULT_LAT, lng: DEFAULT_LNG }}
				defaultZoom={14}
				gestureHandling={'greedy'}>
				<Marker position={positionData} />
			</Map>
		</APIProvider>
	)
}
export default GoogleMap
