'use client'

import 'leaflet/dist/leaflet.css'
import React from 'react'
import { MapContainer, Marker, MarkerProps, Popup, TileLayer } from 'react-leaflet'

function LocationMarker({ position }: MarkerProps) {
	return position === undefined ? null : <Marker position={position}>{<Popup>You are here</Popup>}</Marker>
}

const Map: React.FC<{ lat: number; lng: number }> = ({ lat, lng }) => {
	return (
		<MapContainer style={{ height: '400px', width: '100%' }} center={[lat, lng]} zoom={13} scrollWheelZoom={false}>
			<TileLayer
				attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
				url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
			/>
			<LocationMarker position={{ lat, lng }} />
		</MapContainer>
	)
}

export default Map
