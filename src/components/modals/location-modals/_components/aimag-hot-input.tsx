import { memo } from 'react'
import { FormControl, FormField, FormItem, FormLabel, FormMessage } from '../../../ui/form'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '../../../ui/select'

interface Props {
	aimagHotOptions: any[]
	form: any
}

const AimagHotInput = ({ aimagHotOptions, form }: Props) => {
	return (
		<FormField
			control={form.control}
			name="data.aimagHot"
			render={({ field }) => (
				<FormItem className="w-1/3">
					<FormLabel>City/Province</FormLabel>
					<Select onValueChange={value => field.onChange(parseInt(value, 10))} defaultValue={field.value ? field.value.toString() : undefined}>
						<FormControl>
							<SelectTrigger className="h-10">
								<SelectValue placeholder="Choose" />
							</SelectTrigger>
						</FormControl>
						<SelectContent>
							{aimagHotOptions.map(({ name, code }, i) => (
								<SelectItem key={i} value={code.toString()}>
									{name}
								</SelectItem>
							))}
						</SelectContent>
					</Select>
					<FormMessage />
				</FormItem>
			)}
		/>
	)
}

export default memo(AimagHotInput, (prev, next) => prev.aimagHotOptions.length === next.aimagHotOptions.length)
