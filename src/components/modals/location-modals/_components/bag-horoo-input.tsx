import { memo } from 'react'
import { UseFormReturn } from 'react-hook-form'
import { FormControl, FormField, FormItem, FormLabel, FormMessage } from '../../../ui/form'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '../../../ui/select'
import { AddressFormSchemaType } from '../address-modal'

interface Props {
	bagHorooOpns: any[]
	form: any
}

const BagHorooInput = ({ bagHorooOpns, form }: Props) => {
	return (
		<FormField
			control={form.control}
			name="data.bagHoroo"
			render={({ field }) => (
				<FormItem className="w-1/3">
					<FormLabel>Khoroo/Bag</FormLabel>
					<Select
						disabled={bagHorooOpns.length === 0}
						onValueChange={value => field.onChange(parseInt(value, 10))}
						defaultValue={field.value ? field.value.toString() : undefined}>
						<FormControl>
							<SelectTrigger className="h-10">
								<SelectValue placeholder="Choose" />
							</SelectTrigger>
						</FormControl>
						<SelectContent>
							{bagHorooOpns.map(({ name, code }, i) => (
								<SelectItem key={i} value={code.toString()}>
									{name}
								</SelectItem>
							))}
						</SelectContent>
					</Select>
					<FormMessage />
				</FormItem>
			)}
		/>
	)
}

export default memo(BagHorooInput, (prev, next) => prev.bagHorooOpns.length === next.bagHorooOpns.length)
