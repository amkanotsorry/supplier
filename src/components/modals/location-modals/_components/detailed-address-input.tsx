import { Input } from '@nextui-org/react'
import { UseFormReturn } from 'react-hook-form'
import { FormControl, FormField, FormItem, FormLabel, FormMessage } from '../../../ui/form'
import { AddressFormSchemaType } from '../address-modal'

interface Props {
	form: any
}

const DetailedAddressInput = ({ form }: Props) => {
	return (
		<FormField
			control={form.control}
			name="data.detailAddress"
			render={({ field }) => (
				<FormItem className="w-full">
					<FormLabel>Detailed address</FormLabel>
					<FormControl>
						<Input classNames={{ inputWrapper: 'h-10' }} placeholder="Street, Apartment, Floor etc." {...field} />
					</FormControl>
					<FormMessage />
				</FormItem>
			)}
		/>
	)
}

export default DetailedAddressInput
