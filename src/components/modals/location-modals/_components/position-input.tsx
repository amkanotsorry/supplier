import GoogleMap from '@/src/components/map-position-selector/google-maps'
import { useState } from 'react'
import { FormControl, FormField, FormItem, FormLabel, FormMessage } from '../../../ui/form'

interface Props {
	form: any
}

const PositionInput = ({ form }: Props) => {
	const [location, setLocation] = useState<{ latitude: number; longitude: number }>()

	const onChangePosition = (position: [number, number]) => {
		form.setValue('data.position', position)
		setLocation({ latitude: position[0], longitude: position[1] })
	}

	// useEffect(() => {
	// 	const pos = form.watch('position')
	// 	if (pos && pos.length === 2) {
	// 		setLocation({ latitude: pos[0], longitude: pos[1] })
	// 	} else {
	// 		// TODO: Fix it's bug
	// 		// if ('geolocation' in navigator) {
	// 		// 	// Retrieve latitude & longitude coordinates from `navigator.geolocation` Web API
	// 		// 	navigator.geolocation.getCurrentPosition(({ coords }) => {
	// 		// 		const { latitude, longitude } = coords
	// 		// 		setLocation({ latitude, longitude })
	// 		// 	})
	// 		// }
	// 	}
	// }, [])

	return (
		<FormField
			control={form.control}
			name="data.position"
			render={({ field: { value } }) => (
				<FormItem>
					<FormLabel>Pin from address</FormLabel>
					<FormControl>
						<div className="h-[182px]">
							<GoogleMap position={location ? [location.latitude, location.longitude] : undefined} setPosition={onChangePosition} />
						</div>
					</FormControl>
					<FormMessage />
				</FormItem>
			)}
		/>
	)
}

export default PositionInput
