import { memo } from 'react'
import { UseFormReturn } from 'react-hook-form'
import { FormControl, FormField, FormItem, FormLabel, FormMessage } from '../../../ui/form'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '../../../ui/select'
import { AddressFormSchemaType } from '../address-modal'

interface Props {
	sumDuuregOpns: any[]
	form: any
}

const SumDuuregInput = ({ sumDuuregOpns, form }: Props) => {
	return (
		<FormField
			control={form.control}
			name="data.sumDuureg"
			render={({ field }) => (
				<FormItem className="w-1/3">
					<FormLabel>Duureg/Sum</FormLabel>
					<Select
						disabled={sumDuuregOpns.length === 0}
						onValueChange={value => field.onChange(parseInt(value, 10))}
						defaultValue={field.value ? field.value.toString() : undefined}>
						<FormControl>
							<SelectTrigger className="h-10">
								<SelectValue placeholder="Choose" />
							</SelectTrigger>
						</FormControl>
						<SelectContent>
							{sumDuuregOpns.map(({ name, code }, i) => (
								<SelectItem key={i} value={code.toString()}>
									{name}
								</SelectItem>
							))}
						</SelectContent>
					</Select>
					<FormMessage />
				</FormItem>
			)}
		/>
	)
}

export default memo(SumDuuregInput, (prev, next) => prev.sumDuuregOpns.length === next.sumDuuregOpns.length)
