'use client'
import { Dialog, DialogClose, DialogContent, DialogFooter, DialogHeader, DialogTrigger } from '@/src/components/ui/dialog'
import { Form } from '@/src/components/ui/form'
import { useAddress } from '@/src/lib/address-helper'
import { zodResolver } from '@hookform/resolvers/zod'
import { Button } from '@nextui-org/react'
import React from 'react'
import { useForm } from 'react-hook-form'
import * as z from 'zod'
import { QueryEntityType } from '../../../app/home/actions'
import AimagHotInput from './_components/aimag-hot-input'
import BagHorooInput from './_components/bag-horoo-input'
import DetailedAddressInput from './_components/detailed-address-input'
import PositionInput from './_components/position-input'
import SumDuuregInput from './_components/sum-duureg-input'

const FormSchema = z.object({
	data: z.object({
		aimagHot: z.number(),
		sumDuureg: z.number(),
		bagHoroo: z.number(),
		detailAddress: z.string(),
		position: z.array(z.number()).min(2, { message: 'min 2' }).max(2, { message: 'max 2' }).optional()
	})
})

export type AddressFormSchemaType = z.infer<typeof FormSchema>

export type QueryAddress = Extract<QueryEntityType['address'], any>

type Props = {
	triggerComponent: React.ReactNode
	onCancel?: () => void
	onOk?: (address: QueryAddress) => void
	initialData?: QueryAddress
}

const AddressModal = ({ onCancel, triggerComponent, onOk, initialData }: Props) => {
	const form = useForm<AddressFormSchemaType>({
		resolver: zodResolver(FormSchema),
		defaultValues: {
			data: {
				aimagHot: initialData?.cityProvince.code,
				sumDuureg: initialData?.duuregSoum.code,
				bagHoroo: initialData?.khorooBag.code,
				detailAddress: initialData?.detail,
				position:
					initialData?.coordinates.latitude && initialData?.coordinates.longtitude
						? [(initialData?.coordinates.latitude, initialData?.coordinates.longtitude)]
						: undefined
			}
		}
	})

	const { aimagHotOptions, sumDuuregOpns, bagHorooOpns, address } = useAddress({
		form,
		aimagFormKey: 'data.aimagHot',
		sumFormKey: 'data.sumDuureg',
		bagFormKey: 'data.bagHoroo'
	})

	function onSubmit({ data }: z.infer<typeof FormSchema>) {
		const { cityProvince, duuregSoum, khorooBag } = address ?? {}

		if (cityProvince && duuregSoum && khorooBag && data.position) {
			onOk?.({
				cityProvince,
				duuregSoum,
				khorooBag,
				detail: data.detailAddress,
				coordinates: {
					latitude: data.position[0],
					longtitude: data.position[1]
				}
			})
			onCancel?.()
		}
	}

	return (
		<Dialog>
			<DialogTrigger asChild>{triggerComponent}</DialogTrigger>
			<DialogContent className={'max-w-[544px] overflow-y-auto max-h-screen p-0 gap-0'}>
				<DialogHeader className="w-full p-4 pb-0">
					<div className="text-black text-lg font-semibold leading-7">Add registered address</div>
					<div className="w-100 text-gray-400 text-sm font-medium leading-snug">Fill in your registered address information.</div>
				</DialogHeader>

				<Form {...form}>
					<form onSubmit={form.handleSubmit(onSubmit)}>
						<div className="p-4 flex flex-col gap-y-4">
							<PositionInput form={form} />

							<div className="flex gap-4">
								<AimagHotInput form={form} aimagHotOptions={aimagHotOptions} />
								<SumDuuregInput form={form} sumDuuregOpns={sumDuuregOpns} />
								<BagHorooInput form={form} bagHorooOpns={bagHorooOpns} />
							</div>

							<div className="flex gap-4">
								<DetailedAddressInput form={form} />
							</div>
						</div>
					</form>
				</Form>

				<DialogFooter className="flex justify-end p-4 gap-4 border-t border-gray-100">
					<DialogClose asChild>
						<Button variant="bordered" className="border-[1px]">
							Cancel
						</Button>
					</DialogClose>
					<DialogClose asChild>
						<Button
							color="primary"
							variant="solid"
							onClick={() => {
								form.handleSubmit(onSubmit)()
							}}>
							Save
						</Button>
					</DialogClose>
				</DialogFooter>
			</DialogContent>
		</Dialog>
	)
}

export default AddressModal
