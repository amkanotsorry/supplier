'use client'
import { StockLocationType } from '@/src/app/home/get-started/steps'
import { Dialog, DialogContent, DialogFooter, DialogHeader } from '@/src/components/ui/dialog'
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from '@/src/components/ui/form'
import { useAddress } from '@/src/lib/address-helper'
import { zodResolver } from '@hookform/resolvers/zod'
import { Button, Input } from '@nextui-org/react'
import { FieldErrors, UseFormReturn, useForm } from 'react-hook-form'
import * as z from 'zod'
import { toast } from '../../ui/use-toast'
import AimagHotInput from './_components/aimag-hot-input'
import BagHorooInput from './_components/bag-horoo-input'
import DetailedAddressInput from './_components/detailed-address-input'
import PositionInput from './_components/position-input'
import SumDuuregInput from './_components/sum-duureg-input'
import { useEffect } from 'react'

const FormSchema = z.object({
	data: z.object({
		aimagHot: z.number(),
		sumDuureg: z.number(),
		bagHoroo: z.number(),
		detailAddress: z.string(),
		phoneNumber: z.string(),
		branchName: z.string(),
		position: z.array(z.number()).min(2, { message: 'min 2' }).max(2, { message: 'max 2' }).optional()
	})
})

export type StockLocationFormSchema = z.infer<typeof FormSchema>

type Props = {
	open: boolean
	onOpenChange: () => void
	onOk?: (any: any, branchName?: string) => void
	initialData?: StockLocationType
}

const StockLocationModal = ({ open, onOpenChange, onOk, initialData }: Props) => {
	const initialFormData = {
		branchName: initialData?.branchName || '',
		phoneNumber: initialData?.phoneNumber || '',
		aimagHot: initialData?.address?.cityProvince?.code || 0,
		sumDuureg: initialData?.address?.duuregSoum?.code || 0,
		bagHoroo: initialData?.address?.khorooBag?.code || 0,
		detailAddress: initialData?.address?.detail || '',
		position:
			initialData?.address.coordinates.latitude && initialData?.address.coordinates.longtitude
				? [(initialData?.address.coordinates.latitude, initialData?.address.coordinates.longtitude)]
				: undefined
	}

	const form = useForm<StockLocationFormSchema>({
		resolver: zodResolver(FormSchema),
		defaultValues: {
			data: initialFormData
		}
	})

	const { aimagHotOptions, sumDuuregOpns, bagHorooOpns, address } = useAddress({
		form,
		aimagFormKey: 'data.aimagHot',
		sumFormKey: 'data.sumDuureg',
		bagFormKey: 'data.bagHoroo'
	})

	function onSubmit({ data }: StockLocationFormSchema) {
		const { cityProvince, duuregSoum, khorooBag } = address ?? {}

		if (cityProvince && duuregSoum && khorooBag && data.position) {
			const { branchName } = initialData || {}
			onOk?.(
				{
					_id: initialData?._id,
					branchName: data.branchName,
					phoneNumber: data.phoneNumber,
					address: {
						cityProvince,
						duuregSoum,
						khorooBag,
						detail: data.detailAddress,
						coordinates: {
							latitude: data.position[0],
							longtitude: data.position[1]
						}
					}
				},
				branchName
			)

			onOpenChange()
			return
		}

		toast({
			title: 'Required Fields are missing.',
			variant: 'destructive'
		})
	}

	return (
		<Dialog open={open} onOpenChange={onOpenChange}>
			<DialogContent className={'max-w-[544px] overflow-y-auto max-h-screen p-0 gap-0'}>
				<DialogHeader className="w-full p-4 pb-0">
					<div className="text-black text-lg font-semibold leading-7">Add Branch</div>
					<div className="w-100 text-gray-400 text-sm font-medium leading-snug">
						These branch information will be used for product location, delivery type and more.
					</div>
				</DialogHeader>

				<Form {...form}>
					<form onSubmit={form.handleSubmit(onSubmit)}>
						<div className="p-4 flex flex-col gap-y-4">
							<FormField
								control={form.control}
								name="data.branchName"
								render={({ field }) => (
									<FormItem>
										<FormLabel>Branch name</FormLabel>
										<FormControl>
											<Input classNames={{ inputWrapper: 'h-10' }} placeholder="Branch name" {...field} />
										</FormControl>
										<FormMessage />
									</FormItem>
								)}
							/>

							<PositionInput form={form as UseFormReturn<StockLocationFormSchema>} />

							<div className="flex gap-4">
								<AimagHotInput form={form} aimagHotOptions={aimagHotOptions} />

								<SumDuuregInput form={form} sumDuuregOpns={sumDuuregOpns} />

								<BagHorooInput form={form} bagHorooOpns={bagHorooOpns} />
							</div>

							<div className="flex gap-4">
								<DetailedAddressInput form={form} />

								<FormField
									control={form.control}
									name="data.phoneNumber"
									render={({ field }) => (
										<FormItem className="w-full">
											<FormLabel>Branch contact info</FormLabel>
											<FormControl>
												<Input classNames={{ inputWrapper: 'h-10' }} className="bg-gray-50 border-0" placeholder="Phone number" {...field} />
											</FormControl>
											<FormMessage />
										</FormItem>
									)}
								/>
							</div>
						</div>
					</form>
				</Form>
				<DialogFooter className="flex justify-end p-4 gap-4 border-t border-gray-100">
					<Button onClick={onOpenChange} variant="bordered" className="border-[1px]">
						Cancel
					</Button>
					<Button
						type="submit"
						color="primary"
						variant="solid"
						onClick={() => {
							form.handleSubmit(onSubmit)()
						}}>
						Save
					</Button>
				</DialogFooter>
			</DialogContent>
		</Dialog>
	)
}

export default StockLocationModal
