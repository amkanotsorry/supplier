'use client'

import { Button, Spinner } from '@nextui-org/react'
import React, { Dispatch, SetStateAction, useContext, useState } from 'react'
import { Dialog, DialogClose, DialogContent, DialogFooter, DialogHeader, DialogTrigger } from '../../ui/dialog'
import FileUpload from '@/src/app/home/get-started/upload-documents/Documents/file-upload'
import { FormLabel } from '../../ui/form'
import DatePicker from '../../datepicker'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '../../ui/select'
import { GetStartedContext } from '@/src/app/home/get-started/steps'
import { uploadFile } from '@/src/app/home/upload-file'
import { toast } from '../../ui/use-toast'
import { createCertification } from '@/src/app/home/get-started/actions'
import { CreateCertificationInput } from '@/src/app/api/generated/graphql'
import { SpecialCertificateType } from '@/src/app/home/get-started/upload-documents/Documents'

interface Props {
	open: boolean
	onOpenChange: Dispatch<SetStateAction<boolean>>
	onSave: (cert: SpecialCertificateType) => void
}

const SpecialCertificationModal = ({ open, onOpenChange, onSave: onSuccess }: Props) => {
	const [attachment, setAttachment] = useState<File>()
	const { sources, certificates, createEntitySupplierInput, updateEntitySupplier } = useContext(GetStartedContext)
	const [expiryDate, setExpiryDate] = useState<Date | undefined>(new Date())
	const [issueDate, setIssueDate] = useState<Date | undefined>(new Date())
	const [name, setName] = useState('')
	const [organization, setOrganization] = useState('')
	const [loading, setLoading] = useState(false)

	const onSave = async () => {
		try {
			if (!attachment) {
				toast({
					title: 'Please attach file',
					variant: 'destructive'
				})
				return
			}

			setLoading(true)

			const response = await uploadFile(attachment)

			const createCertificationInput: CreateCertificationInput = {
				attachments: [
					{
						key: '',
						bucket: '',
						location: `https://agrix-supplier.s3.ap-southeast-1.amazonaws.com/${response?.files[0].url}`
					}
				],
				expiryDate,
				issueDate,
				name,
				organization
			}

			const { createCertification: newCertificate, error } = await createCertification(createCertificationInput)

			setLoading(false)

			if (error) {
				console.log(error)
				toast({
					title: 'Error creating certificate. Please try again',
					variant: 'destructive'
				})
				return
			}

			const certificationIds = createEntitySupplierInput?.certificationIds || []

			if (newCertificate?._id) {
				certificationIds.push(newCertificate?._id)
			}

			updateEntitySupplier?.({ certificationIds })

			onOpenChange(false)
			toast({
				title: 'Added file successfully'
			})

			onSuccess({
				certificate: organization,
				owning: name,
				issueDate: issueDate || new Date(),
				expiryDate: expiryDate || new Date(),
				attachment: `https://agrix-supplier.s3.ap-southeast-1.amazonaws.com/${response?.files[0].url}`
			})
		} catch (error) {}
	}

	return (
		<Dialog open={open} onOpenChange={onOpenChange}>
			<DialogContent className={'max-w-[544px] overflow-y-auto max-h-screen p-0 gap-0'}>
				<DialogHeader className="w-full p-4 border-b">
					<div className="text-black text-lg font-semibold leading-7">Add file</div>
					<div className="w-100 text-gray-400 text-sm font-medium leading-snug">Fill in details for your special certificate.</div>
				</DialogHeader>

				<div className="p-4 flex flex-col gap-y-4">
					<div className="flex gap-4">
						<div className="flex flex-1 flex-col gap-2">
							<FormLabel>Issue Date</FormLabel>
							<DatePicker value={issueDate} onChange={setIssueDate} />
						</div>

						<div className="flex flex-1 flex-col gap-2">
							<FormLabel>Expiry Date</FormLabel>
							<DatePicker value={expiryDate} onChange={setExpiryDate} />
						</div>
					</div>

					<div className="flex flex-col gap-2">
						<FormLabel>Owning</FormLabel>

						<Select onValueChange={v => setName(v)}>
							<SelectTrigger className="h-14">
								<SelectValue placeholder="Choose owning" />
							</SelectTrigger>
							<SelectContent>
								{(sources || []).map((source, index) => (
									<SelectItem className="py-3" key={index} value={source.title}>
										{source.title}
									</SelectItem>
								))}
							</SelectContent>
						</Select>
					</div>

					<div className="flex flex-col gap-2">
						<FormLabel>Certificate</FormLabel>

						<Select onValueChange={v => setOrganization(v)}>
							<SelectTrigger className="h-14">
								<SelectValue placeholder="Choose certificate" />
							</SelectTrigger>
							<SelectContent>
								{(certificates || []).map((certifcate, index) => (
									<SelectItem className="py-3" key={index} value={certifcate.name}>
										{certifcate.name}
									</SelectItem>
								))}
							</SelectContent>
						</Select>
					</div>

					<div className="flex flex-col gap-2">
						<FormLabel>Attachment</FormLabel>
						<FileUpload className="h-14" name="attachment" value={attachment} onChange={setAttachment} />
					</div>
				</div>

				<DialogFooter className="flex justify-end p-4 gap-4 border-t border-gray-100">
					<DialogClose asChild>
						<Button variant="bordered" className="border-[1px]">
							Cancel
						</Button>
					</DialogClose>
					<Button disabled={loading} color="primary" variant="solid" onClick={onSave}>
						{loading ? <Spinner color="white" size="sm" /> : 'Save'}
					</Button>
				</DialogFooter>
			</DialogContent>
		</Dialog>
	)
}

export default SpecialCertificationModal
