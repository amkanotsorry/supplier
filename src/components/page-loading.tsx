import { Spinner } from '@nextui-org/react'
import React from 'react'

interface Props {}

const PageLoading: React.FC<Props> = () => {
	return (
		<div className="flex w-full h-full items-center justify-center">
			<Spinner color={'primary'} />
		</div>
	)
}

export default PageLoading
