'use client'

import { Input, InputProps } from '@nextui-org/react'
import React, { useState } from 'react'

const PercentInput = React.forwardRef<HTMLInputElement, InputProps>(({ value, onValueChange, onChange, isInvalid, ...props }, ref) => {
	const [invalid, setInvalid] = useState<boolean>(!!isInvalid)

	return (
		<Input
			ref={ref}
			type="number"
			endContent={'%'}
			placeholder="0"
			onValueChange={val => {
				const value = parseFloat(val)
				setInvalid(value < 0 || value > 100 || value % 1 !== 0)
				onValueChange?.(val)
			}}
			isInvalid={invalid}
			{...props}
		/>
	)
})

PercentInput.displayName = 'Percent input'
export default PercentInput
