import React from 'react'

interface Props {
	label?: string
}

const SearchBar: React.FC<Props> = ({ label }) => {
	return (
		<div className={`max-w-[282px] w-full flex items-center py-1 px-[6px] bg-lightgray-100 rounded-xl border`}>
			<div className="p-1">
				<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
					<path
						d="M14.1057 14.2L17 17M16.0667 9.53333C16.0667 13.1416 13.1416 16.0667 9.53333 16.0667C5.92507 16.0667 3 13.1416 3 9.53333C3 5.92507 5.92507 3 9.53333 3C13.1416 3 16.0667 5.92507 16.0667 9.53333Z"
						stroke="#7F868C"
						strokeWidth="2"
						strokeLinecap="round"
					/>
				</svg>
			</div>

			<div className="flex flex-1 px-[2px]">
				<input className="outline-none bg-transparent text-sm leading-5" placeholder={label} />
			</div>
		</div>
	)
}

export default SearchBar
