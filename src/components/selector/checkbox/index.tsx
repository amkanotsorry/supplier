import { FC, MouseEventHandler } from 'react'
import classNames from 'classnames'
import './style.scss'
import { CheckIcon } from '@/src/assets/icons'

type Props = {
	text?: string
	className?: string
	onClick?: MouseEventHandler
	selected?: boolean
	size?: CheckSizes
}

const CheckBox: FC<Props> = ({ size = 'MD', className = '', selected = false, onClick }) => {
	let CheckSize = {
		XS: 'w-[14px] h-[14px]',
		SM: 'w-[16px] h-[16px]',
		MD: 'w-[18px] h-[18px]',
		LG: 'w-[20px] h-[20px]'
	}

	return (
		<div>
			<div className={classNames('check-container', 'cursor-pointer', 'rounded-[3.5px]', CheckSize[size], className, { selected: selected })}>
				<CheckIcon className={CheckSize[size]} color="white" />
			</div>
		</div>
	)
}

export default CheckBox

type CheckSizes = 'XS' | 'SM' | 'MD' | 'LG'
