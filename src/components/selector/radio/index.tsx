import { FC, MouseEventHandler } from 'react'
import classNames from 'classnames'
import './style.scss'

type Props = {
	text?: string
	className?: string
	onClick?: MouseEventHandler
	selected?: boolean
	size?: RadioSizes
}

const Radio: FC<Props> = ({ size = 'MD', className = '', selected = false, onClick }) => {
	let radioSize = {
		XS: 'w-[14px] h-[14px]',
		SM: 'w-[16px] h-[16px]',
		MD: 'w-[18px] h-[18px]',
		LG: 'w-[20px] h-[20px]'
	}

	return (
		<div>
			<div className={classNames('radio-container', 'cursor-pointer', 'rounded-full', radioSize[size], className, { selected: selected })}></div>
		</div>
	)
}

export default Radio

type RadioSizes = 'XS' | 'SM' | 'MD' | 'LG'
