import React from 'react'
import { ClassNameValue } from 'tailwind-merge'

interface Props {
	className?: ClassNameValue
}

const Seperator: React.FC<Props> = ({ className }) => {
	return <div className={`${className} h-[1px] bg-lightgray-200 border-t`} />
}

export default Seperator
