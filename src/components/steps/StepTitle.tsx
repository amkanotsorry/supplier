import React from 'react'
import { StepItem } from './types'

interface Props extends StepItem {
	counter: number
	isLastStep: boolean
	direction?: 'horizontal' | 'vertical'
	onClick?: () => void
}

const StepTitle = ({ title, counter, isLastStep, onClick, status = 'wait', direction = 'horizontal' }: Props) => {
	const getTitleColor = () => {
		switch (status) {
			case 'wait':
				return 'text-gray-500'
			case 'process':
				return 'text-slate-900 '
			case 'finish':
				return 'text-zinc-900'
			default:
				return 'text-gray-500'
		}
	}

	if (direction === 'vertical') {
		return (
			<div className="cursor-pointer" onClick={onClick}>
				<div className="relative min-h-[56px] flex gap-2 w-100 max-w-[240px] truncate">
					<div className="absolute top-0 flex items-center gap-2">
						<StepCounter {...{ status, counter }} />
						<div className={`${getTitleColor()} text-sm font-medium leading-snug`}>{title}</div>
					</div>
					{!isLastStep ? (
						<div
							className={`absolute top-10 bottom-3.5 left-[3px] h-[1px] w-5 rotate-90  ${
								status === 'finish' ? 'bg-green-500' : 'bg-gray-100'
							} `}></div>
					) : null}
				</div>
			</div>
		)
	}
	return (
		<div onClick={onClick} className={`cursor-pointer flex w-100 items-center  ${isLastStep ? '' : 'grow'}`}>
			<div className="text-center lg:flex lg:gap-2 lg:items-center lg:w-100 lg:max-w-[240px] lg:truncate ">
				<div className="flex flex-col items-center">
					<StepCounter {...{ status, counter }} />
				</div>
				<div className={`${getTitleColor()} text-base font-semibold leading-relaxed`}>{title}</div>
			</div>

			{!isLastStep ? <div className={`mx-2 lg:mx-6 grow h-0.5 ${status === 'finish' ? 'bg-green-500' : 'bg-gray-200'} `}></div> : null}
		</div>
	)
}

export default StepTitle

const StepCounter = (props: { status: string; counter?: number }) => {
	const { status, counter } = props

	const getCircleStyle = () => {
		switch (status) {
			case 'wait':
				return 'bg-gray-100 text-gray-500'
			case 'process':
				return 'border border-slate-900 text-slate-900'
			case 'finish':
				return 'bg-green-500 text-white'
			default:
				return ''
		}
	}
	return (
		<div>
			<div className={`w-[26px] h-[26px] rounded-[100px] flex-col justify-center items-center gap-2.5 inline-flex ${getCircleStyle()} after:`}>
				<div className={` text-center text-base font-semibold leading-relaxed`}>{counter}</div>
			</div>
		</div>
	)
}
