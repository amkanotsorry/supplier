'use client'
import React, { useContext, useEffect, useState } from 'react'
import { StepItem } from './types'
import StepTitle from './StepTitle'
import { GetStartedContext } from '@/src/app/home/get-started/steps'

type Props = {
	items: StepItem[]
	current?: number
	onChange?: (current: number) => void
	onStepClick?: (index: number) => void
	direction?: 'horizontal' | 'vertical'
	status?: 'wait' | 'process' | 'finish' | 'error'
}

const Steps = React.forwardRef<HTMLDivElement, Props>((props, ref) => {
	const { items, direction = 'horizontal', current, onStepClick } = props

	const [myItems, setMyItems] = useState<StepItem[]>([])

	useEffect(() => {
		let newItems = items
		if (typeof current === 'number' && current > -1) {
			newItems = items.map((item, i) => {
				if (i < current) {
					item.status = 'finish'
				} else if (i === current) {
					item.status = 'process'
				} else item.status = 'wait'
				return item
			})
		}
		setMyItems(newItems)
	}, [items, current])

	return (
		<div ref={ref} className="h-full">
			{direction === 'vertical' && (
				<div className="flex h-full" ref={ref}>
					<div className={`flex flex-col min-w-[160px] lg:min-w-[244px] w-100`}>
						{myItems.map((item, i) => (
							<StepTitle
								key={i}
								onClick={() => onStepClick?.(i)}
								counter={i + 1}
								direction={direction}
								isLastStep={items.length - 1 === i}
								{...item}
							/>
						))}
					</div>
					<div className="w-px h-full bg-gray-100 mx-8" />
					<div className="w-full">{typeof current === 'number' && myItems[current]?.content}</div>
				</div>
			)}

			{direction === 'horizontal' && (
				<div className="flex flex-col w-100  h-full" ref={ref}>
					<div className={`flex w-100'}`}>
						{myItems.map((item, i) => (
							<StepTitle
								onClick={() => onStepClick?.(i)}
								key={i}
								counter={i + 1}
								direction={direction}
								isLastStep={items.length - 1 === i}
								{...item}
							/>
						))}
					</div>

					{typeof current === 'number' && myItems[current]?.content}
				</div>
			)}
		</div>
	)
})

Steps.displayName = 'Step'

export default Steps
