import { ReactNode } from 'react'

type StepItem = {
	title: string | ReactNode
	status?: 'wait' | 'process' | 'finish' | 'error'
	content?: ReactNode
}

export { StepItem }
