'use client'

import {
	ColumnDef,
	ColumnFiltersState,
	Row,
	SortingState,
	flexRender,
	getCoreRowModel,
	getFilteredRowModel,
	getPaginationRowModel,
	getSortedRowModel,
	useReactTable
} from '@tanstack/react-table'

import { Table, TableBody, TableCell, TableHead, TableHeader, TableRow } from '@/src/components/ui/table'
import { DragEvent, useState } from 'react'
import { SearchIcon } from 'lucide-react'

interface DataTableProps<TData, TValue> {
	columns: ColumnDef<TData, TValue>[]
	data: TData[]
	onClickRow?: (row: Row<TData>) => void
}

export function DataTable<TData, TValue>({ columns, data, onClickRow }: DataTableProps<TData, TValue>) {
	const [sorting, setSorting] = useState<SortingState>([])
	const [columnFilters, setColumnFilters] = useState<ColumnFiltersState>([])

	const table = useReactTable({
		data,
		columns,
		getCoreRowModel: getCoreRowModel(),
		// getPaginationRowModel: getPaginationRowModel(),
		getSortedRowModel: getSortedRowModel(),
		getFilteredRowModel: getFilteredRowModel(),
		onSortingChange: setSorting,
		onColumnFiltersChange: setColumnFilters,
		state: {
			sorting,
			columnFilters
		}
	})

	let columnBeingDragged: number

	const onDragStart = (e: DragEvent<HTMLElement>): void => {
		columnBeingDragged = Number(e.currentTarget.dataset.columnIndex)
	}

	const onDrop = (e: DragEvent<HTMLElement>): void => {
		e.preventDefault()
		const newPosition = Number(e.currentTarget.dataset.columnIndex)
		const currentCols = table.getVisibleLeafColumns().map(c => c.id)
		const colToBeMoved = currentCols.splice(columnBeingDragged, 1)

		currentCols.splice(newPosition, 0, colToBeMoved[0])
		table.setColumnOrder(currentCols)
	}

	return (
		<Table className="overflow-x-scroll">
			<TableHeader>
				{table.getHeaderGroups().map(headerGroup => (
					<TableRow key={headerGroup.id}>
						{headerGroup.headers.map(header => {
							return (
								<TableHead
									className={`${header.getSize() !== 150 ? header.getSize() : undefined} border-t bg-lightgray-100 p-2 border-r last:border-r-0`}
									key={header.id}
									draggable="true"
									onDragStart={onDragStart}
									onDragOver={(e): void => {
										e.preventDefault()
									}}
									onDrop={onDrop}>
									{header.isPlaceholder ? null : flexRender(header.column.columnDef.header, header.getContext())}

									<div className="px-2 py-[5px] gap-x-2 flex items-center rounded-md border border-input bg-background text-sm">
										<SearchIcon className="w-4 h-4" />
										<input
											className="w-full ring-offset-background file:border-0 file:bg-transparent file:text-xs placeholder:text-muted-foreground placeholder:font-normal placeholder:text-xs text-xs focus-visible:outline-none disabled:cursor-not-allowed disabled:opacity-50"
											placeholder="Search"
											value={(table.getColumn(header.id)?.getFilterValue() as string) ?? ''}
											onChange={event => table.getColumn(header.id)?.setFilterValue(event.target.value)}
										/>
									</div>
								</TableHead>
							)
						})}
					</TableRow>
				))}
			</TableHeader>
			<TableBody>
				{table.getRowModel().rows?.length ? (
					table.getRowModel().rows.map(row => (
						<TableRow
							className="cursor-pointer"
							onClick={onClickRow ? () => onClickRow(row) : undefined}
							key={row.id}
							data-state={row.getIsSelected() && 'selected'}>
							{row.getVisibleCells().map(cell => (
								<TableCell className="border-r last:border-r-0 p-2" key={cell.id}>
									{flexRender(cell.column.columnDef.cell, cell.getContext())}
								</TableCell>
							))}
						</TableRow>
					))
				) : (
					<TableRow>
						<TableCell colSpan={columns.length} className="h-24 text-center">
							No results.
						</TableCell>
					</TableRow>
				)}
			</TableBody>
		</Table>
	)
}
