import React from 'react'
import { CSSProperties, FC, MouseEventHandler } from 'react'
import './text.scss'

type Props = {
	variant?: TextVariants
	color?: TextColors
	className?: string
	style?: CSSProperties
	children?: React.ReactNode
	onClick?: MouseEventHandler
}

const Text: FC<Props> = ({ variant = 'mdMedium', className, children, onClick, style, color }) => {
	return (
		<span className={`${variant} ${color} ${className}`} onClick={onClick} style={style}>
			{children}
		</span>
	)
}

export default Text

export type TextColors =
	| 'neutral700'
	| 'darkNeutral0'
	| 'darkNeutral500'
	| 'darkNeutral700'
	| 'brandMossGreen900'
	| 'brandLightGrey900'
	| 'brandDenimBlue700'
	| 'brandSupportDarkGrey500'
	| 'brandSupportDarkGrey700'
	| 'brandSupportLightGrey800'
	| 'brandSupportLightGrey1000'
	| 'brandSupportLightGrey1100'
	| 'white'

export type TextVariants =
	| 'display2xlRegular'
	| 'display2xlMedium'
	| 'display2xlSemibold'
	| 'display2xlbold'
	| 'displayxlRegular'
	| 'displayxlMedium'
	| 'displayxlSemibold'
	| 'displayxlbold'
	| 'displaylgRegular'
	| 'displaylgMedium'
	| 'displaylgSemibold'
	| 'displaylgbold'
	| 'displaymdRegular'
	| 'displaymdMedium'
	| 'displaymdSemibold'
	| 'displaymdbold'
	| 'displaysmRegular'
	| 'displaysmMedium'
	| 'displaysmSemibold'
	| 'displaysmbold'
	| 'displayxsRegular'
	| 'displayxsMedium'
	| 'displayxsSemibold'
	| 'displayxsbold'
	| 'xlRegular'
	| 'xlMedium'
	| 'xlSemibold'
	| 'xlbold'
	| 'lgRegular'
	| 'lgMedium'
	| 'lgSemibold'
	| 'lgbold'
	| 'mdRegular'
	| 'mdMedium'
	| 'mdSemibold'
	| 'mdbold'
	| 'smRegular'
	| 'smMedium'
	| 'smSemibold'
	| 'smbold'
	| 'xsRegular'
	| 'xsMedium'
	| 'xsSemibold'
	| 'xsBold'
	| 'MacroRegular'
	| 'MacroMedium'
	| 'MacroSemibold'
	| 'Macrobold'
