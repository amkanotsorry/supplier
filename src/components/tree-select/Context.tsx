'use client'
import React, { createContext, useState, useEffect } from 'react'
import { TreeNode, TreeNodeProp } from './types'

interface ContextData {
	data: TreeNode[]
	selected: TreeNode[]
	//eslint-disable-next-line
	setData?: (data: TreeNode[]) => void
	//eslint-disable-next-line
	handleChange?: (value: string, checked: boolean, list?: TreeNode[]) => void
	//eslint-disable-next-line
	initTree?: (list: TreeNodeProp[]) => void
}

const initialVal: ContextData = {
	data: [],
	selected: [],
	setData: () => {},
	initTree: () => {}
}

const TreeSelectContext = createContext(initialVal)

type Props = {
	children: React.ReactElement
	//eslint-disable-next-line
	onChange: (value: string[]) => void
}

function TreeContextProvider({ children, onChange }: Props) {
	const [data, setData] = useState<TreeNode[]>([])
	const [selected, setSelected] = useState<TreeNode[]>([])

	useEffect(() => {
		//filter selected nodes
		if (data.length) {
			let newSelected: TreeNode[] = []
			data.forEach(d => {
				newSelected = newSelected.concat(getSelectedNodes(d))
			})
			setSelected(newSelected)
			onChange?.(newSelected.map(node => node.value))
		}
		//eslint-disable-next-line
	}, [data])

	const convertTreeNode = (tree: TreeNode, list: TreeNodeProp[]) => {
		list.forEach(node => {
			let newChildNode: TreeNode = new TreeNode(node.title, node.value)
			tree.addChild(newChildNode)
			if (node.children) convertTreeNode(newChildNode, node.children)
		})
		return tree
	}

	const initTree = (list: TreeNodeProp[]) => {
		let treeList: TreeNode[] = []
		list.forEach(type => treeList.push(convertTreeNode(new TreeNode(type.title, type.value), type.children || [])))
		setData?.(treeList)
	}

	const getSelectedNodes = (node: TreeNode): TreeNode[] => {
		const iterateTree = (node: TreeNode, checkedNodes: TreeNode[]): TreeNode[] => {
			if (node.checked === true) {
				checkedNodes.push(node)
			} else if (node.checked === 'not-completed') {
				node.children?.forEach(child => {
					return iterateTree(child, checkedNodes)
				})
			}
			return checkedNodes
		}

		let checkedNodes: TreeNode[] = []
		return iterateTree(node, checkedNodes)
	}

	const handleChange = (value: string, checked: boolean) => {
		const updatedData: TreeNode[] = updateData(value, checked, data)
		if (updatedData) setData([...updatedData])
	}

	const updateData = (value: string, checked: boolean, list: TreeNode[]): TreeNode[] => {
		list.forEach(d => {
			if (d.value === value) {
				d.checked = checked
				if (d.children?.length) d = updateChildNodes(d, checked)
				if (d.parent) d = updateParentNodes(d.parent)
			} else if (d.children?.length) {
				updateData(value, checked, d.children)
			}
		})
		return list
	}

	const updateChildNodes = (node: TreeNode, checked: boolean) => {
		node.children?.forEach(childNode => {
			childNode.checked = checked
			updateChildNodes(childNode, checked)
		})
		return node
	}

	const updateParentNodes = (node: TreeNode) => {
		if (node.children?.length) {
			let checkedCount = 0
			let unCheckedCount = 0

			node.children.forEach(n => {
				if (n.checked === true) checkedCount += 1
				else if (n.checked === false) unCheckedCount += 1
			})

			const allChildren = node.children.length
			if (checkedCount === allChildren) node.checked = true
			else if (unCheckedCount === allChildren) node.checked = false
			else node.checked = 'not-completed'
		}
		if (node.parent) updateParentNodes(node.parent)
		return node
	}

	return <TreeSelectContext.Provider value={{ data, selected, setData, handleChange, initTree }}>{children}</TreeSelectContext.Provider>
}

export { TreeSelectContext, TreeContextProvider }
