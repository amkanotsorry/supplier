import React, { useContext } from 'react'
import { TreeNode } from './types'
import { TreeSelectContext } from './Context'

type Props = {
	node: TreeNode
}

const SelectedItem = (props: Props) => {
	const { node } = props
	const { handleChange } = useContext(TreeSelectContext)

	const onClickX = () => handleChange?.(node.value, false)
	return (
		<div className="pl-2 pr-1 py-1 bg-white rounded-lg shadow justify-start items-center gap-1 inline-flex ">
			<div>{node.title}</div>

			<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none" className="cursor-pointer" onClick={onClickX}>
				<path d="M12.75 5.25L5.25 12.75M12.75 12.75L5.25 5.25" stroke="#9BA2AA" strokeWidth="2" strokeLinecap="round" />
			</svg>
		</div>
	)
}

export default SelectedItem
