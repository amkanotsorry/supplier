'use client'
import TreeItem from './TreeItem'

import React, { useState } from 'react'
import { TreeNode } from './types'

type Props = {
	node: TreeNode
}

const TreeGroupItem = (props: Props) => {
	const { node } = props
	const [isExpanded, setIsExpanded] = useState(false)

	return (
		<>
			<TreeItem node={node} isExpanded={isExpanded} toggleExpand={() => setIsExpanded(!isExpanded)} />
			{node.children?.length && isExpanded ? (
				<div className="transition-all ml-7 flex flex-col ">
					{node.children?.map(child => (
						<TreeGroupItem node={child} key={child.value} />
					))}
				</div>
			) : null}
		</>
	)
}

export default TreeGroupItem
