import { useContext } from 'react'
import { TreeNode } from './types'
import { TreeSelectContext } from './Context'
import Image from 'next/image'
import { CheckBoxIcon, UnCheckBoxIcon, CheckNotComplete } from '@/src/assets/icons'

const TreeItem = (props: { node: TreeNode; isExpanded: boolean; toggleExpand: () => void }) => {
	const { node, isExpanded, toggleExpand } = props

	const { handleChange, data } = useContext(TreeSelectContext)
	// console.log('TreeItem data: ', data)

	return (
		<div className="w-100 h-10 px-3 py-1.5 rounded-xl justify-start items-center gap-3 inline-flex">
			<div className="w-3 h-3 flex justify-center items-center">
				{Array.isArray(node.children) && node.children?.length > 0 && (
					<div className={`cursor-pointer transition-transform  ${isExpanded ? 'rotate-90' : ''}`} onClick={toggleExpand}>
						<svg xmlns="http://www.w3.org/2000/svg" width="10" height="12" viewBox="0 0 10 12" fill="none">
							<path
								d="M1.90039 1.5999L8.10039 5.9999L1.90039 10.3999L1.90039 1.5999Z"
								fill="#494C50"
								stroke="#494C50"
								strokeWidth="2"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
						</svg>
					</div>
				)}
			</div>
			<div className="grow shrink basis-0 h-6 justify-start items-center gap-2 flex">
				<div className="w-5 h-5 relative" onClick={() => handleChange?.(node.value, Boolean(!node.checked))}>
					{node.checked === true && <CheckBoxIcon />}
					{node.checked === 'not-completed' && <CheckNotComplete />}
					{!node.checked && <UnCheckBoxIcon />}
				</div>
				<div className="text-zinc-900 text-base font-medium leading-relaxed">{node.title}</div>
			</div>
		</div>
	)
}

export default TreeItem
