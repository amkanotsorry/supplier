'use client'
import { useContext, useEffect } from 'react'
import { TreeSelectContext } from './Context'
import SelectedItem from './SelectedItem'
import TreeGroupItem from './TreeGroupItem'
import { TreeNodeProp } from './types'

type Props = {
	data: TreeNodeProp[]
	//eslint-disable-next-line
	onChange: (value: string[]) => void
}

const TreeSelectContent = (props: Props) => {
	// const { onChange } = props
	const { data, selected, initTree } = useContext(TreeSelectContext)

	useEffect(() => {
		if (props.data.length) initTree?.(props.data)
		//eslint-disable-next-line
	}, [props.data])

	// useEffect(() => {
	// 	// onChange(selected.map(node => node.value))
	// 	console.log('selected', selected)
	// }, [selected])

	return (
		<div>
			<div className="w-full min-h-14 flex-col justify-start items-start gap-1 inline-flex">
				<div className="self-stretch px-2 py-1 bg-gray-50 rounded-xl justify-start items-center inline-flex">
					<div className="grow flex-wrap basis-0 min-h-11 px-2 gap-2 justify-start items-center inline-flex">
						{selected.length ? (
							selected.map(node => <SelectedItem key={node.value} node={node} />)
						) : (
							<div className="text-gray-400 text-base font-normal leading-relaxed">Choose product type</div>
						)}
					</div>
					<div className="w-10 self-stretch p-2 justify-center items-center flex ">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
							<path d="M16.7998 9.5999L11.9998 14.3999L7.19981 9.5999" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
						</svg>
					</div>
				</div>
			</div>

			<div className="w-full h-full mt-2 p-3 rounded-xl shadow border border-gray-100 flex-col justify-start items-start inline-flex">
				<div className="flex flex-col">
					{data.map(type => (
						<TreeGroupItem node={type} key={type.value} />
					))}
				</div>
			</div>
		</div>
	)
}

export default TreeSelectContent
