'use client'
import { TreeContextProvider } from './Context'
import TreeSelectContent from './TreeSelectContent'
import { TreeNodeProp } from './types'

type Props = {
	data: TreeNodeProp[]
	onChange: (value: string[]) => void
}

const TreeSelect = ({ data, onChange }: Props) => {
	return (
		<TreeContextProvider onChange={onChange}>
			<TreeSelectContent data={data} onChange={onChange} />
		</TreeContextProvider>
	)
}

export default TreeSelect
