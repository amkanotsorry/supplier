export class TreeNode {
	value: string
	title: string
	checked?: boolean | 'not-completed'
	children?: TreeNode[]
	parent?: TreeNode

	constructor(title: string, value: string, checked?: boolean) {
		this.title = title
		this.value = value
		this.children = []
		this.checked = checked ?? false
	}

	addChild(childNode: TreeNode) {
		childNode.parent = this
		this.children?.push(childNode)
	}
}

interface TreeNodeProp {
	value: string
	title: string
	children?: TreeNodeProp[]
}

export type { TreeNodeProp }
