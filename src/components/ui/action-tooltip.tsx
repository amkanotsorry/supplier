'use client'
import React from 'react'
import { Tooltip, TooltipContent, TooltipProvider, TooltipTrigger } from './tooltip'

interface ActionTooltipProps {
	label: string
	children: React.ReactNode
	side?: 'top' | 'right' | 'bottom' | 'left'
	align?: 'start' | 'center' | 'end'
	disabled?: boolean
}

export const ActionTooltip = ({ label, children, side, align, disabled = false }: ActionTooltipProps) => {
	return (
		<TooltipProvider>
			{!disabled ? (
				<Tooltip delayDuration={50}>
					<TooltipTrigger asChild>{children}</TooltipTrigger>

					<TooltipContent side={side} align={align}>
						<p className="font-semibold text-sm capitalize">{label.toLowerCase()}</p>
					</TooltipContent>
				</Tooltip>
			) : (
				children
			)}
		</TooltipProvider>
	)
}
