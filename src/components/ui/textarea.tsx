import * as React from 'react'

import { cn } from '@/src/lib/utils'

export interface TextareaProps extends React.TextareaHTMLAttributes<HTMLTextAreaElement> {}

const Textarea = React.forwardRef<HTMLTextAreaElement, TextareaProps>(({ className, ...props }, ref) => {
	return (
		<textarea
			className={cn(
				'flex min-h-[100px] w-full py-2 px-4 rounded-xl bg-[#f8f9fa] placeholder:[#9BA2AA] focus-visible:outline-none disabled:cursor-not-allowed disabled:opacity-50 text-base',
				className
			)}
			ref={ref}
			{...props}
		/>
	)
})
Textarea.displayName = 'Textarea'

export { Textarea }
