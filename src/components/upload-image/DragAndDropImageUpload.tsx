'use client'
import { X } from 'lucide-react'
import React, { useState } from 'react'

type Props = {
	value: File[]
	onChange: (value: File[]) => void
	name: string
	disabled?: boolean
}

const DragAndDropImageUpload = React.forwardRef<HTMLDivElement, Props>((props: Props, ref) => {
	const { value: fileList, onChange: setFileList, name } = props
	const [dragging, setDragging] = useState(false)

	const handleDragEnter = (e: React.DragEvent<HTMLDivElement>) => {
		e.preventDefault()
		setDragging(true)
	}

	const handleDragLeave = () => {
		setDragging(false)
	}

	const handleDrop = (e: React.DragEvent<HTMLDivElement>) => {
		e.preventDefault()
		setDragging(false)

		const files = e.dataTransfer.files

		if (files.length === 1) {
			const file = files[0]
			const fileListClone = [...fileList, file]
			setFileList(fileListClone)
		}
	}

	const removeFile = (index: number) => {
		const newFileList = [...fileList]
		newFileList.splice(index, 1)
		setFileList(newFileList)
	}

	return (
		<div ref={ref}>
			<div
				onClick={() => document.getElementById(name)?.click()}
				className={`w-full h-32 px-6 py-8 bg-white rounded-xl border border-dashed cursor-pointer flex-col justify-start items-center gap-6 inline-flex ${
					dragging ? 'border-blue-500 bg-slate-50' : 'border-zinc-200'
				}`}
				onDragEnter={handleDragEnter}
				onDragOver={e => e.preventDefault()}
				onDragLeave={handleDragLeave}
				onDrop={handleDrop}>
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<path
						d="M2.89328 19.0929C2.50276 19.4834 2.50276 20.1166 2.89328 20.5071C3.28381 20.8977 3.91697 20.8977 4.3075 20.5071L2.89328 19.0929ZM8.40039 15L9.1075 14.2929C8.71697 13.9024 8.08381 13.9024 7.69328 14.2929L8.40039 15ZM10.8004 17.4L10.0933 18.1071C10.4838 18.4977 11.117 18.4977 11.5075 18.1071L10.8004 17.4ZM16.2004 12L16.9075 11.2929C16.517 10.9024 15.8838 10.9024 15.4933 11.2929L16.2004 12ZM20.2933 17.5071C20.6838 17.8977 21.317 17.8977 21.7075 17.5071C22.098 17.1166 22.098 16.4834 21.7075 16.0929L20.2933 17.5071ZM4.3075 20.5071L9.1075 15.7071L7.69328 14.2929L2.89328 19.0929L4.3075 20.5071ZM7.69328 15.7071L10.0933 18.1071L11.5075 16.6929L9.1075 14.2929L7.69328 15.7071ZM11.5075 18.1071L16.9075 12.7071L15.4933 11.2929L10.0933 16.6929L11.5075 18.1071ZM15.4933 12.7071L20.2933 17.5071L21.7075 16.0929L16.9075 11.2929L15.4933 12.7071ZM6.00039 3.40002H18.0004V1.40002H6.00039V3.40002ZM20.6004 6.00002V18H22.6004V6.00002H20.6004ZM18.0004 20.6H6.00039V22.6H18.0004V20.6ZM3.40039 18V6.00002H1.40039V18H3.40039ZM6.00039 20.6C4.56445 20.6 3.40039 19.436 3.40039 18H1.40039C1.40039 20.5405 3.45988 22.6 6.00039 22.6V20.6ZM20.6004 18C20.6004 19.436 19.4363 20.6 18.0004 20.6V22.6C20.5409 22.6 22.6004 20.5405 22.6004 18H20.6004ZM18.0004 3.40002C19.4363 3.40002 20.6004 4.56408 20.6004 6.00002H22.6004C22.6004 3.45951 20.5409 1.40002 18.0004 1.40002V3.40002ZM6.00039 1.40002C3.45988 1.40002 1.40039 3.45951 1.40039 6.00002H3.40039C3.40039 4.56408 4.56445 3.40002 6.00039 3.40002V1.40002ZM8.60039 7.80002C8.60039 8.24185 8.24222 8.60002 7.80039 8.60002V10.6C9.34679 10.6 10.6004 9.34642 10.6004 7.80002H8.60039ZM7.80039 8.60002C7.35856 8.60002 7.00039 8.24185 7.00039 7.80002H5.00039C5.00039 9.34642 6.25399 10.6 7.80039 10.6V8.60002ZM7.00039 7.80002C7.00039 7.3582 7.35856 7.00002 7.80039 7.00002V5.00002C6.25399 5.00002 5.00039 6.25363 5.00039 7.80002H7.00039ZM7.80039 7.00002C8.24222 7.00002 8.60039 7.3582 8.60039 7.80002H10.6004C10.6004 6.25363 9.34679 5.00002 7.80039 5.00002V7.00002Z"
						fill="black"
					/>
				</svg>
				<div className="self-stretch h-5 flex-col justify-start items-center gap-1.5 flex">
					<div className="self-stretch text-center">
						<span className="text-blue-500 text-sm font-medium  leading-snug">Click to upload</span>
						<span className="text-black text-sm font-medium  leading-snug"> </span>
						<span className="text-neutral-500 text-sm leading-snug">or drag and drop</span>
					</div>
				</div>
			</div>

			<input
				id={name}
				multiple
				type="file"
				accept="image/*" // Specify the allowed file types
				className="hidden"
				onChange={e => {
					const files: File[] = e.target.files ? Array.from(e.target.files) : []
					const fileListClone = [...fileList, ...files]
					setFileList(fileListClone)
				}}
			/>

			<div className="mt-6 flex flex-col gap-2">
				{fileList.map((file, i) => (
					<div key={file.lastModified} className="flex p-3 gap-3 rounded-xl border border-gray-100">
						<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" fill="none">
							<path
								d="M6 4.00002C6 3.26365 6.59696 2.66669 7.33333 2.66669H21L26 7.66669V28C26 28.7364 25.4031 29.3334 24.6667 29.3334H7.33333C6.59696 29.3334 6 28.7364 6 28V4.00002Z"
								fill="#19B86B"
							/>
							<path d="M21 2.66669L26 7.66669H22.3333C21.5969 7.66669 21 7.06973 21 6.33335V2.66669Z" fill="#8BDCB5" />
							<path
								fillRule="evenodd"
								clipRule="evenodd"
								d="M8.5 5.83335C8.5 5.46517 8.79848 5.16669 9.16667 5.16669H12.8333C13.2015 5.16669 13.5 5.46517 13.5 5.83335V9.50002C13.5 9.86821 13.2015 10.1667 12.8333 10.1667H9.16667C8.79848 10.1667 8.5 9.86821 8.5 9.50002V5.83335ZM9.33333 7.66669L11 8.50002L11.8333 7.66669L12.6667 8.50002V9.00002C12.6667 9.18411 12.5174 9.33335 12.3333 9.33335H9.66667C9.48257 9.33335 9.33333 9.18411 9.33333 9.00002V7.66669ZM11.8333 7.25002C12.0635 7.25002 12.25 7.06347 12.25 6.83335C12.25 6.60323 12.0635 6.41669 11.8333 6.41669C11.6032 6.41669 11.4167 6.60323 11.4167 6.83335C11.4167 7.06347 11.6032 7.25002 11.8333 7.25002Z"
								fill="white"
							/>
							<path
								d="M10.1846 22.4453H11.0391V24.8842C11.0391 25.1105 10.9862 25.3124 10.8804 25.4897C10.7762 25.6672 10.6297 25.8046 10.4409 25.9024C10.2537 26 10.0446 26.0488 9.81348 26.0488C9.4261 26.0488 9.125 25.9512 8.91016 25.7558C8.6953 25.5605 8.58789 25.2838 8.58789 24.9257H9.44726C9.44726 25.0885 9.47493 25.2065 9.53028 25.2797C9.58724 25.353 9.68164 25.3896 9.81348 25.3896C9.93229 25.3896 10.0234 25.3457 10.0869 25.2578C10.152 25.1682 10.1846 25.0437 10.1846 24.8842V22.4453ZM12.438 24.794V26H11.5811V22.4453H12.9995C13.2713 22.4453 13.5113 22.4957 13.7197 22.5966C13.9297 22.696 14.0916 22.8384 14.2056 23.0238C14.3211 23.2078 14.3789 23.4169 14.3789 23.6513C14.3789 23.998 14.2544 24.2756 14.0053 24.4838C13.758 24.6905 13.4179 24.794 12.9849 24.794H12.438ZM12.438 24.1322H12.9995C13.1655 24.1322 13.2917 24.0908 13.3779 24.0078C13.4659 23.9248 13.5097 23.8076 13.5097 23.6562C13.5097 23.4902 13.4651 23.3576 13.3755 23.2582C13.286 23.159 13.1639 23.1085 13.0093 23.1069H12.438V24.1322ZM17.748 25.5605C17.6163 25.707 17.4233 25.825 17.1695 25.9145C16.9155 26.0041 16.6372 26.0488 16.3345 26.0488C15.8689 26.0488 15.4971 25.9064 15.2188 25.6216C14.9404 25.3368 14.7915 24.9404 14.772 24.4326L14.7695 24.125C14.7695 23.775 14.8313 23.4698 14.9551 23.2094C15.0788 22.9474 15.2553 22.7464 15.4848 22.6064C15.716 22.4648 15.9829 22.394 16.2856 22.394C16.7284 22.394 17.0717 22.4957 17.3159 22.6992C17.5617 22.901 17.7049 23.2029 17.7456 23.6049H16.9204C16.8911 23.4064 16.8276 23.2648 16.73 23.1801C16.6323 23.0956 16.494 23.0532 16.3149 23.0532C16.1001 23.0532 15.9341 23.1444 15.8169 23.3267C15.6997 23.5089 15.6403 23.7693 15.6387 24.1078V24.3228C15.6387 24.6776 15.6989 24.9445 15.8193 25.1236C15.9415 25.3009 16.1327 25.3896 16.3931 25.3896C16.616 25.3896 16.782 25.34 16.8911 25.2406V24.6889H16.2955V24.1005H17.748V25.5605Z"
								fill="white"
							/>
						</svg>
						<div>
							<div className="text-black text-base font-medium leading-relaxed">{file.name}</div>
							<div className="text-neutral-500 text-sm leading-snug">{`${Math.floor(file.size / 1000)} KB`}</div>
						</div>
						<div className="grow items-end flex flex-col flex-end gap-1.5">
							<X className="cursor-pointer" onClick={() => removeFile(i)} />

							<div className="flex gap-1.5">
								<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
									<g clipPath="url(#clip0_5474_9812)">
										<path
											d="M10 0C4.48578 0 0 4.48578 0 10C0 15.5142 4.48578 20 10 20C15.5142 20 20 15.5142 20 10C20 4.48578 15.5142 0 10 0Z"
											fill="#17B26A"
										/>
										<path
											d="M15.0692 7.88086L9.6525 13.2974C9.48999 13.4599 9.27667 13.5417 9.06335 13.5417C8.85004 13.5417 8.63672 13.4599 8.47421 13.2974L5.76593 10.5891C5.44 10.2634 5.44 9.73663 5.76593 9.41086C6.09171 9.08493 6.61829 9.08493 6.94421 9.41086L9.06335 11.53L13.8909 6.70258C14.2167 6.37665 14.7433 6.37665 15.0692 6.70258C15.395 7.02835 15.395 7.55493 15.0692 7.88086Z"
											fill="#FAFAFA"
										/>
									</g>
									<defs>
										<clipPath id="clip0_5474_9812">
											<rect width="20" height="20" fill="white" />
										</clipPath>
									</defs>
								</svg>
								Complete
							</div>
						</div>
					</div>
				))}
			</div>
		</div>
	)
})

DragAndDropImageUpload.displayName = 'Drag and drop image upload'
export default DragAndDropImageUpload
