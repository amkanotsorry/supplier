/* eslint-disable @next/next/no-img-element */
'use client'
import { Eye, Trash } from 'lucide-react'
import React, { useState } from 'react'

type Props = {
	value?: File | string
	//eslint-disable-next-line
	onChange: (value?: File) => void
	name: string
	disabled?: boolean
	setFileAsString?: (value?: string) => void
}

const UploadImage = React.forwardRef<HTMLDivElement, Props>(({ ...props }, ref) => {
	const { value, onChange, name, setFileAsString } = props
	const [isHovered, setIsHovered] = useState(false)
	const [imgPrev, setImgPrev] = useState('')

	const handleClickUpload = () => {
		document.getElementById(name)?.click()
	}

	const handleRemove = () => onChange()

	const fileToURL = (file: File) => {
		if (file) {
			const reader = new FileReader()
			reader.onload = function () {
				const result = reader.result
				if (typeof result === 'string') {
					return result
				}
			}
			reader.readAsDataURL(file)
		}
	}

	return (
		<div ref={ref}>
			{!value ? (
				<div
					className="w-28 h-28 bg-white cursor-pointer rounded-xl border border-dashed border-zinc-200 flex flex-col justify-center items-center"
					onClick={handleClickUpload}>
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
						<path
							d="M2.89328 19.0929C2.50276 19.4834 2.50276 20.1166 2.89328 20.5071C3.28381 20.8977 3.91697 20.8977 4.3075 20.5071L2.89328 19.0929ZM8.40039 15L9.1075 14.2929C8.71697 13.9024 8.08381 13.9024 7.69328 14.2929L8.40039 15ZM10.8004 17.4L10.0933 18.1071C10.4838 18.4977 11.117 18.4977 11.5075 18.1071L10.8004 17.4ZM16.2004 12L16.9075 11.2929C16.517 10.9024 15.8838 10.9024 15.4933 11.2929L16.2004 12ZM20.2933 17.5071C20.6838 17.8977 21.317 17.8977 21.7075 17.5071C22.098 17.1166 22.098 16.4834 21.7075 16.0929L20.2933 17.5071ZM4.3075 20.5071L9.1075 15.7071L7.69328 14.2929L2.89328 19.0929L4.3075 20.5071ZM7.69328 15.7071L10.0933 18.1071L11.5075 16.6929L9.1075 14.2929L7.69328 15.7071ZM11.5075 18.1071L16.9075 12.7071L15.4933 11.2929L10.0933 16.6929L11.5075 18.1071ZM15.4933 12.7071L20.2933 17.5071L21.7075 16.0929L16.9075 11.2929L15.4933 12.7071ZM6.00039 3.40002H18.0004V1.40002H6.00039V3.40002ZM20.6004 6.00002V18H22.6004V6.00002H20.6004ZM18.0004 20.6H6.00039V22.6H18.0004V20.6ZM3.40039 18V6.00002H1.40039V18H3.40039ZM6.00039 20.6C4.56445 20.6 3.40039 19.436 3.40039 18H1.40039C1.40039 20.5405 3.45988 22.6 6.00039 22.6V20.6ZM20.6004 18C20.6004 19.436 19.4363 20.6 18.0004 20.6V22.6C20.5409 22.6 22.6004 20.5405 22.6004 18H20.6004ZM18.0004 3.40002C19.4363 3.40002 20.6004 4.56408 20.6004 6.00002H22.6004C22.6004 3.45951 20.5409 1.40002 18.0004 1.40002V3.40002ZM6.00039 1.40002C3.45988 1.40002 1.40039 3.45951 1.40039 6.00002H3.40039C3.40039 4.56408 4.56445 3.40002 6.00039 3.40002V1.40002ZM8.60039 7.80002C8.60039 8.24185 8.24222 8.60002 7.80039 8.60002V10.6C9.34679 10.6 10.6004 9.34642 10.6004 7.80002H8.60039ZM7.80039 8.60002C7.35856 8.60002 7.00039 8.24185 7.00039 7.80002H5.00039C5.00039 9.34642 6.25399 10.6 7.80039 10.6V8.60002ZM7.00039 7.80002C7.00039 7.3582 7.35856 7.00002 7.80039 7.00002V5.00002C6.25399 5.00002 5.00039 6.25363 5.00039 7.80002H7.00039ZM7.80039 7.00002C8.24222 7.00002 8.60039 7.3582 8.60039 7.80002H10.6004C10.6004 6.25363 9.34679 5.00002 7.80039 5.00002V7.00002Z"
							fill="black"
						/>
					</svg>
					<div className="mt-2 text-center text-black text-xs font-medium font-['Inter'] leading-none">Upload</div>
				</div>
			) : (
				<div className="w-28 h-28 relative border rounded-xl" onMouseEnter={() => setIsHovered(true)} onMouseLeave={() => setIsHovered(false)}>
					{typeof value === 'string' && <img alt="img" className="absolute z-10 w-28 h-28 rounded-xl object-contain " src={value} />}
					{imgPrev && <img alt="img" className="absolute z-10 w-28 h-28 rounded-xl object-contain " src={imgPrev} />}

					{isHovered && (
						<div className="absolute z-20 flex justify-center items-center gap-3 w-28 h-28 rounded-xl bg-gray-500/[.4] text-white">
							<Eye />
							<Trash onClick={handleRemove} />
						</div>
					)}
				</div>
			)}

			<input
				id={name}
				name={name}
				type="file"
				accept="image/*"
				onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
					const file = e.target.files?.[0]

					if (file) {
						onChange(file)
						const reader = new FileReader()
						reader.onload = function () {
							const result = reader.result
							if (typeof result === 'string') {
								setImgPrev(result)
								if (setFileAsString) setFileAsString(result)
							}
						}
						reader.readAsDataURL(file)
					}
				}}
				className="hidden"
			/>
		</div>
	)
})

UploadImage.displayName = 'Upload image'
export default UploadImage
