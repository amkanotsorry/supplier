import * as React from 'react'

export interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
	label: string
}

const XInput = React.forwardRef<HTMLInputElement, InputProps>(({ ...props }, ref) => {
	const handleFocus = (event: React.FocusEvent<HTMLInputElement>) => {
		event.currentTarget.nextElementSibling?.classList.add('translate-y-[9px]')
		event.currentTarget.nextElementSibling?.classList.add('text-xs')
	}

	const handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
		if (!event.currentTarget.value) {
			event.currentTarget.nextElementSibling?.classList.remove('translate-y-[9px]')
			event.currentTarget.nextElementSibling?.classList.remove('text-xs')
		}
	}

	if ('placeholder' in props) delete props.placeholder

	return (
		<div className="relative h-14 w-full">
			<input
				type="text"
				className="w-full bg-gray-50 rounded-xl h-14 px-4 pt-[22px] border-0 focus:outline-none transition-transform"
				onFocus={handleFocus}
				onBlur={handleBlur}
				ref={ref}
				{...props}
			/>
			<label
				htmlFor={props.id}
				className="absolute text-gray-400 text-base font-normal left-4 transform translate-y-[16px] transition-transform duration-200 ease-in-out">
				{props.label}
			</label>
		</div>
		// <div className="relative mb-4 h-14 ">
		// 	<input
		// 		type={type}
		// 		className={cn(
		// 			'flex h-14 w-full rounded-md border-0 bg-gray-50 px-4 pt-5 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50 transition text-base leading-',
		// 			className
		// 		)}
		// 		ref={ref}
		// 		{...props}
		// 	/>
		// 	<label
		// 		htmlFor={props.id}
		// 		className="text-gray-400 text-xs leading-4.5 absolute top-4 left-4 -translate-y-2 transition-transform transform origin-left ">
		// 		{props.label}
		// 	</label>
		// </div>
	)
})
XInput.displayName = 'Input'

export { XInput }
