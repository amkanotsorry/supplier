import { cn } from '@/src/lib/utils'
import * as SelectPrimitive from '@radix-ui/react-select'
import { ChevronDown } from 'lucide-react'
import React from 'react'

type XSelectTriggerProps = {
	hasValue: boolean
	placeholder: String | undefined
} & React.ComponentPropsWithoutRef<typeof SelectPrimitive.Trigger>

const XSelectTrigger = React.forwardRef<React.ElementRef<typeof SelectPrimitive.Trigger>, XSelectTriggerProps>(({ className, children, hasValue, placeholder, ...props }, ref) => {
	const handleFocus = (event: React.FocusEvent<HTMLButtonElement>) => {
		event.currentTarget.nextElementSibling?.classList.add('-translate-y-[46px]')
		event.currentTarget.nextElementSibling?.classList.add('text-xs')
	}

	const handleBlur = (event: React.FocusEvent<HTMLButtonElement>) => {
		if (!hasValue) {
			event.currentTarget.nextElementSibling?.classList.remove('-translate-y-[46px]')
			event.currentTarget.nextElementSibling?.classList.remove('text-xs')
		}
	}

	return (
		<div className="relative h-14 w-full ">
			<SelectPrimitive.Trigger
				ref={ref}
				className={cn(
					`flex h-14 w-full items-center justify-between rounded-md bg-gray-50 border-0 px-3 py-2 text-sm ring-offset-background placeholder:text-muted-foreground focus:outline-none focus:ring-2 focus:ring-ring focus:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50`,
					className
				)}
				// value={value}
				// onChange={e => setValue(e.target.)}
				onFocus={handleFocus}
				onBlur={handleBlur}
				{...props}>
				<div className="pt-4 pl-1 truncate">{children}</div>
				<SelectPrimitive.Icon asChild>
					<ChevronDown className="h-4 w-4 opacity-50" />
				</SelectPrimitive.Icon>
			</SelectPrimitive.Trigger>
			<label
				htmlFor={props.id}
				className={`
					absolute text-gray-400 text-base font-normal left-4 transform -translate-y-[40px] transition-transform duration-200 ease-in-out ${hasValue ? '-translate-y-[46px] text-xs' : ''}
					`}>
				{placeholder}
			</label>
		</div>
	)
})

XSelectTrigger.displayName = SelectPrimitive.Trigger.displayName

export { XSelectTrigger }
