'use client'
import i18next from 'i18next'
import { initReactI18next } from 'react-i18next'

import en from './locales/en.json'
import mn from './locales/mn.json'

const resources = {
	en,
	mn
}

i18next.use(initReactI18next).init({
	lng: 'mn',
	debug: true,
	resources
})

export default i18next
