'use client'
import { useEffect, useState } from 'react'
import { AddressInfo } from '../app/api/generated/graphql'
import { UseFormReturn } from 'react-hook-form'

interface FetchAddressParam {
	parentCode?: Number
}

export const fetchAddress = async (param?: FetchAddressParam): Promise<AddressInfo[]> => {
	const { parentCode } = param ?? {}
	let body: { cityCode?: Number; districtCode?: Number } = {}

	if (typeof parentCode === 'number') {
		const codeLength = parentCode?.toString().length

		if (codeLength === 2) body.cityCode = parentCode
		else if (codeLength === 4) {
			body.cityCode = Math.floor(parentCode / 100)
			body.districtCode = parentCode
		} else return []
	}

	return fetch(`https://admin-api-dev.pickpack.mn/public/v1.0/location/getList`, {
		method: 'POST',
		cache: 'no-cache',
		body: JSON.stringify(body)
	})
		.then(res => res.json())
		.then(data => {
			if (data.statusCode === 200) {
				return data.result?.map((el: any) => ({
					code: el?.code,
					name: el?.name,
					nameMn: el?.name
				}))
			}

			return []
		})
		.catch(err => {
			console.log('fetchAddress', err)
		})
}

type AddressInput = {
	cityProvince: AddressInfo
	duuregSoum: AddressInfo
	khorooBag: AddressInfo
}

interface Props {
	form: UseFormReturn<any, any, undefined>
	aimagFormKey?: string
	sumFormKey?: string
	bagFormKey?: string
}

export const useAddress = ({ form, aimagFormKey = 'aimagHot', sumFormKey = 'sumDuureg', bagFormKey = 'bagHoroo' }: Props) => {
	const [aimagHotOptions, setAimagHotOptions] = useState<AddressInfo[]>([])
	const [sumDuuregOpns, setSumDuuregOptns] = useState<AddressInfo[]>([])
	const [bagHorooOpns, setBagHoroogOptns] = useState<AddressInfo[]>([])

	const [address, setAddress] = useState<AddressInput>()

	const aimagHotCode = parseInt(form.watch(aimagFormKey))
	const sumDuuregCode = parseInt(form.watch(sumFormKey))
	const bagHorooCode = parseInt(form.watch(bagFormKey))

	useEffect(() => {
		fetchAddress().then(value => {
			if (value?.length > 0) setAimagHotOptions(value)
		})
	}, [])

	useEffect(() => {
		form.resetField(sumFormKey)
		form.resetField(bagFormKey)
		if (aimagHotCode) {
			fetchAddress({ parentCode: aimagHotCode }).then(value => setSumDuuregOptns(value))
		}

		//eslint-disable-next-line
	}, [aimagHotCode])

	useEffect(() => {
		form.resetField(bagFormKey)
		if (sumDuuregCode) fetchAddress({ parentCode: sumDuuregCode }).then(value => setBagHoroogOptns(value))
		//eslint-disable-next-line
	}, [sumDuuregCode])

	useEffect(() => {
		const aimagHot = aimagHotOptions.find(op => op.code === aimagHotCode)
		const sumDuureg = sumDuuregOpns.find(op => op.code === sumDuuregCode)
		const bagHoroo = bagHorooOpns.find(op => op.code === bagHorooCode)

		if (aimagHot && sumDuureg && bagHoroo) {
			setAddress({
				cityProvince: aimagHot,
				duuregSoum: sumDuureg,
				khorooBag: bagHoroo
			})
		}
	}, [aimagHotCode, sumDuuregCode, bagHorooCode, aimagHotOptions, sumDuuregOpns, bagHorooOpns])

	return {
		aimagHotOptions,
		sumDuuregOpns,
		bagHorooOpns,
		address
	}
}
