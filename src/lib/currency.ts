export function convertCurrency(amount: number, currency?: 'tugrik' | 'dollar') {
	return new Intl.NumberFormat().format(amount) + '₮'
}
