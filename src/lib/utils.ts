import { type ClassValue, clsx } from 'clsx'
import { twMerge } from 'tailwind-merge'

export function cn(...inputs: ClassValue[]) {
	return twMerge(clsx(inputs))
}

export function enumToArray<T extends Record<string, string | number>>(obj: T) {
	const keys = Object.keys(obj)
	return keys.map(k => ({ key: k, value: obj[k] }))
}

export function timeAgo(date: Date): string {
	const now: number = Date.now()
	const timeDifference: number = now - new Date(date).getTime()

	const minutes: number = Math.floor(timeDifference / (1000 * 60))
	const hours: number = Math.floor(timeDifference / (1000 * 60 * 60))
	const days: number = Math.floor(timeDifference / (1000 * 60 * 60 * 24))

	if (minutes < 1) {
		return 'A few seconds ago'
	} else if (minutes < 60) {
		return `${minutes} minute${minutes > 1 ? 's' : ''} ago`
	} else if (hours < 24) {
		return `${hours} hour${hours > 1 ? 's' : ''} ago`
	} else {
		return `${days} day${days > 1 ? 's' : ''} ago`
	}
}
