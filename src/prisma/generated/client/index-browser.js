
Object.defineProperty(exports, "__esModule", { value: true });

const {
  Decimal,
  objectEnumValues,
  makeStrictEnum,
  Public,
  detectRuntime,
} = require('./runtime/index-browser')


const Prisma = {}

exports.Prisma = Prisma
exports.$Enums = {}

/**
 * Prisma Client JS version: 5.7.0
 * Query Engine version: 79fb5193cf0a8fdbef536e4b4a159cad677ab1b9
 */
Prisma.prismaVersion = {
  client: "5.7.0",
  engine: "79fb5193cf0a8fdbef536e4b4a159cad677ab1b9"
}

Prisma.PrismaClientKnownRequestError = () => {
  throw new Error(`PrismaClientKnownRequestError is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)};
Prisma.PrismaClientUnknownRequestError = () => {
  throw new Error(`PrismaClientUnknownRequestError is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.PrismaClientRustPanicError = () => {
  throw new Error(`PrismaClientRustPanicError is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.PrismaClientInitializationError = () => {
  throw new Error(`PrismaClientInitializationError is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.PrismaClientValidationError = () => {
  throw new Error(`PrismaClientValidationError is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.NotFoundError = () => {
  throw new Error(`NotFoundError is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.Decimal = Decimal

/**
 * Re-export of sql-template-tag
 */
Prisma.sql = () => {
  throw new Error(`sqltag is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.empty = () => {
  throw new Error(`empty is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.join = () => {
  throw new Error(`join is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.raw = () => {
  throw new Error(`raw is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.validator = Public.validator

/**
* Extensions
*/
Prisma.getExtensionContext = () => {
  throw new Error(`Extensions.getExtensionContext is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.defineExtension = () => {
  throw new Error(`Extensions.defineExtension is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}

/**
 * Shorthand utilities for JSON filtering
 */
Prisma.DbNull = objectEnumValues.instances.DbNull
Prisma.JsonNull = objectEnumValues.instances.JsonNull
Prisma.AnyNull = objectEnumValues.instances.AnyNull

Prisma.NullTypes = {
  DbNull: objectEnumValues.classes.DbNull,
  JsonNull: objectEnumValues.classes.JsonNull,
  AnyNull: objectEnumValues.classes.AnyNull
}

/**
 * Enums
 */

exports.Prisma.AddressesScalarFieldEnum = {
  id: 'id'
};

exports.Prisma.CategoriesScalarFieldEnum = {
  id: 'id',
  v: 'v',
  createdAt: 'createdAt',
  name: 'name',
  permissionIds: 'permissionIds',
  platformId: 'platformId',
  updatedAt: 'updatedAt'
};

exports.Prisma.Certificate_sourcesScalarFieldEnum = {
  id: 'id',
  v: 'v',
  createdAt: 'createdAt',
  title: 'title',
  updatedAt: 'updatedAt'
};

exports.Prisma.Certificate_typesScalarFieldEnum = {
  id: 'id',
  v: 'v',
  createdAt: 'createdAt',
  title: 'title',
  updatedAt: 'updatedAt'
};

exports.Prisma.CertificatesScalarFieldEnum = {
  id: 'id',
  v: 'v',
  createdAt: 'createdAt',
  name: 'name',
  organization: 'organization',
  updatedAt: 'updatedAt'
};

exports.Prisma.CertificationsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  createdAt: 'createdAt',
  entityId: 'entityId',
  expiryDate: 'expiryDate',
  isSpecial: 'isSpecial',
  issueDate: 'issueDate',
  name: 'name',
  organization: 'organization',
  status: 'status',
  updatedAt: 'updatedAt'
};

exports.Prisma.CheckpointScalarFieldEnum = {
  id: 'id'
};

exports.Prisma.CheckpointsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  code: 'code',
  createdAt: 'createdAt',
  name: 'name',
  type: 'type',
  updatedAt: 'updatedAt'
};

exports.Prisma.Component_categoriesScalarFieldEnum = {
  id: 'id',
  v: 'v',
  animalType: 'animalType',
  code: 'code',
  createdAt: 'createdAt',
  name: 'name',
  numberOfPart: 'numberOfPart',
  updatedAt: 'updatedAt'
};

exports.Prisma.ComponentsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  categoryId: 'categoryId',
  createdAt: 'createdAt',
  grossWeight: 'grossWeight',
  level: 'level',
  name: 'name',
  originId: 'originId',
  packageId: 'packageId',
  type: 'type',
  updatedAt: 'updatedAt'
};

exports.Prisma.CorporatesScalarFieldEnum = {
  id: 'id',
  code: 'code'
};

exports.Prisma.CustomcertificatesScalarFieldEnum = {
  id: 'id'
};

exports.Prisma.CustomcertificationtypesScalarFieldEnum = {
  id: 'id'
};

exports.Prisma.EntitiesScalarFieldEnum = {
  id: 'id',
  v: 'v',
  areasOfActivity: 'areasOfActivity',
  countryCode: 'countryCode',
  createdAt: 'createdAt',
  createdUserId: 'createdUserId',
  entityBuyerId: 'entityBuyerId',
  entityCompanyId: 'entityCompanyId',
  entitySupplierId: 'entitySupplierId',
  firstName: 'firstName',
  isoCode: 'isoCode',
  lastName: 'lastName',
  name: 'name',
  phoneNumber: 'phoneNumber',
  registrationNumber: 'registrationNumber',
  type: 'type',
  updatedAt: 'updatedAt'
};

exports.Prisma.Entity_buyersScalarFieldEnum = {
  id: 'id',
  v: 'v',
  buyerId: 'buyerId',
  createdAt: 'createdAt',
  entityId: 'entityId',
  prefferedCategories: 'prefferedCategories',
  updatedAt: 'updatedAt'
};

exports.Prisma.Entity_companiesScalarFieldEnum = {
  id: 'id',
  v: 'v',
  createdAt: 'createdAt',
  description: 'description',
  entityId: 'entityId',
  incorporatedDate: 'incorporatedDate',
  updatedAt: 'updatedAt'
};

exports.Prisma.Entity_review_infosScalarFieldEnum = {
  id: 'id'
};

exports.Prisma.Entity_supplier_companiesScalarFieldEnum = {
  id: 'id'
};

exports.Prisma.Entity_supplier_individualsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  entitySupplierId: 'entitySupplierId'
};

exports.Prisma.Entity_suppliersScalarFieldEnum = {
  id: 'id',
  v: 'v',
  certificationIds: 'certificationIds',
  createdAt: 'createdAt',
  deliveryType: 'deliveryType',
  entityId: 'entityId',
  entitySupplierIndividualId: 'entitySupplierIndividualId',
  isDomestic: 'isDomestic',
  isInternational: 'isInternational',
  productTypeIds: 'productTypeIds',
  reviewId: 'reviewId',
  status: 'status',
  stockLocationIds: 'stockLocationIds',
  supplierType: 'supplierType',
  updatedAt: 'updatedAt'
};

exports.Prisma.FillupordersScalarFieldEnum = {
  id: 'id',
  v: 'v',
  animalType: 'animalType',
  code: 'code',
  componentCategory: 'componentCategory',
  componentCategoryName: 'componentCategoryName',
  createdAt: 'createdAt',
  earTag: 'earTag',
  isMain: 'isMain',
  maxQty: 'maxQty',
  parentCode: 'parentCode',
  pointOfFillUp: 'pointOfFillUp',
  pointOfFillUpName: 'pointOfFillUpName',
  quantity: 'quantity',
  status: 'status',
  transportStarted: 'transportStarted',
  transportationNumber: 'transportationNumber',
  updatedAt: 'updatedAt'
};

exports.Prisma.MegsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  createdAt: 'createdAt',
  createdUser: 'createdUser',
  earTag: 'earTag',
  megNumber: 'megNumber',
  megNumberMP: 'megNumberMP',
  updatedAt: 'updatedAt'
};

exports.Prisma.MenusScalarFieldEnum = {
  id: 'id',
  v: 'v',
  code: 'code',
  createdAt: 'createdAt',
  icon: 'icon',
  isActive: 'isActive',
  isParent: 'isParent',
  name: 'name',
  order: 'order',
  permissionCode: 'permissionCode',
  permissionType: 'permissionType',
  platformId: 'platformId',
  updatedAt: 'updatedAt'
};

exports.Prisma.Offer_draftsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  additionalDocuments: 'additionalDocuments',
  composition: 'composition',
  createdAt: 'createdAt',
  createdUserId: 'createdUserId',
  isRefundable: 'isRefundable',
  isReturnable: 'isReturnable',
  isSampleDelivery: 'isSampleDelivery',
  paymentMethods: 'paymentMethods',
  supplierId: 'supplierId',
  updatedAt: 'updatedAt',
  variants: 'variants'
};

exports.Prisma.OffersScalarFieldEnum = {
  id: 'id',
  v: 'v',
  additionalDocuments: 'additionalDocuments',
  code: 'code',
  composition: 'composition',
  createdAt: 'createdAt',
  createdUserId: 'createdUserId',
  deliveryType: 'deliveryType',
  hasTemperatureControl: 'hasTemperatureControl',
  isRefundable: 'isRefundable',
  isReturnable: 'isReturnable',
  isSampleDelivery: 'isSampleDelivery',
  isSensitiveToMoisture: 'isSensitiveToMoisture',
  isShippableThroughAir: 'isShippableThroughAir',
  isVerified: 'isVerified',
  marketCountry: 'marketCountry',
  marketType: 'marketType',
  numberOfCounterOffer: 'numberOfCounterOffer',
  numberOfOrderRequest: 'numberOfOrderRequest',
  offerEndDate: 'offerEndDate',
  offerStartDate: 'offerStartDate',
  packageDetail: 'packageDetail',
  productId: 'productId',
  productName: 'productName',
  status: 'status',
  supplierId: 'supplierId',
  tags: 'tags',
  updatedAt: 'updatedAt',
  warrantyPeriod: 'warrantyPeriod',
  warrantyPeriodType: 'warrantyPeriodType'
};

exports.Prisma.OrdersScalarFieldEnum = {
  id: 'id',
  v: 'v',
  buyerId: 'buyerId',
  createdAt: 'createdAt',
  isVerified: 'isVerified',
  offerCode: 'offerCode',
  productName: 'productName',
  status: 'status',
  updatedAt: 'updatedAt'
};

exports.Prisma.OriginsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  avgTemperature: 'avgTemperature',
  bioSecurityFacilityStartDate: 'bioSecurityFacilityStartDate',
  coldStorageTemperature: 'coldStorageTemperature',
  createdAt: 'createdAt',
  earTag: 'earTag',
  megCertificate: 'megCertificate',
  megCertificateDate: 'megCertificateDate',
  receivedDate: 'receivedDate',
  receivingLocation: 'receivingLocation',
  slaughterhouse: 'slaughterhouse',
  slaughterhouseDate: 'slaughterhouseDate',
  transportStartedDate: 'transportStartedDate',
  updatedAt: 'updatedAt'
};

exports.Prisma.Payment_methodsScalarFieldEnum = {
  id: 'id',
  acountNumber: 'acountNumber'
};

exports.Prisma.Pending_invitesScalarFieldEnum = {
  id: 'id',
  email: 'email'
};

exports.Prisma.PendinginvitesScalarFieldEnum = {
  id: 'id',
  v: 'v',
  createdAt: 'createdAt',
  email: 'email',
  entityId: 'entityId',
  roleId: 'roleId',
  updatedAt: 'updatedAt'
};

exports.Prisma.PermissionsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  code: 'code',
  createdAt: 'createdAt',
  description: 'description',
  isParent: 'isParent',
  name: 'name',
  parentId: 'parentId',
  permissionType: 'permissionType',
  platformId: 'platformId',
  updatedAt: 'updatedAt'
};

exports.Prisma.PlatformsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  code: 'code',
  createdAt: 'createdAt',
  name: 'name',
  updatedAt: 'updatedAt'
};

exports.Prisma.Product_logsScalarFieldEnum = {
  id: 'id'
};

exports.Prisma.Product_metasScalarFieldEnum = {
  id: 'id'
};

exports.Prisma.Product_type_logsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  createdAt: 'createdAt',
  productTypeId: 'productTypeId',
  updatedAt: 'updatedAt'
};

exports.Prisma.Product_typesScalarFieldEnum = {
  id: 'id',
  v: 'v',
  code: 'code',
  createdAt: 'createdAt',
  isParent: 'isParent',
  name: 'name',
  parentId: 'parentId',
  updatedAt: 'updatedAt'
};

exports.Prisma.ProductlogsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  createdAt: 'createdAt',
  employeeEmail: 'employeeEmail',
  ipAddress: 'ipAddress',
  productId: 'productId',
  updatedAt: 'updatedAt'
};

exports.Prisma.ProductmetasScalarFieldEnum = {
  id: 'id',
  v: 'v',
  attributes: 'attributes',
  createdAt: 'createdAt',
  isActive: 'isActive',
  name: 'name',
  productCategoryId: 'productCategoryId',
  unit: 'unit',
  unitVolume: 'unitVolume',
  unitWeight: 'unitWeight',
  updatedAt: 'updatedAt'
};

exports.Prisma.ProductsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  composition: 'composition',
  createdAt: 'createdAt',
  name: 'name',
  productMetaId: 'productMetaId',
  test_report: 'test_report',
  updatedAt: 'updatedAt',
  volume: 'volume',
  weight: 'weight'
};

exports.Prisma.ProducttypesScalarFieldEnum = {
  id: 'id',
  v: 'v',
  code: 'code',
  createdAt: 'createdAt',
  isParent: 'isParent',
  name: 'name',
  parentId: 'parentId',
  updatedAt: 'updatedAt'
};

exports.Prisma.ProducttypeslogsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  createdAt: 'createdAt',
  productTypeId: 'productTypeId',
  updatedAt: 'updatedAt'
};

exports.Prisma.ReceiveordersScalarFieldEnum = {
  id: 'id',
  v: 'v',
  animalType: 'animalType',
  code: 'code',
  componentCategory: 'componentCategory',
  componentCategoryName: 'componentCategoryName',
  createdAt: 'createdAt',
  earTag: 'earTag',
  earTagType: 'earTagType',
  isMain: 'isMain',
  maxQty: 'maxQty',
  parentCode: 'parentCode',
  pointOfFillUp: 'pointOfFillUp',
  pointOfFillUpName: 'pointOfFillUpName',
  pointOfReceive: 'pointOfReceive',
  pointOfReceiveName: 'pointOfReceiveName',
  quantity: 'quantity',
  status: 'status',
  totalWeight: 'totalWeight',
  transportationNumber: 'transportationNumber',
  updatedAt: 'updatedAt'
};

exports.Prisma.RolesScalarFieldEnum = {
  id: 'id',
  v: 'v',
  code: 'code',
  color: 'color',
  createdAt: 'createdAt',
  entityId: 'entityId',
  name: 'name',
  permissionIds: 'permissionIds',
  status: 'status',
  updatedAt: 'updatedAt'
};

exports.Prisma.Stock_locationsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  branchName: 'branchName',
  createdAt: 'createdAt',
  entityId: 'entityId',
  phoneNumber: 'phoneNumber',
  supplierId: 'supplierId',
  updatedAt: 'updatedAt'
};

exports.Prisma.StocklocationsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  createdAt: 'createdAt',
  entityId: 'entityId',
  phoneNumber: 'phoneNumber',
  updatedAt: 'updatedAt'
};

exports.Prisma.StocksScalarFieldEnum = {
  id: 'id',
  v: 'v',
  availableDate: 'availableDate',
  createdAt: 'createdAt',
  expiryDate: 'expiryDate',
  isReady: 'isReady',
  quantity: 'quantity',
  stockLocations: 'stockLocations',
  updatedAt: 'updatedAt',
  variantId: 'variantId'
};

exports.Prisma.Supplier_categoriesScalarFieldEnum = {
  id: 'id'
};

exports.Prisma.Supplier_reviewsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  certificationIds: 'certificationIds',
  certifications: 'certifications',
  createdAt: 'createdAt',
  delivery_type_comment: 'delivery_type_comment',
  delivery_type_status: 'delivery_type_status',
  entity_info_comment: 'entity_info_comment',
  entity_info_status: 'entity_info_status',
  images_comment: 'images_comment',
  images_status: 'images_status',
  product_category_comment: 'product_category_comment',
  product_category_status: 'product_category_status',
  status: 'status',
  stock_location_comment: 'stock_location_comment',
  stock_location_status: 'stock_location_status',
  supplier_id: 'supplier_id',
  supplier_type_comment: 'supplier_type_comment',
  supplier_type_status: 'supplier_type_status',
  updatedAt: 'updatedAt',
  uploaded_document_comment: 'uploaded_document_comment',
  uploaded_document_status: 'uploaded_document_status'
};

exports.Prisma.SuppliercategoriesScalarFieldEnum = {
  id: 'id'
};

exports.Prisma.TrucksScalarFieldEnum = {
  id: 'id',
  v: 'v',
  createdAt: 'createdAt',
  numberplate: 'numberplate',
  updatedAt: 'updatedAt'
};

exports.Prisma.User_tokensScalarFieldEnum = {
  id: 'id',
  v: 'v',
  createdAt: 'createdAt',
  expiresAt: 'expiresAt',
  family: 'family',
  refreshToken: 'refreshToken',
  updatedAt: 'updatedAt',
  userId: 'userId'
};

exports.Prisma.UsersScalarFieldEnum = {
  id: 'id',
  v: 'v',
  countryCode: 'countryCode',
  createdAt: 'createdAt',
  createdEntityIds: 'createdEntityIds',
  currentEntityId: 'currentEntityId',
  currentPlatformId: 'currentPlatformId',
  email: 'email',
  entityIds: 'entityIds',
  firstName: 'firstName',
  isEmailVerified: 'isEmailVerified',
  isoCode: 'isoCode',
  lastName: 'lastName',
  password: 'password',
  phoneNumber: 'phoneNumber',
  refreshToken: 'refreshToken',
  roleIds: 'roleIds',
  roles: 'roles',
  updatedAt: 'updatedAt'
};

exports.Prisma.Users_testsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  countryCode: 'countryCode',
  createdAt: 'createdAt',
  createdEntityIds: 'createdEntityIds',
  currentPlatformId: 'currentPlatformId',
  email: 'email',
  entities: 'entities',
  entityIds: 'entityIds',
  firstName: 'firstName',
  isEmailVerified: 'isEmailVerified',
  isoCode: 'isoCode',
  lastName: 'lastName',
  password: 'password',
  phoneNumber: 'phoneNumber',
  refreshToken: 'refreshToken',
  roles: 'roles',
  updatedAt: 'updatedAt'
};

exports.Prisma.VariantsScalarFieldEnum = {
  id: 'id',
  v: 'v',
  certificates_of_origin: 'certificates_of_origin',
  createdAt: 'createdAt',
  images: 'images',
  name: 'name',
  placeOfOrigin: 'placeOfOrigin',
  price: 'price',
  productId: 'productId',
  updatedAt: 'updatedAt',
  video: 'video'
};

exports.Prisma.SortOrder = {
  asc: 'asc',
  desc: 'desc'
};

exports.Prisma.QueryMode = {
  default: 'default',
  insensitive: 'insensitive'
};


exports.Prisma.ModelName = {
  addresses: 'addresses',
  categories: 'categories',
  certificate_sources: 'certificate_sources',
  certificate_types: 'certificate_types',
  certificates: 'certificates',
  certifications: 'certifications',
  checkpoint: 'checkpoint',
  checkpoints: 'checkpoints',
  component_categories: 'component_categories',
  components: 'components',
  corporates: 'corporates',
  customcertificates: 'customcertificates',
  customcertificationtypes: 'customcertificationtypes',
  entities: 'entities',
  entity_buyers: 'entity_buyers',
  entity_companies: 'entity_companies',
  entity_review_infos: 'entity_review_infos',
  entity_supplier_companies: 'entity_supplier_companies',
  entity_supplier_individuals: 'entity_supplier_individuals',
  entity_suppliers: 'entity_suppliers',
  filluporders: 'filluporders',
  megs: 'megs',
  menus: 'menus',
  offer_drafts: 'offer_drafts',
  offers: 'offers',
  orders: 'orders',
  origins: 'origins',
  payment_methods: 'payment_methods',
  pending_invites: 'pending_invites',
  pendinginvites: 'pendinginvites',
  permissions: 'permissions',
  platforms: 'platforms',
  product_logs: 'product_logs',
  product_metas: 'product_metas',
  product_type_logs: 'product_type_logs',
  product_types: 'product_types',
  productlogs: 'productlogs',
  productmetas: 'productmetas',
  products: 'products',
  producttypes: 'producttypes',
  producttypeslogs: 'producttypeslogs',
  receiveorders: 'receiveorders',
  roles: 'roles',
  stock_locations: 'stock_locations',
  stocklocations: 'stocklocations',
  stocks: 'stocks',
  supplier_categories: 'supplier_categories',
  supplier_reviews: 'supplier_reviews',
  suppliercategories: 'suppliercategories',
  trucks: 'trucks',
  user_tokens: 'user_tokens',
  users: 'users',
  users_tests: 'users_tests',
  variants: 'variants'
};

/**
 * This is a stub Prisma Client that will error at runtime if called.
 */
class PrismaClient {
  constructor() {
    return new Proxy(this, {
      get(target, prop) {
        const runtime = detectRuntime()
        const edgeRuntimeName = {
          'workerd': 'Cloudflare Workers',
          'deno': 'Deno and Deno Deploy',
          'netlify': 'Netlify Edge Functions',
          'edge-light': 'Vercel Edge Functions',
        }[runtime]

        let message = 'PrismaClient is unable to run in '
        if (edgeRuntimeName !== undefined) {
          message += edgeRuntimeName + '. As an alternative, try Accelerate: https://pris.ly/d/accelerate.'
        } else {
          message += 'this browser environment, or has been bundled for the browser (running in `' + runtime + '`).'
        }
        
        message += `
If this is unexpected, please open an issue: https://github.com/prisma/prisma/issues`

        throw new Error(message)
      }
    })
  }
}

exports.PrismaClient = PrismaClient

Object.assign(exports, Prisma)
