import { nextui } from '@nextui-org/react'

/** @type {import('tailwindcss').Config} */
module.exports = {
	darkMode: ['class'],
	content: [
		'./pages/**/*.{ts,tsx}',
		'./components/**/*.{ts,tsx}',
		'./app/**/*.{ts,tsx}',
		'./src/**/*.{ts,tsx}',
		'./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}'
	],
	theme: {
		container: {
			center: true,
			padding: '2rem',
			screens: {
				'2xl': '1400px'
			}
		},
		extend: {
			colors: {
				border: 'hsl(var(--border))',
				input: 'hsl(var(--input))',
				ring: 'hsl(var(--ring))',
				background: 'hsl(var(--background))',
				foreground: 'hsl(var(--foreground))',
				primary: {
					DEFAULT: 'hsl(var(--primary))',
					foreground: 'hsl(var(--primary-foreground))'
				},
				secondary: {
					DEFAULT: 'hsl(var(--secondary))',
					foreground: 'hsl(var(--secondary-foreground))'
				},
				default: {
					100: 'hsl(var(--lightgray-100))',
					200: 'hsl(var(--lightgray-200))'
				},
				darkGrey: {
					600: 'hsl(var(--darkgray-600))'
				},
				lightgray: {
					100: 'hsl(var(--lightgray-100))',
					200: 'hsl(var(--lightgray-200))',
					300: 'hsl(var(--lightgray-300))',
					400: 'hsl(var(--lightgray-400))',
					500: 'hsl(var(--lightgray-500))',
					800: 'hsl(var(--lightgray-800))',
					900: 'hsl(var(--lightgray-900))',
					1000: 'hsl(var(--lightgray-1000))',
					1100: 'hsl(var(--lightgray-1100))',
					1200: 'hsl(var(--lightgray-1200))'
				},
				pinegreen: {
					700: 'hsl(var(--pinegreen-700))'
				},
				warmyellow: {
					700: 'hsl(var(--warmyellow-700))'
				},
				success: {
					DEFAULT: 'hsl(var(--success-700))',
					300: 'hsl(var(--success-300))',
					700: 'hsl(var(--success-700))'
				},
				warning: {
					700: 'hsl(var(--warning-700))'
				},
				error: {
					DEFAULT: 'var(--systemError700)',
					300: 'var(--systemError300)',
					700: 'var(--systemError700)'
				},
				info: {
					DEFAULT: 'var(--systemInfo700)',
					300: 'var(--systemInfo300)',
					700: 'var(--systemInfo700)'
				},
				destructive: {
					DEFAULT: 'hsl(var(--destructive))',
					foreground: 'hsl(var(--destructive-foreground))'
				},
				muted: {
					DEFAULT: 'hsl(var(--muted))',
					foreground: 'hsl(var(--muted-foreground))'
				},
				support: {
					DEFAULT: 'hsl(var(--support))'
				},
				accent: {
					DEFAULT: 'hsl(var(--accent))',
					foreground: 'hsl(var(--accent-foreground))'
				},
				popover: {
					DEFAULT: 'hsl(var(--popover))',
					foreground: 'hsl(var(--popover-foreground))'
				},
				card: {
					DEFAULT: 'hsl(var(--card))',
					foreground: 'hsl(var(--card-foreground))'
				},
				neutral: {
					700: 'hsl(var(--neutral-700))'
				}
			},
			borderRadius: {
				lg: 'var(--radius)',
				md: 'calc(var(--radius) - 2px)',
				sm: 'calc(var(--radius) - 4px)'
			},
			keyframes: {
				'accordion-down': {
					from: { height: 0 },
					to: { height: 'var(--radix-accordion-content-height)' }
				},
				'accordion-up': {
					from: { height: 'var(--radix-accordion-content-height)' },
					to: { height: 0 }
				}
			},
			animation: {
				'accordion-down': 'accordion-down 0.2s ease-out',
				'accordion-up': 'accordion-up 0.2s ease-out'
			}
		}
	},
	plugins: [require('tailwindcss-animate'), nextui()]
}
