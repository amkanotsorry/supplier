import { DefaultSession, DefaultUser } from 'next-auth'
import { JWT, DefaultJWT } from 'next-auth/jwt'

interface CustomUserFields {
	id: string
	token?: string
	name?: string | null
	email?: string | null
	image?: string | null
	currentEntityId?: string | null
	entityIds?: string[] | null
	isEmailVerified?: boolean | null
}

declare module 'next-auth' {
	interface Session extends DefaultSession {
		user: {} & CustomUserFields
	}

	interface User extends DefaultUser, CustomUserFields {}
}

declare module 'next-auth/jwt' {
	interface JWT extends DefaultJWT, CustomUserFields {}
}
